﻿module scintilla.core;

extern(C) {
	version(Windows) {
		/* Return false on failure: */
		int Scintilla_RegisterClasses(void* hInstance);
		int Scintilla_ReleaseResources();
	}
	int Scintilla_LinkLexers();
}

void main() {}
