﻿// Generated from Scintilla.iface, Basics category
module scintilla.internal.functions;

import scintilla.types;

// TODO to C-string: str.ptr[str.length] in some cases
// TODO use CWrap (RT only?)

import std.c.stdlib: malloc, free;
import std.c.string: strlen;

private size_t p_lenParam;

public import std.algorithm: min;

const(char)* toCString(char[] buff, const(char)[] str, out char* tmpBuff, out size_t len = p_lenParam) {
	if(!str.length) {
		len = 1;
		return "".ptr;
	}
	if(!str[$-1]) {
		len = str.length;
		return str.ptr;
	}
	len = str.length + 1;
	auto res = buff.length >= len ? buff.ptr : (tmpBuff = cast(char*)malloc(len));
	res[0 .. str.length] = str;
	buff[str.length] = 0;
	return res;
}

void toCStrings(char[] buff, const(char)[] str1, const(char)[] str2, out const(char)* cstr1, out const(char)* cstr2, out char* tmpBuff) {
	static const(char)* toStrZ(const(char)[] s) {
		if(!s.length)
			return "".ptr;
		if(!s[$-1])
			return s.ptr;
		auto copy = new char[s.length + 1];
		copy[0..s.length] = s;
		copy[s.length] = 0;
		return copy.ptr;
	}
	cstr1 = toStrZ(str1);
	cstr2 = toStrZ(str2);
}

void freeTmpBuff(char* buff) {
	free(buff);
}
/*
char[] toDStringBuff(in size_t len, char[] buff) {
	assert(len <= buff.length);
	return buff[0 .. len - !buff[len-1]];
}

int toDStringBuff(in size_t len, char[] buff, out char[] res) {
	if(buff) {
		assert(len <= buff.length);
		res = buff[0 .. len - !buff[len-1]];
	}
	return len;
}

string toDString(in size_t len, char* buff) {
	if(!len)
		return null;
	else if(len == 1) // buff is stack-allocated
		return buff[0] ? [cast(immutable(char)) buff[0]] : "";
	else
		return cast(string) buff[0 .. len - !buff[len-1]];
}*/

const(char)[] fromCString(const(char)* cstr) {
	return cstr ? cstr[0 .. strlen(cstr)] : null;
}



/*RT funcWrapper(RT, alias sendMessageDirect, T...)(int number, T p) {
	assert(0);
}
// return funcWrapper!(sendMessageDirect, ###)(args);

int funcWrapper(alias sendMessageDirect, int number, T1, T2)(T1 p1, T2 p2) {
	enum
		inString1 = is(typeof(p1) == const(char[])),
		inString2 = is(typeof(p2) == const(char[]));
	static if(inString1 || inString2) {
		char[4096] __sibuff = void;
		char* tmpBuff;
	}
	static if(inString1 && inString2) {
		const(char)* __cstr1, __cstr2;
		toCStrings(__sibuff, p1, p2, __cstr1, __cstr2, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(number, cast(size_t) __cstr1, cast(size_t) __cstr2);
	} else static if(inString1 || inString2) {
	}
}*/
/*
	// (?.*)(At) Position
	struct Margin {
		IScintilla iScintilla;
		int id;
		void typeN(int marginType) @property {
			iScintilla.SetMarginTypeN(id, marginType);
		}
	}
	final Pos positionAt(Position pos) {
		return Pos(this, pos);
	}
*/
