﻿/** D wrapper for Scintilla source code edit control.

Based on Scintilla.h.

Copyright: Denis Shelomovskij 2012

License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module scintilla.types;

import core.stdc.config: c_long;
import std.traits: Signed;

// General Scintilla types
// --------------------------------------------------
alias size_t uptr_t;
alias Signed!uptr_t sptr_t;

alias extern(C++) sptr_t function(sptr_t ptr, uint iMessage, uptr_t wParam, sptr_t lParam) SciFnDirect;


// Types used in Scintilla functions
// --------------------------------------------------
alias int Position;

struct Colour {
	union {
		struct { ubyte r, g, b; }
		uint rgb;
	}
	this(ubyte r, ubyte g, ubyte b) { this.r = r; this.g = g; this.b = b; }
	this(uint rgb) { this.rgb = rgb; }
}

// For AddStyledText
struct Cell {
	char ch;
	ubyte style;
}

struct CharacterRange {
	c_long cpMin;
	c_long cpMax;
}

// For GetStyledText and GetTextRange
struct TextRange {
	CharacterRange chrg;
	char* lpstrText;
}

// For FindText
struct TextToFind {
	CharacterRange chrg;
	char* lpstrText;
	CharacterRange chrgText;
}

alias void* SurfaceID;

struct Rectangle {
	int left, top, right, bottom;
}

// For FormatRange
struct RangeToFormat {
	SurfaceID hdc;
	SurfaceID hdcTarget;
	Rectangle rc;
	Rectangle rcPage;
	CharacterRange chrg;
}


// Types used in Scintilla events
// --------------------------------------------------
struct NotifyHeader {
	/* Compatible with Windows NMHDR.
	 * hwndFrom is really an environment specific window handle or pointer
	 * but most clients of Scintilla.h do not have this type visible. */
	void* hwndFrom;
	uptr_t idFrom;
	uint code;
}

struct SCNotification {
	NotifyHeader nmhdr;
	int position;
	/* SCN_STYLENEEDED, SCN_DOUBLECLICK, SCN_MODIFIED, SCN_MARGINCLICK, */
	/* SCN_NEEDSHOWN, SCN_DWELLSTART, SCN_DWELLEND, SCN_CALLTIPCLICK, */
	/* SCN_HOTSPOTCLICK, SCN_HOTSPOTDOUBLECLICK, SCN_HOTSPOTRELEASECLICK, */
	/* SCN_INDICATORCLICK, SCN_INDICATORRELEASE, */
	/* SCN_USERLISTSELECTION, SCN_AUTOCSELECTION */

	int ch;		/* SCN_CHARADDED, SCN_KEY */
	int modifiers;
	/* SCN_KEY, SCN_DOUBLECLICK, SCN_HOTSPOTCLICK, SCN_HOTSPOTDOUBLECLICK, */
	/* SCN_HOTSPOTRELEASECLICK, SCN_INDICATORCLICK, SCN_INDICATORRELEASE, */

	int modificationType;	/* SCN_MODIFIED */
	const(char*) text;
	/* SCN_MODIFIED, SCN_USERLISTSELECTION, SCN_AUTOCSELECTION, SCN_URIDROPPED */

	int length;		/* SCN_MODIFIED */
	int linesAdded;	/* SCN_MODIFIED */
	int message;	/* SCN_MACRORECORD */
	uptr_t wParam;	/* SCN_MACRORECORD */
	sptr_t lParam;	/* SCN_MACRORECORD */
	int line;		/* SCN_MODIFIED */
	int foldLevelNow;	/* SCN_MODIFIED */
	int foldLevelPrev;	/* SCN_MODIFIED */
	int margin;		/* SCN_MARGINCLICK */
	int listType;	/* SCN_USERLISTSELECTION */
	int x;			/* SCN_DWELLSTART, SCN_DWELLEND */
	int y;		/* SCN_DWELLSTART, SCN_DWELLEND */
	int token;		/* SCN_MODIFIED with SC_MOD_CONTAINER */
	int annotationLinesAdded;	/* SCN_MODIFIED with SC_MOD_CHANGEANNOTATION */
	int updated;	/* SCN_UPDATEUI */
}

// TODO deprecated?
