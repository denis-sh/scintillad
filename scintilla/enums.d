﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category
module scintilla.enums;

// Global manifest constants
enum {
  invalidPosition = -1, // INVALID_POSITION
  sciStart = 2000, // SCI_START
  sciOptionalStart = 3000, // SCI_OPTIONAL_START
  sciLexerStart = 4000, // SCI_LEXER_START
  scCpUtf8 = 65001, // SC_CP_UTF8
  markerMax = 31, // MARKER_MAX
  scMaskFolders = 0xFE000000, // SC_MASK_FOLDERS
  scFontSizeMultiplier = 100, // SC_FONT_SIZE_MULTIPLIER
  indic0Mask = 0x20, // INDIC0_MASK
  indic1Mask = 0x40, // INDIC1_MASK
  indic2Mask = 0x80, // INDIC2_MASK
  indicsMask = 0xE0, // INDICS_MASK
  scTimeForever = 10000000, // SC_TIME_FOREVER
  visibleSlop = 0x01, // VISIBLE_SLOP
  visibleStrict = 0x04, // VISIBLE_STRICT
  scAlphaTransparent = 0, // SC_ALPHA_TRANSPARENT
  scAlphaOpaque = 255, // SC_ALPHA_OPAQUE
  scAlphaNoalpha = 256, // SC_ALPHA_NOALPHA
  undoMayCoalesce = 1, // UNDO_MAY_COALESCE
  scTechnologyDefault = 0, // SC_TECHNOLOGY_DEFAULT
  scTechnologyDirectwrite = 1, // SC_TECHNOLOGY_DIRECTWRITE
  keywordsetMax = 8, // KEYWORDSET_MAX
  scenChange = 768, // SCEN_CHANGE
  scenSetfocus = 512, // SCEN_SETFOCUS
  scenKillfocus = 256, // SCEN_KILLFOCUS
  sceGcDefault = 0, // SCE_GC_DEFAULT
  sceGcCommentline = 1, // SCE_GC_COMMENTLINE
  sceGcCommentblock = 2, // SCE_GC_COMMENTBLOCK
  sceGcGlobal = 3, // SCE_GC_GLOBAL
  sceGcEvent = 4, // SCE_GC_EVENT
  sceGcAttribute = 5, // SCE_GC_ATTRIBUTE
  sceGcControl = 6, // SCE_GC_CONTROL
  sceGcCommand = 7, // SCE_GC_COMMAND
  sceGcString = 8, // SCE_GC_STRING
  sceGcOperator = 9, // SCE_GC_OPERATOR
}

// Prefix: SCWS_
enum WhiteSpace {
  invisible = 0, //SCWS_:INVISIBLE
  visiblealways = 1, //SCWS_:VISIBLEALWAYS
  visibleafterindent = 2, //SCWS_:VISIBLEAFTERINDENT
}

// Prefix: SC_EOL_
enum EndOfLine {
  crlf = 0, //SC_EOL_:CRLF
  cr = 1, //SC_EOL_:CR
  lf = 2, //SC_EOL_:LF
}

// Prefix: SC_MARK_
enum MarkerSymbol {
  circle = 0, //SC_MARK_:CIRCLE
  roundrect = 1, //SC_MARK_:ROUNDRECT
  arrow = 2, //SC_MARK_:ARROW
  smallrect = 3, //SC_MARK_:SMALLRECT
  shortarrow = 4, //SC_MARK_:SHORTARROW
  empty = 5, //SC_MARK_:EMPTY
  arrowdown = 6, //SC_MARK_:ARROWDOWN
  minus = 7, //SC_MARK_:MINUS
  plus = 8, //SC_MARK_:PLUS
  vline = 9, //SC_MARK_:VLINE
  lcorner = 10, //SC_MARK_:LCORNER
  tcorner = 11, //SC_MARK_:TCORNER
  boxplus = 12, //SC_MARK_:BOXPLUS
  boxplusconnected = 13, //SC_MARK_:BOXPLUSCONNECTED
  boxminus = 14, //SC_MARK_:BOXMINUS
  boxminusconnected = 15, //SC_MARK_:BOXMINUSCONNECTED
  lcornercurve = 16, //SC_MARK_:LCORNERCURVE
  tcornercurve = 17, //SC_MARK_:TCORNERCURVE
  circleplus = 18, //SC_MARK_:CIRCLEPLUS
  circleplusconnected = 19, //SC_MARK_:CIRCLEPLUSCONNECTED
  circleminus = 20, //SC_MARK_:CIRCLEMINUS
  circleminusconnected = 21, //SC_MARK_:CIRCLEMINUSCONNECTED
  background = 22, //SC_MARK_:BACKGROUND
  dotdotdot = 23, //SC_MARK_:DOTDOTDOT
  arrows = 24, //SC_MARK_:ARROWS
  pixmap = 25, //SC_MARK_:PIXMAP
  fullrect = 26, //SC_MARK_:FULLRECT
  leftrect = 27, //SC_MARK_:LEFTRECT
  available = 28, //SC_MARK_:AVAILABLE
  underline = 29, //SC_MARK_:UNDERLINE
  rgbaimage = 30, //SC_MARK_:RGBAIMAGE
  character = 10000, //SC_MARK_:CHARACTER
}

// Prefix: SC_MARKNUM_
enum MarkerOutline {
  folderend = 25, //SC_MARKNUM_:FOLDEREND
  folderopenmid = 26, //SC_MARKNUM_:FOLDEROPENMID
  foldermidtail = 27, //SC_MARKNUM_:FOLDERMIDTAIL
  foldertail = 28, //SC_MARKNUM_:FOLDERTAIL
  foldersub = 29, //SC_MARKNUM_:FOLDERSUB
  folder = 30, //SC_MARKNUM_:FOLDER
  folderopen = 31, //SC_MARKNUM_:FOLDEROPEN
}

// Prefix: SC_MARGIN_
enum MarginType {
  symbol = 0, //SC_MARGIN_:SYMBOL
  number = 1, //SC_MARGIN_:NUMBER
  back = 2, //SC_MARGIN_:BACK
  fore = 3, //SC_MARGIN_:FORE
  text = 4, //SC_MARGIN_:TEXT
  rtext = 5, //SC_MARGIN_:RTEXT
}

// Prefix: STYLE_
enum StylesCommon {
  default_ = 32, //STYLE_:DEFAULT
  linenumber = 33, //STYLE_:LINENUMBER
  bracelight = 34, //STYLE_:BRACELIGHT
  bracebad = 35, //STYLE_:BRACEBAD
  controlchar = 36, //STYLE_:CONTROLCHAR
  indentguide = 37, //STYLE_:INDENTGUIDE
  calltip = 38, //STYLE_:CALLTIP
  lastpredefined = 39, //STYLE_:LASTPREDEFINED
  max = 255, //STYLE_:MAX
}

// Prefix: SC_CHARSET_
enum CharacterSet {
  ansi = 0, //SC_CHARSET_:ANSI
  default_ = 1, //SC_CHARSET_:DEFAULT
  baltic = 186, //SC_CHARSET_:BALTIC
  chinesebig5 = 136, //SC_CHARSET_:CHINESEBIG5
  easteurope = 238, //SC_CHARSET_:EASTEUROPE
  gb2312 = 134, //SC_CHARSET_:GB2312
  greek = 161, //SC_CHARSET_:GREEK
  hangul = 129, //SC_CHARSET_:HANGUL
  mac = 77, //SC_CHARSET_:MAC
  oem = 255, //SC_CHARSET_:OEM
  russian = 204, //SC_CHARSET_:RUSSIAN
  cyrillic = 1251, //SC_CHARSET_:CYRILLIC
  shiftjis = 128, //SC_CHARSET_:SHIFTJIS
  symbol = 2, //SC_CHARSET_:SYMBOL
  turkish = 162, //SC_CHARSET_:TURKISH
  johab = 130, //SC_CHARSET_:JOHAB
  hebrew = 177, //SC_CHARSET_:HEBREW
  arabic = 178, //SC_CHARSET_:ARABIC
  vietnamese = 163, //SC_CHARSET_:VIETNAMESE
  thai = 222, //SC_CHARSET_:THAI
  _885915 = 1000, //SC_CHARSET_:8859_15
}

// Prefix: SC_CASE_
enum CaseVisible {
  mixed = 0, //SC_CASE_:MIXED
  upper = 1, //SC_CASE_:UPPER
  lower = 2, //SC_CASE_:LOWER
}

// Prefix: SC_WEIGHT_
enum FontWeight {
  normal = 400, //SC_WEIGHT_:NORMAL
  semibold = 600, //SC_WEIGHT_:SEMIBOLD
  bold = 700, //SC_WEIGHT_:BOLD
}

// Prefix: INDIC_
enum IndicatorStyle {
  plain = 0, //INDIC_:PLAIN
  squiggle = 1, //INDIC_:SQUIGGLE
  tt = 2, //INDIC_:TT
  diagonal = 3, //INDIC_:DIAGONAL
  strike = 4, //INDIC_:STRIKE
  hidden = 5, //INDIC_:HIDDEN
  box = 6, //INDIC_:BOX
  roundbox = 7, //INDIC_:ROUNDBOX
  straightbox = 8, //INDIC_:STRAIGHTBOX
  dash = 9, //INDIC_:DASH
  dots = 10, //INDIC_:DOTS
  squigglelow = 11, //INDIC_:SQUIGGLELOW
  dotbox = 12, //INDIC_:DOTBOX
  max = 31, //INDIC_:MAX
  container = 8, //INDIC_:CONTAINER
}

// Prefix: SC_IV_
enum IndentView {
  none = 0, //SC_IV_:NONE
  real_ = 1, //SC_IV_:REAL
  lookforward = 2, //SC_IV_:LOOKFORWARD
  lookboth = 3, //SC_IV_:LOOKBOTH
}

// Prefix: SC_PRINT_
enum PrintOption {
  normal = 0, //SC_PRINT_:NORMAL
  invertlight = 1, //SC_PRINT_:INVERTLIGHT
  blackonwhite = 2, //SC_PRINT_:BLACKONWHITE
  colouronwhite = 3, //SC_PRINT_:COLOURONWHITE
  colouronwhitedefaultbg = 4, //SC_PRINT_:COLOURONWHITEDEFAULTBG
}

// Prefix: SCFIND_
enum FindOption {
  wholeword = 2, //SCFIND_:WHOLEWORD
  matchcase = 4, //SCFIND_:MATCHCASE
  wordstart = 0x00100000, //SCFIND_:WORDSTART
  regexp = 0x00200000, //SCFIND_:REGEXP
  posix = 0x00400000, //SCFIND_:POSIX
}

// Prefix: SC_FOLDLEVEL
enum FoldLevel {
  base = 0x400, //SC_FOLDLEVEL:BASE
  whiteflag = 0x1000, //SC_FOLDLEVEL:WHITEFLAG
  headerflag = 0x2000, //SC_FOLDLEVEL:HEADERFLAG
  numbermask = 0x0FFF, //SC_FOLDLEVEL:NUMBERMASK
}

// Prefix: SC_FOLDFLAG_
enum FoldFlag {
  linebeforeExpanded = 0x0002, //SC_FOLDFLAG_:LINEBEFORE_EXPANDED
  linebeforeContracted = 0x0004, //SC_FOLDFLAG_:LINEBEFORE_CONTRACTED
  lineafterExpanded = 0x0008, //SC_FOLDFLAG_:LINEAFTER_EXPANDED
  lineafterContracted = 0x0010, //SC_FOLDFLAG_:LINEAFTER_CONTRACTED
  levelnumbers = 0x0040, //SC_FOLDFLAG_:LEVELNUMBERS
}

// Prefix: SC_WRAP_
enum Wrap {
  none = 0, //SC_WRAP_:NONE
  word = 1, //SC_WRAP_:WORD
  char_ = 2, //SC_WRAP_:CHAR
}

// Prefix: SC_WRAPVISUALFLAG_
enum WrapVisualFlag {
  none = 0x0000, //SC_WRAPVISUALFLAG_:NONE
  end = 0x0001, //SC_WRAPVISUALFLAG_:END
  start = 0x0002, //SC_WRAPVISUALFLAG_:START
  margin = 0x0004, //SC_WRAPVISUALFLAG_:MARGIN
}

// Prefix: SC_WRAPVISUALFLAGLOC_
enum WrapVisualLocation {
  default_ = 0x0000, //SC_WRAPVISUALFLAGLOC_:DEFAULT
  endByText = 0x0001, //SC_WRAPVISUALFLAGLOC_:END_BY_TEXT
  startByText = 0x0002, //SC_WRAPVISUALFLAGLOC_:START_BY_TEXT
}

// Prefix: SC_WRAPINDENT_
enum WrapIndentMode {
  fixed = 0, //SC_WRAPINDENT_:FIXED
  same = 1, //SC_WRAPINDENT_:SAME
  indent = 2, //SC_WRAPINDENT_:INDENT
}

// Prefix: SC_CACHE_
enum LineCache {
  none = 0, //SC_CACHE_:NONE
  caret = 1, //SC_CACHE_:CARET
  page = 2, //SC_CACHE_:PAGE
  document = 3, //SC_CACHE_:DOCUMENT
}

// Prefix: SC_EFF_
enum FontQuality {
  qualityMask = 0xF, //SC_EFF_:QUALITY_MASK
  qualityDefault = 0, //SC_EFF_:QUALITY_DEFAULT
  qualityNonAntialiased = 1, //SC_EFF_:QUALITY_NON_ANTIALIASED
  qualityAntialiased = 2, //SC_EFF_:QUALITY_ANTIALIASED
  qualityLcdOptimized = 3, //SC_EFF_:QUALITY_LCD_OPTIMIZED
}

// Prefix: SC_MULTIPASTE_
enum MultiPaste {
  once = 0, //SC_MULTIPASTE_:ONCE
  each = 1, //SC_MULTIPASTE_:EACH
}

// Prefix: EDGE_
enum EdgeVisualStyle {
  none = 0, //EDGE_:NONE
  line = 1, //EDGE_:LINE
  background = 2, //EDGE_:BACKGROUND
}

// Prefix: SC_STATUS_
enum Status {
  ok = 0, //SC_STATUS_:OK
  failure = 1, //SC_STATUS_:FAILURE
  badalloc = 2, //SC_STATUS_:BADALLOC
}

// Prefix: SC_CURSOR
enum CursorShape {
  normal = -1, //SC_CURSOR:NORMAL
  arrow = 2, //SC_CURSOR:ARROW
  wait = 4, //SC_CURSOR:WAIT
  reversearrow = 7, //SC_CURSOR:REVERSEARROW
}

// Prefix: CARET_
enum CaretPolicy {
  slop = 0x01, //CARET_:SLOP
  strict = 0x04, //CARET_:STRICT
  jumps = 0x10, //CARET_:JUMPS
  even = 0x08, //CARET_:EVEN
}

// Prefix: SC_SEL_
enum SelectionMode {
  stream = 0, //SC_SEL_:STREAM
  rectangle = 1, //SC_SEL_:RECTANGLE
  lines = 2, //SC_SEL_:LINES
  thin = 3, //SC_SEL_:THIN
}

// Prefix: SC_CASEINSENSITIVEBEHAVIOUR_
enum CaseInsensitiveBehaviour {
  respectcase = 0, //SC_CASEINSENSITIVEBEHAVIOUR_:RESPECTCASE
  ignorecase = 1, //SC_CASEINSENSITIVEBEHAVIOUR_:IGNORECASE
}

// Prefix: SC_CARETSTICKY_
enum CaretSticky {
  off = 0, //SC_CARETSTICKY_:OFF
  on = 1, //SC_CARETSTICKY_:ON
  whitespace = 2, //SC_CARETSTICKY_:WHITESPACE
}

// Prefix: CARETSTYLE_
enum CaretStyle {
  invisible = 0, //CARETSTYLE_:INVISIBLE
  line = 1, //CARETSTYLE_:LINE
  block = 2, //CARETSTYLE_:BLOCK
}

// Prefix: SC_MARGINOPTION_
enum MarginOption {
  none = 0, //SC_MARGINOPTION_:NONE
  sublineselect = 1, //SC_MARGINOPTION_:SUBLINESELECT
}

// Prefix: ANNOTATION_
enum AnnotationVisible {
  hidden = 0, //ANNOTATION_:HIDDEN
  standard = 1, //ANNOTATION_:STANDARD
  boxed = 2, //ANNOTATION_:BOXED
}

// Prefix: SCVS_
enum VirtualSpace {
  none = 0, //SCVS_:NONE
  rectangularselection = 1, //SCVS_:RECTANGULARSELECTION
  useraccessible = 2, //SCVS_:USERACCESSIBLE
}

// Prefix: SC_TYPE_
enum TypeProperty {
  boolean = 0, //SC_TYPE_:BOOLEAN
  integer = 1, //SC_TYPE_:INTEGER
  string = 2, //SC_TYPE_:STRING
}

// Prefixes: SC_MOD_ SC_PERFORMED_ SC_MULTISTEPUNDOREDO SC_LASTSTEPINUNDOREDO SC_MULTILINEUNDOREDO SC_STARTACTION SC_MODEVENTMASKALL
// Common prefix: SC_
enum ModificationFlags {
  modInserttext = 0x1, //SC_MOD_:INSERTTEXT
  modDeletetext = 0x2, //SC_MOD_:DELETETEXT
  modChangestyle = 0x4, //SC_MOD_:CHANGESTYLE
  modChangefold = 0x8, //SC_MOD_:CHANGEFOLD
  performedUser = 0x10, //SC_PERFORMED_:USER
  performedUndo = 0x20, //SC_PERFORMED_:UNDO
  performedRedo = 0x40, //SC_PERFORMED_:REDO
  multistepundoredo = 0x80, //SC_MULTISTEPUNDOREDO:
  laststepinundoredo = 0x100, //SC_LASTSTEPINUNDOREDO:
  modChangemarker = 0x200, //SC_MOD_:CHANGEMARKER
  modBeforeinsert = 0x400, //SC_MOD_:BEFOREINSERT
  modBeforedelete = 0x800, //SC_MOD_:BEFOREDELETE
  multilineundoredo = 0x1000, //SC_MULTILINEUNDOREDO:
  startaction = 0x2000, //SC_STARTACTION:
  modChangeindicator = 0x4000, //SC_MOD_:CHANGEINDICATOR
  modChangelinestate = 0x8000, //SC_MOD_:CHANGELINESTATE
  modChangemargin = 0x10000, //SC_MOD_:CHANGEMARGIN
  modChangeannotation = 0x20000, //SC_MOD_:CHANGEANNOTATION
  modContainer = 0x40000, //SC_MOD_:CONTAINER
  modLexerstate = 0x80000, //SC_MOD_:LEXERSTATE
  modeventmaskall = 0xFFFFF, //SC_MODEVENTMASKALL:
}

// Prefix: SC_UPDATE_
enum Update {
  content = 0x1, //SC_UPDATE_:CONTENT
  selection = 0x2, //SC_UPDATE_:SELECTION
  vScroll = 0x4, //SC_UPDATE_:V_SCROLL
  hScroll = 0x8, //SC_UPDATE_:H_SCROLL
}

// Prefix: SCK_
enum Keys {
  down = 300, //SCK_:DOWN
  up = 301, //SCK_:UP
  left = 302, //SCK_:LEFT
  right = 303, //SCK_:RIGHT
  home = 304, //SCK_:HOME
  end = 305, //SCK_:END
  prior = 306, //SCK_:PRIOR
  next = 307, //SCK_:NEXT
  delete_ = 308, //SCK_:DELETE
  insert = 309, //SCK_:INSERT
  escape = 7, //SCK_:ESCAPE
  back = 8, //SCK_:BACK
  tab = 9, //SCK_:TAB
  return_ = 13, //SCK_:RETURN
  add = 310, //SCK_:ADD
  subtract = 311, //SCK_:SUBTRACT
  divide = 312, //SCK_:DIVIDE
  win = 313, //SCK_:WIN
  rwin = 314, //SCK_:RWIN
  menu = 315, //SCK_:MENU
}

// Prefix: SCMOD_
enum KeyMod {
  norm = 0, //SCMOD_:NORM
  shift = 1, //SCMOD_:SHIFT
  ctrl = 2, //SCMOD_:CTRL
  alt = 4, //SCMOD_:ALT
  super_ = 8, //SCMOD_:SUPER
  meta = 16, //SCMOD_:META
}
