﻿// Generated from Scintilla.iface, Basics category
module scintilla.functions;

import scintilla.types;

interface IScintilla {
	sptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam);

	/// Add text to the document at current position.
	final void addText(int length, string text) { // AddText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(length, toCString(buff, text));
	}

	/// Add array of cells to document.
	final void addStyledText(int length, Cells c) { // AddStyledText
		sendMessageDirect(length, c);
	}

	/// Insert string at a position.
	final void insertText(Position pos, string text) { // InsertText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(pos, toCString(buff, text));
	}

	/// Delete all text in the document.
	final void clearAll() { // ClearAll
		sendMessageDirect(0, 0);
	}

	/// Set all style bytes to 0, remove all folding information.
	final void clearDocumentStyle() { // ClearDocumentStyle
		sendMessageDirect(0, 0);
	}

	/// Returns the number of bytes in the document.
	final @property int length() { // GetLength
		return sendMessageDirect(0, 0);
	}

	/// Returns the character byte at the position.
	final @property int charAt(Position pos) { // GetCharAt
		return sendMessageDirect(pos, 0);
	}

	/// Returns the position of the caret.
	final @property Position currentPos() { // GetCurrentPos
		return sendMessageDirect(0, 0);
	}

	/// Returns the position of the opposite end of the selection to the caret.
	final @property Position anchor() { // GetAnchor
		return sendMessageDirect(0, 0);
	}

	/// Returns the style byte at the position.
	final @property int styleAt(Position pos) { // GetStyleAt
		return sendMessageDirect(pos, 0);
	}

	/// Redoes the next action on the undo history.
	final void redo() { // Redo
		sendMessageDirect(0, 0);
	}

	/// history and discarding them.
	final @property void undoCollection(bool collectUndo) { // SetUndoCollection
		sendMessageDirect(collectUndo, 0);
	}

	/// Select all the text in the document.
	final void selectAll() { // SelectAll
		sendMessageDirect(0, 0);
	}

	/// at which the document was saved.
	final void setSavePoint() { // SetSavePoint
		sendMessageDirect(0, 0);
	}

	/// Returns the number of bytes in the buffer not including terminating NULs.
	final int getStyledText(TextRange tr) { // GetStyledText
		return sendMessageDirect(0, tr);
	}

	/// Are there any redoable actions in the undo history?
	final bool canRedo() { // CanRedo
		return !!sendMessageDirect(0, 0);
	}

	/// Retrieve the line number at which a particular marker is located.
	final int markerLineFromHandle(int handle) { // MarkerLineFromHandle
		return sendMessageDirect(handle, 0);
	}

	/// Delete a marker.
	final void markerDeleteHandle(int handle) { // MarkerDeleteHandle
		sendMessageDirect(handle, 0);
	}

	/// Is undo history being collected?
	final @property bool undoCollection() { // GetUndoCollection
		return !!sendMessageDirect(0, 0);
	}

	/// Returns one of SCWS_* constants.
	final @property int viewWS() { // GetViewWS
		return sendMessageDirect(0, 0);
	}

	/// Make white space characters invisible, always visible or visible outside indentation.
	final @property void viewWS(int viewWS) { // SetViewWS
		sendMessageDirect(viewWS, 0);
	}

	/// Find the position from a point within the window.
	final Position positionFromPoint(int x, int y) { // PositionFromPoint
		return sendMessageDirect(x, y);
	}

	/// INVALID_POSITION if not close to text.
	final Position positionFromPointClose(int x, int y) { // PositionFromPointClose
		return sendMessageDirect(x, y);
	}

	/// Set caret to start of a line and ensure it is visible.
	final void gotoLine(int line) { // GotoLine
		sendMessageDirect(line, 0);
	}

	/// Set caret to a position and ensure it is visible.
	final void gotoPos(Position pos) { // GotoPos
		sendMessageDirect(pos, 0);
	}

	/// end of the selection from the caret.
	final @property void anchor(Position posAnchor) { // SetAnchor
		sendMessageDirect(posAnchor, 0);
	}

	/// Returns the index of the caret on the line.
	final int getCurLine(int length, out string text) { // GetCurLine
		sendMessageDirect(length, 0);
		return sendMessageDirect(length, text);
	}

	/// Retrieve the position of the last correctly styled character.
	final @property Position endStyled() { // GetEndStyled
		return sendMessageDirect(0, 0);
	}

	/// Convert all line endings in the document to one mode.
	final void convertEOLs(int eolMode) { // ConvertEOLs
		sendMessageDirect(eolMode, 0);
	}

	/// Retrieve the current end of line mode - one of CRLF, CR, or LF.
	final @property int eOLMode() { // GetEOLMode
		return sendMessageDirect(0, 0);
	}

	/// Set the current end of line mode.
	final @property void eOLMode(int eolMode) { // SetEOLMode
		sendMessageDirect(eolMode, 0);
	}

	/// The styling mask can be used to protect some bits in each styling byte from modification.
	final void startStyling(Position pos, int mask) { // StartStyling
		sendMessageDirect(pos, mask);
	}

	/// and move the current styling position to after this newly styled segment.
	final void setStyling(int length, int style) { // SetStyling
		sendMessageDirect(length, style);
	}

	/// Is drawing done first into a buffer or direct to the screen?
	final @property bool bufferedDraw() { // GetBufferedDraw
		return !!sendMessageDirect(0, 0);
	}

	/// before drawing it to the screen to avoid flicker.
	final @property void bufferedDraw(bool buffered) { // SetBufferedDraw
		sendMessageDirect(buffered, 0);
	}

	/// Change the visible size of a tab to be a multiple of the width of a space character.
	final @property void tabWidth(int tabWidth) { // SetTabWidth
		sendMessageDirect(tabWidth, 0);
	}

	/// Retrieve the visible size of a tab.
	final @property int tabWidth() { // GetTabWidth
		return sendMessageDirect(0, 0);
	}

	/// The SC_CP_UTF8 value can be used to enter Unicode mode.
	final @property void codePage(int codePage) { // SetCodePage
		sendMessageDirect(codePage, 0);
	}

	/// Set the symbol used for a particular marker number.
	final void markerDefine(int markerNumber, int markerSymbol) { // MarkerDefine
		sendMessageDirect(markerNumber, markerSymbol);
	}

	/// Set the foreground colour used for a particular marker number.
	final void markerSetFore(int markerNumber, Colour fore) { // MarkerSetFore
		sendMessageDirect(markerNumber, fore.asInt);
	}

	/// Set the background colour used for a particular marker number.
	final void markerSetBack(int markerNumber, Colour back) { // MarkerSetBack
		sendMessageDirect(markerNumber, back.asInt);
	}

	/// Set the background colour used for a particular marker number when its folding block is selected.
	final void markerSetBackSelected(int markerNumber, Colour back) { // MarkerSetBackSelected
		sendMessageDirect(markerNumber, back.asInt);
	}

	/// Enable/disable highlight for current folding bloc (smallest one that contains the caret)
	final void markerEnableHighlight(bool enabled) { // MarkerEnableHighlight
		sendMessageDirect(enabled, 0);
	}

	/// Add a marker to a line, returning an ID which can be used to find or delete the marker.
	final int markerAdd(int line, int markerNumber) { // MarkerAdd
		return sendMessageDirect(line, markerNumber);
	}

	/// Delete a marker from a line.
	final void markerDelete(int line, int markerNumber) { // MarkerDelete
		sendMessageDirect(line, markerNumber);
	}

	/// Delete all markers with a particular number from all lines.
	final void markerDeleteAll(int markerNumber) { // MarkerDeleteAll
		sendMessageDirect(markerNumber, 0);
	}

	/// Get a bit mask of all the markers set on a line.
	final int markerGet(int line) { // MarkerGet
		return sendMessageDirect(line, 0);
	}

	/// Return -1 when no more lines.
	final int markerNext(int lineStart, int markerMask) { // MarkerNext
		return sendMessageDirect(lineStart, markerMask);
	}

	/// Find the previous line before lineStart that includes a marker in mask.
	final int markerPrevious(int lineStart, int markerMask) { // MarkerPrevious
		return sendMessageDirect(lineStart, markerMask);
	}

	/// Define a marker from a pixmap.
	final void markerDefinePixmap(int markerNumber, string pixmap) { // MarkerDefinePixmap
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(markerNumber, toCString(buff, pixmap));
	}

	/// Add a set of markers to a line.
	final void markerAddSet(int line, int set) { // MarkerAddSet
		sendMessageDirect(line, set);
	}

	/// Set the alpha used for a marker that is drawn in the text area, not the margin.
	final void markerSetAlpha(int markerNumber, int alpha) { // MarkerSetAlpha
		sendMessageDirect(markerNumber, alpha);
	}

	/// Set a margin to be either numeric or symbolic.
	final @property void marginTypeN(int margin, int marginType) { // SetMarginTypeN
		sendMessageDirect(margin, marginType);
	}

	/// Retrieve the type of a margin.
	final @property int marginTypeN(int margin) { // GetMarginTypeN
		return sendMessageDirect(margin, 0);
	}

	/// Set the width of a margin to a width expressed in pixels.
	final @property void marginWidthN(int margin, int pixelWidth) { // SetMarginWidthN
		sendMessageDirect(margin, pixelWidth);
	}

	/// Retrieve the width of a margin in pixels.
	final @property int marginWidthN(int margin) { // GetMarginWidthN
		return sendMessageDirect(margin, 0);
	}

	/// Set a mask that determines which markers are displayed in a margin.
	final @property void marginMaskN(int margin, int mask) { // SetMarginMaskN
		sendMessageDirect(margin, mask);
	}

	/// Retrieve the marker mask of a margin.
	final @property int marginMaskN(int margin) { // GetMarginMaskN
		return sendMessageDirect(margin, 0);
	}

	/// Make a margin sensitive or insensitive to mouse clicks.
	final @property void marginSensitiveN(int margin, bool sensitive) { // SetMarginSensitiveN
		sendMessageDirect(margin, sensitive);
	}

	/// Retrieve the mouse click sensitivity of a margin.
	final @property bool marginSensitiveN(int margin) { // GetMarginSensitiveN
		return !!sendMessageDirect(margin, 0);
	}

	/// Set the cursor shown when the mouse is inside a margin.
	final @property void marginCursorN(int margin, int cursor) { // SetMarginCursorN
		sendMessageDirect(margin, cursor);
	}

	/// Retrieve the cursor shown in a margin.
	final @property int marginCursorN(int margin) { // GetMarginCursorN
		return sendMessageDirect(margin, 0);
	}

	/// Clear all the styles and make equivalent to the global default style.
	final @property void styleClearAll() { // StyleClearAll
		sendMessageDirect(0, 0);
	}

	/// Set the foreground colour of a style.
	final @property void styleSetFore(int style, Colour fore) { // StyleSetFore
		sendMessageDirect(style, fore.asInt);
	}

	/// Set the background colour of a style.
	final @property void styleSetBack(int style, Colour back) { // StyleSetBack
		sendMessageDirect(style, back.asInt);
	}

	/// Set a style to be bold or not.
	final @property void styleSetBold(int style, bool bold) { // StyleSetBold
		sendMessageDirect(style, bold);
	}

	/// Set a style to be italic or not.
	final @property void styleSetItalic(int style, bool italic) { // StyleSetItalic
		sendMessageDirect(style, italic);
	}

	/// Set the size of characters of a style.
	final @property void styleSetSize(int style, int sizePoints) { // StyleSetSize
		sendMessageDirect(style, sizePoints);
	}

	/// Set the font of a style.
	final @property void styleSetFont(int style, string fontName) { // StyleSetFont
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(style, toCString(buff, fontName));
	}

	/// Set a style to have its end of line filled or not.
	final @property void styleSetEOLFilled(int style, bool filled) { // StyleSetEOLFilled
		sendMessageDirect(style, filled);
	}

	/// Reset the default style to its state at startup
	final void styleResetDefault() { // StyleResetDefault
		sendMessageDirect(0, 0);
	}

	/// Set a style to be underlined or not.
	final @property void styleSetUnderline(int style, bool underline) { // StyleSetUnderline
		sendMessageDirect(style, underline);
	}

	/// Get the foreground colour of a style.
	final @property Colour styleGetFore(int style) { // StyleGetFore
		return cast(Colour)sendMessageDirect(style, 0);
	}

	/// Get the background colour of a style.
	final @property Colour styleGetBack(int style) { // StyleGetBack
		return cast(Colour)sendMessageDirect(style, 0);
	}

	/// Get is a style bold or not.
	final @property bool styleGetBold(int style) { // StyleGetBold
		return !!sendMessageDirect(style, 0);
	}

	/// Get is a style italic or not.
	final @property bool styleGetItalic(int style) { // StyleGetItalic
		return !!sendMessageDirect(style, 0);
	}

	/// Get the size of characters of a style.
	final @property int styleGetSize(int style) { // StyleGetSize
		return sendMessageDirect(style, 0);
	}

	/// Returns the length of the fontName
	final int styleGetFont(int style, out string fontName) { // StyleGetFont
		sendMessageDirect(style, 0);
		return sendMessageDirect(style, fontName);
	}

	/// Get is a style to have its end of line filled or not.
	final @property bool styleGetEOLFilled(int style) { // StyleGetEOLFilled
		return !!sendMessageDirect(style, 0);
	}

	/// Get is a style underlined or not.
	final @property bool styleGetUnderline(int style) { // StyleGetUnderline
		return !!sendMessageDirect(style, 0);
	}

	/// Get is a style mixed case, or to force upper or lower case.
	final @property int styleGetCase(int style) { // StyleGetCase
		return sendMessageDirect(style, 0);
	}

	/// Get the character get of the font in a style.
	final @property int styleGetCharacterSet(int style) { // StyleGetCharacterSet
		return sendMessageDirect(style, 0);
	}

	/// Get is a style visible or not.
	final @property bool styleGetVisible(int style) { // StyleGetVisible
		return !!sendMessageDirect(style, 0);
	}

	/// Experimental feature, currently buggy.
	final @property bool styleGetChangeable(int style) { // StyleGetChangeable
		return !!sendMessageDirect(style, 0);
	}

	/// Get is a style a hotspot or not.
	final @property bool styleGetHotSpot(int style) { // StyleGetHotSpot
		return !!sendMessageDirect(style, 0);
	}

	/// Set a style to be mixed case, or to force upper or lower case.
	final @property void styleSetCase(int style, int caseForce) { // StyleSetCase
		sendMessageDirect(style, caseForce);
	}

	/// Set the size of characters of a style. Size is in points multiplied by 100.
	final @property void styleSetSizeFractional(int style, int caseForce) { // StyleSetSizeFractional
		sendMessageDirect(style, caseForce);
	}

	/// Get the size of characters of a style in points multiplied by 100
	final @property int styleGetSizeFractional(int style) { // StyleGetSizeFractional
		return sendMessageDirect(style, 0);
	}

	/// Set the weight of characters of a style.
	final @property void styleSetWeight(int style, int weight) { // StyleSetWeight
		sendMessageDirect(style, weight);
	}

	/// Get the weight of characters of a style.
	final @property int styleGetWeight(int style) { // StyleGetWeight
		return sendMessageDirect(style, 0);
	}

	/// Set the character set of the font in a style.
	final @property void styleSetCharacterSet(int style, int characterSet) { // StyleSetCharacterSet
		sendMessageDirect(style, characterSet);
	}

	/// Set a style to be a hotspot or not.
	final @property void styleSetHotSpot(int style, bool hotspot) { // StyleSetHotSpot
		sendMessageDirect(style, hotspot);
	}

	/// Set the foreground colour of the main and additional selections and whether to use this setting.
	final void setSelFore(bool useSetting, Colour fore) { // SetSelFore
		sendMessageDirect(useSetting, fore.asInt);
	}

	/// Set the background colour of the main and additional selections and whether to use this setting.
	final void setSelBack(bool useSetting, Colour back) { // SetSelBack
		sendMessageDirect(useSetting, back.asInt);
	}

	/// Get the alpha of the selection.
	final @property int selAlpha() { // GetSelAlpha
		return sendMessageDirect(0, 0);
	}

	/// Set the alpha of the selection.
	final @property void selAlpha(int alpha) { // SetSelAlpha
		sendMessageDirect(alpha, 0);
	}

	/// Is the selection end of line filled?
	final @property bool selEOLFilled() { // GetSelEOLFilled
		return !!sendMessageDirect(0, 0);
	}

	/// Set the selection to have its end of line filled or not.
	final @property void selEOLFilled(bool filled) { // SetSelEOLFilled
		sendMessageDirect(filled, 0);
	}

	/// Set the foreground colour of the caret.
	final @property void caretFore(Colour fore) { // SetCaretFore
		sendMessageDirect(fore.asInt, 0);
	}

	/// When key+modifier combination km is pressed perform msg.
	final void assignCmdKey(KeyMod km, int msg) { // AssignCmdKey
		sendMessageDirect(km, msg);
	}

	/// When key+modifier combination km is pressed do nothing.
	final void clearCmdKey(KeyMod km) { // ClearCmdKey
		sendMessageDirect(km, 0);
	}

	/// Drop all key mappings.
	final void clearAllCmdKeys() { // ClearAllCmdKeys
		sendMessageDirect(0, 0);
	}

	/// Set the styles for a segment of the document.
	final void setStylingEx(int length, string styles) { // SetStylingEx
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(length, toCString(buff, styles));
	}

	/// Set a style to be visible or not.
	final @property void styleSetVisible(int style, bool visible) { // StyleSetVisible
		sendMessageDirect(style, visible);
	}

	/// Get the time in milliseconds that the caret is on and off.
	final @property int caretPeriod() { // GetCaretPeriod
		return sendMessageDirect(0, 0);
	}

	/// Get the time in milliseconds that the caret is on and off. 0 = steady on.
	final @property void caretPeriod(int periodMilliseconds) { // SetCaretPeriod
		sendMessageDirect(periodMilliseconds, 0);
	}

	/// First sets defaults like SetCharsDefault.
	final @property void wordChars(string characters) { // SetWordChars
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, characters));
	}

	/// May be nested.
	final void beginUndoAction() { // BeginUndoAction
		sendMessageDirect(0, 0);
	}

	/// End a sequence of actions that is undone and redone as a unit.
	final void endUndoAction() { // EndUndoAction
		sendMessageDirect(0, 0);
	}

	/// Set an indicator to plain, squiggle or TT.
	final @property void indicSetStyle(int indic, int style) { // IndicSetStyle
		sendMessageDirect(indic, style);
	}

	/// Retrieve the style of an indicator.
	final @property int indicGetStyle(int indic) { // IndicGetStyle
		return sendMessageDirect(indic, 0);
	}

	/// Set the foreground colour of an indicator.
	final @property void indicSetFore(int indic, Colour fore) { // IndicSetFore
		sendMessageDirect(indic, fore.asInt);
	}

	/// Retrieve the foreground colour of an indicator.
	final @property Colour indicGetFore(int indic) { // IndicGetFore
		return cast(Colour)sendMessageDirect(indic, 0);
	}

	/// Set an indicator to draw under text or over(default).
	final @property void indicSetUnder(int indic, bool under) { // IndicSetUnder
		sendMessageDirect(indic, under);
	}

	/// Retrieve whether indicator drawn under or over text.
	final @property bool indicGetUnder(int indic) { // IndicGetUnder
		return !!sendMessageDirect(indic, 0);
	}

	/// Set the foreground colour of all whitespace and whether to use this setting.
	final void setWhitespaceFore(bool useSetting, Colour fore) { // SetWhitespaceFore
		sendMessageDirect(useSetting, fore.asInt);
	}

	/// Set the background colour of all whitespace and whether to use this setting.
	final void setWhitespaceBack(bool useSetting, Colour back) { // SetWhitespaceBack
		sendMessageDirect(useSetting, back.asInt);
	}

	/// Set the size of the dots used to mark space characters.
	final @property void whitespaceSize(int size) { // SetWhitespaceSize
		sendMessageDirect(size, 0);
	}

	/// Get the size of the dots used to mark space characters.
	final @property int whitespaceSize() { // GetWhitespaceSize
		return sendMessageDirect(0, 0);
	}

	/// is used to expand the possible states.
	final @property void styleBits(int bits) { // SetStyleBits
		sendMessageDirect(bits, 0);
	}

	/// Retrieve number of bits in style bytes used to hold the lexical state.
	final @property int styleBits() { // GetStyleBits
		return sendMessageDirect(0, 0);
	}

	/// Used to hold extra styling information for each line.
	final @property void lineState(int line, int state) { // SetLineState
		sendMessageDirect(line, state);
	}

	/// Retrieve the extra styling information for a line.
	final @property int lineState(int line) { // GetLineState
		return sendMessageDirect(line, 0);
	}

	/// Retrieve the last line number that has line state.
	final @property int maxLineState() { // GetMaxLineState
		return sendMessageDirect(0, 0);
	}

	/// Is the background of the line containing the caret in a different colour?
	final @property bool caretLineVisible() { // GetCaretLineVisible
		return !!sendMessageDirect(0, 0);
	}

	/// Display the background of the line containing the caret in a different colour.
	final @property void caretLineVisible(bool show) { // SetCaretLineVisible
		sendMessageDirect(show, 0);
	}

	/// Get the colour of the background of the line containing the caret.
	final @property Colour caretLineBack() { // GetCaretLineBack
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// Set the colour of the background of the line containing the caret.
	final @property void caretLineBack(Colour back) { // SetCaretLineBack
		sendMessageDirect(back.asInt, 0);
	}

	/// Experimental feature, currently buggy.
	final @property void styleSetChangeable(int style, bool changeable) { // StyleSetChangeable
		sendMessageDirect(style, changeable);
	}

	/// the caret should be used to provide context.
	final void autoCShow(int lenEntered, string itemList) { // AutoCShow
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(lenEntered, toCString(buff, itemList));
	}

	/// Remove the auto-completion list from the screen.
	final void autoCCancel() { // AutoCCancel
		sendMessageDirect(0, 0);
	}

	/// Is there an auto-completion list visible?
	final bool autoCActive() { // AutoCActive
		return !!sendMessageDirect(0, 0);
	}

	/// Retrieve the position of the caret when the auto-completion list was displayed.
	final Position autoCPosStart() { // AutoCPosStart
		return sendMessageDirect(0, 0);
	}

	/// User has selected an item so remove the list and insert the selection.
	final void autoCComplete() { // AutoCComplete
		sendMessageDirect(0, 0);
	}

	/// Define a set of character that when typed cancel the auto-completion list.
	final void autoCStops(string characterSet) { // AutoCStops
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, characterSet));
	}

	/// Default is space but can be changed if items contain space.
	final @property void autoCSetSeparator(int separatorCharacter) { // AutoCSetSeparator
		sendMessageDirect(separatorCharacter, 0);
	}

	/// Retrieve the auto-completion list separator character.
	final @property int autoCGetSeparator() { // AutoCGetSeparator
		return sendMessageDirect(0, 0);
	}

	/// Select the item in the auto-completion list that starts with a string.
	final void autoCSelect(string text) { // AutoCSelect
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, text));
	}

	/// position before where the box was created.
	final @property void autoCSetCancelAtStart(bool cancel) { // AutoCSetCancelAtStart
		sendMessageDirect(cancel, 0);
	}

	/// Retrieve whether auto-completion cancelled by backspacing before start.
	final @property bool autoCGetCancelAtStart() { // AutoCGetCancelAtStart
		return !!sendMessageDirect(0, 0);
	}

	/// choose the selected item.
	final @property void autoCSetFillUps(string characterSet) { // AutoCSetFillUps
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, characterSet));
	}

	/// Should a single item auto-completion list automatically choose the item.
	final @property void autoCSetChooseSingle(bool chooseSingle) { // AutoCSetChooseSingle
		sendMessageDirect(chooseSingle, 0);
	}

	/// Retrieve whether a single item auto-completion list automatically choose the item.
	final @property bool autoCGetChooseSingle() { // AutoCGetChooseSingle
		return !!sendMessageDirect(0, 0);
	}

	/// Set whether case is significant when performing auto-completion searches.
	final @property void autoCSetIgnoreCase(bool ignoreCase) { // AutoCSetIgnoreCase
		sendMessageDirect(ignoreCase, 0);
	}

	/// Retrieve state of ignore case flag.
	final @property bool autoCGetIgnoreCase() { // AutoCGetIgnoreCase
		return !!sendMessageDirect(0, 0);
	}

	/// Display a list of strings and send notification when user chooses one.
	final void userListShow(int listType, string itemList) { // UserListShow
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(listType, toCString(buff, itemList));
	}

	/// Set whether or not autocompletion is hidden automatically when nothing matches.
	final @property void autoCSetAutoHide(bool autoHide) { // AutoCSetAutoHide
		sendMessageDirect(autoHide, 0);
	}

	/// Retrieve whether or not autocompletion is hidden automatically when nothing matches.
	final @property bool autoCGetAutoHide() { // AutoCGetAutoHide
		return !!sendMessageDirect(0, 0);
	}

	/// after the inserted text upon completion.
	final @property void autoCSetDropRestOfWord(bool dropRestOfWord) { // AutoCSetDropRestOfWord
		sendMessageDirect(dropRestOfWord, 0);
	}

	/// after the inserted text upon completion.
	final @property bool autoCGetDropRestOfWord() { // AutoCGetDropRestOfWord
		return !!sendMessageDirect(0, 0);
	}

	/// Register an XPM image for use in autocompletion lists.
	final void registerImage(int type, string xpmData) { // RegisterImage
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(type, toCString(buff, xpmData));
	}

	/// Clear all the registered XPM images.
	final void clearRegisteredImages() { // ClearRegisteredImages
		sendMessageDirect(0, 0);
	}

	/// Retrieve the auto-completion list type-separator character.
	final @property int autoCGetTypeSeparator() { // AutoCGetTypeSeparator
		return sendMessageDirect(0, 0);
	}

	/// Default is '?' but can be changed if items contain '?'.
	final @property void autoCSetTypeSeparator(int separatorCharacter) { // AutoCSetTypeSeparator
		sendMessageDirect(separatorCharacter, 0);
	}

	/// Set to 0 to autosize to fit longest item, which is the default.
	final @property void autoCSetMaxWidth(int characterCount) { // AutoCSetMaxWidth
		sendMessageDirect(characterCount, 0);
	}

	/// Get the maximum width, in characters, of auto-completion and user lists.
	final @property int autoCGetMaxWidth() { // AutoCGetMaxWidth
		return sendMessageDirect(0, 0);
	}

	/// The default is 5 rows.
	final @property void autoCSetMaxHeight(int rowCount) { // AutoCSetMaxHeight
		sendMessageDirect(rowCount, 0);
	}

	/// Set the maximum height, in rows, of auto-completion and user lists.
	final @property int autoCGetMaxHeight() { // AutoCGetMaxHeight
		return sendMessageDirect(0, 0);
	}

	/// Set the number of spaces used for one level of indentation.
	final @property void indent(int indentSize) { // SetIndent
		sendMessageDirect(indentSize, 0);
	}

	/// Retrieve indentation size.
	final @property int indent() { // GetIndent
		return sendMessageDirect(0, 0);
	}

	/// it will use a combination of tabs and spaces.
	final @property void useTabs(bool useTabs) { // SetUseTabs
		sendMessageDirect(useTabs, 0);
	}

	/// Retrieve whether tabs will be used in indentation.
	final @property bool useTabs() { // GetUseTabs
		return !!sendMessageDirect(0, 0);
	}

	/// Change the indentation of a line to a number of columns.
	final @property void lineIndentation(int line, int indentSize) { // SetLineIndentation
		sendMessageDirect(line, indentSize);
	}

	/// Retrieve the number of columns that a line is indented.
	final @property int lineIndentation(int line) { // GetLineIndentation
		return sendMessageDirect(line, 0);
	}

	/// Retrieve the position before the first non indentation character on a line.
	final @property Position lineIndentPosition(int line) { // GetLineIndentPosition
		return sendMessageDirect(line, 0);
	}

	/// Retrieve the column number of a position, taking tab width into account.
	final @property int column(Position pos) { // GetColumn
		return sendMessageDirect(pos, 0);
	}

	/// Count characters between two positions.
	final int countCharacters(int startPos, int endPos) { // CountCharacters
		return sendMessageDirect(startPos, endPos);
	}

	/// Show or hide the horizontal scroll bar.
	final @property void hScrollBar(bool show) { // SetHScrollBar
		sendMessageDirect(show, 0);
	}

	/// Is the horizontal scroll bar visible?
	final @property bool hScrollBar() { // GetHScrollBar
		return !!sendMessageDirect(0, 0);
	}

	/// Show or hide indentation guides.
	final @property void indentationGuides(int indentView) { // SetIndentationGuides
		sendMessageDirect(indentView, 0);
	}

	/// Are the indentation guides visible?
	final @property int indentationGuides() { // GetIndentationGuides
		return sendMessageDirect(0, 0);
	}

	/// 0 = no highlighted guide.
	final @property void highlightGuide(int column) { // SetHighlightGuide
		sendMessageDirect(column, 0);
	}

	/// Get the highlighted indentation guide column.
	final @property int highlightGuide() { // GetHighlightGuide
		return sendMessageDirect(0, 0);
	}

	/// Get the position after the last visible characters on a line.
	final @property int lineEndPosition(int line) { // GetLineEndPosition
		return sendMessageDirect(line, 0);
	}

	/// Get the code page used to interpret the bytes of the document as characters.
	final @property int codePage() { // GetCodePage
		return sendMessageDirect(0, 0);
	}

	/// Get the foreground colour of the caret.
	final @property Colour caretFore() { // GetCaretFore
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// In read-only mode?
	final @property bool readOnly() { // GetReadOnly
		return !!sendMessageDirect(0, 0);
	}

	/// Sets the position of the caret.
	final @property void currentPos(Position pos) { // SetCurrentPos
		sendMessageDirect(pos, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	final @property void selectionStart(Position pos) { // SetSelectionStart
		sendMessageDirect(pos, 0);
	}

	/// Returns the position at the start of the selection.
	final @property Position selectionStart() { // GetSelectionStart
		return sendMessageDirect(0, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	final @property void selectionEnd(Position pos) { // SetSelectionEnd
		sendMessageDirect(pos, 0);
	}

	/// Returns the position at the end of the selection.
	final @property Position selectionEnd() { // GetSelectionEnd
		return sendMessageDirect(0, 0);
	}

	/// Set caret to a position, while removing any existing selection.
	final void setEmptySelection(Position pos) { // SetEmptySelection
		sendMessageDirect(pos, 0);
	}

	/// Sets the print magnification added to the point size of each style for printing.
	final @property void printMagnification(int magnification) { // SetPrintMagnification
		sendMessageDirect(magnification, 0);
	}

	/// Returns the print magnification.
	final @property int printMagnification() { // GetPrintMagnification
		return sendMessageDirect(0, 0);
	}

	/// Modify colours when printing for clearer printed text.
	final @property void printColourMode(int mode) { // SetPrintColourMode
		sendMessageDirect(mode, 0);
	}

	/// Returns the print colour mode.
	final @property int printColourMode() { // GetPrintColourMode
		return sendMessageDirect(0, 0);
	}

	/// Find some text in the document.
	final Position findText(int flags, FindText ft) { // FindText
		return sendMessageDirect(flags, ft);
	}

	/// On Windows, will draw the document into a display context such as a printer.
	final Position formatRange(bool draw, FormatRange fr) { // FormatRange
		return sendMessageDirect(draw, fr);
	}

	/// Retrieve the display line at the top of the display.
	final @property int firstVisibleLine() { // GetFirstVisibleLine
		return sendMessageDirect(0, 0);
	}

	/// Returns the length of the line.
	final int getLine(int line, out string text) { // GetLine
		sendMessageDirect(line, 0);
		return sendMessageDirect(line, text);
	}

	/// Returns the number of lines in the document. There is always at least one.
	final @property int lineCount() { // GetLineCount
		return sendMessageDirect(0, 0);
	}

	/// Sets the size in pixels of the left margin.
	final @property void marginLeft(int pixelWidth) { // SetMarginLeft
		sendMessageDirect(0, pixelWidth);
	}

	/// Returns the size in pixels of the left margin.
	final @property int marginLeft() { // GetMarginLeft
		return sendMessageDirect(0, 0);
	}

	/// Sets the size in pixels of the right margin.
	final @property void marginRight(int pixelWidth) { // SetMarginRight
		sendMessageDirect(0, pixelWidth);
	}

	/// Returns the size in pixels of the right margin.
	final @property int marginRight() { // GetMarginRight
		return sendMessageDirect(0, 0);
	}

	/// Is the document different from when it was last saved?
	final @property bool modify() { // GetModify
		return !!sendMessageDirect(0, 0);
	}

	/// Select a range of text.
	final void setSel(Position start, Position end) { // SetSel
		sendMessageDirect(start, end);
	}

	/// Return the length of the text.
	final int getSelText(out string text) { // GetSelText
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, text);
	}

	/// Return the length of the text.
	final int getTextRange(TextRange tr) { // GetTextRange
		return sendMessageDirect(0, tr);
	}

	/// Draw the selection in normal style or with selection highlighted.
	final void hideSelection(bool normal) { // HideSelection
		sendMessageDirect(normal, 0);
	}

	/// Retrieve the x value of the point in the window where a position is displayed.
	final int pointXFromPosition(Position pos) { // PointXFromPosition
		return sendMessageDirect(0, pos);
	}

	/// Retrieve the y value of the point in the window where a position is displayed.
	final int pointYFromPosition(Position pos) { // PointYFromPosition
		return sendMessageDirect(0, pos);
	}

	/// Retrieve the line containing a position.
	final int lineFromPosition(Position pos) { // LineFromPosition
		return sendMessageDirect(pos, 0);
	}

	/// Retrieve the position at the start of a line.
	final Position positionFromLine(int line) { // PositionFromLine
		return sendMessageDirect(line, 0);
	}

	/// Scroll horizontally and vertically.
	final void lineScroll(int columns, int lines) { // LineScroll
		sendMessageDirect(columns, lines);
	}

	/// Ensure the caret is visible.
	final void scrollCaret() { // ScrollCaret
		sendMessageDirect(0, 0);
	}

	/// Replace the selected text with the argument text.
	final void replaceSel(string text) { // ReplaceSel
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, text));
	}

	/// Set to read only or read write.
	final @property void readOnly(bool readOnly) { // SetReadOnly
		sendMessageDirect(readOnly, 0);
	}

	/// Null operation.
	final void null() { // Null
		sendMessageDirect(0, 0);
	}

	/// Will a paste succeed?
	final bool canPaste() { // CanPaste
		return !!sendMessageDirect(0, 0);
	}

	/// Are there any undoable actions in the undo history?
	final bool canUndo() { // CanUndo
		return !!sendMessageDirect(0, 0);
	}

	/// Delete the undo history.
	final void emptyUndoBuffer() { // EmptyUndoBuffer
		sendMessageDirect(0, 0);
	}

	/// Undo one action in the undo history.
	final void undo() { // Undo
		sendMessageDirect(0, 0);
	}

	/// Cut the selection to the clipboard.
	final void cut() { // Cut
		sendMessageDirect(0, 0);
	}

	/// Copy the selection to the clipboard.
	final void copy() { // Copy
		sendMessageDirect(0, 0);
	}

	/// Paste the contents of the clipboard into the document replacing the selection.
	final void paste() { // Paste
		sendMessageDirect(0, 0);
	}

	/// Clear the selection.
	final void clear() { // Clear
		sendMessageDirect(0, 0);
	}

	/// Replace the contents of the document with the argument text.
	final void setText(string text) { // SetText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, text));
	}

	/// Returns number of characters retrieved.
	final int getText(int length, out string text) { // GetText
		sendMessageDirect(length, 0);
		return sendMessageDirect(length, text);
	}

	/// Retrieve the number of characters in the document.
	final @property int textLength() { // GetTextLength
		return sendMessageDirect(0, 0);
	}

	/// Retrieve a pointer to a function that processes messages for this Scintilla.
	final @property int directFunction() { // GetDirectFunction
		return sendMessageDirect(0, 0);
	}

	/// the function returned by GetDirectFunction.
	final @property int directPointer() { // GetDirectPointer
		return sendMessageDirect(0, 0);
	}

	/// Set to overtype (true) or insert mode.
	final @property void overtype(bool overtype) { // SetOvertype
		sendMessageDirect(overtype, 0);
	}

	/// Returns true if overtype mode is active otherwise false is returned.
	final @property bool overtype() { // GetOvertype
		return !!sendMessageDirect(0, 0);
	}

	/// Set the width of the insert mode caret.
	final @property void caretWidth(int pixelWidth) { // SetCaretWidth
		sendMessageDirect(pixelWidth, 0);
	}

	/// Returns the width of the insert mode caret.
	final @property int caretWidth() { // GetCaretWidth
		return sendMessageDirect(0, 0);
	}

	/// document without affecting the scroll position.
	final @property void targetStart(Position pos) { // SetTargetStart
		sendMessageDirect(pos, 0);
	}

	/// Get the position that starts the target.
	final @property Position targetStart() { // GetTargetStart
		return sendMessageDirect(0, 0);
	}

	/// document without affecting the scroll position.
	final @property void targetEnd(Position pos) { // SetTargetEnd
		sendMessageDirect(pos, 0);
	}

	/// Get the position that ends the target.
	final @property Position targetEnd() { // GetTargetEnd
		return sendMessageDirect(0, 0);
	}

	/// Returns the length of the replacement text.
	final int replaceTarget(int length, string text) { // ReplaceTarget
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(length, toCString(buff, text));
	}

	/// caused by processing the \d patterns.
	final int replaceTargetRE(int length, string text) { // ReplaceTargetRE
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(length, toCString(buff, text));
	}

	/// Returns length of range or -1 for failure in which case target is not moved.
	final int searchInTarget(int length, string text) { // SearchInTarget
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(length, toCString(buff, text));
	}

	/// Set the search flags used by SearchInTarget.
	final @property void searchFlags(int flags) { // SetSearchFlags
		sendMessageDirect(flags, 0);
	}

	/// Get the search flags used by SearchInTarget.
	final @property int searchFlags() { // GetSearchFlags
		return sendMessageDirect(0, 0);
	}

	/// Show a call tip containing a definition near position pos.
	final void callTipShow(Position pos, string definition) { // CallTipShow
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(pos, toCString(buff, definition));
	}

	/// Remove the call tip from the screen.
	final void callTipCancel() { // CallTipCancel
		sendMessageDirect(0, 0);
	}

	/// Is there an active call tip?
	final bool callTipActive() { // CallTipActive
		return !!sendMessageDirect(0, 0);
	}

	/// Retrieve the position where the caret was before displaying the call tip.
	final Position callTipPosStart() { // CallTipPosStart
		return sendMessageDirect(0, 0);
	}

	/// Highlight a segment of the definition.
	final void callTipSetHlt(int start, int end) { // CallTipSetHlt
		sendMessageDirect(start, end);
	}

	/// Set the background colour for the call tip.
	final @property void callTipSetBack(Colour back) { // CallTipSetBack
		sendMessageDirect(back.asInt, 0);
	}

	/// Set the foreground colour for the call tip.
	final @property void callTipSetFore(Colour fore) { // CallTipSetFore
		sendMessageDirect(fore.asInt, 0);
	}

	/// Set the foreground colour for the highlighted part of the call tip.
	final @property void callTipSetForeHlt(Colour fore) { // CallTipSetForeHlt
		sendMessageDirect(fore.asInt, 0);
	}

	/// Enable use of STYLE_CALLTIP and set call tip tab size in pixels.
	final @property void callTipUseStyle(int tabSize) { // CallTipUseStyle
		sendMessageDirect(tabSize, 0);
	}

	/// Set position of calltip, above or below text.
	final @property void callTipSetPosition(bool above) { // CallTipSetPosition
		sendMessageDirect(above, 0);
	}

	/// Find the display line of a document line taking hidden lines into account.
	final int visibleFromDocLine(int line) { // VisibleFromDocLine
		return sendMessageDirect(line, 0);
	}

	/// Find the document line of a display line taking hidden lines into account.
	final int docLineFromVisible(int lineDisplay) { // DocLineFromVisible
		return sendMessageDirect(lineDisplay, 0);
	}

	/// The number of display lines needed to wrap a document line
	final int wrapCount(int line) { // WrapCount
		return sendMessageDirect(line, 0);
	}

	/// line is a header and whether it is effectively white space.
	final @property void foldLevel(int line, int level) { // SetFoldLevel
		sendMessageDirect(line, level);
	}

	/// Retrieve the fold level of a line.
	final @property int foldLevel(int line) { // GetFoldLevel
		return sendMessageDirect(line, 0);
	}

	/// Find the last child line of a header line.
	final @property int lastChild(int line, int level) { // GetLastChild
		return sendMessageDirect(line, level);
	}

	/// Find the parent line of a child line.
	final @property int foldParent(int line) { // GetFoldParent
		return sendMessageDirect(line, 0);
	}

	/// Make a range of lines visible.
	final void showLines(int lineStart, int lineEnd) { // ShowLines
		sendMessageDirect(lineStart, lineEnd);
	}

	/// Make a range of lines invisible.
	final void hideLines(int lineStart, int lineEnd) { // HideLines
		sendMessageDirect(lineStart, lineEnd);
	}

	/// Is a line visible?
	final @property bool lineVisible(int line) { // GetLineVisible
		return !!sendMessageDirect(line, 0);
	}

	/// Are all lines visible?
	final @property bool allLinesVisible() { // GetAllLinesVisible
		return !!sendMessageDirect(0, 0);
	}

	/// Show the children of a header line.
	final @property void foldExpanded(int line, bool expanded) { // SetFoldExpanded
		sendMessageDirect(line, expanded);
	}

	/// Is a header line expanded?
	final @property bool foldExpanded(int line) { // GetFoldExpanded
		return !!sendMessageDirect(line, 0);
	}

	/// Switch a header line between expanded and contracted.
	final void toggleFold(int line) { // ToggleFold
		sendMessageDirect(line, 0);
	}

	/// Ensure a particular line is visible by expanding any header line hiding it.
	final void ensureVisible(int line) { // EnsureVisible
		sendMessageDirect(line, 0);
	}

	/// Set some style options for folding.
	final void setFoldFlags(int flags) { // SetFoldFlags
		sendMessageDirect(flags, 0);
	}

	/// Use the currently set visibility policy to determine which range to display.
	final void ensureVisibleEnforcePolicy(int line) { // EnsureVisibleEnforcePolicy
		sendMessageDirect(line, 0);
	}

	/// Sets whether a tab pressed when caret is within indentation indents.
	final @property void tabIndents(bool tabIndents) { // SetTabIndents
		sendMessageDirect(tabIndents, 0);
	}

	/// Does a tab pressed when caret is within indentation indent?
	final @property bool tabIndents() { // GetTabIndents
		return !!sendMessageDirect(0, 0);
	}

	/// Sets whether a backspace pressed when caret is within indentation unindents.
	final @property void backSpaceUnIndents(bool bsUnIndents) { // SetBackSpaceUnIndents
		sendMessageDirect(bsUnIndents, 0);
	}

	/// Does a backspace pressed when caret is within indentation unindent?
	final @property bool backSpaceUnIndents() { // GetBackSpaceUnIndents
		return !!sendMessageDirect(0, 0);
	}

	/// Sets the time the mouse must sit still to generate a mouse dwell event.
	final @property void mouseDwellTime(int periodMilliseconds) { // SetMouseDwellTime
		sendMessageDirect(periodMilliseconds, 0);
	}

	/// Retrieve the time the mouse must sit still to generate a mouse dwell event.
	final @property int mouseDwellTime() { // GetMouseDwellTime
		return sendMessageDirect(0, 0);
	}

	/// Get position of start of word.
	final int wordStartPosition(Position pos, bool onlyWordCharacters) { // WordStartPosition
		return sendMessageDirect(pos, onlyWordCharacters);
	}

	/// Get position of end of word.
	final int wordEndPosition(Position pos, bool onlyWordCharacters) { // WordEndPosition
		return sendMessageDirect(pos, onlyWordCharacters);
	}

	/// Sets whether text is word wrapped.
	final @property void wrapMode(int mode) { // SetWrapMode
		sendMessageDirect(mode, 0);
	}

	/// Retrieve whether text is word wrapped.
	final @property int wrapMode() { // GetWrapMode
		return sendMessageDirect(0, 0);
	}

	/// Set the display mode of visual flags for wrapped lines.
	final @property void wrapVisualFlags(int wrapVisualFlags) { // SetWrapVisualFlags
		sendMessageDirect(wrapVisualFlags, 0);
	}

	/// Retrive the display mode of visual flags for wrapped lines.
	final @property int wrapVisualFlags() { // GetWrapVisualFlags
		return sendMessageDirect(0, 0);
	}

	/// Set the location of visual flags for wrapped lines.
	final @property void wrapVisualFlagsLocation(int wrapVisualFlagsLocation) { // SetWrapVisualFlagsLocation
		sendMessageDirect(wrapVisualFlagsLocation, 0);
	}

	/// Retrive the location of visual flags for wrapped lines.
	final @property int wrapVisualFlagsLocation() { // GetWrapVisualFlagsLocation
		return sendMessageDirect(0, 0);
	}

	/// Set the start indent for wrapped lines.
	final @property void wrapStartIndent(int indent) { // SetWrapStartIndent
		sendMessageDirect(indent, 0);
	}

	/// Retrive the start indent for wrapped lines.
	final @property int wrapStartIndent() { // GetWrapStartIndent
		return sendMessageDirect(0, 0);
	}

	/// Sets how wrapped sublines are placed. Default is fixed.
	final @property void wrapIndentMode(int mode) { // SetWrapIndentMode
		sendMessageDirect(mode, 0);
	}

	/// Retrieve how wrapped sublines are placed. Default is fixed.
	final @property int wrapIndentMode() { // GetWrapIndentMode
		return sendMessageDirect(0, 0);
	}

	/// Sets the degree of caching of layout information.
	final @property void layoutCache(int mode) { // SetLayoutCache
		sendMessageDirect(mode, 0);
	}

	/// Retrieve the degree of caching of layout information.
	final @property int layoutCache() { // GetLayoutCache
		return sendMessageDirect(0, 0);
	}

	/// Sets the document width assumed for scrolling.
	final @property void scrollWidth(int pixelWidth) { // SetScrollWidth
		sendMessageDirect(pixelWidth, 0);
	}

	/// Retrieve the document width assumed for scrolling.
	final @property int scrollWidth() { // GetScrollWidth
		return sendMessageDirect(0, 0);
	}

	/// Sets whether the maximum width line displayed is used to set scroll width.
	final @property void scrollWidthTracking(bool tracking) { // SetScrollWidthTracking
		sendMessageDirect(tracking, 0);
	}

	/// Retrieve whether the scroll width tracks wide lines.
	final @property bool scrollWidthTracking() { // GetScrollWidthTracking
		return !!sendMessageDirect(0, 0);
	}

	/// Does not handle tab or control characters.
	final int textWidth(int style, string text) { // TextWidth
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(style, toCString(buff, text));
	}

	/// Setting this to false allows scrolling one page below the last line.
	final @property void endAtLastLine(bool endAtLastLine) { // SetEndAtLastLine
		sendMessageDirect(endAtLastLine, 0);
	}

	/// line at the bottom of the view.
	final @property bool endAtLastLine() { // GetEndAtLastLine
		return !!sendMessageDirect(0, 0);
	}

	/// Retrieve the height of a particular line of text in pixels.
	final int textHeight(int line) { // TextHeight
		return sendMessageDirect(line, 0);
	}

	/// Show or hide the vertical scroll bar.
	final @property void vScrollBar(bool show) { // SetVScrollBar
		sendMessageDirect(show, 0);
	}

	/// Is the vertical scroll bar visible?
	final @property bool vScrollBar() { // GetVScrollBar
		return !!sendMessageDirect(0, 0);
	}

	/// Append a string to the end of the document without changing the selection.
	final void appendText(int length, string text) { // AppendText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(length, toCString(buff, text));
	}

	/// Is drawing done in two phases with backgrounds drawn before faoregrounds?
	final @property bool twoPhaseDraw() { // GetTwoPhaseDraw
		return !!sendMessageDirect(0, 0);
	}

	/// and then the foreground. This avoids chopping off characters that overlap the next run.
	final @property void twoPhaseDraw(bool twoPhase) { // SetTwoPhaseDraw
		sendMessageDirect(twoPhase, 0);
	}

	/// Choose the quality level for text from the FontQuality enumeration.
	final @property void fontQuality(int fontQuality) { // SetFontQuality
		sendMessageDirect(fontQuality, 0);
	}

	/// Retrieve the quality level for text.
	final @property int fontQuality() { // GetFontQuality
		return sendMessageDirect(0, 0);
	}

	/// Scroll so that a display line is at the top of the display.
	final @property void firstVisibleLine(int lineDisplay) { // SetFirstVisibleLine
		sendMessageDirect(lineDisplay, 0);
	}

	/// Change the effect of pasting when there are multiple selections.
	final @property void multiPaste(int multiPaste) { // SetMultiPaste
		sendMessageDirect(multiPaste, 0);
	}

	/// Retrieve the effect of pasting when there are multiple selections..
	final @property int multiPaste() { // GetMultiPaste
		return sendMessageDirect(0, 0);
	}

	/// Retrieve the value of a tag from a regular expression search.
	final int getTag(int tagNumber, out string tagValue) { // GetTag
		sendMessageDirect(tagNumber, 0);
		return sendMessageDirect(tagNumber, tagValue);
	}

	/// Make the target range start and end be the same as the selection range start and end.
	final void targetFromSelection() { // TargetFromSelection
		sendMessageDirect(0, 0);
	}

	/// Join the lines in the target.
	final void linesJoin() { // LinesJoin
		sendMessageDirect(0, 0);
	}

	/// where possible.
	final void linesSplit(int pixelWidth) { // LinesSplit
		sendMessageDirect(pixelWidth, 0);
	}

	/// Set the colours used as a chequerboard pattern in the fold margin
	final void setFoldMarginColour(bool useSetting, Colour back) { // SetFoldMarginColour
		sendMessageDirect(useSetting, back.asInt);
	}

	///
	final void setFoldMarginHiColour(bool useSetting, Colour fore) { // SetFoldMarginHiColour
		sendMessageDirect(useSetting, fore.asInt);
	}

	/// Move caret down one line.
	final void lineDown() { // LineDown
		sendMessageDirect(0, 0);
	}

	/// Move caret down one line extending selection to new caret position.
	final void lineDownExtend() { // LineDownExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret up one line.
	final void lineUp() { // LineUp
		sendMessageDirect(0, 0);
	}

	/// Move caret up one line extending selection to new caret position.
	final void lineUpExtend() { // LineUpExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret left one character.
	final void charLeft() { // CharLeft
		sendMessageDirect(0, 0);
	}

	/// Move caret left one character extending selection to new caret position.
	final void charLeftExtend() { // CharLeftExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret right one character.
	final void charRight() { // CharRight
		sendMessageDirect(0, 0);
	}

	/// Move caret right one character extending selection to new caret position.
	final void charRightExtend() { // CharRightExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret left one word.
	final void wordLeft() { // WordLeft
		sendMessageDirect(0, 0);
	}

	/// Move caret left one word extending selection to new caret position.
	final void wordLeftExtend() { // WordLeftExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret right one word.
	final void wordRight() { // WordRight
		sendMessageDirect(0, 0);
	}

	/// Move caret right one word extending selection to new caret position.
	final void wordRightExtend() { // WordRightExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position on line.
	final void home() { // Home
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position on line extending selection to new caret position.
	final void homeExtend() { // HomeExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position on line.
	final void lineEnd() { // LineEnd
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position on line extending selection to new caret position.
	final void lineEndExtend() { // LineEndExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position in document.
	final void documentStart() { // DocumentStart
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position in document extending selection to new caret position.
	final void documentStartExtend() { // DocumentStartExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position in document.
	final void documentEnd() { // DocumentEnd
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position in document extending selection to new caret position.
	final void documentEndExtend() { // DocumentEndExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret one page up.
	final void pageUp() { // PageUp
		sendMessageDirect(0, 0);
	}

	/// Move caret one page up extending selection to new caret position.
	final void pageUpExtend() { // PageUpExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret one page down.
	final void pageDown() { // PageDown
		sendMessageDirect(0, 0);
	}

	/// Move caret one page down extending selection to new caret position.
	final void pageDownExtend() { // PageDownExtend
		sendMessageDirect(0, 0);
	}

	/// Switch from insert to overtype mode or the reverse.
	final void editToggleOvertype() { // EditToggleOvertype
		sendMessageDirect(0, 0);
	}

	/// Cancel any modes such as call tip or auto-completion list display.
	final void cancel() { // Cancel
		sendMessageDirect(0, 0);
	}

	/// Delete the selection or if no selection, the character before the caret.
	final void deleteBack() { // DeleteBack
		sendMessageDirect(0, 0);
	}

	/// If more than one line selected, indent the lines.
	final void tab() { // Tab
		sendMessageDirect(0, 0);
	}

	/// Dedent the selected lines.
	final void backTab() { // BackTab
		sendMessageDirect(0, 0);
	}

	/// Insert a new line, may use a CRLF, CR or LF depending on EOL mode.
	final void newLine() { // NewLine
		sendMessageDirect(0, 0);
	}

	/// Insert a Form Feed character.
	final void formFeed() { // FormFeed
		sendMessageDirect(0, 0);
	}

	/// If already there move to first character on line.
	final void vCHome() { // VCHome
		sendMessageDirect(0, 0);
	}

	/// Like VCHome but extending selection to new caret position.
	final void vCHomeExtend() { // VCHomeExtend
		sendMessageDirect(0, 0);
	}

	/// Magnify the displayed text by increasing the sizes by 1 point.
	final void zoomIn() { // ZoomIn
		sendMessageDirect(0, 0);
	}

	/// Make the displayed text smaller by decreasing the sizes by 1 point.
	final void zoomOut() { // ZoomOut
		sendMessageDirect(0, 0);
	}

	/// Delete the word to the left of the caret.
	final void delWordLeft() { // DelWordLeft
		sendMessageDirect(0, 0);
	}

	/// Delete the word to the right of the caret.
	final void delWordRight() { // DelWordRight
		sendMessageDirect(0, 0);
	}

	/// Delete the word to the right of the caret, but not the trailing non-word characters.
	final void delWordRightEnd() { // DelWordRightEnd
		sendMessageDirect(0, 0);
	}

	/// Cut the line containing the caret.
	final void lineCut() { // LineCut
		sendMessageDirect(0, 0);
	}

	/// Delete the line containing the caret.
	final void lineDelete() { // LineDelete
		sendMessageDirect(0, 0);
	}

	/// Switch the current line with the previous.
	final void lineTranspose() { // LineTranspose
		sendMessageDirect(0, 0);
	}

	/// Duplicate the current line.
	final void lineDuplicate() { // LineDuplicate
		sendMessageDirect(0, 0);
	}

	/// Transform the selection to lower case.
	final void lowerCase() { // LowerCase
		sendMessageDirect(0, 0);
	}

	/// Transform the selection to upper case.
	final void upperCase() { // UpperCase
		sendMessageDirect(0, 0);
	}

	/// Scroll the document down, keeping the caret visible.
	final void lineScrollDown() { // LineScrollDown
		sendMessageDirect(0, 0);
	}

	/// Scroll the document up, keeping the caret visible.
	final void lineScrollUp() { // LineScrollUp
		sendMessageDirect(0, 0);
	}

	/// Will not delete the character before at the start of a line.
	final void deleteBackNotLine() { // DeleteBackNotLine
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position on display line.
	final void homeDisplay() { // HomeDisplay
		sendMessageDirect(0, 0);
	}

	/// new caret position.
	final void homeDisplayExtend() { // HomeDisplayExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position on display line.
	final void lineEndDisplay() { // LineEndDisplay
		sendMessageDirect(0, 0);
	}

	/// caret position.
	final void lineEndDisplayExtend() { // LineEndDisplayExtend
		sendMessageDirect(0, 0);
	}

	/// or end of the document line, as appropriate for (Home|LineEnd|VCHome)(Extend)?.
	final void homeWrap() { // HomeWrap
		sendMessageDirect(0, 0);
	}

	///
	final void homeWrapExtend() { // HomeWrapExtend
		sendMessageDirect(0, 0);
	}

	///
	final void lineEndWrap() { // LineEndWrap
		sendMessageDirect(0, 0);
	}

	///
	final void lineEndWrapExtend() { // LineEndWrapExtend
		sendMessageDirect(0, 0);
	}

	///
	final void vCHomeWrap() { // VCHomeWrap
		sendMessageDirect(0, 0);
	}

	///
	final void vCHomeWrapExtend() { // VCHomeWrapExtend
		sendMessageDirect(0, 0);
	}

	/// Copy the line containing the caret.
	final void lineCopy() { // LineCopy
		sendMessageDirect(0, 0);
	}

	/// Move the caret inside current view if it's not there already.
	final void moveCaretInsideView() { // MoveCaretInsideView
		sendMessageDirect(0, 0);
	}

	/// How many characters are on a line, including end of line characters?
	final int lineLength(int line) { // LineLength
		return sendMessageDirect(line, 0);
	}

	/// Highlight the characters at two positions.
	final void braceHighlight(Position pos1, Position pos2) { // BraceHighlight
		sendMessageDirect(pos1, pos2);
	}

	/// Use specified indicator to highlight matching braces instead of changing their style.
	final void braceHighlightIndicator(bool useBraceHighlightIndicator, int indicator) { // BraceHighlightIndicator
		sendMessageDirect(useBraceHighlightIndicator, indicator);
	}

	/// Highlight the character at a position indicating there is no matching brace.
	final void braceBadLight(Position pos) { // BraceBadLight
		sendMessageDirect(pos, 0);
	}

	/// Use specified indicator to highlight non matching brace instead of changing its style.
	final void braceBadLightIndicator(bool useBraceBadLightIndicator, int indicator) { // BraceBadLightIndicator
		sendMessageDirect(useBraceBadLightIndicator, indicator);
	}

	/// Find the position of a matching brace or INVALID_POSITION if no match.
	final Position braceMatch(Position pos) { // BraceMatch
		return sendMessageDirect(pos, 0);
	}

	/// Are the end of line characters visible?
	final @property bool viewEOL() { // GetViewEOL
		return !!sendMessageDirect(0, 0);
	}

	/// Make the end of line characters visible or invisible.
	final @property void viewEOL(bool visible) { // SetViewEOL
		sendMessageDirect(visible, 0);
	}

	/// Retrieve a pointer to the document object.
	final @property int docPointer() { // GetDocPointer
		return sendMessageDirect(0, 0);
	}

	/// Change the document object used.
	final @property void docPointer(int pointer) { // SetDocPointer
		sendMessageDirect(0, pointer);
	}

	/// Set which document modification events are sent to the container.
	final @property void modEventMask(int mask) { // SetModEventMask
		sendMessageDirect(mask, 0);
	}

	/// Retrieve the column number which text should be kept within.
	final @property int edgeColumn() { // GetEdgeColumn
		return sendMessageDirect(0, 0);
	}

	/// If text goes past the edge then it is highlighted.
	final @property void edgeColumn(int column) { // SetEdgeColumn
		sendMessageDirect(column, 0);
	}

	/// Retrieve the edge highlight mode.
	final @property int edgeMode() { // GetEdgeMode
		return sendMessageDirect(0, 0);
	}

	/// goes beyond it (EDGE_BACKGROUND) or not displayed at all (EDGE_NONE).
	final @property void edgeMode(int mode) { // SetEdgeMode
		sendMessageDirect(mode, 0);
	}

	/// Retrieve the colour used in edge indication.
	final @property Colour edgeColour() { // GetEdgeColour
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// Change the colour used in edge indication.
	final @property void edgeColour(Colour edgeColour) { // SetEdgeColour
		sendMessageDirect(edgeColour.asInt, 0);
	}

	/// Sets the current caret position to be the search anchor.
	final void searchAnchor() { // SearchAnchor
		sendMessageDirect(0, 0);
	}

	/// Does not ensure the selection is visible.
	final int searchNext(int flags, string text) { // SearchNext
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(flags, toCString(buff, text));
	}

	/// Does not ensure the selection is visible.
	final int searchPrev(int flags, string text) { // SearchPrev
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(flags, toCString(buff, text));
	}

	/// Retrieves the number of lines completely visible.
	final @property int linesOnScreen() { // LinesOnScreen
		return sendMessageDirect(0, 0);
	}

	/// the wrong mouse button.
	final void usePopUp(bool allowPopUp) { // UsePopUp
		sendMessageDirect(allowPopUp, 0);
	}

	/// Is the selection rectangular? The alternative is the more common stream selection.
	final @property bool selectionIsRectangle() { // SelectionIsRectangle
		return !!sendMessageDirect(0, 0);
	}

	/// It may be positive to magnify or negative to reduce.
	final @property void zoom(int zoom) { // SetZoom
		sendMessageDirect(zoom, 0);
	}

	/// Retrieve the zoom level.
	final @property int zoom() { // GetZoom
		return sendMessageDirect(0, 0);
	}

	/// Starts with reference count of 1 and not selected into editor.
	final int createDocument() { // CreateDocument
		return sendMessageDirect(0, 0);
	}

	/// Extend life of document.
	final void addRefDocument(int doc) { // AddRefDocument
		sendMessageDirect(0, doc);
	}

	/// Release a reference to the document, deleting document if it fades to black.
	final void releaseDocument(int doc) { // ReleaseDocument
		sendMessageDirect(0, doc);
	}

	/// Get which document modification events are sent to the container.
	final @property int modEventMask() { // GetModEventMask
		return sendMessageDirect(0, 0);
	}

	/// Change internal focus flag.
	final @property void focus(bool focus) { // SetFocus
		sendMessageDirect(focus, 0);
	}

	/// Get internal focus flag.
	final @property bool focus() { // GetFocus
		return !!sendMessageDirect(0, 0);
	}

	/// Change error status - 0 = OK.
	final @property void status(int statusCode) { // SetStatus
		sendMessageDirect(statusCode, 0);
	}

	/// Get error status.
	final @property int status() { // GetStatus
		return sendMessageDirect(0, 0);
	}

	/// Set whether the mouse is captured when its button is pressed.
	final @property void mouseDownCaptures(bool captures) { // SetMouseDownCaptures
		sendMessageDirect(captures, 0);
	}

	/// Get whether mouse gets captured.
	final @property bool mouseDownCaptures() { // GetMouseDownCaptures
		return !!sendMessageDirect(0, 0);
	}

	/// Sets the cursor to one of the SC_CURSOR* values.
	final @property void cursor(int cursorType) { // SetCursor
		sendMessageDirect(cursorType, 0);
	}

	/// Get cursor type.
	final @property int cursor() { // GetCursor
		return sendMessageDirect(0, 0);
	}

	/// If symbol is < 32, keep the drawn way, else, use the given character.
	final @property void controlCharSymbol(int symbol) { // SetControlCharSymbol
		sendMessageDirect(symbol, 0);
	}

	/// Get the way control characters are displayed.
	final @property int controlCharSymbol() { // GetControlCharSymbol
		return sendMessageDirect(0, 0);
	}

	/// Move to the previous change in capitalisation.
	final void wordPartLeft() { // WordPartLeft
		sendMessageDirect(0, 0);
	}

	/// to new caret position.
	final void wordPartLeftExtend() { // WordPartLeftExtend
		sendMessageDirect(0, 0);
	}

	/// Move to the change next in capitalisation.
	final void wordPartRight() { // WordPartRight
		sendMessageDirect(0, 0);
	}

	/// to new caret position.
	final void wordPartRightExtend() { // WordPartRightExtend
		sendMessageDirect(0, 0);
	}

	/// is to be moved to by Find, FindNext, GotoLine, etc.
	final void setVisiblePolicy(int visiblePolicy, int visibleSlop) { // SetVisiblePolicy
		sendMessageDirect(visiblePolicy, visibleSlop);
	}

	/// Delete back from the current position to the start of the line.
	final void delLineLeft() { // DelLineLeft
		sendMessageDirect(0, 0);
	}

	/// Delete forwards from the current position to the end of the line.
	final void delLineRight() { // DelLineRight
		sendMessageDirect(0, 0);
	}

	/// Get and Set the xOffset (ie, horizonal scroll position).
	final @property void xOffset(int newOffset) { // SetXOffset
		sendMessageDirect(newOffset, 0);
	}

	///
	final @property int xOffset() { // GetXOffset
		return sendMessageDirect(0, 0);
	}

	/// Set the last x chosen value to be the caret x position.
	final void chooseCaretX() { // ChooseCaretX
		sendMessageDirect(0, 0);
	}

	/// Set the focus to this Scintilla widget.
	final void grabFocus() { // GrabFocus
		sendMessageDirect(0, 0);
	}

	/// The exclusion zone is given in pixels.
	final void setXCaretPolicy(int caretPolicy, int caretSlop) { // SetXCaretPolicy
		sendMessageDirect(caretPolicy, caretSlop);
	}

	/// The exclusion zone is given in lines.
	final void setYCaretPolicy(int caretPolicy, int caretSlop) { // SetYCaretPolicy
		sendMessageDirect(caretPolicy, caretSlop);
	}

	/// Set printing to line wrapped (SC_WRAP_WORD) or not line wrapped (SC_WRAP_NONE).
	final @property void printWrapMode(int mode) { // SetPrintWrapMode
		sendMessageDirect(mode, 0);
	}

	/// Is printing line wrapped?
	final @property int printWrapMode() { // GetPrintWrapMode
		return sendMessageDirect(0, 0);
	}

	/// Set a fore colour for active hotspots.
	final @property void hotspotActiveFore(bool useSetting, Colour fore) { // SetHotspotActiveFore
		sendMessageDirect(useSetting, fore.asInt);
	}

	/// Get the fore colour for active hotspots.
	final @property Colour hotspotActiveFore() { // GetHotspotActiveFore
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// Set a back colour for active hotspots.
	final @property void hotspotActiveBack(bool useSetting, Colour back) { // SetHotspotActiveBack
		sendMessageDirect(useSetting, back.asInt);
	}

	/// Get the back colour for active hotspots.
	final @property Colour hotspotActiveBack() { // GetHotspotActiveBack
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// Enable / Disable underlining active hotspots.
	final @property void hotspotActiveUnderline(bool underline) { // SetHotspotActiveUnderline
		sendMessageDirect(underline, 0);
	}

	/// Get whether underlining for active hotspots.
	final @property bool hotspotActiveUnderline() { // GetHotspotActiveUnderline
		return !!sendMessageDirect(0, 0);
	}

	/// Limit hotspots to single line so hotspots on two lines don't merge.
	final @property void hotspotSingleLine(bool singleLine) { // SetHotspotSingleLine
		sendMessageDirect(singleLine, 0);
	}

	/// Get the HotspotSingleLine property
	final @property bool hotspotSingleLine() { // GetHotspotSingleLine
		return !!sendMessageDirect(0, 0);
	}

	/// Move caret between paragraphs (delimited by empty lines).
	final void paraDown() { // ParaDown
		sendMessageDirect(0, 0);
	}

	///
	final void paraDownExtend() { // ParaDownExtend
		sendMessageDirect(0, 0);
	}

	///
	final void paraUp() { // ParaUp
		sendMessageDirect(0, 0);
	}

	///
	final void paraUpExtend() { // ParaUpExtend
		sendMessageDirect(0, 0);
	}

	/// page into account. Returns 0 if passed 0.
	final Position positionBefore(Position pos) { // PositionBefore
		return sendMessageDirect(pos, 0);
	}

	/// page into account. Maximum value returned is the last position in the document.
	final Position positionAfter(Position pos) { // PositionAfter
		return sendMessageDirect(pos, 0);
	}

	/// Copy a range of text to the clipboard. Positions are clipped into the document.
	final void copyRange(Position start, Position end) { // CopyRange
		sendMessageDirect(start, end);
	}

	/// Copy argument text to the clipboard.
	final void copyText(int length, string text) { // CopyText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(length, toCString(buff, text));
	}

	/// by lines (SC_SEL_LINES).
	final @property void selectionMode(int mode) { // SetSelectionMode
		sendMessageDirect(mode, 0);
	}

	/// Get the mode of the current selection.
	final @property int selectionMode() { // GetSelectionMode
		return sendMessageDirect(0, 0);
	}

	/// Retrieve the position of the start of the selection at the given line (INVALID_POSITION if no selection on this line).
	final Position getLineSelStartPosition(int line) { // GetLineSelStartPosition
		return sendMessageDirect(line, 0);
	}

	/// Retrieve the position of the end of the selection at the given line (INVALID_POSITION if no selection on this line).
	final Position getLineSelEndPosition(int line) { // GetLineSelEndPosition
		return sendMessageDirect(line, 0);
	}

	/// Move caret down one line, extending rectangular selection to new caret position.
	final void lineDownRectExtend() { // LineDownRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret up one line, extending rectangular selection to new caret position.
	final void lineUpRectExtend() { // LineUpRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret left one character, extending rectangular selection to new caret position.
	final void charLeftRectExtend() { // CharLeftRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret right one character, extending rectangular selection to new caret position.
	final void charRightRectExtend() { // CharRightRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to first position on line, extending rectangular selection to new caret position.
	final void homeRectExtend() { // HomeRectExtend
		sendMessageDirect(0, 0);
	}

	/// In either case, extend rectangular selection to new caret position.
	final void vCHomeRectExtend() { // VCHomeRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to last position on line, extending rectangular selection to new caret position.
	final void lineEndRectExtend() { // LineEndRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret one page up, extending rectangular selection to new caret position.
	final void pageUpRectExtend() { // PageUpRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret one page down, extending rectangular selection to new caret position.
	final void pageDownRectExtend() { // PageDownRectExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page.
	final void stutteredPageUp() { // StutteredPageUp
		sendMessageDirect(0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page, extending selection to new caret position.
	final void stutteredPageUpExtend() { // StutteredPageUpExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page.
	final void stutteredPageDown() { // StutteredPageDown
		sendMessageDirect(0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page, extending selection to new caret position.
	final void stutteredPageDownExtend() { // StutteredPageDownExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret left one word, position cursor at end of word.
	final void wordLeftEnd() { // WordLeftEnd
		sendMessageDirect(0, 0);
	}

	/// Move caret left one word, position cursor at end of word, extending selection to new caret position.
	final void wordLeftEndExtend() { // WordLeftEndExtend
		sendMessageDirect(0, 0);
	}

	/// Move caret right one word, position cursor at end of word.
	final void wordRightEnd() { // WordRightEnd
		sendMessageDirect(0, 0);
	}

	/// Move caret right one word, position cursor at end of word, extending selection to new caret position.
	final void wordRightEndExtend() { // WordRightEndExtend
		sendMessageDirect(0, 0);
	}

	/// Should be called after SetWordChars.
	final @property void whitespaceChars(string characters) { // SetWhitespaceChars
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, characters));
	}

	/// Reset the set of characters for whitespace and word characters to the defaults.
	final void setCharsDefault() { // SetCharsDefault
		sendMessageDirect(0, 0);
	}

	/// Get currently selected item position in the auto-completion list
	final int autoCGetCurrent() { // AutoCGetCurrent
		return sendMessageDirect(0, 0);
	}

	/// Returns the length of the item text
	final int autoCGetCurrentText(out string s) { // AutoCGetCurrentText
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, s);
	}

	/// Enlarge the document to a particular size of text bytes.
	final void allocate(int bytes) { // Allocate
		sendMessageDirect(bytes, 0);
	}

	/// Return the length in bytes.
	final int targetAsUTF8(out string s) { // TargetAsUTF8
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, s);
	}

	/// Set to -1 and the string will be measured to the first nul.
	final void setLengthForEncode(int bytes) { // SetLengthForEncode
		sendMessageDirect(bytes, 0);
	}

	/// On error return 0.
	final int encodedFromUTF8(string utf8, out string encoded) { // EncodedFromUTF8
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(toCString(buff, utf8), 0);
		return sendMessageDirect(toCString(buff, utf8), encoded);
	}

	/// multi-byte characters. If beyond end of line, return line end position.
	final int findColumn(int line, int column) { // FindColumn
		return sendMessageDirect(line, column);
	}

	/// Can the caret preferred x position only be changed by explicit movement commands?
	final @property int caretSticky() { // GetCaretSticky
		return sendMessageDirect(0, 0);
	}

	/// Stop the caret preferred x position changing when the user types.
	final @property void caretSticky(int useCaretStickyBehaviour) { // SetCaretSticky
		sendMessageDirect(useCaretStickyBehaviour, 0);
	}

	/// Switch between sticky and non-sticky: meant to be bound to a key.
	final void toggleCaretSticky() { // ToggleCaretSticky
		sendMessageDirect(0, 0);
	}

	/// Enable/Disable convert-on-paste for line endings
	final @property void pasteConvertEndings(bool convert) { // SetPasteConvertEndings
		sendMessageDirect(convert, 0);
	}

	/// Get convert-on-paste setting
	final @property bool pasteConvertEndings() { // GetPasteConvertEndings
		return !!sendMessageDirect(0, 0);
	}

	/// Duplicate the selection. If selection empty duplicate the line containing the caret.
	final void selectionDuplicate() { // SelectionDuplicate
		sendMessageDirect(0, 0);
	}

	/// Set background alpha of the caret line.
	final @property void caretLineBackAlpha(int alpha) { // SetCaretLineBackAlpha
		sendMessageDirect(alpha, 0);
	}

	/// Get the background alpha of the caret line.
	final @property int caretLineBackAlpha() { // GetCaretLineBackAlpha
		return sendMessageDirect(0, 0);
	}

	/// Set the style of the caret to be drawn.
	final @property void caretStyle(int caretStyle) { // SetCaretStyle
		sendMessageDirect(caretStyle, 0);
	}

	/// Returns the current style of the caret.
	final @property int caretStyle() { // GetCaretStyle
		return sendMessageDirect(0, 0);
	}

	/// Set the indicator used for IndicatorFillRange and IndicatorClearRange
	final @property void indicatorCurrent(int indicator) { // SetIndicatorCurrent
		sendMessageDirect(indicator, 0);
	}

	/// Get the current indicator
	final @property int indicatorCurrent() { // GetIndicatorCurrent
		return sendMessageDirect(0, 0);
	}

	/// Set the value used for IndicatorFillRange
	final @property void indicatorValue(int value) { // SetIndicatorValue
		sendMessageDirect(value, 0);
	}

	/// Get the current indicator vaue
	final @property int indicatorValue() { // GetIndicatorValue
		return sendMessageDirect(0, 0);
	}

	/// Turn a indicator on over a range.
	final void indicatorFillRange(int position, int fillLength) { // IndicatorFillRange
		sendMessageDirect(position, fillLength);
	}

	/// Turn a indicator off over a range.
	final void indicatorClearRange(int position, int clearLength) { // IndicatorClearRange
		sendMessageDirect(position, clearLength);
	}

	/// Are any indicators present at position?
	final int indicatorAllOnFor(int position) { // IndicatorAllOnFor
		return sendMessageDirect(position, 0);
	}

	/// What value does a particular indicator have at at a position?
	final int indicatorValueAt(int indicator, int position) { // IndicatorValueAt
		return sendMessageDirect(indicator, position);
	}

	/// Where does a particular indicator start?
	final int indicatorStart(int indicator, int position) { // IndicatorStart
		return sendMessageDirect(indicator, position);
	}

	/// Where does a particular indicator end?
	final int indicatorEnd(int indicator, int position) { // IndicatorEnd
		return sendMessageDirect(indicator, position);
	}

	/// Set number of entries in position cache
	final @property void positionCache(int size) { // SetPositionCache
		sendMessageDirect(size, 0);
	}

	/// How many entries are allocated to the position cache?
	final @property int positionCache() { // GetPositionCache
		return sendMessageDirect(0, 0);
	}

	/// Copy the selection, if selection empty copy the line with the caret
	final void copyAllowLine() { // CopyAllowLine
		sendMessageDirect(0, 0);
	}

	/// characters in the document.
	final @property int characterPointer() { // GetCharacterPointer
		return sendMessageDirect(0, 0);
	}

	/// Always interpret keyboard input as Unicode
	final @property void keysUnicode(bool keysUnicode) { // SetKeysUnicode
		sendMessageDirect(keysUnicode, 0);
	}

	/// Are keys always interpreted as Unicode?
	final @property bool keysUnicode() { // GetKeysUnicode
		return !!sendMessageDirect(0, 0);
	}

	/// Set the alpha fill colour of the given indicator.
	final @property void indicSetAlpha(int indicator, int alpha) { // IndicSetAlpha
		sendMessageDirect(indicator, alpha);
	}

	/// Get the alpha fill colour of the given indicator.
	final @property int indicGetAlpha(int indicator) { // IndicGetAlpha
		return sendMessageDirect(indicator, 0);
	}

	/// Set the alpha outline colour of the given indicator.
	final @property void indicSetOutlineAlpha(int indicator, int alpha) { // IndicSetOutlineAlpha
		sendMessageDirect(indicator, alpha);
	}

	/// Get the alpha outline colour of the given indicator.
	final @property int indicGetOutlineAlpha(int indicator) { // IndicGetOutlineAlpha
		return sendMessageDirect(indicator, 0);
	}

	/// Set extra ascent for each line
	final @property void extraAscent(int extraAscent) { // SetExtraAscent
		sendMessageDirect(extraAscent, 0);
	}

	/// Get extra ascent for each line
	final @property int extraAscent() { // GetExtraAscent
		return sendMessageDirect(0, 0);
	}

	/// Set extra descent for each line
	final @property void extraDescent(int extraDescent) { // SetExtraDescent
		sendMessageDirect(extraDescent, 0);
	}

	/// Get extra descent for each line
	final @property int extraDescent() { // GetExtraDescent
		return sendMessageDirect(0, 0);
	}

	/// Which symbol was defined for markerNumber with MarkerDefine
	final int markerSymbolDefined(int markerNumber) { // MarkerSymbolDefined
		return sendMessageDirect(markerNumber, 0);
	}

	/// Set the text in the text margin for a line
	final @property void marginSetText(int line, string text) { // MarginSetText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(line, toCString(buff, text));
	}

	/// Get the text in the text margin for a line
	final @property int marginGetText(int line, out string text) { // MarginGetText
		sendMessageDirect(line, 0);
		return sendMessageDirect(line, text);
	}

	/// Set the style number for the text margin for a line
	final @property void marginSetStyle(int line, int style) { // MarginSetStyle
		sendMessageDirect(line, style);
	}

	/// Get the style number for the text margin for a line
	final @property int marginGetStyle(int line) { // MarginGetStyle
		return sendMessageDirect(line, 0);
	}

	/// Set the style in the text margin for a line
	final @property void marginSetStyles(int line, string styles) { // MarginSetStyles
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(line, toCString(buff, styles));
	}

	/// Get the styles in the text margin for a line
	final @property int marginGetStyles(int line, out string styles) { // MarginGetStyles
		sendMessageDirect(line, 0);
		return sendMessageDirect(line, styles);
	}

	/// Clear the margin text on all lines
	final void marginTextClearAll() { // MarginTextClearAll
		sendMessageDirect(0, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	final @property void marginSetStyleOffset(int style) { // MarginSetStyleOffset
		sendMessageDirect(style, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	final @property int marginGetStyleOffset() { // MarginGetStyleOffset
		return sendMessageDirect(0, 0);
	}

	/// Set the margin options.
	final @property void marginOptions(int marginOptions) { // SetMarginOptions
		sendMessageDirect(marginOptions, 0);
	}

	/// Get the margin options.
	final @property int marginOptions() { // GetMarginOptions
		return sendMessageDirect(0, 0);
	}

	/// Set the annotation text for a line
	final @property void annotationSetText(int line, string text) { // AnnotationSetText
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(line, toCString(buff, text));
	}

	/// Get the annotation text for a line
	final @property int annotationGetText(int line, out string text) { // AnnotationGetText
		sendMessageDirect(line, 0);
		return sendMessageDirect(line, text);
	}

	/// Set the style number for the annotations for a line
	final @property void annotationSetStyle(int line, int style) { // AnnotationSetStyle
		sendMessageDirect(line, style);
	}

	/// Get the style number for the annotations for a line
	final @property int annotationGetStyle(int line) { // AnnotationGetStyle
		return sendMessageDirect(line, 0);
	}

	/// Set the annotation styles for a line
	final @property void annotationSetStyles(int line, string styles) { // AnnotationSetStyles
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(line, toCString(buff, styles));
	}

	/// Get the annotation styles for a line
	final @property int annotationGetStyles(int line, out string styles) { // AnnotationGetStyles
		sendMessageDirect(line, 0);
		return sendMessageDirect(line, styles);
	}

	/// Get the number of annotation lines for a line
	final @property int annotationGetLines(int line) { // AnnotationGetLines
		return sendMessageDirect(line, 0);
	}

	/// Clear the annotations from all lines
	final void annotationClearAll() { // AnnotationClearAll
		sendMessageDirect(0, 0);
	}

	/// Set the visibility for the annotations for a view
	final @property void annotationSetVisible(int visible) { // AnnotationSetVisible
		sendMessageDirect(visible, 0);
	}

	/// Get the visibility for the annotations for a view
	final @property int annotationGetVisible() { // AnnotationGetVisible
		return sendMessageDirect(0, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	final @property void annotationSetStyleOffset(int style) { // AnnotationSetStyleOffset
		sendMessageDirect(style, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	final @property int annotationGetStyleOffset() { // AnnotationGetStyleOffset
		return sendMessageDirect(0, 0);
	}

	/// Add a container action to the undo stack
	final void addUndoAction(int token, int flags) { // AddUndoAction
		sendMessageDirect(token, flags);
	}

	/// Find the position of a character from a point within the window.
	final Position charPositionFromPoint(int x, int y) { // CharPositionFromPoint
		return sendMessageDirect(x, y);
	}

	/// Return INVALID_POSITION if not close to text.
	final Position charPositionFromPointClose(int x, int y) { // CharPositionFromPointClose
		return sendMessageDirect(x, y);
	}

	/// Set whether multiple selections can be made
	final @property void multipleSelection(bool multipleSelection) { // SetMultipleSelection
		sendMessageDirect(multipleSelection, 0);
	}

	/// Whether multiple selections can be made
	final @property bool multipleSelection() { // GetMultipleSelection
		return !!sendMessageDirect(0, 0);
	}

	/// Set whether typing can be performed into multiple selections
	final @property void additionalSelectionTyping(bool additionalSelectionTyping) { // SetAdditionalSelectionTyping
		sendMessageDirect(additionalSelectionTyping, 0);
	}

	/// Whether typing can be performed into multiple selections
	final @property bool additionalSelectionTyping() { // GetAdditionalSelectionTyping
		return !!sendMessageDirect(0, 0);
	}

	/// Set whether additional carets will blink
	final @property void additionalCaretsBlink(bool additionalCaretsBlink) { // SetAdditionalCaretsBlink
		sendMessageDirect(additionalCaretsBlink, 0);
	}

	/// Whether additional carets will blink
	final @property bool additionalCaretsBlink() { // GetAdditionalCaretsBlink
		return !!sendMessageDirect(0, 0);
	}

	/// Set whether additional carets are visible
	final @property void additionalCaretsVisible(bool additionalCaretsBlink) { // SetAdditionalCaretsVisible
		sendMessageDirect(additionalCaretsBlink, 0);
	}

	/// Whether additional carets are visible
	final @property bool additionalCaretsVisible() { // GetAdditionalCaretsVisible
		return !!sendMessageDirect(0, 0);
	}

	/// How many selections are there?
	final @property int selections() { // GetSelections
		return sendMessageDirect(0, 0);
	}

	/// Clear selections to a single empty stream selection
	final void clearSelections() { // ClearSelections
		sendMessageDirect(0, 0);
	}

	/// Set a simple selection
	final int setSelection(int caret, int anchor) { // SetSelection
		return sendMessageDirect(caret, anchor);
	}

	/// Add a selection
	final int addSelection(int caret, int anchor) { // AddSelection
		return sendMessageDirect(caret, anchor);
	}

	/// Set the main selection
	final @property void mainSelection(int selection) { // SetMainSelection
		sendMessageDirect(selection, 0);
	}

	/// Which selection is the main selection
	final @property int mainSelection() { // GetMainSelection
		return sendMessageDirect(0, 0);
	}

	///
	final @property void selectionNCaret(int selection, Position pos) { // SetSelectionNCaret
		sendMessageDirect(selection, pos);
	}

	///
	final @property Position selectionNCaret(int selection) { // GetSelectionNCaret
		return sendMessageDirect(selection, 0);
	}

	///
	final @property void selectionNAnchor(int selection, Position posAnchor) { // SetSelectionNAnchor
		sendMessageDirect(selection, posAnchor);
	}

	///
	final @property Position selectionNAnchor(int selection) { // GetSelectionNAnchor
		return sendMessageDirect(selection, 0);
	}

	///
	final @property void selectionNCaretVirtualSpace(int selection, int space) { // SetSelectionNCaretVirtualSpace
		sendMessageDirect(selection, space);
	}

	///
	final @property int selectionNCaretVirtualSpace(int selection) { // GetSelectionNCaretVirtualSpace
		return sendMessageDirect(selection, 0);
	}

	///
	final @property void selectionNAnchorVirtualSpace(int selection, int space) { // SetSelectionNAnchorVirtualSpace
		sendMessageDirect(selection, space);
	}

	///
	final @property int selectionNAnchorVirtualSpace(int selection) { // GetSelectionNAnchorVirtualSpace
		return sendMessageDirect(selection, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	final @property void selectionNStart(int selection, Position pos) { // SetSelectionNStart
		sendMessageDirect(selection, pos);
	}

	/// Returns the position at the start of the selection.
	final @property Position selectionNStart(int selection) { // GetSelectionNStart
		return sendMessageDirect(selection, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	final @property void selectionNEnd(int selection, Position pos) { // SetSelectionNEnd
		sendMessageDirect(selection, pos);
	}

	/// Returns the position at the end of the selection.
	final @property Position selectionNEnd(int selection) { // GetSelectionNEnd
		return sendMessageDirect(selection, 0);
	}

	///
	final @property void rectangularSelectionCaret(Position pos) { // SetRectangularSelectionCaret
		sendMessageDirect(pos, 0);
	}

	///
	final @property Position rectangularSelectionCaret() { // GetRectangularSelectionCaret
		return sendMessageDirect(0, 0);
	}

	///
	final @property void rectangularSelectionAnchor(Position posAnchor) { // SetRectangularSelectionAnchor
		sendMessageDirect(posAnchor, 0);
	}

	///
	final @property Position rectangularSelectionAnchor() { // GetRectangularSelectionAnchor
		return sendMessageDirect(0, 0);
	}

	///
	final @property void rectangularSelectionCaretVirtualSpace(int space) { // SetRectangularSelectionCaretVirtualSpace
		sendMessageDirect(space, 0);
	}

	///
	final @property int rectangularSelectionCaretVirtualSpace() { // GetRectangularSelectionCaretVirtualSpace
		return sendMessageDirect(0, 0);
	}

	///
	final @property void rectangularSelectionAnchorVirtualSpace(int space) { // SetRectangularSelectionAnchorVirtualSpace
		sendMessageDirect(space, 0);
	}

	///
	final @property int rectangularSelectionAnchorVirtualSpace() { // GetRectangularSelectionAnchorVirtualSpace
		return sendMessageDirect(0, 0);
	}

	///
	final @property void virtualSpaceOptions(int virtualSpaceOptions) { // SetVirtualSpaceOptions
		sendMessageDirect(virtualSpaceOptions, 0);
	}

	///
	final @property int virtualSpaceOptions() { // GetVirtualSpaceOptions
		return sendMessageDirect(0, 0);
	}

	/// Valid values are SCMOD_CTRL(default), SCMOD_ALT, or SCMOD_SUPER.
	final @property void rectangularSelectionModifier(int modifier) { // SetRectangularSelectionModifier
		sendMessageDirect(modifier, 0);
	}

	/// Get the modifier key used for rectangular selection.
	final @property int rectangularSelectionModifier() { // GetRectangularSelectionModifier
		return sendMessageDirect(0, 0);
	}

	/// Must have previously called SetSelFore with non-zero first argument for this to have an effect.
	final @property void additionalSelFore(Colour fore) { // SetAdditionalSelFore
		sendMessageDirect(fore.asInt, 0);
	}

	/// Must have previously called SetSelBack with non-zero first argument for this to have an effect.
	final @property void additionalSelBack(Colour back) { // SetAdditionalSelBack
		sendMessageDirect(back.asInt, 0);
	}

	/// Set the alpha of the selection.
	final @property void additionalSelAlpha(int alpha) { // SetAdditionalSelAlpha
		sendMessageDirect(alpha, 0);
	}

	/// Get the alpha of the selection.
	final @property int additionalSelAlpha() { // GetAdditionalSelAlpha
		return sendMessageDirect(0, 0);
	}

	/// Set the foreground colour of additional carets.
	final @property void additionalCaretFore(Colour fore) { // SetAdditionalCaretFore
		sendMessageDirect(fore.asInt, 0);
	}

	/// Get the foreground colour of additional carets.
	final @property Colour additionalCaretFore() { // GetAdditionalCaretFore
		return cast(Colour)sendMessageDirect(0, 0);
	}

	/// Set the main selection to the next selection.
	final void rotateSelection() { // RotateSelection
		sendMessageDirect(0, 0);
	}

	/// Swap that caret and anchor of the main selection.
	final void swapMainAnchorCaret() { // SwapMainAnchorCaret
		sendMessageDirect(0, 0);
	}

	/// there may be a need to redraw.
	final int changeLexerState(Position start, Position end) { // ChangeLexerState
		return sendMessageDirect(start, end);
	}

	/// Return -1 when no more lines.
	final int contractedFoldNext(int lineStart) { // ContractedFoldNext
		return sendMessageDirect(lineStart, 0);
	}

	/// Centre current line in window.
	final void verticalCentreCaret() { // VerticalCentreCaret
		sendMessageDirect(0, 0);
	}

	/// Move the selected lines up one line, shifting the line above after the selection
	final void moveSelectedLinesUp() { // MoveSelectedLinesUp
		sendMessageDirect(0, 0);
	}

	/// Move the selected lines down one line, shifting the line below before the selection
	final void moveSelectedLinesDown() { // MoveSelectedLinesDown
		sendMessageDirect(0, 0);
	}

	/// Set the identifier reported as idFrom in notification messages.
	final @property void identifier(int identifier) { // SetIdentifier
		sendMessageDirect(identifier, 0);
	}

	/// Get the identifier.
	final @property int identifier() { // GetIdentifier
		return sendMessageDirect(0, 0);
	}

	/// Set the width for future RGBA image data.
	final @property void rGBAImageSetWidth(int width) { // RGBAImageSetWidth
		sendMessageDirect(width, 0);
	}

	/// Set the height for future RGBA image data.
	final @property void rGBAImageSetHeight(int height) { // RGBAImageSetHeight
		sendMessageDirect(height, 0);
	}

	/// It has the width and height from RGBAImageSetWidth/Height
	final void markerDefineRGBAImage(int markerNumber, string pixels) { // MarkerDefineRGBAImage
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(markerNumber, toCString(buff, pixels));
	}

	/// It has the width and height from RGBAImageSetWidth/Height
	final void registerRGBAImage(int type, string pixels) { // RegisterRGBAImage
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(type, toCString(buff, pixels));
	}

	/// Scroll to start of document.
	final void scrollToStart() { // ScrollToStart
		sendMessageDirect(0, 0);
	}

	/// Scroll to end of document.
	final void scrollToEnd() { // ScrollToEnd
		sendMessageDirect(0, 0);
	}

	/// Set the technolgy used.
	final @property void technology(int technology) { // SetTechnology
		sendMessageDirect(technology, 0);
	}

	/// Get the tech.
	final @property int technology() { // GetTechnology
		return sendMessageDirect(0, 0);
	}

	/// Create an ILoader*.
	final int createLoader(int bytes) { // CreateLoader
		return sendMessageDirect(bytes, 0);
	}

	/// Start notifying the container of all key presses and commands.
	final void startRecord() { // StartRecord
		sendMessageDirect(0, 0);
	}

	/// Stop notifying the container of all key presses and commands.
	final void stopRecord() { // StopRecord
		sendMessageDirect(0, 0);
	}

	/// Set the lexing language of the document.
	final @property void lexer(int lexer) { // SetLexer
		sendMessageDirect(lexer, 0);
	}

	/// Retrieve the lexing language of the document.
	final @property int lexer() { // GetLexer
		return sendMessageDirect(0, 0);
	}

	/// Colourise a segment of the document using the current lexing language.
	final void colourise(Position start, Position end) { // Colourise
		sendMessageDirect(start, end);
	}

	/// Set up a value that may be used by a lexer for some optional feature.
	final @property void property(string key, string value) { // SetProperty
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(toCString(buff, key), toCString(buff, value));
	}

	/// Set up the key words used by the lexer.
	final @property void keyWords(int keywordSet, string keyWords) { // SetKeyWords
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(keywordSet, toCString(buff, keyWords));
	}

	/// Set the lexing language of the document based on string name.
	final @property void lexerLanguage(string language) { // SetLexerLanguage
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, language));
	}

	/// Load a lexer library (dll / so).
	final void loadLexerLibrary(string path) { // LoadLexerLibrary
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(0, toCString(buff, path));
	}

	/// Retrieve a "property" value previously set with SetProperty.
	final int getProperty(string key, out string buf) { // GetProperty
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(toCString(buff, key), 0);
		return sendMessageDirect(toCString(buff, key), buf);
	}

	/// with "$()" variable replacement on returned buffer.
	final int getPropertyExpanded(string key, out string buf) { // GetPropertyExpanded
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(toCString(buff, key), 0);
		return sendMessageDirect(toCString(buff, key), buf);
	}

	/// interpreted as an int AFTER any "$()" variable replacement.
	final @property int propertyInt(string key) { // GetPropertyInt
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(toCString(buff, key), 0);
	}

	/// Retrieve the number of bits the current lexer needs for styling.
	final @property int styleBitsNeeded() { // GetStyleBitsNeeded
		return sendMessageDirect(0, 0);
	}

	/// Return the length of the text.
	final @property int lexerLanguage(out string text) { // GetLexerLanguage
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, text);
	}

	/// For private communication between an application and a known lexer.
	final int privateLexerCall(int operation, int pointer) { // PrivateLexerCall
		return sendMessageDirect(operation, pointer);
	}

	/// Retrieve a '\n' separated list of properties understood by the current lexer.
	final int propertyNames(out string names) { // PropertyNames
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, names);
	}

	/// Retrieve the type of a property.
	final int propertyType(string name) { // PropertyType
		char[4096] sbuff = void; char[] buff = sbuff;
		return sendMessageDirect(toCString(buff, name), 0);
	}

	/// Describe a property.
	final int describeProperty(string name, out string description) { // DescribeProperty
		char[4096] sbuff = void; char[] buff = sbuff;
		sendMessageDirect(toCString(buff, name), 0);
		return sendMessageDirect(toCString(buff, name), description);
	}

	/// Retrieve a '\n' separated list of descriptions of the keyword sets understood by the current lexer.
	final int describeKeyWordSets(out string descriptions) { // DescribeKeyWordSets
		sendMessageDirect(0, 0);
		return sendMessageDirect(0, descriptions);
	}
}
