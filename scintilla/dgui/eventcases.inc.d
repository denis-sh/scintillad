﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category

case 2000: // StyleNeeded
	scope e = new SciStyleNeededEventArgs();
	e.position = notification.position;
	this.onSciStyleNeeded(e);
	break;

case 2001: // CharAdded
	scope e = new SciCharAddedEventArgs();
	e.ch = cast(dchar)notification.ch;
	this.onSciCharAdded(e);
	break;

case 2002: // SavePointReached
	scope e = new SciSavePointReachedEventArgs();
	this.onSciSavePointReached(e);
	break;

case 2003: // SavePointLeft
	scope e = new SciSavePointLeftEventArgs();
	this.onSciSavePointLeft(e);
	break;

case 2004: // ModifyAttemptRO
	scope e = new SciModifyAttemptROEventArgs();
	this.onSciModifyAttemptRO(e);
	break;

case 2005: // Key
	scope e = new SciKeyEventArgs();
	e.ch = cast(dchar)notification.ch;
	e.modifiers = cast(KeyMod)notification.modifiers;
	this.onSciKey(e);
	break;

case 2006: // DoubleClick
	scope e = new SciDoubleClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	e.line = notification.line;
	this.onSciDoubleClick(e);
	break;

case 2007: // UpdateUI
	scope e = new SciUpdateUIEventArgs();
	e.updated = cast(Update)notification.updated;
	this.onSciUpdateUI(e);
	break;

case 2008: // Modified
	scope e = new SciModifiedEventArgs();
	e.position = notification.position;
	e.modificationType = cast(ModificationFlags)notification.modificationType;
	e.text = notification.text[0 .. notification.length];
	e.length = notification.length;
	e.linesAdded = notification.linesAdded;
	e.line = notification.line;
	e.foldLevelNow = notification.foldLevelNow;
	e.foldLevelPrev = notification.foldLevelPrev;
	e.token = notification.token;
	e.annotationLinesAdded = notification.annotationLinesAdded;
	this.onSciModified(e);
	break;

case 2009: // MacroRecord
	scope e = new SciMacroRecordEventArgs();
	e.message = notification.message;
	e.wParam = notification.wParam;
	e.lParam = notification.lParam;
	this.onSciMacroRecord(e);
	break;

case 2010: // MarginClick
	scope e = new SciMarginClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	e.margin = notification.margin;
	this.onSciMarginClick(e);
	break;

case 2011: // NeedShown
	scope e = new SciNeedShownEventArgs();
	e.position = notification.position;
	e.length = notification.length;
	this.onSciNeedShown(e);
	break;

case 2013: // Painted
	scope e = new SciPaintedEventArgs();
	this.onSciPainted(e);
	break;

case 2014: // UserListSelection
	scope e = new SciUserListSelectionEventArgs();
	e.listType = notification.listType;
	e.text = fromCString(notification.text);
	e.position = notification.position;
	this.onSciUserListSelection(e);
	break;

case 2015: // URIDropped
	scope e = new SciURIDroppedEventArgs();
	e.text = fromCString(notification.text);
	this.onSciURIDropped(e);
	break;

case 2016: // DwellStart
	scope e = new SciDwellStartEventArgs();
	e.position = notification.position;
	e.x = notification.x;
	e.y = notification.y;
	this.onSciDwellStart(e);
	break;

case 2017: // DwellEnd
	scope e = new SciDwellEndEventArgs();
	e.position = notification.position;
	e.x = notification.x;
	e.y = notification.y;
	this.onSciDwellEnd(e);
	break;

case 2018: // Zoom
	scope e = new SciZoomEventArgs();
	this.onSciZoom(e);
	break;

case 2019: // HotSpotClick
	scope e = new SciHotSpotClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	this.onSciHotSpotClick(e);
	break;

case 2020: // HotSpotDoubleClick
	scope e = new SciHotSpotDoubleClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	this.onSciHotSpotDoubleClick(e);
	break;

case 2021: // CallTipClick
	scope e = new SciCallTipClickEventArgs();
	e.position = notification.position;
	this.onSciCallTipClick(e);
	break;

case 2022: // AutoCSelection
	scope e = new SciAutoCSelectionEventArgs();
	e.text = fromCString(notification.text);
	e.position = notification.position;
	this.onSciAutoCSelection(e);
	break;

case 2023: // IndicatorClick
	scope e = new SciIndicatorClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	this.onSciIndicatorClick(e);
	break;

case 2024: // IndicatorRelease
	scope e = new SciIndicatorReleaseEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	this.onSciIndicatorRelease(e);
	break;

case 2025: // AutoCCancelled
	scope e = new SciAutoCCancelledEventArgs();
	this.onSciAutoCCancelled(e);
	break;

case 2026: // AutoCCharDeleted
	scope e = new SciAutoCCharDeletedEventArgs();
	this.onSciAutoCCharDeleted(e);
	break;

case 2027: // HotSpotReleaseClick
	scope e = new SciHotSpotReleaseClickEventArgs();
	e.modifiers = cast(KeyMod)notification.modifiers;
	e.position = notification.position;
	this.onSciHotSpotReleaseClick(e);
	break;
