﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category
module scintilla.dgui.eventargs;

import dgui.core.events.eventargs;
import scintilla.enums;

class SciStyleNeededEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciCharAddedEventArgs: EventArgs {

	private dchar ch_;
	@property dchar ch() const { return ch_; }
	@property dchar ch(dchar value) { return ch_ = value; }
}

class SciSavePointReachedEventArgs: EventArgs {
}

class SciSavePointLeftEventArgs: EventArgs {
}

class SciModifyAttemptROEventArgs: EventArgs {
}

class SciKeyEventArgs: EventArgs {

	private dchar ch_;
	@property dchar ch() const { return ch_; }
	@property dchar ch(dchar value) { return ch_ = value; }

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }
}

class SciDoubleClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private int line_;
	@property int line() const { return line_; }
	@property int line(int value) { return line_ = value; }
}

class SciUpdateUIEventArgs: EventArgs {

	private Update updated_;
	@property Update updated() const { return updated_; }
	@property Update updated(Update value) { return updated_ = value; }
}

class SciModifiedEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private ModificationFlags modificationType_;
	@property ModificationFlags modificationType() const { return modificationType_; }
	@property ModificationFlags modificationType(ModificationFlags value) { return modificationType_ = value; }

	private const(char)[] text_;
	@property const(char)[] text() const { return text_; }
	@property const(char)[] text(const(char)[] value) { return text_ = value; }

	private int length_;
	@property int length() const { return length_; }
	@property int length(int value) { return length_ = value; }

	private int linesAdded_;
	@property int linesAdded() const { return linesAdded_; }
	@property int linesAdded(int value) { return linesAdded_ = value; }

	private int line_;
	@property int line() const { return line_; }
	@property int line(int value) { return line_ = value; }

	private int foldLevelNow_;
	@property int foldLevelNow() const { return foldLevelNow_; }
	@property int foldLevelNow(int value) { return foldLevelNow_ = value; }

	private int foldLevelPrev_;
	@property int foldLevelPrev() const { return foldLevelPrev_; }
	@property int foldLevelPrev(int value) { return foldLevelPrev_ = value; }

	private int token_;
	@property int token() const { return token_; }
	@property int token(int value) { return token_ = value; }

	private int annotationLinesAdded_;
	@property int annotationLinesAdded() const { return annotationLinesAdded_; }
	@property int annotationLinesAdded(int value) { return annotationLinesAdded_ = value; }
}

class SciMacroRecordEventArgs: EventArgs {

	private int message_;
	@property int message() const { return message_; }
	@property int message(int value) { return message_ = value; }

	private int wParam_;
	@property int wParam() const { return wParam_; }
	@property int wParam(int value) { return wParam_ = value; }

	private int lParam_;
	@property int lParam() const { return lParam_; }
	@property int lParam(int value) { return lParam_ = value; }
}

class SciMarginClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private int margin_;
	@property int margin() const { return margin_; }
	@property int margin(int value) { return margin_ = value; }
}

class SciNeedShownEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private int length_;
	@property int length() const { return length_; }
	@property int length(int value) { return length_ = value; }
}

class SciPaintedEventArgs: EventArgs {
}

class SciUserListSelectionEventArgs: EventArgs {

	private int listType_;
	@property int listType() const { return listType_; }
	@property int listType(int value) { return listType_ = value; }

	private const(char)[] text_;
	@property const(char)[] text() const { return text_; }
	@property const(char)[] text(const(char)[] value) { return text_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciURIDroppedEventArgs: EventArgs {

	private const(char)[] text_;
	@property const(char)[] text() const { return text_; }
	@property const(char)[] text(const(char)[] value) { return text_ = value; }
}

class SciDwellStartEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private int x_;
	@property int x() const { return x_; }
	@property int x(int value) { return x_ = value; }

	private int y_;
	@property int y() const { return y_; }
	@property int y(int value) { return y_ = value; }
}

class SciDwellEndEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }

	private int x_;
	@property int x() const { return x_; }
	@property int x(int value) { return x_ = value; }

	private int y_;
	@property int y() const { return y_; }
	@property int y(int value) { return y_ = value; }
}

class SciZoomEventArgs: EventArgs {
}

class SciHotSpotClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciHotSpotDoubleClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciCallTipClickEventArgs: EventArgs {

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciAutoCSelectionEventArgs: EventArgs {

	private const(char)[] text_;
	@property const(char)[] text() const { return text_; }
	@property const(char)[] text(const(char)[] value) { return text_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciIndicatorClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciIndicatorReleaseEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}

class SciAutoCCancelledEventArgs: EventArgs {
}

class SciAutoCCharDeletedEventArgs: EventArgs {
}

class SciHotSpotReleaseClickEventArgs: EventArgs {

	private KeyMod modifiers_;
	@property KeyMod modifiers() const { return modifiers_; }
	@property KeyMod modifiers(KeyMod value) { return modifiers_ = value; }

	private int position_;
	@property int position() const { return position_; }
	@property int position(int value) { return position_ = value; }
}
