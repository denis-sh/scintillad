﻿module scintilla.dgui.control;

import scintilla.types;
import scintilla.enums;
import scintilla.lexer;
import scintilla.internal.functions;
import scintilla.iscintilla;
import scintilla.dgui.eventargs;

debug import std.stdio;
import std.conv;
import core.stdc.wchar_;
import std.string : toStringz, join;
import std.utf : toUTF16;
import dgui.all;

enum
{
	SCINTILLA  = "Scintilla",
	DSCINTILLA = "DScintilla",
}

alias Lexer ScintillaLanguage;

alias uint ScintillaProperty;

struct KeywordDefinition
{
	int KeywordSet;
	string[] Keywords;
	Color ForeColor;
	Color BackColor;
	string FontName;
	int FontHeight;
	FontStyle KeywordStyle = FontStyle.NORMAL;

	public static KeywordDefinition opCall(int kwSet, string[] kw, Color fc, Color bc, string fn, int fh, FontStyle fs)
	{
		KeywordDefinition kd;
		kd.KeywordSet = kwSet;
		kd.Keywords = kw;
		kd.ForeColor = fc;
		kd.BackColor = bc;
		kd.FontName = fn;
		kd.FontHeight = fh;
		kd.KeywordStyle = fs;

		return kd;
	}
}

struct StyleDefinition
{
	ScintillaProperty Property;
	Color ForeColor;
	Color BackColor;
	string FontName;
	int FontHeight;
	FontStyle Style = FontStyle.NORMAL;

	public static StyleDefinition opCall(ScintillaProperty sp, Color fc, Color bc, string fn, int fh, FontStyle fs)
	{
		StyleDefinition sd;
		sd.Property = sp;
		sd.ForeColor = fc;
		sd.BackColor = bc;
		sd.FontName = fn;
		sd.FontHeight = fh;
		sd.Style = fs;

		return sd;
	}
}

class LanguageDefinition
{
	private ScintillaLanguage _language;
	private Collection!(KeywordDefinition) _kd;
	private Collection!(StyleDefinition) _sd;

	public this()
	{
		this._kd = new Collection!(KeywordDefinition)();
		this._sd = new Collection!(StyleDefinition)();
	}

	@property public void language(ScintillaLanguage sl)
	{
		this._language = sl;
	}

	@property public ScintillaLanguage language()
	{
		return this._language;
	}

	@property public KeywordDefinition[] keywords()
	{
		return this._kd.get();
	}

	@property public StyleDefinition[] styles()
	{
		return this._sd.get();
	}

	public void addKeywordDefinition(int kwSet, string[] kw, Color fc)
	{
		this.addKeywordDefinition(kwSet, kw, fc, Color.invalid, null);
	}

	public void addKeywordDefinition(int kwSet, string[] kw, Color fc, Color bc)
	{
		this.addKeywordDefinition(kwSet, kw, fc, bc, null);
	}

	public void addKeywordDefinition(int kwSet, string[] kw, Color fc, string fn, int fh = 0, FontStyle fs = FontStyle.NORMAL)
	{
		KeywordDefinition kd = KeywordDefinition(kwSet, kw, fc, Color.invalid, fn, fh, fs);
		this._kd.add(kd);
	}

	public void addKeywordDefinition(int kwSet, string[] kw, Color fc, Color bc, string fn, int fh = 0, FontStyle fs = FontStyle.NORMAL)
	{
		KeywordDefinition kd = KeywordDefinition(kwSet, kw, fc, bc, fn, fh, fs);
		this._kd.add(kd);
	}

	public void addStyleDefinition(ScintillaProperty sp, Color fc)
	{
		this.addStyleDefinition(sp, fc, Color.invalid, null);
	}

	public void addStyleDefinition(ScintillaProperty sp, Color fc, Color bc)
	{
		this.addStyleDefinition(sp, fc, bc, null);
	}

	public void addStyleDefinition(ScintillaProperty sp, Color fc, string fn, int fh = 0, FontStyle fs = FontStyle.NORMAL)
	{
		this.addStyleDefinition(sp, fc, Color.invalid, fn, fh, fs);
	}

	public void addStyleDefinition(ScintillaProperty sp, Color fc, Color bc, string fn, int fh = 0, FontStyle fs = FontStyle.NORMAL)
	{
		StyleDefinition sd = StyleDefinition(sp, fc, bc, fn, fh, fs);
		this._sd.add(sd);
	}
}

class ScintillaControl: TextControl, IScintilla
{
	private struct LineNumbersStyle
	{
		string FontName;
		int FontHeight;
		Color ForeColor;
		Color BackColor;
		FontStyle NumbersStyle;
	}

	private static string _dllName;
	private static string _dllName2;
	private static uint _refCount;
	private static HMODULE _hScintillaDLL;

	private bool _lineNumbers = false;
	private LineNumbersStyle _lns;
	private LanguageDefinition _ld;

	mixin(import("eventmembers.inc.d"));

	public this(string dllName = "")
	{
		if(!_hScintillaDLL)
		{
			_dllName = !dllName.length ? "Scintilla.dll" : dllName;
		}
	}

	public override void dispose()
	{
		--_refCount;

		if(!_refCount)
		{
			FreeLibrary(_hScintillaDLL);
			_hScintillaDLL = null;
		}

		super.dispose();
	}

	sptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam)
	{
		return this.sendMessage(msg, wParam, lParam);
	}

	/+private void updateLanguageDefinition()
	{
		this.sendMessage(SCI_SETLEXER,  this._ld.language, 0);

		foreach(int i, KeywordDefinition kd; this._ld.keywords)
		{
			this.sendMessage(SCI_SETKEYWORDS, i, cast(LPARAM)toStringz(join(kd.Keywords, " ")));

			if(kd.FontName.length)
			{
				this.sendMessage(SCI_STYLESETFONT, kd.KeywordSet, cast(LPARAM)toStringz(kd.FontName));

				if(kd.FontHeight)
				{
					this.sendMessage(SCI_STYLESETSIZE, kd.KeywordSet, kd.FontHeight);
				}

				if(kd.KeywordStyle !is FontStyle.NORMAL)
				{
					if(kd.KeywordStyle & FontStyle.BOLD)
					{
						this.sendMessage(SCI_STYLESETBOLD, kd.KeywordSet, true);
					}

					if(kd.KeywordStyle & FontStyle.ITALIC)
					{
						this.sendMessage(SCI_STYLESETITALIC, kd.KeywordSet, true);
					}

					if(kd.KeywordStyle & FontStyle.UNDERLINE)
					{
						this.sendMessage(SCI_STYLESETUNDERLINE, kd.KeywordSet, true);
					}
				}
			}

			if(kd.ForeColor.valid)
			{
				this.sendMessage(SCI_STYLESETFORE, ScintillaProperty.D_WORD, kd.ForeColor.colorref);
			}

			if(kd.BackColor.valid)
			{
				this.sendMessage(SCI_STYLESETBACK, kd.KeywordSet, kd.BackColor.colorref);
			}
		}

		foreach(StyleDefinition sd; this._ld.styles)
		{
			if(sd.FontName.length)
			{
				this.sendMessage(SCI_STYLESETFONT, sd.Property, cast(LPARAM)toStringz(sd.FontName));

				if(sd.FontHeight)
				{
					this.sendMessage(SCI_STYLESETSIZE, sd.Property, sd.FontHeight);
				}

				if(sd.Style !is FontStyle.NORMAL)
				{
					if(sd.Style & FontStyle.BOLD)
					{
						this.sendMessage(SCI_STYLESETBOLD, sd.Property, true);
					}

					if(sd.Style & FontStyle.ITALIC)
					{
						this.sendMessage(SCI_STYLESETITALIC, sd.Property, true);
					}

					if(sd.Style & FontStyle.UNDERLINE)
					{
						this.sendMessage(SCI_STYLESETUNDERLINE, sd.Property, true);
					}
				}
			}

			if(sd.ForeColor.valid)
			{
				this.sendMessage(SCI_STYLESETFORE, sd.Property, sd.ForeColor.colorref);
			}

			if(sd.BackColor.valid)
			{
				this.sendMessage(SCI_STYLESETBACK, sd.Property, sd.BackColor.colorref);
			}
		}
	}

	private void updateLineNumbersStyle()
	{
		if(this._lns.FontName.length)
		{
			this.sendMessage(SCI_STYLESETFONT, STYLE_LINENUMBER, cast(LPARAM)toStringz(this._lns.FontName));
		}

		if(this._lns.FontHeight)
		{
			this.sendMessage(SCI_STYLESETSIZE, STYLE_LINENUMBER, this._lns.FontHeight);
		}

		if(this._lns.NumbersStyle !is FontStyle.NORMAL)
		{
			if(this._lns.NumbersStyle & FontStyle.BOLD)
			{
				this.sendMessage(SCI_STYLESETBOLD, STYLE_LINENUMBER, true);
			}

			if(this._lns.NumbersStyle & FontStyle.ITALIC)
			{
				this.sendMessage(SCI_STYLESETITALIC, STYLE_LINENUMBER, true);
			}

			if(this._lns.NumbersStyle & FontStyle.UNDERLINE)
			{
				this.sendMessage(SCI_STYLESETUNDERLINE, STYLE_LINENUMBER, true);
			}
		}

		if(this._lns.ForeColor.valid)
		{
			this.sendMessage(SCI_STYLESETFORE, STYLE_LINENUMBER, this._lns.ForeColor.colorref);
		}

		if(this._lns.BackColor.valid)
		{
			this.sendMessage(SCI_STYLESETBACK, STYLE_LINENUMBER, this._lns.BackColor.colorref);
		}
	}

	@property public void lineNumbers(bool b)
	{
		this._lineNumbers = b;

		if(this.created)
		{
			if(b)
			{
				int marginSize = this.sendMessage(SCI_TEXTWIDTH, STYLE_LINENUMBER, cast(LPARAM)toStringz("_99999"));
				this.sendMessage(SCI_SETMARGINWIDTHN, 0, marginSize);
			}
			else
			{
				this.sendMessage(SCI_SETMARGINWIDTHN, 0, 0);
			}
		}
	}

	public void setLineNumbersStyle(FontStyle fs, Color fc)
	{
		this.setLineNumbersStyle(null, 0, fs, fc, Color.invalid);
	}

	public void setLineNumbersStyle(string font, int h, FontStyle fs, Color fc, Color bc)
	{
		this._lns.FontName = font;
		this._lns.FontHeight = h;
		this._lns.NumbersStyle = fs;
		this._lns.ForeColor = fc;
		this._lns.BackColor = bc;

		if(this.created)
		{
			this.updateLineNumbersStyle();
		}
	}

	@property public LanguageDefinition languageDefinition()
	{
		return this._ld;
	}

	@property public void languageDefinition(LanguageDefinition ld)
	{
		this._ld = ld;

		if(this.created)
		{
			this.updateLanguageDefinition();
		}
	}

	public void setProperty(string prop, string val)
	{
		this.sendMessage(SCI_SETPROPERTY, cast(WPARAM)toStringz(prop), cast(LPARAM)toStringz(val));
	}

	public string getProperty(string prop)
	{
		char[] res;
		int len = this.sendMessage(SCI_GETPROPERTY, cast(WPARAM)toStringz(prop), 0);

		if(len)
		{
			res = new char[len + 1];
			this.sendMessage(SCI_GETPROPERTY, cast(WPARAM)toStringz(prop), cast(LPARAM)res.ptr);
			return to!(string)(res.ptr);
		}

		return null;
	}+/

	protected override void createControlParams(ref CreateControlParams ccp)
	{
		++_refCount;

		if(!_hScintillaDLL)
		{
			_hScintillaDLL = loadLibrary(_dllName);

			if(!_hScintillaDLL)
			{
				throwException!(DGuiException)("Scintilla DLL not found");
			}
		}

		/* For Scintilla, WM_PAINT's wParam is not a Device Context but a
		   PAINTSTRUCT struct pointer (PAINTSTRUCT*).
		   *** Removing ORIGINAL_PAINT bit can cause a lot of issues ***

		   From Scintilla's source code (in WM_PAINT):
		   bool IsOcxCtrl = (wParam != 0); // if wParam != 0, it contains
								   		   // a PAINSTRUCT* from the OCX
		*/

		//Scintilla.setBit(this._cBits, ControlBits.ORIGINAL_PAINT, true);

		ccp.SuperclassName = SCINTILLA;
		ccp.ClassName = DSCINTILLA;

		super.createControlParams(ccp);
	}

	protected override void onHandleCreated(EventArgs e)
	{
/+
01234567890
	x
		y
01234567890
+/

		this.styleSetFont(StylesCommon.default_, this.font.name);
		this.styleSetSize(StylesCommon.default_, this.font.height);

		/+this.sendMessage(SCI_STYLECLEARALL, 0, 0);

		if(this._ld)
		{
			this.updateLanguageDefinition();
		}

		if(this._lineNumbers)
		{
			int marginSize = this.sendMessage(SCI_TEXTWIDTH, STYLE_LINENUMBER, cast(LPARAM)toStringz("_99999"));
			this.sendMessage(SCI_SETMARGINWIDTHN, 0, marginSize);

			this.updateLineNumbersStyle();
		}+/

		super.onHandleCreated(e);
	}

	protected override void onReflectedMessage(ref Message m)
	{
		switch(m.Msg)
		{
			case WM_NOTIFY:
				auto notification = cast(SCNotification*) m.lParam;
				switch(notification.nmhdr.code)
				{
					mixin(import("eventcases.inc.d"));
					default:
						debug writeln("Unknown event: ", notification.nmhdr.code);
						break;
				}
				break;

			default:
				break;
		}

		super.onReflectedMessage(m);
	}

	protected override void wndProc(ref Message m)
	{
		switch(m.Msg)
		{
			/+case EM_REPLACESEL:
				Message rm = Message(this._handle, SCI_REPLACESEL, m.wParam, cast(LPARAM)unicodeToAnsiPtr(cast(wchar*)m.lParam));
				m.Result = this.originalWndProc(rm);
				break;

			case WM_GETTEXT:
				char[] buffer = new char[m.wParam + 1];
				Message rm = Message(this._handle, SCI_GETTEXT, m.wParam, cast(LPARAM)buffer.ptr);
				m.Result = this.originalWndProc(rm);
				core.stdc.wchar_.wcscpy(cast(wchar*)m.lParam, toUTFz!(wchar*)(to!(string)(buffer.ptr)));
				break;

			case WM_SETTEXT:
				Message rm = Message(this._handle, SCI_SETTEXT, m.wParam, cast(LPARAM)unicodeToAnsiPtr(cast(wchar*)m.lParam));
				m.Result = this.originalWndProc(rm);
				break;

			case EM_UNDO:
				Message rm = Message(this._handle, SCI_UNDO, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;

			case WM_CUT:
				Message rm = Message(this._handle, SCI_CUT, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;

			case WM_COPY:
				Message rm = Message(this._handle, SCI_COPY, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;

			case WM_PASTE:
				Message rm = Message(this._handle, SCI_PASTE, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;

			case EM_GETMODIFY:
				Message rm = Message(this._handle, SCI_GETMODIFY, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;

			case EM_SETMODIFY:
				Message rm = Message(this._handle, SCI_SETSAVEPOINT, m.wParam, m.lParam);
				m.Result = this.originalWndProc(rm);
				break;+/

			default:
				super.wndProc(m);
				break;
		}
	}
}
