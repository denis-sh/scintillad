﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category
public Event!(ScintillaControl, SciStyleNeededEventArgs) sciStyleNeeded;
public Event!(ScintillaControl, SciCharAddedEventArgs) sciCharAdded;
public Event!(ScintillaControl, SciSavePointReachedEventArgs) sciSavePointReached;
public Event!(ScintillaControl, SciSavePointLeftEventArgs) sciSavePointLeft;
public Event!(ScintillaControl, SciModifyAttemptROEventArgs) sciModifyAttemptRO;
public Event!(ScintillaControl, SciKeyEventArgs) sciKey;
public Event!(ScintillaControl, SciDoubleClickEventArgs) sciDoubleClick;
public Event!(ScintillaControl, SciUpdateUIEventArgs) sciUpdateUI;
public Event!(ScintillaControl, SciModifiedEventArgs) sciModified;
public Event!(ScintillaControl, SciMacroRecordEventArgs) sciMacroRecord;
public Event!(ScintillaControl, SciMarginClickEventArgs) sciMarginClick;
public Event!(ScintillaControl, SciNeedShownEventArgs) sciNeedShown;
public Event!(ScintillaControl, SciPaintedEventArgs) sciPainted;
public Event!(ScintillaControl, SciUserListSelectionEventArgs) sciUserListSelection;
public Event!(ScintillaControl, SciURIDroppedEventArgs) sciURIDropped;
public Event!(ScintillaControl, SciDwellStartEventArgs) sciDwellStart;
public Event!(ScintillaControl, SciDwellEndEventArgs) sciDwellEnd;
public Event!(ScintillaControl, SciZoomEventArgs) sciZoom;
public Event!(ScintillaControl, SciHotSpotClickEventArgs) sciHotSpotClick;
public Event!(ScintillaControl, SciHotSpotDoubleClickEventArgs) sciHotSpotDoubleClick;
public Event!(ScintillaControl, SciCallTipClickEventArgs) sciCallTipClick;
public Event!(ScintillaControl, SciAutoCSelectionEventArgs) sciAutoCSelection;
public Event!(ScintillaControl, SciIndicatorClickEventArgs) sciIndicatorClick;
public Event!(ScintillaControl, SciIndicatorReleaseEventArgs) sciIndicatorRelease;
public Event!(ScintillaControl, SciAutoCCancelledEventArgs) sciAutoCCancelled;
public Event!(ScintillaControl, SciAutoCCharDeletedEventArgs) sciAutoCCharDeleted;
public Event!(ScintillaControl, SciHotSpotReleaseClickEventArgs) sciHotSpotReleaseClick;

protected void onSciStyleNeeded(SciStyleNeededEventArgs e) { this.sciStyleNeeded(this, e); }
protected void onSciCharAdded(SciCharAddedEventArgs e) { this.sciCharAdded(this, e); }
protected void onSciSavePointReached(SciSavePointReachedEventArgs e) { this.sciSavePointReached(this, e); }
protected void onSciSavePointLeft(SciSavePointLeftEventArgs e) { this.sciSavePointLeft(this, e); }
protected void onSciModifyAttemptRO(SciModifyAttemptROEventArgs e) { this.sciModifyAttemptRO(this, e); }
protected void onSciKey(SciKeyEventArgs e) { this.sciKey(this, e); }
protected void onSciDoubleClick(SciDoubleClickEventArgs e) { this.sciDoubleClick(this, e); }
protected void onSciUpdateUI(SciUpdateUIEventArgs e) { this.sciUpdateUI(this, e); }
protected void onSciModified(SciModifiedEventArgs e) { this.sciModified(this, e); }
protected void onSciMacroRecord(SciMacroRecordEventArgs e) { this.sciMacroRecord(this, e); }
protected void onSciMarginClick(SciMarginClickEventArgs e) { this.sciMarginClick(this, e); }
protected void onSciNeedShown(SciNeedShownEventArgs e) { this.sciNeedShown(this, e); }
protected void onSciPainted(SciPaintedEventArgs e) { this.sciPainted(this, e); }
protected void onSciUserListSelection(SciUserListSelectionEventArgs e) { this.sciUserListSelection(this, e); }
protected void onSciURIDropped(SciURIDroppedEventArgs e) { this.sciURIDropped(this, e); }
protected void onSciDwellStart(SciDwellStartEventArgs e) { this.sciDwellStart(this, e); }
protected void onSciDwellEnd(SciDwellEndEventArgs e) { this.sciDwellEnd(this, e); }
protected void onSciZoom(SciZoomEventArgs e) { this.sciZoom(this, e); }
protected void onSciHotSpotClick(SciHotSpotClickEventArgs e) { this.sciHotSpotClick(this, e); }
protected void onSciHotSpotDoubleClick(SciHotSpotDoubleClickEventArgs e) { this.sciHotSpotDoubleClick(this, e); }
protected void onSciCallTipClick(SciCallTipClickEventArgs e) { this.sciCallTipClick(this, e); }
protected void onSciAutoCSelection(SciAutoCSelectionEventArgs e) { this.sciAutoCSelection(this, e); }
protected void onSciIndicatorClick(SciIndicatorClickEventArgs e) { this.sciIndicatorClick(this, e); }
protected void onSciIndicatorRelease(SciIndicatorReleaseEventArgs e) { this.sciIndicatorRelease(this, e); }
protected void onSciAutoCCancelled(SciAutoCCancelledEventArgs e) { this.sciAutoCCancelled(this, e); }
protected void onSciAutoCCharDeleted(SciAutoCCharDeletedEventArgs e) { this.sciAutoCCharDeleted(this, e); }
protected void onSciHotSpotReleaseClick(SciHotSpotReleaseClickEventArgs e) { this.sciHotSpotReleaseClick(this, e); }
