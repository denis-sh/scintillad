﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category
module scintilla.lexer;


// Prefix: SCLEX_
enum Lexer {
  container = 0, //SCLEX_:CONTAINER
  null_ = 1, //SCLEX_:NULL
  python = 2, //SCLEX_:PYTHON
  cpp = 3, //SCLEX_:CPP
  html = 4, //SCLEX_:HTML
  xml = 5, //SCLEX_:XML
  perl = 6, //SCLEX_:PERL
  sql = 7, //SCLEX_:SQL
  vb = 8, //SCLEX_:VB
  properties = 9, //SCLEX_:PROPERTIES
  errorlist = 10, //SCLEX_:ERRORLIST
  makefile = 11, //SCLEX_:MAKEFILE
  batch = 12, //SCLEX_:BATCH
  xcode = 13, //SCLEX_:XCODE
  latex = 14, //SCLEX_:LATEX
  lua = 15, //SCLEX_:LUA
  diff = 16, //SCLEX_:DIFF
  conf = 17, //SCLEX_:CONF
  pascal = 18, //SCLEX_:PASCAL
  ave = 19, //SCLEX_:AVE
  ada = 20, //SCLEX_:ADA
  lisp = 21, //SCLEX_:LISP
  ruby = 22, //SCLEX_:RUBY
  eiffel = 23, //SCLEX_:EIFFEL
  eiffelkw = 24, //SCLEX_:EIFFELKW
  tcl = 25, //SCLEX_:TCL
  nncrontab = 26, //SCLEX_:NNCRONTAB
  bullant = 27, //SCLEX_:BULLANT
  vbscript = 28, //SCLEX_:VBSCRIPT
  baan = 31, //SCLEX_:BAAN
  matlab = 32, //SCLEX_:MATLAB
  scriptol = 33, //SCLEX_:SCRIPTOL
  asm_ = 34, //SCLEX_:ASM
  cppnocase = 35, //SCLEX_:CPPNOCASE
  fortran = 36, //SCLEX_:FORTRAN
  f77 = 37, //SCLEX_:F77
  css = 38, //SCLEX_:CSS
  pov = 39, //SCLEX_:POV
  lout = 40, //SCLEX_:LOUT
  escript = 41, //SCLEX_:ESCRIPT
  ps = 42, //SCLEX_:PS
  nsis = 43, //SCLEX_:NSIS
  mmixal = 44, //SCLEX_:MMIXAL
  clw = 45, //SCLEX_:CLW
  clwnocase = 46, //SCLEX_:CLWNOCASE
  lot = 47, //SCLEX_:LOT
  yaml = 48, //SCLEX_:YAML
  tex = 49, //SCLEX_:TEX
  metapost = 50, //SCLEX_:METAPOST
  powerbasic = 51, //SCLEX_:POWERBASIC
  forth = 52, //SCLEX_:FORTH
  erlang = 53, //SCLEX_:ERLANG
  octave = 54, //SCLEX_:OCTAVE
  mssql = 55, //SCLEX_:MSSQL
  verilog = 56, //SCLEX_:VERILOG
  kix = 57, //SCLEX_:KIX
  gui4cli = 58, //SCLEX_:GUI4CLI
  specman = 59, //SCLEX_:SPECMAN
  au3 = 60, //SCLEX_:AU3
  apdl = 61, //SCLEX_:APDL
  bash = 62, //SCLEX_:BASH
  asn1 = 63, //SCLEX_:ASN1
  vhdl = 64, //SCLEX_:VHDL
  caml = 65, //SCLEX_:CAML
  blitzbasic = 66, //SCLEX_:BLITZBASIC
  purebasic = 67, //SCLEX_:PUREBASIC
  haskell = 68, //SCLEX_:HASKELL
  phpscript = 69, //SCLEX_:PHPSCRIPT
  tads3 = 70, //SCLEX_:TADS3
  rebol = 71, //SCLEX_:REBOL
  smalltalk = 72, //SCLEX_:SMALLTALK
  flagship = 73, //SCLEX_:FLAGSHIP
  csound = 74, //SCLEX_:CSOUND
  freebasic = 75, //SCLEX_:FREEBASIC
  innosetup = 76, //SCLEX_:INNOSETUP
  opal = 77, //SCLEX_:OPAL
  spice = 78, //SCLEX_:SPICE
  d = 79, //SCLEX_:D
  cmake = 80, //SCLEX_:CMAKE
  gap = 81, //SCLEX_:GAP
  plm = 82, //SCLEX_:PLM
  progress = 83, //SCLEX_:PROGRESS
  abaqus = 84, //SCLEX_:ABAQUS
  asymptote = 85, //SCLEX_:ASYMPTOTE
  r = 86, //SCLEX_:R
  magik = 87, //SCLEX_:MAGIK
  powershell = 88, //SCLEX_:POWERSHELL
  mysql = 89, //SCLEX_:MYSQL
  po = 90, //SCLEX_:PO
  tal = 91, //SCLEX_:TAL
  cobol = 92, //SCLEX_:COBOL
  tacl = 93, //SCLEX_:TACL
  sorcus = 94, //SCLEX_:SORCUS
  powerpro = 95, //SCLEX_:POWERPRO
  nimrod = 96, //SCLEX_:NIMROD
  sml = 97, //SCLEX_:SML
  markdown = 98, //SCLEX_:MARKDOWN
  txt2tags = 99, //SCLEX_:TXT2TAGS
  a68k = 100, //SCLEX_:A68K
  modula = 101, //SCLEX_:MODULA
  coffeescript = 102, //SCLEX_:COFFEESCRIPT
  tcmd = 103, //SCLEX_:TCMD
  avs = 104, //SCLEX_:AVS
  ecl = 105, //SCLEX_:ECL
  automatic = 1000, //SCLEX_:AUTOMATIC
}

// Prefix: SCE_P_
enum Python {
  lexerVal = Lexer.python, // SCLEX_PYTHON
  default_ = 0, //SCE_P_:DEFAULT
  commentline = 1, //SCE_P_:COMMENTLINE
  number = 2, //SCE_P_:NUMBER
  string = 3, //SCE_P_:STRING
  character = 4, //SCE_P_:CHARACTER
  word = 5, //SCE_P_:WORD
  triple = 6, //SCE_P_:TRIPLE
  tripledouble = 7, //SCE_P_:TRIPLEDOUBLE
  classname = 8, //SCE_P_:CLASSNAME
  defname = 9, //SCE_P_:DEFNAME
  operator = 10, //SCE_P_:OPERATOR
  identifier = 11, //SCE_P_:IDENTIFIER
  commentblock = 12, //SCE_P_:COMMENTBLOCK
  stringeol = 13, //SCE_P_:STRINGEOL
  word2 = 14, //SCE_P_:WORD2
  decorator = 15, //SCE_P_:DECORATOR
}

// Prefix: SCE_P_
enum Nimrod {
  lexerVal = Lexer.nimrod, // SCLEX_NIMROD
  default_ = 0, //SCE_P_:DEFAULT
  commentline = 1, //SCE_P_:COMMENTLINE
  number = 2, //SCE_P_:NUMBER
  string = 3, //SCE_P_:STRING
  character = 4, //SCE_P_:CHARACTER
  word = 5, //SCE_P_:WORD
  triple = 6, //SCE_P_:TRIPLE
  tripledouble = 7, //SCE_P_:TRIPLEDOUBLE
  classname = 8, //SCE_P_:CLASSNAME
  defname = 9, //SCE_P_:DEFNAME
  operator = 10, //SCE_P_:OPERATOR
  identifier = 11, //SCE_P_:IDENTIFIER
  commentblock = 12, //SCE_P_:COMMENTBLOCK
  stringeol = 13, //SCE_P_:STRINGEOL
  word2 = 14, //SCE_P_:WORD2
  decorator = 15, //SCE_P_:DECORATOR
}

// Prefix: SCE_C_
enum Cpp {
  lexerVal = Lexer.cpp, // SCLEX_CPP
  default_ = 0, //SCE_C_:DEFAULT
  comment = 1, //SCE_C_:COMMENT
  commentline = 2, //SCE_C_:COMMENTLINE
  commentdoc = 3, //SCE_C_:COMMENTDOC
  number = 4, //SCE_C_:NUMBER
  word = 5, //SCE_C_:WORD
  string = 6, //SCE_C_:STRING
  character = 7, //SCE_C_:CHARACTER
  uuid = 8, //SCE_C_:UUID
  preprocessor = 9, //SCE_C_:PREPROCESSOR
  operator = 10, //SCE_C_:OPERATOR
  identifier = 11, //SCE_C_:IDENTIFIER
  stringeol = 12, //SCE_C_:STRINGEOL
  verbatim = 13, //SCE_C_:VERBATIM
  regex = 14, //SCE_C_:REGEX
  commentlinedoc = 15, //SCE_C_:COMMENTLINEDOC
  word2 = 16, //SCE_C_:WORD2
  commentdockeyword = 17, //SCE_C_:COMMENTDOCKEYWORD
  commentdockeyworderror = 18, //SCE_C_:COMMENTDOCKEYWORDERROR
  globalclass = 19, //SCE_C_:GLOBALCLASS
  stringraw = 20, //SCE_C_:STRINGRAW
  tripleverbatim = 21, //SCE_C_:TRIPLEVERBATIM
  hashquotedstring = 22, //SCE_C_:HASHQUOTEDSTRING
}

// Prefix: SCE_C_
enum BullAnt {
  lexerVal = Lexer.bullant, // SCLEX_BULLANT
  default_ = 0, //SCE_C_:DEFAULT
  comment = 1, //SCE_C_:COMMENT
  commentline = 2, //SCE_C_:COMMENTLINE
  commentdoc = 3, //SCE_C_:COMMENTDOC
  number = 4, //SCE_C_:NUMBER
  word = 5, //SCE_C_:WORD
  string = 6, //SCE_C_:STRING
  character = 7, //SCE_C_:CHARACTER
  uuid = 8, //SCE_C_:UUID
  preprocessor = 9, //SCE_C_:PREPROCESSOR
  operator = 10, //SCE_C_:OPERATOR
  identifier = 11, //SCE_C_:IDENTIFIER
  stringeol = 12, //SCE_C_:STRINGEOL
  verbatim = 13, //SCE_C_:VERBATIM
  regex = 14, //SCE_C_:REGEX
  commentlinedoc = 15, //SCE_C_:COMMENTLINEDOC
  word2 = 16, //SCE_C_:WORD2
  commentdockeyword = 17, //SCE_C_:COMMENTDOCKEYWORD
  commentdockeyworderror = 18, //SCE_C_:COMMENTDOCKEYWORDERROR
  globalclass = 19, //SCE_C_:GLOBALCLASS
  stringraw = 20, //SCE_C_:STRINGRAW
  tripleverbatim = 21, //SCE_C_:TRIPLEVERBATIM
  hashquotedstring = 22, //SCE_C_:HASHQUOTEDSTRING
}

// Prefix: SCE_D_
enum D {
  lexerVal = Lexer.d, // SCLEX_D
  default_ = 0, //SCE_D_:DEFAULT
  comment = 1, //SCE_D_:COMMENT
  commentline = 2, //SCE_D_:COMMENTLINE
  commentdoc = 3, //SCE_D_:COMMENTDOC
  commentnested = 4, //SCE_D_:COMMENTNESTED
  number = 5, //SCE_D_:NUMBER
  word = 6, //SCE_D_:WORD
  word2 = 7, //SCE_D_:WORD2
  word3 = 8, //SCE_D_:WORD3
  typedef_ = 9, //SCE_D_:TYPEDEF
  string = 10, //SCE_D_:STRING
  stringeol = 11, //SCE_D_:STRINGEOL
  character = 12, //SCE_D_:CHARACTER
  operator = 13, //SCE_D_:OPERATOR
  identifier = 14, //SCE_D_:IDENTIFIER
  commentlinedoc = 15, //SCE_D_:COMMENTLINEDOC
  commentdockeyword = 16, //SCE_D_:COMMENTDOCKEYWORD
  commentdockeyworderror = 17, //SCE_D_:COMMENTDOCKEYWORDERROR
  stringb = 18, //SCE_D_:STRINGB
  stringr = 19, //SCE_D_:STRINGR
  word5 = 20, //SCE_D_:WORD5
  word6 = 21, //SCE_D_:WORD6
  word7 = 22, //SCE_D_:WORD7
}

// Prefix: SCE_TCL_
enum TCL {
  lexerVal = Lexer.tcl, // SCLEX_TCL
  default_ = 0, //SCE_TCL_:DEFAULT
  comment = 1, //SCE_TCL_:COMMENT
  commentline = 2, //SCE_TCL_:COMMENTLINE
  number = 3, //SCE_TCL_:NUMBER
  wordInQuote = 4, //SCE_TCL_:WORD_IN_QUOTE
  inQuote = 5, //SCE_TCL_:IN_QUOTE
  operator = 6, //SCE_TCL_:OPERATOR
  identifier = 7, //SCE_TCL_:IDENTIFIER
  substitution = 8, //SCE_TCL_:SUBSTITUTION
  subBrace = 9, //SCE_TCL_:SUB_BRACE
  modifier = 10, //SCE_TCL_:MODIFIER
  expand = 11, //SCE_TCL_:EXPAND
  word = 12, //SCE_TCL_:WORD
  word2 = 13, //SCE_TCL_:WORD2
  word3 = 14, //SCE_TCL_:WORD3
  word4 = 15, //SCE_TCL_:WORD4
  word5 = 16, //SCE_TCL_:WORD5
  word6 = 17, //SCE_TCL_:WORD6
  word7 = 18, //SCE_TCL_:WORD7
  word8 = 19, //SCE_TCL_:WORD8
  commentBox = 20, //SCE_TCL_:COMMENT_BOX
  blockComment = 21, //SCE_TCL_:BLOCK_COMMENT
}

// Prefixes: SCE_H_ SCE_HJ_ SCE_HJA_ SCE_HB_ SCE_HBA_ SCE_HP_ SCE_HPHP_ SCE_HPA_
// Common prefix: SCE_H
enum HTML {
  lexerVal = Lexer.html, // SCLEX_HTML
  default_ = 0, //SCE_H_:DEFAULT
  tag = 1, //SCE_H_:TAG
  tagunknown = 2, //SCE_H_:TAGUNKNOWN
  attribute = 3, //SCE_H_:ATTRIBUTE
  attributeunknown = 4, //SCE_H_:ATTRIBUTEUNKNOWN
  number = 5, //SCE_H_:NUMBER
  doublestring = 6, //SCE_H_:DOUBLESTRING
  singlestring = 7, //SCE_H_:SINGLESTRING
  other = 8, //SCE_H_:OTHER
  comment = 9, //SCE_H_:COMMENT
  entity = 10, //SCE_H_:ENTITY
  tagend = 11, //SCE_H_:TAGEND
  xmlstart = 12, //SCE_H_:XMLSTART
  xmlend = 13, //SCE_H_:XMLEND
  script = 14, //SCE_H_:SCRIPT
  asp = 15, //SCE_H_:ASP
  aspat = 16, //SCE_H_:ASPAT
  cdata = 17, //SCE_H_:CDATA
  question = 18, //SCE_H_:QUESTION
  value = 19, //SCE_H_:VALUE
  xccomment = 20, //SCE_H_:XCCOMMENT
  sgmlDefault = 21, //SCE_H_:SGML_DEFAULT
  sgmlCommand = 22, //SCE_H_:SGML_COMMAND
  sgml1stParam = 23, //SCE_H_:SGML_1ST_PARAM
  sgmlDoublestring = 24, //SCE_H_:SGML_DOUBLESTRING
  sgmlSimplestring = 25, //SCE_H_:SGML_SIMPLESTRING
  sgmlError = 26, //SCE_H_:SGML_ERROR
  sgmlSpecial = 27, //SCE_H_:SGML_SPECIAL
  sgmlEntity = 28, //SCE_H_:SGML_ENTITY
  sgmlComment = 29, //SCE_H_:SGML_COMMENT
  sgml1stParamComment = 30, //SCE_H_:SGML_1ST_PARAM_COMMENT
  sgmlBlockDefault = 31, //SCE_H_:SGML_BLOCK_DEFAULT
  jStart = 40, //SCE_HJ_:START
  jDefault = 41, //SCE_HJ_:DEFAULT
  jComment = 42, //SCE_HJ_:COMMENT
  jCommentline = 43, //SCE_HJ_:COMMENTLINE
  jCommentdoc = 44, //SCE_HJ_:COMMENTDOC
  jNumber = 45, //SCE_HJ_:NUMBER
  jWord = 46, //SCE_HJ_:WORD
  jKeyword = 47, //SCE_HJ_:KEYWORD
  jDoublestring = 48, //SCE_HJ_:DOUBLESTRING
  jSinglestring = 49, //SCE_HJ_:SINGLESTRING
  jSymbols = 50, //SCE_HJ_:SYMBOLS
  jStringeol = 51, //SCE_HJ_:STRINGEOL
  jRegex = 52, //SCE_HJ_:REGEX
  jaStart = 55, //SCE_HJA_:START
  jaDefault = 56, //SCE_HJA_:DEFAULT
  jaComment = 57, //SCE_HJA_:COMMENT
  jaCommentline = 58, //SCE_HJA_:COMMENTLINE
  jaCommentdoc = 59, //SCE_HJA_:COMMENTDOC
  jaNumber = 60, //SCE_HJA_:NUMBER
  jaWord = 61, //SCE_HJA_:WORD
  jaKeyword = 62, //SCE_HJA_:KEYWORD
  jaDoublestring = 63, //SCE_HJA_:DOUBLESTRING
  jaSinglestring = 64, //SCE_HJA_:SINGLESTRING
  jaSymbols = 65, //SCE_HJA_:SYMBOLS
  jaStringeol = 66, //SCE_HJA_:STRINGEOL
  jaRegex = 67, //SCE_HJA_:REGEX
  bStart = 70, //SCE_HB_:START
  bDefault = 71, //SCE_HB_:DEFAULT
  bCommentline = 72, //SCE_HB_:COMMENTLINE
  bNumber = 73, //SCE_HB_:NUMBER
  bWord = 74, //SCE_HB_:WORD
  bString = 75, //SCE_HB_:STRING
  bIdentifier = 76, //SCE_HB_:IDENTIFIER
  bStringeol = 77, //SCE_HB_:STRINGEOL
  baStart = 80, //SCE_HBA_:START
  baDefault = 81, //SCE_HBA_:DEFAULT
  baCommentline = 82, //SCE_HBA_:COMMENTLINE
  baNumber = 83, //SCE_HBA_:NUMBER
  baWord = 84, //SCE_HBA_:WORD
  baString = 85, //SCE_HBA_:STRING
  baIdentifier = 86, //SCE_HBA_:IDENTIFIER
  baStringeol = 87, //SCE_HBA_:STRINGEOL
  pStart = 90, //SCE_HP_:START
  pDefault = 91, //SCE_HP_:DEFAULT
  pCommentline = 92, //SCE_HP_:COMMENTLINE
  pNumber = 93, //SCE_HP_:NUMBER
  pString = 94, //SCE_HP_:STRING
  pCharacter = 95, //SCE_HP_:CHARACTER
  pWord = 96, //SCE_HP_:WORD
  pTriple = 97, //SCE_HP_:TRIPLE
  pTripledouble = 98, //SCE_HP_:TRIPLEDOUBLE
  pClassname = 99, //SCE_HP_:CLASSNAME
  pDefname = 100, //SCE_HP_:DEFNAME
  pOperator = 101, //SCE_HP_:OPERATOR
  pIdentifier = 102, //SCE_HP_:IDENTIFIER
  phpComplexVariable = 104, //SCE_HPHP_:COMPLEX_VARIABLE
  paStart = 105, //SCE_HPA_:START
  paDefault = 106, //SCE_HPA_:DEFAULT
  paCommentline = 107, //SCE_HPA_:COMMENTLINE
  paNumber = 108, //SCE_HPA_:NUMBER
  paString = 109, //SCE_HPA_:STRING
  paCharacter = 110, //SCE_HPA_:CHARACTER
  paWord = 111, //SCE_HPA_:WORD
  paTriple = 112, //SCE_HPA_:TRIPLE
  paTripledouble = 113, //SCE_HPA_:TRIPLEDOUBLE
  paClassname = 114, //SCE_HPA_:CLASSNAME
  paDefname = 115, //SCE_HPA_:DEFNAME
  paOperator = 116, //SCE_HPA_:OPERATOR
  paIdentifier = 117, //SCE_HPA_:IDENTIFIER
  phpDefault = 118, //SCE_HPHP_:DEFAULT
  phpHstring = 119, //SCE_HPHP_:HSTRING
  phpSimplestring = 120, //SCE_HPHP_:SIMPLESTRING
  phpWord = 121, //SCE_HPHP_:WORD
  phpNumber = 122, //SCE_HPHP_:NUMBER
  phpVariable = 123, //SCE_HPHP_:VARIABLE
  phpComment = 124, //SCE_HPHP_:COMMENT
  phpCommentline = 125, //SCE_HPHP_:COMMENTLINE
  phpHstringVariable = 126, //SCE_HPHP_:HSTRING_VARIABLE
  phpOperator = 127, //SCE_HPHP_:OPERATOR
}

// Prefixes: SCE_H_ SCE_HJ_ SCE_HJA_ SCE_HB_ SCE_HBA_ SCE_HP_ SCE_HPHP_ SCE_HPA_
// Common prefix: SCE_H
enum XML {
  lexerVal = Lexer.xml, // SCLEX_XML
  default_ = 0, //SCE_H_:DEFAULT
  tag = 1, //SCE_H_:TAG
  tagunknown = 2, //SCE_H_:TAGUNKNOWN
  attribute = 3, //SCE_H_:ATTRIBUTE
  attributeunknown = 4, //SCE_H_:ATTRIBUTEUNKNOWN
  number = 5, //SCE_H_:NUMBER
  doublestring = 6, //SCE_H_:DOUBLESTRING
  singlestring = 7, //SCE_H_:SINGLESTRING
  other = 8, //SCE_H_:OTHER
  comment = 9, //SCE_H_:COMMENT
  entity = 10, //SCE_H_:ENTITY
  tagend = 11, //SCE_H_:TAGEND
  xmlstart = 12, //SCE_H_:XMLSTART
  xmlend = 13, //SCE_H_:XMLEND
  script = 14, //SCE_H_:SCRIPT
  asp = 15, //SCE_H_:ASP
  aspat = 16, //SCE_H_:ASPAT
  cdata = 17, //SCE_H_:CDATA
  question = 18, //SCE_H_:QUESTION
  value = 19, //SCE_H_:VALUE
  xccomment = 20, //SCE_H_:XCCOMMENT
  sgmlDefault = 21, //SCE_H_:SGML_DEFAULT
  sgmlCommand = 22, //SCE_H_:SGML_COMMAND
  sgml1stParam = 23, //SCE_H_:SGML_1ST_PARAM
  sgmlDoublestring = 24, //SCE_H_:SGML_DOUBLESTRING
  sgmlSimplestring = 25, //SCE_H_:SGML_SIMPLESTRING
  sgmlError = 26, //SCE_H_:SGML_ERROR
  sgmlSpecial = 27, //SCE_H_:SGML_SPECIAL
  sgmlEntity = 28, //SCE_H_:SGML_ENTITY
  sgmlComment = 29, //SCE_H_:SGML_COMMENT
  sgml1stParamComment = 30, //SCE_H_:SGML_1ST_PARAM_COMMENT
  sgmlBlockDefault = 31, //SCE_H_:SGML_BLOCK_DEFAULT
  jStart = 40, //SCE_HJ_:START
  jDefault = 41, //SCE_HJ_:DEFAULT
  jComment = 42, //SCE_HJ_:COMMENT
  jCommentline = 43, //SCE_HJ_:COMMENTLINE
  jCommentdoc = 44, //SCE_HJ_:COMMENTDOC
  jNumber = 45, //SCE_HJ_:NUMBER
  jWord = 46, //SCE_HJ_:WORD
  jKeyword = 47, //SCE_HJ_:KEYWORD
  jDoublestring = 48, //SCE_HJ_:DOUBLESTRING
  jSinglestring = 49, //SCE_HJ_:SINGLESTRING
  jSymbols = 50, //SCE_HJ_:SYMBOLS
  jStringeol = 51, //SCE_HJ_:STRINGEOL
  jRegex = 52, //SCE_HJ_:REGEX
  jaStart = 55, //SCE_HJA_:START
  jaDefault = 56, //SCE_HJA_:DEFAULT
  jaComment = 57, //SCE_HJA_:COMMENT
  jaCommentline = 58, //SCE_HJA_:COMMENTLINE
  jaCommentdoc = 59, //SCE_HJA_:COMMENTDOC
  jaNumber = 60, //SCE_HJA_:NUMBER
  jaWord = 61, //SCE_HJA_:WORD
  jaKeyword = 62, //SCE_HJA_:KEYWORD
  jaDoublestring = 63, //SCE_HJA_:DOUBLESTRING
  jaSinglestring = 64, //SCE_HJA_:SINGLESTRING
  jaSymbols = 65, //SCE_HJA_:SYMBOLS
  jaStringeol = 66, //SCE_HJA_:STRINGEOL
  jaRegex = 67, //SCE_HJA_:REGEX
  bStart = 70, //SCE_HB_:START
  bDefault = 71, //SCE_HB_:DEFAULT
  bCommentline = 72, //SCE_HB_:COMMENTLINE
  bNumber = 73, //SCE_HB_:NUMBER
  bWord = 74, //SCE_HB_:WORD
  bString = 75, //SCE_HB_:STRING
  bIdentifier = 76, //SCE_HB_:IDENTIFIER
  bStringeol = 77, //SCE_HB_:STRINGEOL
  baStart = 80, //SCE_HBA_:START
  baDefault = 81, //SCE_HBA_:DEFAULT
  baCommentline = 82, //SCE_HBA_:COMMENTLINE
  baNumber = 83, //SCE_HBA_:NUMBER
  baWord = 84, //SCE_HBA_:WORD
  baString = 85, //SCE_HBA_:STRING
  baIdentifier = 86, //SCE_HBA_:IDENTIFIER
  baStringeol = 87, //SCE_HBA_:STRINGEOL
  pStart = 90, //SCE_HP_:START
  pDefault = 91, //SCE_HP_:DEFAULT
  pCommentline = 92, //SCE_HP_:COMMENTLINE
  pNumber = 93, //SCE_HP_:NUMBER
  pString = 94, //SCE_HP_:STRING
  pCharacter = 95, //SCE_HP_:CHARACTER
  pWord = 96, //SCE_HP_:WORD
  pTriple = 97, //SCE_HP_:TRIPLE
  pTripledouble = 98, //SCE_HP_:TRIPLEDOUBLE
  pClassname = 99, //SCE_HP_:CLASSNAME
  pDefname = 100, //SCE_HP_:DEFNAME
  pOperator = 101, //SCE_HP_:OPERATOR
  pIdentifier = 102, //SCE_HP_:IDENTIFIER
  phpComplexVariable = 104, //SCE_HPHP_:COMPLEX_VARIABLE
  paStart = 105, //SCE_HPA_:START
  paDefault = 106, //SCE_HPA_:DEFAULT
  paCommentline = 107, //SCE_HPA_:COMMENTLINE
  paNumber = 108, //SCE_HPA_:NUMBER
  paString = 109, //SCE_HPA_:STRING
  paCharacter = 110, //SCE_HPA_:CHARACTER
  paWord = 111, //SCE_HPA_:WORD
  paTriple = 112, //SCE_HPA_:TRIPLE
  paTripledouble = 113, //SCE_HPA_:TRIPLEDOUBLE
  paClassname = 114, //SCE_HPA_:CLASSNAME
  paDefname = 115, //SCE_HPA_:DEFNAME
  paOperator = 116, //SCE_HPA_:OPERATOR
  paIdentifier = 117, //SCE_HPA_:IDENTIFIER
  phpDefault = 118, //SCE_HPHP_:DEFAULT
  phpHstring = 119, //SCE_HPHP_:HSTRING
  phpSimplestring = 120, //SCE_HPHP_:SIMPLESTRING
  phpWord = 121, //SCE_HPHP_:WORD
  phpNumber = 122, //SCE_HPHP_:NUMBER
  phpVariable = 123, //SCE_HPHP_:VARIABLE
  phpComment = 124, //SCE_HPHP_:COMMENT
  phpCommentline = 125, //SCE_HPHP_:COMMENTLINE
  phpHstringVariable = 126, //SCE_HPHP_:HSTRING_VARIABLE
  phpOperator = 127, //SCE_HPHP_:OPERATOR
}

// Prefixes: SCE_H_ SCE_HJ_ SCE_HJA_ SCE_HB_ SCE_HBA_ SCE_HP_ SCE_HPHP_ SCE_HPA_
// Common prefix: SCE_H
enum ASP {
  // lexerVal = ???, // SCLEX_ASP
  default_ = 0, //SCE_H_:DEFAULT
  tag = 1, //SCE_H_:TAG
  tagunknown = 2, //SCE_H_:TAGUNKNOWN
  attribute = 3, //SCE_H_:ATTRIBUTE
  attributeunknown = 4, //SCE_H_:ATTRIBUTEUNKNOWN
  number = 5, //SCE_H_:NUMBER
  doublestring = 6, //SCE_H_:DOUBLESTRING
  singlestring = 7, //SCE_H_:SINGLESTRING
  other = 8, //SCE_H_:OTHER
  comment = 9, //SCE_H_:COMMENT
  entity = 10, //SCE_H_:ENTITY
  tagend = 11, //SCE_H_:TAGEND
  xmlstart = 12, //SCE_H_:XMLSTART
  xmlend = 13, //SCE_H_:XMLEND
  script = 14, //SCE_H_:SCRIPT
  asp = 15, //SCE_H_:ASP
  aspat = 16, //SCE_H_:ASPAT
  cdata = 17, //SCE_H_:CDATA
  question = 18, //SCE_H_:QUESTION
  value = 19, //SCE_H_:VALUE
  xccomment = 20, //SCE_H_:XCCOMMENT
  sgmlDefault = 21, //SCE_H_:SGML_DEFAULT
  sgmlCommand = 22, //SCE_H_:SGML_COMMAND
  sgml1stParam = 23, //SCE_H_:SGML_1ST_PARAM
  sgmlDoublestring = 24, //SCE_H_:SGML_DOUBLESTRING
  sgmlSimplestring = 25, //SCE_H_:SGML_SIMPLESTRING
  sgmlError = 26, //SCE_H_:SGML_ERROR
  sgmlSpecial = 27, //SCE_H_:SGML_SPECIAL
  sgmlEntity = 28, //SCE_H_:SGML_ENTITY
  sgmlComment = 29, //SCE_H_:SGML_COMMENT
  sgml1stParamComment = 30, //SCE_H_:SGML_1ST_PARAM_COMMENT
  sgmlBlockDefault = 31, //SCE_H_:SGML_BLOCK_DEFAULT
  jStart = 40, //SCE_HJ_:START
  jDefault = 41, //SCE_HJ_:DEFAULT
  jComment = 42, //SCE_HJ_:COMMENT
  jCommentline = 43, //SCE_HJ_:COMMENTLINE
  jCommentdoc = 44, //SCE_HJ_:COMMENTDOC
  jNumber = 45, //SCE_HJ_:NUMBER
  jWord = 46, //SCE_HJ_:WORD
  jKeyword = 47, //SCE_HJ_:KEYWORD
  jDoublestring = 48, //SCE_HJ_:DOUBLESTRING
  jSinglestring = 49, //SCE_HJ_:SINGLESTRING
  jSymbols = 50, //SCE_HJ_:SYMBOLS
  jStringeol = 51, //SCE_HJ_:STRINGEOL
  jRegex = 52, //SCE_HJ_:REGEX
  jaStart = 55, //SCE_HJA_:START
  jaDefault = 56, //SCE_HJA_:DEFAULT
  jaComment = 57, //SCE_HJA_:COMMENT
  jaCommentline = 58, //SCE_HJA_:COMMENTLINE
  jaCommentdoc = 59, //SCE_HJA_:COMMENTDOC
  jaNumber = 60, //SCE_HJA_:NUMBER
  jaWord = 61, //SCE_HJA_:WORD
  jaKeyword = 62, //SCE_HJA_:KEYWORD
  jaDoublestring = 63, //SCE_HJA_:DOUBLESTRING
  jaSinglestring = 64, //SCE_HJA_:SINGLESTRING
  jaSymbols = 65, //SCE_HJA_:SYMBOLS
  jaStringeol = 66, //SCE_HJA_:STRINGEOL
  jaRegex = 67, //SCE_HJA_:REGEX
  bStart = 70, //SCE_HB_:START
  bDefault = 71, //SCE_HB_:DEFAULT
  bCommentline = 72, //SCE_HB_:COMMENTLINE
  bNumber = 73, //SCE_HB_:NUMBER
  bWord = 74, //SCE_HB_:WORD
  bString = 75, //SCE_HB_:STRING
  bIdentifier = 76, //SCE_HB_:IDENTIFIER
  bStringeol = 77, //SCE_HB_:STRINGEOL
  baStart = 80, //SCE_HBA_:START
  baDefault = 81, //SCE_HBA_:DEFAULT
  baCommentline = 82, //SCE_HBA_:COMMENTLINE
  baNumber = 83, //SCE_HBA_:NUMBER
  baWord = 84, //SCE_HBA_:WORD
  baString = 85, //SCE_HBA_:STRING
  baIdentifier = 86, //SCE_HBA_:IDENTIFIER
  baStringeol = 87, //SCE_HBA_:STRINGEOL
  pStart = 90, //SCE_HP_:START
  pDefault = 91, //SCE_HP_:DEFAULT
  pCommentline = 92, //SCE_HP_:COMMENTLINE
  pNumber = 93, //SCE_HP_:NUMBER
  pString = 94, //SCE_HP_:STRING
  pCharacter = 95, //SCE_HP_:CHARACTER
  pWord = 96, //SCE_HP_:WORD
  pTriple = 97, //SCE_HP_:TRIPLE
  pTripledouble = 98, //SCE_HP_:TRIPLEDOUBLE
  pClassname = 99, //SCE_HP_:CLASSNAME
  pDefname = 100, //SCE_HP_:DEFNAME
  pOperator = 101, //SCE_HP_:OPERATOR
  pIdentifier = 102, //SCE_HP_:IDENTIFIER
  phpComplexVariable = 104, //SCE_HPHP_:COMPLEX_VARIABLE
  paStart = 105, //SCE_HPA_:START
  paDefault = 106, //SCE_HPA_:DEFAULT
  paCommentline = 107, //SCE_HPA_:COMMENTLINE
  paNumber = 108, //SCE_HPA_:NUMBER
  paString = 109, //SCE_HPA_:STRING
  paCharacter = 110, //SCE_HPA_:CHARACTER
  paWord = 111, //SCE_HPA_:WORD
  paTriple = 112, //SCE_HPA_:TRIPLE
  paTripledouble = 113, //SCE_HPA_:TRIPLEDOUBLE
  paClassname = 114, //SCE_HPA_:CLASSNAME
  paDefname = 115, //SCE_HPA_:DEFNAME
  paOperator = 116, //SCE_HPA_:OPERATOR
  paIdentifier = 117, //SCE_HPA_:IDENTIFIER
  phpDefault = 118, //SCE_HPHP_:DEFAULT
  phpHstring = 119, //SCE_HPHP_:HSTRING
  phpSimplestring = 120, //SCE_HPHP_:SIMPLESTRING
  phpWord = 121, //SCE_HPHP_:WORD
  phpNumber = 122, //SCE_HPHP_:NUMBER
  phpVariable = 123, //SCE_HPHP_:VARIABLE
  phpComment = 124, //SCE_HPHP_:COMMENT
  phpCommentline = 125, //SCE_HPHP_:COMMENTLINE
  phpHstringVariable = 126, //SCE_HPHP_:HSTRING_VARIABLE
  phpOperator = 127, //SCE_HPHP_:OPERATOR
}

// Prefixes: SCE_H_ SCE_HJ_ SCE_HJA_ SCE_HB_ SCE_HBA_ SCE_HP_ SCE_HPHP_ SCE_HPA_
// Common prefix: SCE_H
enum PHP {
  // lexerVal = ???, // SCLEX_PHP
  default_ = 0, //SCE_H_:DEFAULT
  tag = 1, //SCE_H_:TAG
  tagunknown = 2, //SCE_H_:TAGUNKNOWN
  attribute = 3, //SCE_H_:ATTRIBUTE
  attributeunknown = 4, //SCE_H_:ATTRIBUTEUNKNOWN
  number = 5, //SCE_H_:NUMBER
  doublestring = 6, //SCE_H_:DOUBLESTRING
  singlestring = 7, //SCE_H_:SINGLESTRING
  other = 8, //SCE_H_:OTHER
  comment = 9, //SCE_H_:COMMENT
  entity = 10, //SCE_H_:ENTITY
  tagend = 11, //SCE_H_:TAGEND
  xmlstart = 12, //SCE_H_:XMLSTART
  xmlend = 13, //SCE_H_:XMLEND
  script = 14, //SCE_H_:SCRIPT
  asp = 15, //SCE_H_:ASP
  aspat = 16, //SCE_H_:ASPAT
  cdata = 17, //SCE_H_:CDATA
  question = 18, //SCE_H_:QUESTION
  value = 19, //SCE_H_:VALUE
  xccomment = 20, //SCE_H_:XCCOMMENT
  sgmlDefault = 21, //SCE_H_:SGML_DEFAULT
  sgmlCommand = 22, //SCE_H_:SGML_COMMAND
  sgml1stParam = 23, //SCE_H_:SGML_1ST_PARAM
  sgmlDoublestring = 24, //SCE_H_:SGML_DOUBLESTRING
  sgmlSimplestring = 25, //SCE_H_:SGML_SIMPLESTRING
  sgmlError = 26, //SCE_H_:SGML_ERROR
  sgmlSpecial = 27, //SCE_H_:SGML_SPECIAL
  sgmlEntity = 28, //SCE_H_:SGML_ENTITY
  sgmlComment = 29, //SCE_H_:SGML_COMMENT
  sgml1stParamComment = 30, //SCE_H_:SGML_1ST_PARAM_COMMENT
  sgmlBlockDefault = 31, //SCE_H_:SGML_BLOCK_DEFAULT
  jStart = 40, //SCE_HJ_:START
  jDefault = 41, //SCE_HJ_:DEFAULT
  jComment = 42, //SCE_HJ_:COMMENT
  jCommentline = 43, //SCE_HJ_:COMMENTLINE
  jCommentdoc = 44, //SCE_HJ_:COMMENTDOC
  jNumber = 45, //SCE_HJ_:NUMBER
  jWord = 46, //SCE_HJ_:WORD
  jKeyword = 47, //SCE_HJ_:KEYWORD
  jDoublestring = 48, //SCE_HJ_:DOUBLESTRING
  jSinglestring = 49, //SCE_HJ_:SINGLESTRING
  jSymbols = 50, //SCE_HJ_:SYMBOLS
  jStringeol = 51, //SCE_HJ_:STRINGEOL
  jRegex = 52, //SCE_HJ_:REGEX
  jaStart = 55, //SCE_HJA_:START
  jaDefault = 56, //SCE_HJA_:DEFAULT
  jaComment = 57, //SCE_HJA_:COMMENT
  jaCommentline = 58, //SCE_HJA_:COMMENTLINE
  jaCommentdoc = 59, //SCE_HJA_:COMMENTDOC
  jaNumber = 60, //SCE_HJA_:NUMBER
  jaWord = 61, //SCE_HJA_:WORD
  jaKeyword = 62, //SCE_HJA_:KEYWORD
  jaDoublestring = 63, //SCE_HJA_:DOUBLESTRING
  jaSinglestring = 64, //SCE_HJA_:SINGLESTRING
  jaSymbols = 65, //SCE_HJA_:SYMBOLS
  jaStringeol = 66, //SCE_HJA_:STRINGEOL
  jaRegex = 67, //SCE_HJA_:REGEX
  bStart = 70, //SCE_HB_:START
  bDefault = 71, //SCE_HB_:DEFAULT
  bCommentline = 72, //SCE_HB_:COMMENTLINE
  bNumber = 73, //SCE_HB_:NUMBER
  bWord = 74, //SCE_HB_:WORD
  bString = 75, //SCE_HB_:STRING
  bIdentifier = 76, //SCE_HB_:IDENTIFIER
  bStringeol = 77, //SCE_HB_:STRINGEOL
  baStart = 80, //SCE_HBA_:START
  baDefault = 81, //SCE_HBA_:DEFAULT
  baCommentline = 82, //SCE_HBA_:COMMENTLINE
  baNumber = 83, //SCE_HBA_:NUMBER
  baWord = 84, //SCE_HBA_:WORD
  baString = 85, //SCE_HBA_:STRING
  baIdentifier = 86, //SCE_HBA_:IDENTIFIER
  baStringeol = 87, //SCE_HBA_:STRINGEOL
  pStart = 90, //SCE_HP_:START
  pDefault = 91, //SCE_HP_:DEFAULT
  pCommentline = 92, //SCE_HP_:COMMENTLINE
  pNumber = 93, //SCE_HP_:NUMBER
  pString = 94, //SCE_HP_:STRING
  pCharacter = 95, //SCE_HP_:CHARACTER
  pWord = 96, //SCE_HP_:WORD
  pTriple = 97, //SCE_HP_:TRIPLE
  pTripledouble = 98, //SCE_HP_:TRIPLEDOUBLE
  pClassname = 99, //SCE_HP_:CLASSNAME
  pDefname = 100, //SCE_HP_:DEFNAME
  pOperator = 101, //SCE_HP_:OPERATOR
  pIdentifier = 102, //SCE_HP_:IDENTIFIER
  phpComplexVariable = 104, //SCE_HPHP_:COMPLEX_VARIABLE
  paStart = 105, //SCE_HPA_:START
  paDefault = 106, //SCE_HPA_:DEFAULT
  paCommentline = 107, //SCE_HPA_:COMMENTLINE
  paNumber = 108, //SCE_HPA_:NUMBER
  paString = 109, //SCE_HPA_:STRING
  paCharacter = 110, //SCE_HPA_:CHARACTER
  paWord = 111, //SCE_HPA_:WORD
  paTriple = 112, //SCE_HPA_:TRIPLE
  paTripledouble = 113, //SCE_HPA_:TRIPLEDOUBLE
  paClassname = 114, //SCE_HPA_:CLASSNAME
  paDefname = 115, //SCE_HPA_:DEFNAME
  paOperator = 116, //SCE_HPA_:OPERATOR
  paIdentifier = 117, //SCE_HPA_:IDENTIFIER
  phpDefault = 118, //SCE_HPHP_:DEFAULT
  phpHstring = 119, //SCE_HPHP_:HSTRING
  phpSimplestring = 120, //SCE_HPHP_:SIMPLESTRING
  phpWord = 121, //SCE_HPHP_:WORD
  phpNumber = 122, //SCE_HPHP_:NUMBER
  phpVariable = 123, //SCE_HPHP_:VARIABLE
  phpComment = 124, //SCE_HPHP_:COMMENT
  phpCommentline = 125, //SCE_HPHP_:COMMENTLINE
  phpHstringVariable = 126, //SCE_HPHP_:HSTRING_VARIABLE
  phpOperator = 127, //SCE_HPHP_:OPERATOR
}

// Prefix: SCE_PL_
enum Perl {
  lexerVal = Lexer.perl, // SCLEX_PERL
  default_ = 0, //SCE_PL_:DEFAULT
  error = 1, //SCE_PL_:ERROR
  commentline = 2, //SCE_PL_:COMMENTLINE
  pod = 3, //SCE_PL_:POD
  number = 4, //SCE_PL_:NUMBER
  word = 5, //SCE_PL_:WORD
  string = 6, //SCE_PL_:STRING
  character = 7, //SCE_PL_:CHARACTER
  punctuation = 8, //SCE_PL_:PUNCTUATION
  preprocessor = 9, //SCE_PL_:PREPROCESSOR
  operator = 10, //SCE_PL_:OPERATOR
  identifier = 11, //SCE_PL_:IDENTIFIER
  scalar = 12, //SCE_PL_:SCALAR
  array = 13, //SCE_PL_:ARRAY
  hash = 14, //SCE_PL_:HASH
  symboltable = 15, //SCE_PL_:SYMBOLTABLE
  variableIndexer = 16, //SCE_PL_:VARIABLE_INDEXER
  regex = 17, //SCE_PL_:REGEX
  regsubst = 18, //SCE_PL_:REGSUBST
  longquote = 19, //SCE_PL_:LONGQUOTE
  backticks = 20, //SCE_PL_:BACKTICKS
  datasection = 21, //SCE_PL_:DATASECTION
  hereDelim = 22, //SCE_PL_:HERE_DELIM
  hereQ = 23, //SCE_PL_:HERE_Q
  hereQq = 24, //SCE_PL_:HERE_QQ
  hereQx = 25, //SCE_PL_:HERE_QX
  stringQ = 26, //SCE_PL_:STRING_Q
  stringQq = 27, //SCE_PL_:STRING_QQ
  stringQx = 28, //SCE_PL_:STRING_QX
  stringQr = 29, //SCE_PL_:STRING_QR
  stringQw = 30, //SCE_PL_:STRING_QW
  podVerb = 31, //SCE_PL_:POD_VERB
  subPrototype = 40, //SCE_PL_:SUB_PROTOTYPE
  formatIdent = 41, //SCE_PL_:FORMAT_IDENT
  format = 42, //SCE_PL_:FORMAT
  stringVar = 43, //SCE_PL_:STRING_VAR
  xlat = 44, //SCE_PL_:XLAT
  regexVar = 54, //SCE_PL_:REGEX_VAR
  regsubstVar = 55, //SCE_PL_:REGSUBST_VAR
  backticksVar = 57, //SCE_PL_:BACKTICKS_VAR
  hereQqVar = 61, //SCE_PL_:HERE_QQ_VAR
  hereQxVar = 62, //SCE_PL_:HERE_QX_VAR
  stringQqVar = 64, //SCE_PL_:STRING_QQ_VAR
  stringQxVar = 65, //SCE_PL_:STRING_QX_VAR
  stringQrVar = 66, //SCE_PL_:STRING_QR_VAR
}

// Prefix: SCE_RB_
enum Ruby {
  lexerVal = Lexer.ruby, // SCLEX_RUBY
  default_ = 0, //SCE_RB_:DEFAULT
  error = 1, //SCE_RB_:ERROR
  commentline = 2, //SCE_RB_:COMMENTLINE
  pod = 3, //SCE_RB_:POD
  number = 4, //SCE_RB_:NUMBER
  word = 5, //SCE_RB_:WORD
  string = 6, //SCE_RB_:STRING
  character = 7, //SCE_RB_:CHARACTER
  classname = 8, //SCE_RB_:CLASSNAME
  defname = 9, //SCE_RB_:DEFNAME
  operator = 10, //SCE_RB_:OPERATOR
  identifier = 11, //SCE_RB_:IDENTIFIER
  regex = 12, //SCE_RB_:REGEX
  global = 13, //SCE_RB_:GLOBAL
  symbol = 14, //SCE_RB_:SYMBOL
  moduleName = 15, //SCE_RB_:MODULE_NAME
  instanceVar = 16, //SCE_RB_:INSTANCE_VAR
  classVar = 17, //SCE_RB_:CLASS_VAR
  backticks = 18, //SCE_RB_:BACKTICKS
  datasection = 19, //SCE_RB_:DATASECTION
  hereDelim = 20, //SCE_RB_:HERE_DELIM
  hereQ = 21, //SCE_RB_:HERE_Q
  hereQq = 22, //SCE_RB_:HERE_QQ
  hereQx = 23, //SCE_RB_:HERE_QX
  stringQ = 24, //SCE_RB_:STRING_Q
  stringQq = 25, //SCE_RB_:STRING_QQ
  stringQx = 26, //SCE_RB_:STRING_QX
  stringQr = 27, //SCE_RB_:STRING_QR
  stringQw = 28, //SCE_RB_:STRING_QW
  wordDemoted = 29, //SCE_RB_:WORD_DEMOTED
  stdin = 30, //SCE_RB_:STDIN
  stdout = 31, //SCE_RB_:STDOUT
  stderr = 40, //SCE_RB_:STDERR
  upperBound = 41, //SCE_RB_:UPPER_BOUND
}

// Prefix: SCE_B_
enum VB {
  lexerVal = Lexer.vb, // SCLEX_VB
  default_ = 0, //SCE_B_:DEFAULT
  comment = 1, //SCE_B_:COMMENT
  number = 2, //SCE_B_:NUMBER
  keyword = 3, //SCE_B_:KEYWORD
  string = 4, //SCE_B_:STRING
  preprocessor = 5, //SCE_B_:PREPROCESSOR
  operator = 6, //SCE_B_:OPERATOR
  identifier = 7, //SCE_B_:IDENTIFIER
  date = 8, //SCE_B_:DATE
  stringeol = 9, //SCE_B_:STRINGEOL
  keyword2 = 10, //SCE_B_:KEYWORD2
  keyword3 = 11, //SCE_B_:KEYWORD3
  keyword4 = 12, //SCE_B_:KEYWORD4
  constant = 13, //SCE_B_:CONSTANT
  asm_ = 14, //SCE_B_:ASM
  label = 15, //SCE_B_:LABEL
  error = 16, //SCE_B_:ERROR
  hexnumber = 17, //SCE_B_:HEXNUMBER
  binnumber = 18, //SCE_B_:BINNUMBER
}

// Prefix: SCE_B_
enum VBScript {
  lexerVal = Lexer.vbscript, // SCLEX_VBSCRIPT
  default_ = 0, //SCE_B_:DEFAULT
  comment = 1, //SCE_B_:COMMENT
  number = 2, //SCE_B_:NUMBER
  keyword = 3, //SCE_B_:KEYWORD
  string = 4, //SCE_B_:STRING
  preprocessor = 5, //SCE_B_:PREPROCESSOR
  operator = 6, //SCE_B_:OPERATOR
  identifier = 7, //SCE_B_:IDENTIFIER
  date = 8, //SCE_B_:DATE
  stringeol = 9, //SCE_B_:STRINGEOL
  keyword2 = 10, //SCE_B_:KEYWORD2
  keyword3 = 11, //SCE_B_:KEYWORD3
  keyword4 = 12, //SCE_B_:KEYWORD4
  constant = 13, //SCE_B_:CONSTANT
  asm_ = 14, //SCE_B_:ASM
  label = 15, //SCE_B_:LABEL
  error = 16, //SCE_B_:ERROR
  hexnumber = 17, //SCE_B_:HEXNUMBER
  binnumber = 18, //SCE_B_:BINNUMBER
}

// Prefix: SCE_B_
enum PowerBasic {
  lexerVal = Lexer.powerbasic, // SCLEX_POWERBASIC
  default_ = 0, //SCE_B_:DEFAULT
  comment = 1, //SCE_B_:COMMENT
  number = 2, //SCE_B_:NUMBER
  keyword = 3, //SCE_B_:KEYWORD
  string = 4, //SCE_B_:STRING
  preprocessor = 5, //SCE_B_:PREPROCESSOR
  operator = 6, //SCE_B_:OPERATOR
  identifier = 7, //SCE_B_:IDENTIFIER
  date = 8, //SCE_B_:DATE
  stringeol = 9, //SCE_B_:STRINGEOL
  keyword2 = 10, //SCE_B_:KEYWORD2
  keyword3 = 11, //SCE_B_:KEYWORD3
  keyword4 = 12, //SCE_B_:KEYWORD4
  constant = 13, //SCE_B_:CONSTANT
  asm_ = 14, //SCE_B_:ASM
  label = 15, //SCE_B_:LABEL
  error = 16, //SCE_B_:ERROR
  hexnumber = 17, //SCE_B_:HEXNUMBER
  binnumber = 18, //SCE_B_:BINNUMBER
}

// Prefix: SCE_PROPS_
enum Properties {
  lexerVal = Lexer.properties, // SCLEX_PROPERTIES
  default_ = 0, //SCE_PROPS_:DEFAULT
  comment = 1, //SCE_PROPS_:COMMENT
  section = 2, //SCE_PROPS_:SECTION
  assignment = 3, //SCE_PROPS_:ASSIGNMENT
  defval = 4, //SCE_PROPS_:DEFVAL
  key = 5, //SCE_PROPS_:KEY
}

// Prefix: SCE_L_
enum LaTeX {
  lexerVal = Lexer.latex, // SCLEX_LATEX
  default_ = 0, //SCE_L_:DEFAULT
  command = 1, //SCE_L_:COMMAND
  tag = 2, //SCE_L_:TAG
  math = 3, //SCE_L_:MATH
  comment = 4, //SCE_L_:COMMENT
  tag2 = 5, //SCE_L_:TAG2
  math2 = 6, //SCE_L_:MATH2
  comment2 = 7, //SCE_L_:COMMENT2
  verbatim = 8, //SCE_L_:VERBATIM
  shortcmd = 9, //SCE_L_:SHORTCMD
  special = 10, //SCE_L_:SPECIAL
  cmdopt = 11, //SCE_L_:CMDOPT
  error = 12, //SCE_L_:ERROR
}

// Prefix: SCE_LUA_
enum Lua {
  lexerVal = Lexer.lua, // SCLEX_LUA
  default_ = 0, //SCE_LUA_:DEFAULT
  comment = 1, //SCE_LUA_:COMMENT
  commentline = 2, //SCE_LUA_:COMMENTLINE
  commentdoc = 3, //SCE_LUA_:COMMENTDOC
  number = 4, //SCE_LUA_:NUMBER
  word = 5, //SCE_LUA_:WORD
  string = 6, //SCE_LUA_:STRING
  character = 7, //SCE_LUA_:CHARACTER
  literalstring = 8, //SCE_LUA_:LITERALSTRING
  preprocessor = 9, //SCE_LUA_:PREPROCESSOR
  operator = 10, //SCE_LUA_:OPERATOR
  identifier = 11, //SCE_LUA_:IDENTIFIER
  stringeol = 12, //SCE_LUA_:STRINGEOL
  word2 = 13, //SCE_LUA_:WORD2
  word3 = 14, //SCE_LUA_:WORD3
  word4 = 15, //SCE_LUA_:WORD4
  word5 = 16, //SCE_LUA_:WORD5
  word6 = 17, //SCE_LUA_:WORD6
  word7 = 18, //SCE_LUA_:WORD7
  word8 = 19, //SCE_LUA_:WORD8
  label = 20, //SCE_LUA_:LABEL
}

// Prefix: SCE_ERR_
enum ErrorList {
  lexerVal = Lexer.errorlist, // SCLEX_ERRORLIST
  default_ = 0, //SCE_ERR_:DEFAULT
  python = 1, //SCE_ERR_:PYTHON
  gcc = 2, //SCE_ERR_:GCC
  ms = 3, //SCE_ERR_:MS
  cmd = 4, //SCE_ERR_:CMD
  borland = 5, //SCE_ERR_:BORLAND
  perl = 6, //SCE_ERR_:PERL
  net = 7, //SCE_ERR_:NET
  lua = 8, //SCE_ERR_:LUA
  ctag = 9, //SCE_ERR_:CTAG
  diffChanged = 10, //SCE_ERR_:DIFF_CHANGED
  diffAddition = 11, //SCE_ERR_:DIFF_ADDITION
  diffDeletion = 12, //SCE_ERR_:DIFF_DELETION
  diffMessage = 13, //SCE_ERR_:DIFF_MESSAGE
  php = 14, //SCE_ERR_:PHP
  elf = 15, //SCE_ERR_:ELF
  ifc = 16, //SCE_ERR_:IFC
  ifort = 17, //SCE_ERR_:IFORT
  absf = 18, //SCE_ERR_:ABSF
  tidy = 19, //SCE_ERR_:TIDY
  javaStack = 20, //SCE_ERR_:JAVA_STACK
  value = 21, //SCE_ERR_:VALUE
}

// Prefix: SCE_BAT_
enum Batch {
  lexerVal = Lexer.batch, // SCLEX_BATCH
  default_ = 0, //SCE_BAT_:DEFAULT
  comment = 1, //SCE_BAT_:COMMENT
  word = 2, //SCE_BAT_:WORD
  label = 3, //SCE_BAT_:LABEL
  hide = 4, //SCE_BAT_:HIDE
  command = 5, //SCE_BAT_:COMMAND
  identifier = 6, //SCE_BAT_:IDENTIFIER
  operator = 7, //SCE_BAT_:OPERATOR
}

// Prefix: SCE_TCMD_
enum TCMD {
  lexerVal = Lexer.tcmd, // SCLEX_TCMD
  default_ = 0, //SCE_TCMD_:DEFAULT
  comment = 1, //SCE_TCMD_:COMMENT
  word = 2, //SCE_TCMD_:WORD
  label = 3, //SCE_TCMD_:LABEL
  hide = 4, //SCE_TCMD_:HIDE
  command = 5, //SCE_TCMD_:COMMAND
  identifier = 6, //SCE_TCMD_:IDENTIFIER
  operator = 7, //SCE_TCMD_:OPERATOR
  environment = 8, //SCE_TCMD_:ENVIRONMENT
  expansion = 9, //SCE_TCMD_:EXPANSION
  clabel = 10, //SCE_TCMD_:CLABEL
}

// Prefix: SCE_MAKE_
enum MakeFile {
  lexerVal = Lexer.makefile, // SCLEX_MAKEFILE
  default_ = 0, //SCE_MAKE_:DEFAULT
  comment = 1, //SCE_MAKE_:COMMENT
  preprocessor = 2, //SCE_MAKE_:PREPROCESSOR
  identifier = 3, //SCE_MAKE_:IDENTIFIER
  operator = 4, //SCE_MAKE_:OPERATOR
  target = 5, //SCE_MAKE_:TARGET
  ideol = 9, //SCE_MAKE_:IDEOL
}

// Prefix: SCE_DIFF_
enum Diff {
  lexerVal = Lexer.diff, // SCLEX_DIFF
  default_ = 0, //SCE_DIFF_:DEFAULT
  comment = 1, //SCE_DIFF_:COMMENT
  command = 2, //SCE_DIFF_:COMMAND
  header = 3, //SCE_DIFF_:HEADER
  position = 4, //SCE_DIFF_:POSITION
  deleted = 5, //SCE_DIFF_:DELETED
  added = 6, //SCE_DIFF_:ADDED
  changed = 7, //SCE_DIFF_:CHANGED
}

// Prefix: SCE_CONF_
enum Conf {
  lexerVal = Lexer.conf, // SCLEX_CONF
  default_ = 0, //SCE_CONF_:DEFAULT
  comment = 1, //SCE_CONF_:COMMENT
  number = 2, //SCE_CONF_:NUMBER
  identifier = 3, //SCE_CONF_:IDENTIFIER
  extension = 4, //SCE_CONF_:EXTENSION
  parameter = 5, //SCE_CONF_:PARAMETER
  string = 6, //SCE_CONF_:STRING
  operator = 7, //SCE_CONF_:OPERATOR
  ip = 8, //SCE_CONF_:IP
  directive = 9, //SCE_CONF_:DIRECTIVE
}

// Prefix: SCE_AVE_
enum Avenue {
  lexerVal = Lexer.ave, // SCLEX_AVE
  default_ = 0, //SCE_AVE_:DEFAULT
  comment = 1, //SCE_AVE_:COMMENT
  number = 2, //SCE_AVE_:NUMBER
  word = 3, //SCE_AVE_:WORD
  string = 6, //SCE_AVE_:STRING
  enum_ = 7, //SCE_AVE_:ENUM
  stringeol = 8, //SCE_AVE_:STRINGEOL
  identifier = 9, //SCE_AVE_:IDENTIFIER
  operator = 10, //SCE_AVE_:OPERATOR
  word1 = 11, //SCE_AVE_:WORD1
  word2 = 12, //SCE_AVE_:WORD2
  word3 = 13, //SCE_AVE_:WORD3
  word4 = 14, //SCE_AVE_:WORD4
  word5 = 15, //SCE_AVE_:WORD5
  word6 = 16, //SCE_AVE_:WORD6
}

// Prefix: SCE_ADA_
enum Ada {
  lexerVal = Lexer.ada, // SCLEX_ADA
  default_ = 0, //SCE_ADA_:DEFAULT
  word = 1, //SCE_ADA_:WORD
  identifier = 2, //SCE_ADA_:IDENTIFIER
  number = 3, //SCE_ADA_:NUMBER
  delimiter = 4, //SCE_ADA_:DELIMITER
  character = 5, //SCE_ADA_:CHARACTER
  charactereol = 6, //SCE_ADA_:CHARACTEREOL
  string = 7, //SCE_ADA_:STRING
  stringeol = 8, //SCE_ADA_:STRINGEOL
  label = 9, //SCE_ADA_:LABEL
  commentline = 10, //SCE_ADA_:COMMENTLINE
  illegal = 11, //SCE_ADA_:ILLEGAL
}

// Prefix: SCE_BAAN_
enum Baan {
  lexerVal = Lexer.baan, // SCLEX_BAAN
  default_ = 0, //SCE_BAAN_:DEFAULT
  comment = 1, //SCE_BAAN_:COMMENT
  commentdoc = 2, //SCE_BAAN_:COMMENTDOC
  number = 3, //SCE_BAAN_:NUMBER
  word = 4, //SCE_BAAN_:WORD
  string = 5, //SCE_BAAN_:STRING
  preprocessor = 6, //SCE_BAAN_:PREPROCESSOR
  operator = 7, //SCE_BAAN_:OPERATOR
  identifier = 8, //SCE_BAAN_:IDENTIFIER
  stringeol = 9, //SCE_BAAN_:STRINGEOL
  word2 = 10, //SCE_BAAN_:WORD2
}

// Prefix: SCE_LISP_
enum Lisp {
  lexerVal = Lexer.lisp, // SCLEX_LISP
  default_ = 0, //SCE_LISP_:DEFAULT
  comment = 1, //SCE_LISP_:COMMENT
  number = 2, //SCE_LISP_:NUMBER
  keyword = 3, //SCE_LISP_:KEYWORD
  keywordKw = 4, //SCE_LISP_:KEYWORD_KW
  symbol = 5, //SCE_LISP_:SYMBOL
  string = 6, //SCE_LISP_:STRING
  stringeol = 8, //SCE_LISP_:STRINGEOL
  identifier = 9, //SCE_LISP_:IDENTIFIER
  operator = 10, //SCE_LISP_:OPERATOR
  special = 11, //SCE_LISP_:SPECIAL
  multiComment = 12, //SCE_LISP_:MULTI_COMMENT
}

// Prefix: SCE_EIFFEL_
enum Eiffel {
  lexerVal = Lexer.eiffel, // SCLEX_EIFFEL
  default_ = 0, //SCE_EIFFEL_:DEFAULT
  commentline = 1, //SCE_EIFFEL_:COMMENTLINE
  number = 2, //SCE_EIFFEL_:NUMBER
  word = 3, //SCE_EIFFEL_:WORD
  string = 4, //SCE_EIFFEL_:STRING
  character = 5, //SCE_EIFFEL_:CHARACTER
  operator = 6, //SCE_EIFFEL_:OPERATOR
  identifier = 7, //SCE_EIFFEL_:IDENTIFIER
  stringeol = 8, //SCE_EIFFEL_:STRINGEOL
}

// Prefix: SCE_EIFFEL_
enum EiffelKW {
  lexerVal = Lexer.eiffelkw, // SCLEX_EIFFELKW
  default_ = 0, //SCE_EIFFEL_:DEFAULT
  commentline = 1, //SCE_EIFFEL_:COMMENTLINE
  number = 2, //SCE_EIFFEL_:NUMBER
  word = 3, //SCE_EIFFEL_:WORD
  string = 4, //SCE_EIFFEL_:STRING
  character = 5, //SCE_EIFFEL_:CHARACTER
  operator = 6, //SCE_EIFFEL_:OPERATOR
  identifier = 7, //SCE_EIFFEL_:IDENTIFIER
  stringeol = 8, //SCE_EIFFEL_:STRINGEOL
}

// Prefix: SCE_NNCRONTAB_
enum NNCronTab {
  lexerVal = Lexer.nncrontab, // SCLEX_NNCRONTAB
  default_ = 0, //SCE_NNCRONTAB_:DEFAULT
  comment = 1, //SCE_NNCRONTAB_:COMMENT
  task = 2, //SCE_NNCRONTAB_:TASK
  section = 3, //SCE_NNCRONTAB_:SECTION
  keyword = 4, //SCE_NNCRONTAB_:KEYWORD
  modifier = 5, //SCE_NNCRONTAB_:MODIFIER
  asterisk = 6, //SCE_NNCRONTAB_:ASTERISK
  number = 7, //SCE_NNCRONTAB_:NUMBER
  string = 8, //SCE_NNCRONTAB_:STRING
  environment = 9, //SCE_NNCRONTAB_:ENVIRONMENT
  identifier = 10, //SCE_NNCRONTAB_:IDENTIFIER
}

// Prefix: SCE_FORTH_
enum Forth {
  lexerVal = Lexer.forth, // SCLEX_FORTH
  default_ = 0, //SCE_FORTH_:DEFAULT
  comment = 1, //SCE_FORTH_:COMMENT
  commentMl = 2, //SCE_FORTH_:COMMENT_ML
  identifier = 3, //SCE_FORTH_:IDENTIFIER
  control = 4, //SCE_FORTH_:CONTROL
  keyword = 5, //SCE_FORTH_:KEYWORD
  defword = 6, //SCE_FORTH_:DEFWORD
  preword1 = 7, //SCE_FORTH_:PREWORD1
  preword2 = 8, //SCE_FORTH_:PREWORD2
  number = 9, //SCE_FORTH_:NUMBER
  string = 10, //SCE_FORTH_:STRING
  locale = 11, //SCE_FORTH_:LOCALE
}

// Prefix: SCE_MATLAB_
enum MatLab {
  lexerVal = Lexer.matlab, // SCLEX_MATLAB
  default_ = 0, //SCE_MATLAB_:DEFAULT
  comment = 1, //SCE_MATLAB_:COMMENT
  command = 2, //SCE_MATLAB_:COMMAND
  number = 3, //SCE_MATLAB_:NUMBER
  keyword = 4, //SCE_MATLAB_:KEYWORD
  string = 5, //SCE_MATLAB_:STRING
  operator = 6, //SCE_MATLAB_:OPERATOR
  identifier = 7, //SCE_MATLAB_:IDENTIFIER
  doublequotestring = 8, //SCE_MATLAB_:DOUBLEQUOTESTRING
}

// Prefix: SCE_SCRIPTOL_
enum Sol {
  lexerVal = Lexer.scriptol, // SCLEX_SCRIPTOL
  default_ = 0, //SCE_SCRIPTOL_:DEFAULT
  white = 1, //SCE_SCRIPTOL_:WHITE
  commentline = 2, //SCE_SCRIPTOL_:COMMENTLINE
  persistent = 3, //SCE_SCRIPTOL_:PERSISTENT
  cstyle = 4, //SCE_SCRIPTOL_:CSTYLE
  commentblock = 5, //SCE_SCRIPTOL_:COMMENTBLOCK
  number = 6, //SCE_SCRIPTOL_:NUMBER
  string = 7, //SCE_SCRIPTOL_:STRING
  character = 8, //SCE_SCRIPTOL_:CHARACTER
  stringeol = 9, //SCE_SCRIPTOL_:STRINGEOL
  keyword = 10, //SCE_SCRIPTOL_:KEYWORD
  operator = 11, //SCE_SCRIPTOL_:OPERATOR
  identifier = 12, //SCE_SCRIPTOL_:IDENTIFIER
  triple = 13, //SCE_SCRIPTOL_:TRIPLE
  classname = 14, //SCE_SCRIPTOL_:CLASSNAME
  preprocessor = 15, //SCE_SCRIPTOL_:PREPROCESSOR
}

// Prefix: SCE_ASM_
enum Asm {
  lexerVal = Lexer.asm_, // SCLEX_ASM
  default_ = 0, //SCE_ASM_:DEFAULT
  comment = 1, //SCE_ASM_:COMMENT
  number = 2, //SCE_ASM_:NUMBER
  string = 3, //SCE_ASM_:STRING
  operator = 4, //SCE_ASM_:OPERATOR
  identifier = 5, //SCE_ASM_:IDENTIFIER
  cpuinstruction = 6, //SCE_ASM_:CPUINSTRUCTION
  mathinstruction = 7, //SCE_ASM_:MATHINSTRUCTION
  register = 8, //SCE_ASM_:REGISTER
  directive = 9, //SCE_ASM_:DIRECTIVE
  directiveoperand = 10, //SCE_ASM_:DIRECTIVEOPERAND
  commentblock = 11, //SCE_ASM_:COMMENTBLOCK
  character = 12, //SCE_ASM_:CHARACTER
  stringeol = 13, //SCE_ASM_:STRINGEOL
  extinstruction = 14, //SCE_ASM_:EXTINSTRUCTION
  commentdirective = 15, //SCE_ASM_:COMMENTDIRECTIVE
}

// Prefix: SCE_F_
enum Fortran {
  lexerVal = Lexer.fortran, // SCLEX_FORTRAN
  default_ = 0, //SCE_F_:DEFAULT
  comment = 1, //SCE_F_:COMMENT
  number = 2, //SCE_F_:NUMBER
  string1 = 3, //SCE_F_:STRING1
  string2 = 4, //SCE_F_:STRING2
  stringeol = 5, //SCE_F_:STRINGEOL
  operator = 6, //SCE_F_:OPERATOR
  identifier = 7, //SCE_F_:IDENTIFIER
  word = 8, //SCE_F_:WORD
  word2 = 9, //SCE_F_:WORD2
  word3 = 10, //SCE_F_:WORD3
  preprocessor = 11, //SCE_F_:PREPROCESSOR
  operator2 = 12, //SCE_F_:OPERATOR2
  label = 13, //SCE_F_:LABEL
  continuation = 14, //SCE_F_:CONTINUATION
}

// Prefix: SCE_F_
enum F77 {
  lexerVal = Lexer.f77, // SCLEX_F77
  default_ = 0, //SCE_F_:DEFAULT
  comment = 1, //SCE_F_:COMMENT
  number = 2, //SCE_F_:NUMBER
  string1 = 3, //SCE_F_:STRING1
  string2 = 4, //SCE_F_:STRING2
  stringeol = 5, //SCE_F_:STRINGEOL
  operator = 6, //SCE_F_:OPERATOR
  identifier = 7, //SCE_F_:IDENTIFIER
  word = 8, //SCE_F_:WORD
  word2 = 9, //SCE_F_:WORD2
  word3 = 10, //SCE_F_:WORD3
  preprocessor = 11, //SCE_F_:PREPROCESSOR
  operator2 = 12, //SCE_F_:OPERATOR2
  label = 13, //SCE_F_:LABEL
  continuation = 14, //SCE_F_:CONTINUATION
}

// Prefix: SCE_CSS_
enum CSS {
  lexerVal = Lexer.css, // SCLEX_CSS
  default_ = 0, //SCE_CSS_:DEFAULT
  tag = 1, //SCE_CSS_:TAG
  class_ = 2, //SCE_CSS_:CLASS
  pseudoclass = 3, //SCE_CSS_:PSEUDOCLASS
  unknownPseudoclass = 4, //SCE_CSS_:UNKNOWN_PSEUDOCLASS
  operator = 5, //SCE_CSS_:OPERATOR
  identifier = 6, //SCE_CSS_:IDENTIFIER
  unknownIdentifier = 7, //SCE_CSS_:UNKNOWN_IDENTIFIER
  value = 8, //SCE_CSS_:VALUE
  comment = 9, //SCE_CSS_:COMMENT
  id = 10, //SCE_CSS_:ID
  important = 11, //SCE_CSS_:IMPORTANT
  directive = 12, //SCE_CSS_:DIRECTIVE
  doublestring = 13, //SCE_CSS_:DOUBLESTRING
  singlestring = 14, //SCE_CSS_:SINGLESTRING
  identifier2 = 15, //SCE_CSS_:IDENTIFIER2
  attribute = 16, //SCE_CSS_:ATTRIBUTE
  identifier3 = 17, //SCE_CSS_:IDENTIFIER3
  pseudoelement = 18, //SCE_CSS_:PSEUDOELEMENT
  extendedIdentifier = 19, //SCE_CSS_:EXTENDED_IDENTIFIER
  extendedPseudoclass = 20, //SCE_CSS_:EXTENDED_PSEUDOCLASS
  extendedPseudoelement = 21, //SCE_CSS_:EXTENDED_PSEUDOELEMENT
  media = 22, //SCE_CSS_:MEDIA
  variable = 23, //SCE_CSS_:VARIABLE
}

// Prefix: SCE_POV_
enum POV {
  lexerVal = Lexer.pov, // SCLEX_POV
  default_ = 0, //SCE_POV_:DEFAULT
  comment = 1, //SCE_POV_:COMMENT
  commentline = 2, //SCE_POV_:COMMENTLINE
  number = 3, //SCE_POV_:NUMBER
  operator = 4, //SCE_POV_:OPERATOR
  identifier = 5, //SCE_POV_:IDENTIFIER
  string = 6, //SCE_POV_:STRING
  stringeol = 7, //SCE_POV_:STRINGEOL
  directive = 8, //SCE_POV_:DIRECTIVE
  baddirective = 9, //SCE_POV_:BADDIRECTIVE
  word2 = 10, //SCE_POV_:WORD2
  word3 = 11, //SCE_POV_:WORD3
  word4 = 12, //SCE_POV_:WORD4
  word5 = 13, //SCE_POV_:WORD5
  word6 = 14, //SCE_POV_:WORD6
  word7 = 15, //SCE_POV_:WORD7
  word8 = 16, //SCE_POV_:WORD8
}

// Prefix: SCE_LOUT_
enum LOUT {
  lexerVal = Lexer.lout, // SCLEX_LOUT
  default_ = 0, //SCE_LOUT_:DEFAULT
  comment = 1, //SCE_LOUT_:COMMENT
  number = 2, //SCE_LOUT_:NUMBER
  word = 3, //SCE_LOUT_:WORD
  word2 = 4, //SCE_LOUT_:WORD2
  word3 = 5, //SCE_LOUT_:WORD3
  word4 = 6, //SCE_LOUT_:WORD4
  string = 7, //SCE_LOUT_:STRING
  operator = 8, //SCE_LOUT_:OPERATOR
  identifier = 9, //SCE_LOUT_:IDENTIFIER
  stringeol = 10, //SCE_LOUT_:STRINGEOL
}

// Prefix: SCE_ESCRIPT_
enum ESCRIPT {
  lexerVal = Lexer.escript, // SCLEX_ESCRIPT
  default_ = 0, //SCE_ESCRIPT_:DEFAULT
  comment = 1, //SCE_ESCRIPT_:COMMENT
  commentline = 2, //SCE_ESCRIPT_:COMMENTLINE
  commentdoc = 3, //SCE_ESCRIPT_:COMMENTDOC
  number = 4, //SCE_ESCRIPT_:NUMBER
  word = 5, //SCE_ESCRIPT_:WORD
  string = 6, //SCE_ESCRIPT_:STRING
  operator = 7, //SCE_ESCRIPT_:OPERATOR
  identifier = 8, //SCE_ESCRIPT_:IDENTIFIER
  brace = 9, //SCE_ESCRIPT_:BRACE
  word2 = 10, //SCE_ESCRIPT_:WORD2
  word3 = 11, //SCE_ESCRIPT_:WORD3
}

// Prefix: SCE_PS_
enum PS {
  lexerVal = Lexer.ps, // SCLEX_PS
  default_ = 0, //SCE_PS_:DEFAULT
  comment = 1, //SCE_PS_:COMMENT
  dscComment = 2, //SCE_PS_:DSC_COMMENT
  dscValue = 3, //SCE_PS_:DSC_VALUE
  number = 4, //SCE_PS_:NUMBER
  name = 5, //SCE_PS_:NAME
  keyword = 6, //SCE_PS_:KEYWORD
  literal = 7, //SCE_PS_:LITERAL
  immeval = 8, //SCE_PS_:IMMEVAL
  parenArray = 9, //SCE_PS_:PAREN_ARRAY
  parenDict = 10, //SCE_PS_:PAREN_DICT
  parenProc = 11, //SCE_PS_:PAREN_PROC
  text = 12, //SCE_PS_:TEXT
  hexstring = 13, //SCE_PS_:HEXSTRING
  base85string = 14, //SCE_PS_:BASE85STRING
  badstringchar = 15, //SCE_PS_:BADSTRINGCHAR
}

// Prefix: SCE_NSIS_
enum NSIS {
  lexerVal = Lexer.nsis, // SCLEX_NSIS
  default_ = 0, //SCE_NSIS_:DEFAULT
  comment = 1, //SCE_NSIS_:COMMENT
  stringdq = 2, //SCE_NSIS_:STRINGDQ
  stringlq = 3, //SCE_NSIS_:STRINGLQ
  stringrq = 4, //SCE_NSIS_:STRINGRQ
  function_ = 5, //SCE_NSIS_:FUNCTION
  variable = 6, //SCE_NSIS_:VARIABLE
  label = 7, //SCE_NSIS_:LABEL
  userdefined = 8, //SCE_NSIS_:USERDEFINED
  sectiondef = 9, //SCE_NSIS_:SECTIONDEF
  subsectiondef = 10, //SCE_NSIS_:SUBSECTIONDEF
  ifdefinedef = 11, //SCE_NSIS_:IFDEFINEDEF
  macrodef = 12, //SCE_NSIS_:MACRODEF
  stringvar = 13, //SCE_NSIS_:STRINGVAR
  number = 14, //SCE_NSIS_:NUMBER
  sectiongroup = 15, //SCE_NSIS_:SECTIONGROUP
  pageex = 16, //SCE_NSIS_:PAGEEX
  functiondef = 17, //SCE_NSIS_:FUNCTIONDEF
  commentbox = 18, //SCE_NSIS_:COMMENTBOX
}

// Prefix: SCE_MMIXAL_
enum MMIXAL {
  lexerVal = Lexer.mmixal, // SCLEX_MMIXAL
  leadws = 0, //SCE_MMIXAL_:LEADWS
  comment = 1, //SCE_MMIXAL_:COMMENT
  label = 2, //SCE_MMIXAL_:LABEL
  opcode = 3, //SCE_MMIXAL_:OPCODE
  opcodePre = 4, //SCE_MMIXAL_:OPCODE_PRE
  opcodeValid = 5, //SCE_MMIXAL_:OPCODE_VALID
  opcodeUnknown = 6, //SCE_MMIXAL_:OPCODE_UNKNOWN
  opcodePost = 7, //SCE_MMIXAL_:OPCODE_POST
  operands = 8, //SCE_MMIXAL_:OPERANDS
  number = 9, //SCE_MMIXAL_:NUMBER
  ref_ = 10, //SCE_MMIXAL_:REF
  char_ = 11, //SCE_MMIXAL_:CHAR
  string = 12, //SCE_MMIXAL_:STRING
  register = 13, //SCE_MMIXAL_:REGISTER
  hex = 14, //SCE_MMIXAL_:HEX
  operator = 15, //SCE_MMIXAL_:OPERATOR
  symbol = 16, //SCE_MMIXAL_:SYMBOL
  include = 17, //SCE_MMIXAL_:INCLUDE
}

// Prefix: SCE_CLW_
enum Clarion {
  lexerVal = Lexer.clw, // SCLEX_CLW
  default_ = 0, //SCE_CLW_:DEFAULT
  label = 1, //SCE_CLW_:LABEL
  comment = 2, //SCE_CLW_:COMMENT
  string = 3, //SCE_CLW_:STRING
  userIdentifier = 4, //SCE_CLW_:USER_IDENTIFIER
  integerConstant = 5, //SCE_CLW_:INTEGER_CONSTANT
  realConstant = 6, //SCE_CLW_:REAL_CONSTANT
  pictureString = 7, //SCE_CLW_:PICTURE_STRING
  keyword = 8, //SCE_CLW_:KEYWORD
  compilerDirective = 9, //SCE_CLW_:COMPILER_DIRECTIVE
  runtimeExpressions = 10, //SCE_CLW_:RUNTIME_EXPRESSIONS
  builtinProceduresFunction = 11, //SCE_CLW_:BUILTIN_PROCEDURES_FUNCTION
  structureDataType = 12, //SCE_CLW_:STRUCTURE_DATA_TYPE
  attribute = 13, //SCE_CLW_:ATTRIBUTE
  standardEquate = 14, //SCE_CLW_:STANDARD_EQUATE
  error = 15, //SCE_CLW_:ERROR
  deprecated_ = 16, //SCE_CLW_:DEPRECATED
}

// Prefix: SCE_LOT_
enum LOT {
  lexerVal = Lexer.lot, // SCLEX_LOT
  default_ = 0, //SCE_LOT_:DEFAULT
  header = 1, //SCE_LOT_:HEADER
  break_ = 2, //SCE_LOT_:BREAK
  set = 3, //SCE_LOT_:SET
  pass = 4, //SCE_LOT_:PASS
  fail = 5, //SCE_LOT_:FAIL
  abort = 6, //SCE_LOT_:ABORT
}

// Prefix: SCE_YAML_
enum YAML {
  lexerVal = Lexer.yaml, // SCLEX_YAML
  default_ = 0, //SCE_YAML_:DEFAULT
  comment = 1, //SCE_YAML_:COMMENT
  identifier = 2, //SCE_YAML_:IDENTIFIER
  keyword = 3, //SCE_YAML_:KEYWORD
  number = 4, //SCE_YAML_:NUMBER
  reference = 5, //SCE_YAML_:REFERENCE
  document = 6, //SCE_YAML_:DOCUMENT
  text = 7, //SCE_YAML_:TEXT
  error = 8, //SCE_YAML_:ERROR
  operator = 9, //SCE_YAML_:OPERATOR
}

// Prefix: SCE_TEX_
enum TeX {
  lexerVal = Lexer.tex, // SCLEX_TEX
  default_ = 0, //SCE_TEX_:DEFAULT
  special = 1, //SCE_TEX_:SPECIAL
  group = 2, //SCE_TEX_:GROUP
  symbol = 3, //SCE_TEX_:SYMBOL
  command = 4, //SCE_TEX_:COMMAND
  text = 5, //SCE_TEX_:TEXT
}

// Prefix: SCE_METAPOST_
enum Metapost {
  lexerVal = Lexer.metapost, // SCLEX_METAPOST
  default_ = 0, //SCE_METAPOST_:DEFAULT
  special = 1, //SCE_METAPOST_:SPECIAL
  group = 2, //SCE_METAPOST_:GROUP
  symbol = 3, //SCE_METAPOST_:SYMBOL
  command = 4, //SCE_METAPOST_:COMMAND
  text = 5, //SCE_METAPOST_:TEXT
  extra = 6, //SCE_METAPOST_:EXTRA
}

// Prefix: SCE_ERLANG_
enum Erlang {
  lexerVal = Lexer.erlang, // SCLEX_ERLANG
  default_ = 0, //SCE_ERLANG_:DEFAULT
  comment = 1, //SCE_ERLANG_:COMMENT
  variable = 2, //SCE_ERLANG_:VARIABLE
  number = 3, //SCE_ERLANG_:NUMBER
  keyword = 4, //SCE_ERLANG_:KEYWORD
  string = 5, //SCE_ERLANG_:STRING
  operator = 6, //SCE_ERLANG_:OPERATOR
  atom = 7, //SCE_ERLANG_:ATOM
  functionName = 8, //SCE_ERLANG_:FUNCTION_NAME
  character = 9, //SCE_ERLANG_:CHARACTER
  macro_ = 10, //SCE_ERLANG_:MACRO
  record = 11, //SCE_ERLANG_:RECORD
  preproc = 12, //SCE_ERLANG_:PREPROC
  nodeName = 13, //SCE_ERLANG_:NODE_NAME
  commentFunction = 14, //SCE_ERLANG_:COMMENT_FUNCTION
  commentModule = 15, //SCE_ERLANG_:COMMENT_MODULE
  commentDoc = 16, //SCE_ERLANG_:COMMENT_DOC
  commentDocMacro = 17, //SCE_ERLANG_:COMMENT_DOC_MACRO
  atomQuoted = 18, //SCE_ERLANG_:ATOM_QUOTED
  macroQuoted = 19, //SCE_ERLANG_:MACRO_QUOTED
  recordQuoted = 20, //SCE_ERLANG_:RECORD_QUOTED
  nodeNameQuoted = 21, //SCE_ERLANG_:NODE_NAME_QUOTED
  bifs = 22, //SCE_ERLANG_:BIFS
  modules = 23, //SCE_ERLANG_:MODULES
  modulesAtt = 24, //SCE_ERLANG_:MODULES_ATT
  unknown = 31, //SCE_ERLANG_:UNKNOWN
}

// Prefix: SCE_MATLAB_
enum Octave {
  lexerVal = Lexer.octave, // SCLEX_OCTAVE
  default_ = 0, //SCE_MATLAB_:DEFAULT
  comment = 1, //SCE_MATLAB_:COMMENT
  command = 2, //SCE_MATLAB_:COMMAND
  number = 3, //SCE_MATLAB_:NUMBER
  keyword = 4, //SCE_MATLAB_:KEYWORD
  string = 5, //SCE_MATLAB_:STRING
  operator = 6, //SCE_MATLAB_:OPERATOR
  identifier = 7, //SCE_MATLAB_:IDENTIFIER
  doublequotestring = 8, //SCE_MATLAB_:DOUBLEQUOTESTRING
}

// Prefix: SCE_MSSQL_
enum MSSQL {
  lexerVal = Lexer.mssql, // SCLEX_MSSQL
  default_ = 0, //SCE_MSSQL_:DEFAULT
  comment = 1, //SCE_MSSQL_:COMMENT
  lineComment = 2, //SCE_MSSQL_:LINE_COMMENT
  number = 3, //SCE_MSSQL_:NUMBER
  string = 4, //SCE_MSSQL_:STRING
  operator = 5, //SCE_MSSQL_:OPERATOR
  identifier = 6, //SCE_MSSQL_:IDENTIFIER
  variable = 7, //SCE_MSSQL_:VARIABLE
  columnName = 8, //SCE_MSSQL_:COLUMN_NAME
  statement = 9, //SCE_MSSQL_:STATEMENT
  datatype = 10, //SCE_MSSQL_:DATATYPE
  systable = 11, //SCE_MSSQL_:SYSTABLE
  globalVariable = 12, //SCE_MSSQL_:GLOBAL_VARIABLE
  function_ = 13, //SCE_MSSQL_:FUNCTION
  storedProcedure = 14, //SCE_MSSQL_:STORED_PROCEDURE
  defaultPrefDatatype = 15, //SCE_MSSQL_:DEFAULT_PREF_DATATYPE
  columnName2 = 16, //SCE_MSSQL_:COLUMN_NAME_2
}

// Prefix: SCE_V_
enum Verilog {
  lexerVal = Lexer.verilog, // SCLEX_VERILOG
  default_ = 0, //SCE_V_:DEFAULT
  comment = 1, //SCE_V_:COMMENT
  commentline = 2, //SCE_V_:COMMENTLINE
  commentlinebang = 3, //SCE_V_:COMMENTLINEBANG
  number = 4, //SCE_V_:NUMBER
  word = 5, //SCE_V_:WORD
  string = 6, //SCE_V_:STRING
  word2 = 7, //SCE_V_:WORD2
  word3 = 8, //SCE_V_:WORD3
  preprocessor = 9, //SCE_V_:PREPROCESSOR
  operator = 10, //SCE_V_:OPERATOR
  identifier = 11, //SCE_V_:IDENTIFIER
  stringeol = 12, //SCE_V_:STRINGEOL
  user = 19, //SCE_V_:USER
}

// Prefix: SCE_KIX_
enum Kix {
  lexerVal = Lexer.kix, // SCLEX_KIX
  default_ = 0, //SCE_KIX_:DEFAULT
  comment = 1, //SCE_KIX_:COMMENT
  string1 = 2, //SCE_KIX_:STRING1
  string2 = 3, //SCE_KIX_:STRING2
  number = 4, //SCE_KIX_:NUMBER
  var = 5, //SCE_KIX_:VAR
  macro_ = 6, //SCE_KIX_:MACRO
  keyword = 7, //SCE_KIX_:KEYWORD
  functions = 8, //SCE_KIX_:FUNCTIONS
  operator = 9, //SCE_KIX_:OPERATOR
  identifier = 31, //SCE_KIX_:IDENTIFIER
}

// Prefix: SCE_SN_
enum Specman {
  lexerVal = Lexer.specman, // SCLEX_SPECMAN
  default_ = 0, //SCE_SN_:DEFAULT
  code = 1, //SCE_SN_:CODE
  commentline = 2, //SCE_SN_:COMMENTLINE
  commentlinebang = 3, //SCE_SN_:COMMENTLINEBANG
  number = 4, //SCE_SN_:NUMBER
  word = 5, //SCE_SN_:WORD
  string = 6, //SCE_SN_:STRING
  word2 = 7, //SCE_SN_:WORD2
  word3 = 8, //SCE_SN_:WORD3
  preprocessor = 9, //SCE_SN_:PREPROCESSOR
  operator = 10, //SCE_SN_:OPERATOR
  identifier = 11, //SCE_SN_:IDENTIFIER
  stringeol = 12, //SCE_SN_:STRINGEOL
  regextag = 13, //SCE_SN_:REGEXTAG
  signal = 14, //SCE_SN_:SIGNAL
  user = 19, //SCE_SN_:USER
}

// Prefix: SCE_AU3_
enum Au3 {
  lexerVal = Lexer.au3, // SCLEX_AU3
  default_ = 0, //SCE_AU3_:DEFAULT
  comment = 1, //SCE_AU3_:COMMENT
  commentblock = 2, //SCE_AU3_:COMMENTBLOCK
  number = 3, //SCE_AU3_:NUMBER
  function_ = 4, //SCE_AU3_:FUNCTION
  keyword = 5, //SCE_AU3_:KEYWORD
  macro_ = 6, //SCE_AU3_:MACRO
  string = 7, //SCE_AU3_:STRING
  operator = 8, //SCE_AU3_:OPERATOR
  variable = 9, //SCE_AU3_:VARIABLE
  sent = 10, //SCE_AU3_:SENT
  preprocessor = 11, //SCE_AU3_:PREPROCESSOR
  special = 12, //SCE_AU3_:SPECIAL
  expand = 13, //SCE_AU3_:EXPAND
  comobj = 14, //SCE_AU3_:COMOBJ
  udf = 15, //SCE_AU3_:UDF
}

// Prefix: SCE_APDL_
enum APDL {
  lexerVal = Lexer.apdl, // SCLEX_APDL
  default_ = 0, //SCE_APDL_:DEFAULT
  comment = 1, //SCE_APDL_:COMMENT
  commentblock = 2, //SCE_APDL_:COMMENTBLOCK
  number = 3, //SCE_APDL_:NUMBER
  string = 4, //SCE_APDL_:STRING
  operator = 5, //SCE_APDL_:OPERATOR
  word = 6, //SCE_APDL_:WORD
  processor = 7, //SCE_APDL_:PROCESSOR
  command = 8, //SCE_APDL_:COMMAND
  slashcommand = 9, //SCE_APDL_:SLASHCOMMAND
  starcommand = 10, //SCE_APDL_:STARCOMMAND
  argument = 11, //SCE_APDL_:ARGUMENT
  function_ = 12, //SCE_APDL_:FUNCTION
}

// Prefix: SCE_SH_
enum Bash {
  lexerVal = Lexer.bash, // SCLEX_BASH
  default_ = 0, //SCE_SH_:DEFAULT
  error = 1, //SCE_SH_:ERROR
  commentline = 2, //SCE_SH_:COMMENTLINE
  number = 3, //SCE_SH_:NUMBER
  word = 4, //SCE_SH_:WORD
  string = 5, //SCE_SH_:STRING
  character = 6, //SCE_SH_:CHARACTER
  operator = 7, //SCE_SH_:OPERATOR
  identifier = 8, //SCE_SH_:IDENTIFIER
  scalar = 9, //SCE_SH_:SCALAR
  param = 10, //SCE_SH_:PARAM
  backticks = 11, //SCE_SH_:BACKTICKS
  hereDelim = 12, //SCE_SH_:HERE_DELIM
  hereQ = 13, //SCE_SH_:HERE_Q
}

// Prefix: SCE_ASN1_
enum Asn1 {
  lexerVal = Lexer.asn1, // SCLEX_ASN1
  default_ = 0, //SCE_ASN1_:DEFAULT
  comment = 1, //SCE_ASN1_:COMMENT
  identifier = 2, //SCE_ASN1_:IDENTIFIER
  string = 3, //SCE_ASN1_:STRING
  oid = 4, //SCE_ASN1_:OID
  scalar = 5, //SCE_ASN1_:SCALAR
  keyword = 6, //SCE_ASN1_:KEYWORD
  attribute = 7, //SCE_ASN1_:ATTRIBUTE
  descriptor = 8, //SCE_ASN1_:DESCRIPTOR
  type = 9, //SCE_ASN1_:TYPE
  operator = 10, //SCE_ASN1_:OPERATOR
}

// Prefix: SCE_VHDL_
enum VHDL {
  lexerVal = Lexer.vhdl, // SCLEX_VHDL
  default_ = 0, //SCE_VHDL_:DEFAULT
  comment = 1, //SCE_VHDL_:COMMENT
  commentlinebang = 2, //SCE_VHDL_:COMMENTLINEBANG
  number = 3, //SCE_VHDL_:NUMBER
  string = 4, //SCE_VHDL_:STRING
  operator = 5, //SCE_VHDL_:OPERATOR
  identifier = 6, //SCE_VHDL_:IDENTIFIER
  stringeol = 7, //SCE_VHDL_:STRINGEOL
  keyword = 8, //SCE_VHDL_:KEYWORD
  stdoperator = 9, //SCE_VHDL_:STDOPERATOR
  attribute = 10, //SCE_VHDL_:ATTRIBUTE
  stdfunction = 11, //SCE_VHDL_:STDFUNCTION
  stdpackage = 12, //SCE_VHDL_:STDPACKAGE
  stdtype = 13, //SCE_VHDL_:STDTYPE
  userword = 14, //SCE_VHDL_:USERWORD
}

// Prefix: SCE_CAML_
enum Caml {
  lexerVal = Lexer.caml, // SCLEX_CAML
  default_ = 0, //SCE_CAML_:DEFAULT
  identifier = 1, //SCE_CAML_:IDENTIFIER
  tagname = 2, //SCE_CAML_:TAGNAME
  keyword = 3, //SCE_CAML_:KEYWORD
  keyword2 = 4, //SCE_CAML_:KEYWORD2
  keyword3 = 5, //SCE_CAML_:KEYWORD3
  linenum = 6, //SCE_CAML_:LINENUM
  operator = 7, //SCE_CAML_:OPERATOR
  number = 8, //SCE_CAML_:NUMBER
  char_ = 9, //SCE_CAML_:CHAR
  white = 10, //SCE_CAML_:WHITE
  string = 11, //SCE_CAML_:STRING
  comment = 12, //SCE_CAML_:COMMENT
  comment1 = 13, //SCE_CAML_:COMMENT1
  comment2 = 14, //SCE_CAML_:COMMENT2
  comment3 = 15, //SCE_CAML_:COMMENT3
}

// Prefix: SCE_HA_
enum Haskell {
  lexerVal = Lexer.haskell, // SCLEX_HASKELL
  default_ = 0, //SCE_HA_:DEFAULT
  identifier = 1, //SCE_HA_:IDENTIFIER
  keyword = 2, //SCE_HA_:KEYWORD
  number = 3, //SCE_HA_:NUMBER
  string = 4, //SCE_HA_:STRING
  character = 5, //SCE_HA_:CHARACTER
  class_ = 6, //SCE_HA_:CLASS
  module_ = 7, //SCE_HA_:MODULE
  capital = 8, //SCE_HA_:CAPITAL
  data = 9, //SCE_HA_:DATA
  import_ = 10, //SCE_HA_:IMPORT
  operator = 11, //SCE_HA_:OPERATOR
  instance = 12, //SCE_HA_:INSTANCE
  commentline = 13, //SCE_HA_:COMMENTLINE
  commentblock = 14, //SCE_HA_:COMMENTBLOCK
  commentblock2 = 15, //SCE_HA_:COMMENTBLOCK2
  commentblock3 = 16, //SCE_HA_:COMMENTBLOCK3
}

// Prefix: SCE_T3_
enum TADS3 {
  lexerVal = Lexer.tads3, // SCLEX_TADS3
  default_ = 0, //SCE_T3_:DEFAULT
  xDefault = 1, //SCE_T3_:X_DEFAULT
  preprocessor = 2, //SCE_T3_:PREPROCESSOR
  blockComment = 3, //SCE_T3_:BLOCK_COMMENT
  lineComment = 4, //SCE_T3_:LINE_COMMENT
  operator = 5, //SCE_T3_:OPERATOR
  keyword = 6, //SCE_T3_:KEYWORD
  number = 7, //SCE_T3_:NUMBER
  identifier = 8, //SCE_T3_:IDENTIFIER
  sString = 9, //SCE_T3_:S_STRING
  dString = 10, //SCE_T3_:D_STRING
  xString = 11, //SCE_T3_:X_STRING
  libDirective = 12, //SCE_T3_:LIB_DIRECTIVE
  msgParam = 13, //SCE_T3_:MSG_PARAM
  htmlTag = 14, //SCE_T3_:HTML_TAG
  htmlDefault = 15, //SCE_T3_:HTML_DEFAULT
  htmlString = 16, //SCE_T3_:HTML_STRING
  user1 = 17, //SCE_T3_:USER1
  user2 = 18, //SCE_T3_:USER2
  user3 = 19, //SCE_T3_:USER3
  brace = 20, //SCE_T3_:BRACE
}

// Prefix: SCE_REBOL_
enum Rebol {
  lexerVal = Lexer.rebol, // SCLEX_REBOL
  default_ = 0, //SCE_REBOL_:DEFAULT
  commentline = 1, //SCE_REBOL_:COMMENTLINE
  commentblock = 2, //SCE_REBOL_:COMMENTBLOCK
  preface = 3, //SCE_REBOL_:PREFACE
  operator = 4, //SCE_REBOL_:OPERATOR
  character = 5, //SCE_REBOL_:CHARACTER
  quotedstring = 6, //SCE_REBOL_:QUOTEDSTRING
  bracedstring = 7, //SCE_REBOL_:BRACEDSTRING
  number = 8, //SCE_REBOL_:NUMBER
  pair = 9, //SCE_REBOL_:PAIR
  tuple = 10, //SCE_REBOL_:TUPLE
  binary = 11, //SCE_REBOL_:BINARY
  money = 12, //SCE_REBOL_:MONEY
  issue = 13, //SCE_REBOL_:ISSUE
  tag = 14, //SCE_REBOL_:TAG
  file = 15, //SCE_REBOL_:FILE
  email = 16, //SCE_REBOL_:EMAIL
  url = 17, //SCE_REBOL_:URL
  date = 18, //SCE_REBOL_:DATE
  time = 19, //SCE_REBOL_:TIME
  identifier = 20, //SCE_REBOL_:IDENTIFIER
  word = 21, //SCE_REBOL_:WORD
  word2 = 22, //SCE_REBOL_:WORD2
  word3 = 23, //SCE_REBOL_:WORD3
  word4 = 24, //SCE_REBOL_:WORD4
  word5 = 25, //SCE_REBOL_:WORD5
  word6 = 26, //SCE_REBOL_:WORD6
  word7 = 27, //SCE_REBOL_:WORD7
  word8 = 28, //SCE_REBOL_:WORD8
}

// Prefix: SCE_SQL_
enum SQL {
  lexerVal = Lexer.sql, // SCLEX_SQL
  default_ = 0, //SCE_SQL_:DEFAULT
  comment = 1, //SCE_SQL_:COMMENT
  commentline = 2, //SCE_SQL_:COMMENTLINE
  commentdoc = 3, //SCE_SQL_:COMMENTDOC
  number = 4, //SCE_SQL_:NUMBER
  word = 5, //SCE_SQL_:WORD
  string = 6, //SCE_SQL_:STRING
  character = 7, //SCE_SQL_:CHARACTER
  sqlplus = 8, //SCE_SQL_:SQLPLUS
  sqlplusPrompt = 9, //SCE_SQL_:SQLPLUS_PROMPT
  operator = 10, //SCE_SQL_:OPERATOR
  identifier = 11, //SCE_SQL_:IDENTIFIER
  sqlplusComment = 13, //SCE_SQL_:SQLPLUS_COMMENT
  commentlinedoc = 15, //SCE_SQL_:COMMENTLINEDOC
  word2 = 16, //SCE_SQL_:WORD2
  commentdockeyword = 17, //SCE_SQL_:COMMENTDOCKEYWORD
  commentdockeyworderror = 18, //SCE_SQL_:COMMENTDOCKEYWORDERROR
  user1 = 19, //SCE_SQL_:USER1
  user2 = 20, //SCE_SQL_:USER2
  user3 = 21, //SCE_SQL_:USER3
  user4 = 22, //SCE_SQL_:USER4
  quotedidentifier = 23, //SCE_SQL_:QUOTEDIDENTIFIER
}

// Prefix: SCE_ST_
enum Smalltalk {
  lexerVal = Lexer.smalltalk, // SCLEX_SMALLTALK
  default_ = 0, //SCE_ST_:DEFAULT
  string = 1, //SCE_ST_:STRING
  number = 2, //SCE_ST_:NUMBER
  comment = 3, //SCE_ST_:COMMENT
  symbol = 4, //SCE_ST_:SYMBOL
  binary = 5, //SCE_ST_:BINARY
  bool_ = 6, //SCE_ST_:BOOL
  self = 7, //SCE_ST_:SELF
  super_ = 8, //SCE_ST_:SUPER
  nil = 9, //SCE_ST_:NIL
  global = 10, //SCE_ST_:GLOBAL
  return_ = 11, //SCE_ST_:RETURN
  special = 12, //SCE_ST_:SPECIAL
  kwsend = 13, //SCE_ST_:KWSEND
  assign = 14, //SCE_ST_:ASSIGN
  character = 15, //SCE_ST_:CHARACTER
  specSel = 16, //SCE_ST_:SPEC_SEL
}

// Prefix: SCE_FS_
enum FlagShip {
  lexerVal = Lexer.flagship, // SCLEX_FLAGSHIP
  default_ = 0, //SCE_FS_:DEFAULT
  comment = 1, //SCE_FS_:COMMENT
  commentline = 2, //SCE_FS_:COMMENTLINE
  commentdoc = 3, //SCE_FS_:COMMENTDOC
  commentlinedoc = 4, //SCE_FS_:COMMENTLINEDOC
  commentdockeyword = 5, //SCE_FS_:COMMENTDOCKEYWORD
  commentdockeyworderror = 6, //SCE_FS_:COMMENTDOCKEYWORDERROR
  keyword = 7, //SCE_FS_:KEYWORD
  keyword2 = 8, //SCE_FS_:KEYWORD2
  keyword3 = 9, //SCE_FS_:KEYWORD3
  keyword4 = 10, //SCE_FS_:KEYWORD4
  number = 11, //SCE_FS_:NUMBER
  string = 12, //SCE_FS_:STRING
  preprocessor = 13, //SCE_FS_:PREPROCESSOR
  operator = 14, //SCE_FS_:OPERATOR
  identifier = 15, //SCE_FS_:IDENTIFIER
  date = 16, //SCE_FS_:DATE
  stringeol = 17, //SCE_FS_:STRINGEOL
  constant = 18, //SCE_FS_:CONSTANT
  wordoperator = 19, //SCE_FS_:WORDOPERATOR
  disabledcode = 20, //SCE_FS_:DISABLEDCODE
  defaultC = 21, //SCE_FS_:DEFAULT_C
  commentdocC = 22, //SCE_FS_:COMMENTDOC_C
  commentlinedocC = 23, //SCE_FS_:COMMENTLINEDOC_C
  keywordC = 24, //SCE_FS_:KEYWORD_C
  keyword2C = 25, //SCE_FS_:KEYWORD2_C
  numberC = 26, //SCE_FS_:NUMBER_C
  stringC = 27, //SCE_FS_:STRING_C
  preprocessorC = 28, //SCE_FS_:PREPROCESSOR_C
  operatorC = 29, //SCE_FS_:OPERATOR_C
  identifierC = 30, //SCE_FS_:IDENTIFIER_C
  stringeolC = 31, //SCE_FS_:STRINGEOL_C
}

// Prefix: SCE_CSOUND_
enum Csound {
  lexerVal = Lexer.csound, // SCLEX_CSOUND
  default_ = 0, //SCE_CSOUND_:DEFAULT
  comment = 1, //SCE_CSOUND_:COMMENT
  number = 2, //SCE_CSOUND_:NUMBER
  operator = 3, //SCE_CSOUND_:OPERATOR
  instr = 4, //SCE_CSOUND_:INSTR
  identifier = 5, //SCE_CSOUND_:IDENTIFIER
  opcode = 6, //SCE_CSOUND_:OPCODE
  headerstmt = 7, //SCE_CSOUND_:HEADERSTMT
  userkeyword = 8, //SCE_CSOUND_:USERKEYWORD
  commentblock = 9, //SCE_CSOUND_:COMMENTBLOCK
  param = 10, //SCE_CSOUND_:PARAM
  arateVar = 11, //SCE_CSOUND_:ARATE_VAR
  krateVar = 12, //SCE_CSOUND_:KRATE_VAR
  irateVar = 13, //SCE_CSOUND_:IRATE_VAR
  globalVar = 14, //SCE_CSOUND_:GLOBAL_VAR
  stringeol = 15, //SCE_CSOUND_:STRINGEOL
}

// Prefix: SCE_INNO_
enum Inno {
  lexerVal = Lexer.innosetup, // SCLEX_INNOSETUP
  default_ = 0, //SCE_INNO_:DEFAULT
  comment = 1, //SCE_INNO_:COMMENT
  keyword = 2, //SCE_INNO_:KEYWORD
  parameter = 3, //SCE_INNO_:PARAMETER
  section = 4, //SCE_INNO_:SECTION
  preproc = 5, //SCE_INNO_:PREPROC
  inlineExpansion = 6, //SCE_INNO_:INLINE_EXPANSION
  commentPascal = 7, //SCE_INNO_:COMMENT_PASCAL
  keywordPascal = 8, //SCE_INNO_:KEYWORD_PASCAL
  keywordUser = 9, //SCE_INNO_:KEYWORD_USER
  stringDouble = 10, //SCE_INNO_:STRING_DOUBLE
  stringSingle = 11, //SCE_INNO_:STRING_SINGLE
  identifier = 12, //SCE_INNO_:IDENTIFIER
}

// Prefix: SCE_OPAL_
enum Opal {
  lexerVal = Lexer.opal, // SCLEX_OPAL
  space = 0, //SCE_OPAL_:SPACE
  commentBlock = 1, //SCE_OPAL_:COMMENT_BLOCK
  commentLine = 2, //SCE_OPAL_:COMMENT_LINE
  integer = 3, //SCE_OPAL_:INTEGER
  keyword = 4, //SCE_OPAL_:KEYWORD
  sort = 5, //SCE_OPAL_:SORT
  string = 6, //SCE_OPAL_:STRING
  par = 7, //SCE_OPAL_:PAR
  boolConst = 8, //SCE_OPAL_:BOOL_CONST
  default_ = 32, //SCE_OPAL_:DEFAULT
}

// Prefix: SCE_SPICE_
enum Spice {
  lexerVal = Lexer.spice, // SCLEX_SPICE
  default_ = 0, //SCE_SPICE_:DEFAULT
  identifier = 1, //SCE_SPICE_:IDENTIFIER
  keyword = 2, //SCE_SPICE_:KEYWORD
  keyword2 = 3, //SCE_SPICE_:KEYWORD2
  keyword3 = 4, //SCE_SPICE_:KEYWORD3
  number = 5, //SCE_SPICE_:NUMBER
  delimiter = 6, //SCE_SPICE_:DELIMITER
  value = 7, //SCE_SPICE_:VALUE
  commentline = 8, //SCE_SPICE_:COMMENTLINE
}

// Prefix: SCE_CMAKE_
enum CMAKE {
  lexerVal = Lexer.cmake, // SCLEX_CMAKE
  default_ = 0, //SCE_CMAKE_:DEFAULT
  comment = 1, //SCE_CMAKE_:COMMENT
  stringdq = 2, //SCE_CMAKE_:STRINGDQ
  stringlq = 3, //SCE_CMAKE_:STRINGLQ
  stringrq = 4, //SCE_CMAKE_:STRINGRQ
  commands = 5, //SCE_CMAKE_:COMMANDS
  parameters = 6, //SCE_CMAKE_:PARAMETERS
  variable = 7, //SCE_CMAKE_:VARIABLE
  userdefined = 8, //SCE_CMAKE_:USERDEFINED
  whiledef = 9, //SCE_CMAKE_:WHILEDEF
  foreachdef = 10, //SCE_CMAKE_:FOREACHDEF
  ifdefinedef = 11, //SCE_CMAKE_:IFDEFINEDEF
  macrodef = 12, //SCE_CMAKE_:MACRODEF
  stringvar = 13, //SCE_CMAKE_:STRINGVAR
  number = 14, //SCE_CMAKE_:NUMBER
}

// Prefix: SCE_GAP_
enum Gap {
  lexerVal = Lexer.gap, // SCLEX_GAP
  default_ = 0, //SCE_GAP_:DEFAULT
  identifier = 1, //SCE_GAP_:IDENTIFIER
  keyword = 2, //SCE_GAP_:KEYWORD
  keyword2 = 3, //SCE_GAP_:KEYWORD2
  keyword3 = 4, //SCE_GAP_:KEYWORD3
  keyword4 = 5, //SCE_GAP_:KEYWORD4
  string = 6, //SCE_GAP_:STRING
  char_ = 7, //SCE_GAP_:CHAR
  operator = 8, //SCE_GAP_:OPERATOR
  comment = 9, //SCE_GAP_:COMMENT
  number = 10, //SCE_GAP_:NUMBER
  stringeol = 11, //SCE_GAP_:STRINGEOL
}

// Prefix: SCE_PLM_
enum PLM {
  lexerVal = Lexer.plm, // SCLEX_PLM
  default_ = 0, //SCE_PLM_:DEFAULT
  comment = 1, //SCE_PLM_:COMMENT
  string = 2, //SCE_PLM_:STRING
  number = 3, //SCE_PLM_:NUMBER
  identifier = 4, //SCE_PLM_:IDENTIFIER
  operator = 5, //SCE_PLM_:OPERATOR
  control = 6, //SCE_PLM_:CONTROL
  keyword = 7, //SCE_PLM_:KEYWORD
}

// Prefix: SCE_4GL_
enum Progress {
  lexerVal = Lexer.progress, // SCLEX_PROGRESS
  default_ = 0, //SCE_4GL_:DEFAULT
  number = 1, //SCE_4GL_:NUMBER
  word = 2, //SCE_4GL_:WORD
  string = 3, //SCE_4GL_:STRING
  character = 4, //SCE_4GL_:CHARACTER
  preprocessor = 5, //SCE_4GL_:PREPROCESSOR
  operator = 6, //SCE_4GL_:OPERATOR
  identifier = 7, //SCE_4GL_:IDENTIFIER
  block = 8, //SCE_4GL_:BLOCK
  end = 9, //SCE_4GL_:END
  comment1 = 10, //SCE_4GL_:COMMENT1
  comment2 = 11, //SCE_4GL_:COMMENT2
  comment3 = 12, //SCE_4GL_:COMMENT3
  comment4 = 13, //SCE_4GL_:COMMENT4
  comment5 = 14, //SCE_4GL_:COMMENT5
  comment6 = 15, //SCE_4GL_:COMMENT6
  default__ = 16, //SCE_4GL_:DEFAULT_
  number_ = 17, //SCE_4GL_:NUMBER_
  word_ = 18, //SCE_4GL_:WORD_
  string_ = 19, //SCE_4GL_:STRING_
  character_ = 20, //SCE_4GL_:CHARACTER_
  preprocessor_ = 21, //SCE_4GL_:PREPROCESSOR_
  operator_ = 22, //SCE_4GL_:OPERATOR_
  identifier_ = 23, //SCE_4GL_:IDENTIFIER_
  block_ = 24, //SCE_4GL_:BLOCK_
  end_ = 25, //SCE_4GL_:END_
  comment1_ = 26, //SCE_4GL_:COMMENT1_
  comment2_ = 27, //SCE_4GL_:COMMENT2_
  comment3_ = 28, //SCE_4GL_:COMMENT3_
  comment4_ = 29, //SCE_4GL_:COMMENT4_
  comment5_ = 30, //SCE_4GL_:COMMENT5_
  comment6_ = 31, //SCE_4GL_:COMMENT6_
}

// Prefix: SCE_ABAQUS_
enum ABAQUS {
  lexerVal = Lexer.abaqus, // SCLEX_ABAQUS
  default_ = 0, //SCE_ABAQUS_:DEFAULT
  comment = 1, //SCE_ABAQUS_:COMMENT
  commentblock = 2, //SCE_ABAQUS_:COMMENTBLOCK
  number = 3, //SCE_ABAQUS_:NUMBER
  string = 4, //SCE_ABAQUS_:STRING
  operator = 5, //SCE_ABAQUS_:OPERATOR
  word = 6, //SCE_ABAQUS_:WORD
  processor = 7, //SCE_ABAQUS_:PROCESSOR
  command = 8, //SCE_ABAQUS_:COMMAND
  slashcommand = 9, //SCE_ABAQUS_:SLASHCOMMAND
  starcommand = 10, //SCE_ABAQUS_:STARCOMMAND
  argument = 11, //SCE_ABAQUS_:ARGUMENT
  function_ = 12, //SCE_ABAQUS_:FUNCTION
}

// Prefix: SCE_ASY_
enum Asymptote {
  lexerVal = Lexer.asymptote, // SCLEX_ASYMPTOTE
  default_ = 0, //SCE_ASY_:DEFAULT
  comment = 1, //SCE_ASY_:COMMENT
  commentline = 2, //SCE_ASY_:COMMENTLINE
  number = 3, //SCE_ASY_:NUMBER
  word = 4, //SCE_ASY_:WORD
  string = 5, //SCE_ASY_:STRING
  character = 6, //SCE_ASY_:CHARACTER
  operator = 7, //SCE_ASY_:OPERATOR
  identifier = 8, //SCE_ASY_:IDENTIFIER
  stringeol = 9, //SCE_ASY_:STRINGEOL
  commentlinedoc = 10, //SCE_ASY_:COMMENTLINEDOC
  word2 = 11, //SCE_ASY_:WORD2
}

// Prefix: SCE_R_
enum R {
  lexerVal = Lexer.r, // SCLEX_R
  default_ = 0, //SCE_R_:DEFAULT
  comment = 1, //SCE_R_:COMMENT
  kword = 2, //SCE_R_:KWORD
  basekword = 3, //SCE_R_:BASEKWORD
  otherkword = 4, //SCE_R_:OTHERKWORD
  number = 5, //SCE_R_:NUMBER
  string = 6, //SCE_R_:STRING
  string2 = 7, //SCE_R_:STRING2
  operator = 8, //SCE_R_:OPERATOR
  identifier = 9, //SCE_R_:IDENTIFIER
  infix = 10, //SCE_R_:INFIX
  infixeol = 11, //SCE_R_:INFIXEOL
}

// Prefix: SCE_MAGIK_
enum MagikSF {
  // lexerVal = ???, // SCLEX_MAGIKSF
  default_ = 0, //SCE_MAGIK_:DEFAULT
  comment = 1, //SCE_MAGIK_:COMMENT
  hyperComment = 16, //SCE_MAGIK_:HYPER_COMMENT
  string = 2, //SCE_MAGIK_:STRING
  character = 3, //SCE_MAGIK_:CHARACTER
  number = 4, //SCE_MAGIK_:NUMBER
  identifier = 5, //SCE_MAGIK_:IDENTIFIER
  operator = 6, //SCE_MAGIK_:OPERATOR
  flow = 7, //SCE_MAGIK_:FLOW
  container = 8, //SCE_MAGIK_:CONTAINER
  bracketBlock = 9, //SCE_MAGIK_:BRACKET_BLOCK
  braceBlock = 10, //SCE_MAGIK_:BRACE_BLOCK
  sqbracketBlock = 11, //SCE_MAGIK_:SQBRACKET_BLOCK
  unknownKeyword = 12, //SCE_MAGIK_:UNKNOWN_KEYWORD
  keyword = 13, //SCE_MAGIK_:KEYWORD
  pragma_ = 14, //SCE_MAGIK_:PRAGMA
  symbol = 15, //SCE_MAGIK_:SYMBOL
}

// Prefix: SCE_POWERSHELL_
enum PowerShell {
  lexerVal = Lexer.powershell, // SCLEX_POWERSHELL
  default_ = 0, //SCE_POWERSHELL_:DEFAULT
  comment = 1, //SCE_POWERSHELL_:COMMENT
  string = 2, //SCE_POWERSHELL_:STRING
  character = 3, //SCE_POWERSHELL_:CHARACTER
  number = 4, //SCE_POWERSHELL_:NUMBER
  variable = 5, //SCE_POWERSHELL_:VARIABLE
  operator = 6, //SCE_POWERSHELL_:OPERATOR
  identifier = 7, //SCE_POWERSHELL_:IDENTIFIER
  keyword = 8, //SCE_POWERSHELL_:KEYWORD
  cmdlet = 9, //SCE_POWERSHELL_:CMDLET
  alias_ = 10, //SCE_POWERSHELL_:ALIAS
  function_ = 11, //SCE_POWERSHELL_:FUNCTION
  user1 = 12, //SCE_POWERSHELL_:USER1
  commentstream = 13, //SCE_POWERSHELL_:COMMENTSTREAM
}

// Prefix: SCE_MYSQL_
enum MySQL {
  lexerVal = Lexer.mysql, // SCLEX_MYSQL
  default_ = 0, //SCE_MYSQL_:DEFAULT
  comment = 1, //SCE_MYSQL_:COMMENT
  commentline = 2, //SCE_MYSQL_:COMMENTLINE
  variable = 3, //SCE_MYSQL_:VARIABLE
  systemvariable = 4, //SCE_MYSQL_:SYSTEMVARIABLE
  knownsystemvariable = 5, //SCE_MYSQL_:KNOWNSYSTEMVARIABLE
  number = 6, //SCE_MYSQL_:NUMBER
  majorkeyword = 7, //SCE_MYSQL_:MAJORKEYWORD
  keyword = 8, //SCE_MYSQL_:KEYWORD
  databaseobject = 9, //SCE_MYSQL_:DATABASEOBJECT
  procedurekeyword = 10, //SCE_MYSQL_:PROCEDUREKEYWORD
  string = 11, //SCE_MYSQL_:STRING
  sqstring = 12, //SCE_MYSQL_:SQSTRING
  dqstring = 13, //SCE_MYSQL_:DQSTRING
  operator = 14, //SCE_MYSQL_:OPERATOR
  function_ = 15, //SCE_MYSQL_:FUNCTION
  identifier = 16, //SCE_MYSQL_:IDENTIFIER
  quotedidentifier = 17, //SCE_MYSQL_:QUOTEDIDENTIFIER
  user1 = 18, //SCE_MYSQL_:USER1
  user2 = 19, //SCE_MYSQL_:USER2
  user3 = 20, //SCE_MYSQL_:USER3
  hiddencommand = 21, //SCE_MYSQL_:HIDDENCOMMAND
}

// Prefix: SCE_PO_
enum Po {
  lexerVal = Lexer.po, // SCLEX_PO
  default_ = 0, //SCE_PO_:DEFAULT
  comment = 1, //SCE_PO_:COMMENT
  msgid = 2, //SCE_PO_:MSGID
  msgidText = 3, //SCE_PO_:MSGID_TEXT
  msgstr = 4, //SCE_PO_:MSGSTR
  msgstrText = 5, //SCE_PO_:MSGSTR_TEXT
  msgctxt = 6, //SCE_PO_:MSGCTXT
  msgctxtText = 7, //SCE_PO_:MSGCTXT_TEXT
  fuzzy = 8, //SCE_PO_:FUZZY
}

// Prefix: SCE_PAS_
enum Pascal {
  lexerVal = Lexer.pascal, // SCLEX_PASCAL
  default_ = 0, //SCE_PAS_:DEFAULT
  identifier = 1, //SCE_PAS_:IDENTIFIER
  comment = 2, //SCE_PAS_:COMMENT
  comment2 = 3, //SCE_PAS_:COMMENT2
  commentline = 4, //SCE_PAS_:COMMENTLINE
  preprocessor = 5, //SCE_PAS_:PREPROCESSOR
  preprocessor2 = 6, //SCE_PAS_:PREPROCESSOR2
  number = 7, //SCE_PAS_:NUMBER
  hexnumber = 8, //SCE_PAS_:HEXNUMBER
  word = 9, //SCE_PAS_:WORD
  string = 10, //SCE_PAS_:STRING
  stringeol = 11, //SCE_PAS_:STRINGEOL
  character = 12, //SCE_PAS_:CHARACTER
  operator = 13, //SCE_PAS_:OPERATOR
  asm_ = 14, //SCE_PAS_:ASM
}

// Prefix: SCE_SORCUS_
enum SORCUS {
  lexerVal = Lexer.sorcus, // SCLEX_SORCUS
  default_ = 0, //SCE_SORCUS_:DEFAULT
  command = 1, //SCE_SORCUS_:COMMAND
  parameter = 2, //SCE_SORCUS_:PARAMETER
  commentline = 3, //SCE_SORCUS_:COMMENTLINE
  string = 4, //SCE_SORCUS_:STRING
  stringeol = 5, //SCE_SORCUS_:STRINGEOL
  identifier = 6, //SCE_SORCUS_:IDENTIFIER
  operator = 7, //SCE_SORCUS_:OPERATOR
  number = 8, //SCE_SORCUS_:NUMBER
  constant = 9, //SCE_SORCUS_:CONSTANT
}

// Prefix: SCE_POWERPRO_
enum PowerPro {
  lexerVal = Lexer.powerpro, // SCLEX_POWERPRO
  default_ = 0, //SCE_POWERPRO_:DEFAULT
  commentblock = 1, //SCE_POWERPRO_:COMMENTBLOCK
  commentline = 2, //SCE_POWERPRO_:COMMENTLINE
  number = 3, //SCE_POWERPRO_:NUMBER
  word = 4, //SCE_POWERPRO_:WORD
  word2 = 5, //SCE_POWERPRO_:WORD2
  word3 = 6, //SCE_POWERPRO_:WORD3
  word4 = 7, //SCE_POWERPRO_:WORD4
  doublequotedstring = 8, //SCE_POWERPRO_:DOUBLEQUOTEDSTRING
  singlequotedstring = 9, //SCE_POWERPRO_:SINGLEQUOTEDSTRING
  linecontinue = 10, //SCE_POWERPRO_:LINECONTINUE
  operator = 11, //SCE_POWERPRO_:OPERATOR
  identifier = 12, //SCE_POWERPRO_:IDENTIFIER
  stringeol = 13, //SCE_POWERPRO_:STRINGEOL
  verbatim = 14, //SCE_POWERPRO_:VERBATIM
  altquote = 15, //SCE_POWERPRO_:ALTQUOTE
  function_ = 16, //SCE_POWERPRO_:FUNCTION
}

// Prefix: SCE_SML_
enum SML {
  lexerVal = Lexer.sml, // SCLEX_SML
  default_ = 0, //SCE_SML_:DEFAULT
  identifier = 1, //SCE_SML_:IDENTIFIER
  tagname = 2, //SCE_SML_:TAGNAME
  keyword = 3, //SCE_SML_:KEYWORD
  keyword2 = 4, //SCE_SML_:KEYWORD2
  keyword3 = 5, //SCE_SML_:KEYWORD3
  linenum = 6, //SCE_SML_:LINENUM
  operator = 7, //SCE_SML_:OPERATOR
  number = 8, //SCE_SML_:NUMBER
  char_ = 9, //SCE_SML_:CHAR
  string = 11, //SCE_SML_:STRING
  comment = 12, //SCE_SML_:COMMENT
  comment1 = 13, //SCE_SML_:COMMENT1
  comment2 = 14, //SCE_SML_:COMMENT2
  comment3 = 15, //SCE_SML_:COMMENT3
}

// Prefix: SCE_MARKDOWN_
enum Markdown {
  lexerVal = Lexer.markdown, // SCLEX_MARKDOWN
  default_ = 0, //SCE_MARKDOWN_:DEFAULT
  lineBegin = 1, //SCE_MARKDOWN_:LINE_BEGIN
  strong1 = 2, //SCE_MARKDOWN_:STRONG1
  strong2 = 3, //SCE_MARKDOWN_:STRONG2
  em1 = 4, //SCE_MARKDOWN_:EM1
  em2 = 5, //SCE_MARKDOWN_:EM2
  header1 = 6, //SCE_MARKDOWN_:HEADER1
  header2 = 7, //SCE_MARKDOWN_:HEADER2
  header3 = 8, //SCE_MARKDOWN_:HEADER3
  header4 = 9, //SCE_MARKDOWN_:HEADER4
  header5 = 10, //SCE_MARKDOWN_:HEADER5
  header6 = 11, //SCE_MARKDOWN_:HEADER6
  prechar = 12, //SCE_MARKDOWN_:PRECHAR
  ulistItem = 13, //SCE_MARKDOWN_:ULIST_ITEM
  olistItem = 14, //SCE_MARKDOWN_:OLIST_ITEM
  blockquote = 15, //SCE_MARKDOWN_:BLOCKQUOTE
  strikeout = 16, //SCE_MARKDOWN_:STRIKEOUT
  hrule = 17, //SCE_MARKDOWN_:HRULE
  link = 18, //SCE_MARKDOWN_:LINK
  code = 19, //SCE_MARKDOWN_:CODE
  code2 = 20, //SCE_MARKDOWN_:CODE2
  codebk = 21, //SCE_MARKDOWN_:CODEBK
}

// Prefix: SCE_TXT2TAGS_
enum Txt2tags {
  lexerVal = Lexer.txt2tags, // SCLEX_TXT2TAGS
  default_ = 0, //SCE_TXT2TAGS_:DEFAULT
  lineBegin = 1, //SCE_TXT2TAGS_:LINE_BEGIN
  strong1 = 2, //SCE_TXT2TAGS_:STRONG1
  strong2 = 3, //SCE_TXT2TAGS_:STRONG2
  em1 = 4, //SCE_TXT2TAGS_:EM1
  em2 = 5, //SCE_TXT2TAGS_:EM2
  header1 = 6, //SCE_TXT2TAGS_:HEADER1
  header2 = 7, //SCE_TXT2TAGS_:HEADER2
  header3 = 8, //SCE_TXT2TAGS_:HEADER3
  header4 = 9, //SCE_TXT2TAGS_:HEADER4
  header5 = 10, //SCE_TXT2TAGS_:HEADER5
  header6 = 11, //SCE_TXT2TAGS_:HEADER6
  prechar = 12, //SCE_TXT2TAGS_:PRECHAR
  ulistItem = 13, //SCE_TXT2TAGS_:ULIST_ITEM
  olistItem = 14, //SCE_TXT2TAGS_:OLIST_ITEM
  blockquote = 15, //SCE_TXT2TAGS_:BLOCKQUOTE
  strikeout = 16, //SCE_TXT2TAGS_:STRIKEOUT
  hrule = 17, //SCE_TXT2TAGS_:HRULE
  link = 18, //SCE_TXT2TAGS_:LINK
  code = 19, //SCE_TXT2TAGS_:CODE
  code2 = 20, //SCE_TXT2TAGS_:CODE2
  codebk = 21, //SCE_TXT2TAGS_:CODEBK
  comment = 22, //SCE_TXT2TAGS_:COMMENT
  option = 23, //SCE_TXT2TAGS_:OPTION
  preproc = 24, //SCE_TXT2TAGS_:PREPROC
  postproc = 25, //SCE_TXT2TAGS_:POSTPROC
}

// Prefix: SCE_A68K_
enum A68k {
  lexerVal = Lexer.a68k, // SCLEX_A68K
  default_ = 0, //SCE_A68K_:DEFAULT
  comment = 1, //SCE_A68K_:COMMENT
  numberDec = 2, //SCE_A68K_:NUMBER_DEC
  numberBin = 3, //SCE_A68K_:NUMBER_BIN
  numberHex = 4, //SCE_A68K_:NUMBER_HEX
  string1 = 5, //SCE_A68K_:STRING1
  operator = 6, //SCE_A68K_:OPERATOR
  cpuinstruction = 7, //SCE_A68K_:CPUINSTRUCTION
  extinstruction = 8, //SCE_A68K_:EXTINSTRUCTION
  register = 9, //SCE_A68K_:REGISTER
  directive = 10, //SCE_A68K_:DIRECTIVE
  macroArg = 11, //SCE_A68K_:MACRO_ARG
  label = 12, //SCE_A68K_:LABEL
  string2 = 13, //SCE_A68K_:STRING2
  identifier = 14, //SCE_A68K_:IDENTIFIER
  macroDeclaration = 15, //SCE_A68K_:MACRO_DECLARATION
  commentWord = 16, //SCE_A68K_:COMMENT_WORD
  commentSpecial = 17, //SCE_A68K_:COMMENT_SPECIAL
  commentDoxygen = 18, //SCE_A68K_:COMMENT_DOXYGEN
}

// Prefix: SCE_MODULA_
enum Modula {
  lexerVal = Lexer.modula, // SCLEX_MODULA
  default_ = 0, //SCE_MODULA_:DEFAULT
  comment = 1, //SCE_MODULA_:COMMENT
  doxycomm = 2, //SCE_MODULA_:DOXYCOMM
  doxykey = 3, //SCE_MODULA_:DOXYKEY
  keyword = 4, //SCE_MODULA_:KEYWORD
  reserved = 5, //SCE_MODULA_:RESERVED
  number = 6, //SCE_MODULA_:NUMBER
  basenum = 7, //SCE_MODULA_:BASENUM
  float_ = 8, //SCE_MODULA_:FLOAT
  string = 9, //SCE_MODULA_:STRING
  strspec = 10, //SCE_MODULA_:STRSPEC
  char_ = 11, //SCE_MODULA_:CHAR
  charspec = 12, //SCE_MODULA_:CHARSPEC
  proc = 13, //SCE_MODULA_:PROC
  pragma_ = 14, //SCE_MODULA_:PRAGMA
  prgkey = 15, //SCE_MODULA_:PRGKEY
  operator = 16, //SCE_MODULA_:OPERATOR
  badstr = 17, //SCE_MODULA_:BADSTR
}

// Prefix: SCE_COFFEESCRIPT_
enum CoffeeScript {
  lexerVal = Lexer.coffeescript, // SCLEX_COFFEESCRIPT
  default_ = 0, //SCE_COFFEESCRIPT_:DEFAULT
  comment = 1, //SCE_COFFEESCRIPT_:COMMENT
  commentline = 2, //SCE_COFFEESCRIPT_:COMMENTLINE
  commentdoc = 3, //SCE_COFFEESCRIPT_:COMMENTDOC
  number = 4, //SCE_COFFEESCRIPT_:NUMBER
  word = 5, //SCE_COFFEESCRIPT_:WORD
  string = 6, //SCE_COFFEESCRIPT_:STRING
  character = 7, //SCE_COFFEESCRIPT_:CHARACTER
  uuid = 8, //SCE_COFFEESCRIPT_:UUID
  preprocessor = 9, //SCE_COFFEESCRIPT_:PREPROCESSOR
  operator = 10, //SCE_COFFEESCRIPT_:OPERATOR
  identifier = 11, //SCE_COFFEESCRIPT_:IDENTIFIER
  stringeol = 12, //SCE_COFFEESCRIPT_:STRINGEOL
  verbatim = 13, //SCE_COFFEESCRIPT_:VERBATIM
  regex = 14, //SCE_COFFEESCRIPT_:REGEX
  commentlinedoc = 15, //SCE_COFFEESCRIPT_:COMMENTLINEDOC
  word2 = 16, //SCE_COFFEESCRIPT_:WORD2
  commentdockeyword = 17, //SCE_COFFEESCRIPT_:COMMENTDOCKEYWORD
  commentdockeyworderror = 18, //SCE_COFFEESCRIPT_:COMMENTDOCKEYWORDERROR
  globalclass = 19, //SCE_COFFEESCRIPT_:GLOBALCLASS
  stringraw = 20, //SCE_COFFEESCRIPT_:STRINGRAW
  tripleverbatim = 21, //SCE_COFFEESCRIPT_:TRIPLEVERBATIM
  hashquotedstring = 22, //SCE_COFFEESCRIPT_:HASHQUOTEDSTRING
  commentblock = 22, //SCE_COFFEESCRIPT_:COMMENTBLOCK
  verboseRegex = 23, //SCE_COFFEESCRIPT_:VERBOSE_REGEX
  verboseRegexComment = 24, //SCE_COFFEESCRIPT_:VERBOSE_REGEX_COMMENT
}

// Prefix: SCE_AVS_
enum AVS {
  lexerVal = Lexer.avs, // SCLEX_AVS
  default_ = 0, //SCE_AVS_:DEFAULT
  commentblock = 1, //SCE_AVS_:COMMENTBLOCK
  commentblockn = 2, //SCE_AVS_:COMMENTBLOCKN
  commentline = 3, //SCE_AVS_:COMMENTLINE
  number = 4, //SCE_AVS_:NUMBER
  operator = 5, //SCE_AVS_:OPERATOR
  identifier = 6, //SCE_AVS_:IDENTIFIER
  string = 7, //SCE_AVS_:STRING
  triplestring = 8, //SCE_AVS_:TRIPLESTRING
  keyword = 9, //SCE_AVS_:KEYWORD
  filter = 10, //SCE_AVS_:FILTER
  plugin = 11, //SCE_AVS_:PLUGIN
  function_ = 12, //SCE_AVS_:FUNCTION
  clipprop = 13, //SCE_AVS_:CLIPPROP
  userdfn = 14, //SCE_AVS_:USERDFN
}

// Prefix: SCE_ECL_
enum ECL {
  lexerVal = Lexer.ecl, // SCLEX_ECL
  default_ = 0, //SCE_ECL_:DEFAULT
  comment = 1, //SCE_ECL_:COMMENT
  commentline = 2, //SCE_ECL_:COMMENTLINE
  number = 3, //SCE_ECL_:NUMBER
  string = 4, //SCE_ECL_:STRING
  word0 = 5, //SCE_ECL_:WORD0
  operator = 6, //SCE_ECL_:OPERATOR
  character = 7, //SCE_ECL_:CHARACTER
  uuid = 8, //SCE_ECL_:UUID
  preprocessor = 9, //SCE_ECL_:PREPROCESSOR
  unknown = 10, //SCE_ECL_:UNKNOWN
  identifier = 11, //SCE_ECL_:IDENTIFIER
  stringeol = 12, //SCE_ECL_:STRINGEOL
  verbatim = 13, //SCE_ECL_:VERBATIM
  regex = 14, //SCE_ECL_:REGEX
  commentlinedoc = 15, //SCE_ECL_:COMMENTLINEDOC
  word1 = 16, //SCE_ECL_:WORD1
  commentdockeyword = 17, //SCE_ECL_:COMMENTDOCKEYWORD
  commentdockeyworderror = 18, //SCE_ECL_:COMMENTDOCKEYWORDERROR
  word2 = 19, //SCE_ECL_:WORD2
  word3 = 20, //SCE_ECL_:WORD3
  word4 = 21, //SCE_ECL_:WORD4
  word5 = 22, //SCE_ECL_:WORD5
  commentdoc = 23, //SCE_ECL_:COMMENTDOC
  added = 24, //SCE_ECL_:ADDED
  deleted = 25, //SCE_ECL_:DELETED
  changed = 26, //SCE_ECL_:CHANGED
  moved = 27, //SCE_ECL_:MOVED
}
