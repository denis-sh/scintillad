﻿// Generated from Scintilla.iface, Basics category
module scintilla.iscintilla;

import scintilla.types, scintilla.internal.functions;

interface IScintilla {
	sptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam);

	/// Add text to the document at current position.
	final void addText(in char[] text) { // AddText
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2001, __len+1, cast(size_t) __cstr_text);
	}

	/// Add array of cells to document.
	final void addStyledText(int length, in Cell[] c) { // AddStyledText
		sendMessageDirect(2002, length, cast(size_t) c.ptr);
	}

	/// Insert string at a position.
	final void insertText(Position pos, in char[] text) { // InsertText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2003, pos, cast(size_t) __cstr_text);
	}

	/// Delete all text in the document.
	final void clearAll() { // ClearAll
		sendMessageDirect(2004, 0, 0);
	}

	/// Set all style bytes to 0, remove all folding information.
	final void clearDocumentStyle() { // ClearDocumentStyle
		sendMessageDirect(2005, 0, 0);
	}

	/// Returns the number of bytes in the document.
	final @property int length() { // GetLength
		return sendMessageDirect(2006, 0, 0);
	}

	/// Returns the character byte at the position.
	final int getCharAt(Position pos) { // GetCharAt
		return sendMessageDirect(2007, pos, 0);
	}

	/// Returns the position of the caret.
	final @property Position currentPos() { // GetCurrentPos
		return sendMessageDirect(2008, 0, 0);
	}

	/// Returns the position of the opposite end of the selection to the caret.
	final @property Position anchor() { // GetAnchor
		return sendMessageDirect(2009, 0, 0);
	}

	/// Returns the style byte at the position.
	final int getStyleAt(Position pos) { // GetStyleAt
		return sendMessageDirect(2010, pos, 0);
	}

	/// Redoes the next action on the undo history.
	final void redo() { // Redo
		sendMessageDirect(2011, 0, 0);
	}

	/// Choose between collecting actions into the undo
	/// history and discarding them.
	final @property void undoCollection(bool collectUndo) { // SetUndoCollection
		sendMessageDirect(2012, collectUndo, 0);
	}

	/// Select all the text in the document.
	final void selectAll() { // SelectAll
		sendMessageDirect(2013, 0, 0);
	}

	/// Remember the current position in the undo history as the position
	/// at which the document was saved.
	final void setSavePoint() { // SetSavePoint
		sendMessageDirect(2014, 0, 0);
	}

	/// Retrieve a buffer of cells.
	/// Returns the number of bytes in the buffer not including terminating NULs.
	final int getStyledText(ref TextRange tr) { // GetStyledText
		return sendMessageDirect(2015, 0, cast(size_t) &tr);
	}

	/// Are there any redoable actions in the undo history?
	final bool canRedo() { // CanRedo
		return !!sendMessageDirect(2016, 0, 0);
	}

	/// Retrieve the line number at which a particular marker is located.
	final int markerLineFromHandle(int handle) { // MarkerLineFromHandle
		return sendMessageDirect(2017, handle, 0);
	}

	/// Delete a marker.
	final void markerDeleteHandle(int handle) { // MarkerDeleteHandle
		sendMessageDirect(2018, handle, 0);
	}

	/// Is undo history being collected?
	final @property bool undoCollection() { // GetUndoCollection
		return !!sendMessageDirect(2019, 0, 0);
	}

	/// Are white space characters currently visible?
	/// Returns one of SCWS_* constants.
	final @property int viewWS() { // GetViewWS
		return sendMessageDirect(2020, 0, 0);
	}

	/// Make white space characters invisible, always visible or visible outside indentation.
	final @property void viewWS(int viewWS) { // SetViewWS
		sendMessageDirect(2021, viewWS, 0);
	}

	/// Find the position from a point within the window.
	final Position positionFromPoint(int x, int y) { // PositionFromPoint
		return sendMessageDirect(2022, x, y);
	}

	/// Find the position from a point within the window but return
	/// INVALID_POSITION if not close to text.
	final Position positionFromPointClose(int x, int y) { // PositionFromPointClose
		return sendMessageDirect(2023, x, y);
	}

	/// Set caret to start of a line and ensure it is visible.
	final void gotoLine(int line) { // GotoLine
		sendMessageDirect(2024, line, 0);
	}

	/// Set caret to a position and ensure it is visible.
	final void gotoPos(Position pos) { // GotoPos
		sendMessageDirect(2025, pos, 0);
	}

	/// Set the selection anchor to a position. The anchor is the opposite
	/// end of the selection from the caret.
	final @property void anchor(Position posAnchor) { // SetAnchor
		sendMessageDirect(2026, posAnchor, 0);
	}

	/// Retrieve the text of the line containing the caret.
	/// Returns the index of the caret on the line.
	final int getCurLine(out string text) { // GetCurLine
		immutable __len = sendMessageDirect(2027, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2027, __len+1, cast(size_t) __optr);
		text = toDString(__len, __optr);
		return __res;
	}

	/// ditto
	final int getCurLine(char[] textBuff, out char[] text) {
		alias textBuff buff;
		if(textBuff) {
			immutable __len = sendMessageDirect(2027, 0, 0);
			immutable __res = sendMessageDirect(2027, buff.length, cast(size_t) buff.ptr);
			text = toDStringBuff(__len, textBuff);
			return __res;
		} else
			return sendMessageDirect(2027, 0, 0);
	}

	/// Retrieve the position of the last correctly styled character.
	final @property Position endStyled() { // GetEndStyled
		return sendMessageDirect(2028, 0, 0);
	}

	/// Convert all line endings in the document to one mode.
	final void convertEOLs(int eolMode) { // ConvertEOLs
		sendMessageDirect(2029, eolMode, 0);
	}

	/// Retrieve the current end of line mode - one of CRLF, CR, or LF.
	final @property int eOLMode() { // GetEOLMode
		return sendMessageDirect(2030, 0, 0);
	}

	/// Set the current end of line mode.
	final @property void eOLMode(int eolMode) { // SetEOLMode
		sendMessageDirect(2031, eolMode, 0);
	}

	/// Set the current styling position to pos and the styling mask to mask.
	/// The styling mask can be used to protect some bits in each styling byte from modification.
	final void startStyling(Position pos, int mask) { // StartStyling
		sendMessageDirect(2032, pos, mask);
	}

	/// Change style from current styling position for length characters to a style
	/// and move the current styling position to after this newly styled segment.
	final void setStyling(int length, int style) { // SetStyling
		sendMessageDirect(2033, length, style);
	}

	/// Is drawing done first into a buffer or direct to the screen?
	final @property bool bufferedDraw() { // GetBufferedDraw
		return !!sendMessageDirect(2034, 0, 0);
	}

	/// If drawing is buffered then each line of text is drawn into a bitmap buffer
	/// before drawing it to the screen to avoid flicker.
	final @property void bufferedDraw(bool buffered) { // SetBufferedDraw
		sendMessageDirect(2035, buffered, 0);
	}

	/// Change the visible size of a tab to be a multiple of the width of a space character.
	final @property void tabWidth(int tabWidth) { // SetTabWidth
		sendMessageDirect(2036, tabWidth, 0);
	}

	/// Retrieve the visible size of a tab.
	final @property int tabWidth() { // GetTabWidth
		return sendMessageDirect(2121, 0, 0);
	}

	/// Set the code page used to interpret the bytes of the document as characters.
	/// The SC_CP_UTF8 value can be used to enter Unicode mode.
	final @property void codePage(int codePage) { // SetCodePage
		sendMessageDirect(2037, codePage, 0);
	}

	/// Set the symbol used for a particular marker number.
	final void markerDefine(int markerNumber, int markerSymbol) { // MarkerDefine
		sendMessageDirect(2040, markerNumber, markerSymbol);
	}

	/// Set the foreground colour used for a particular marker number.
	final void markerSetFore(int markerNumber, Colour fore) { // MarkerSetFore
		sendMessageDirect(2041, markerNumber, fore.rgb);
	}

	/// Set the background colour used for a particular marker number.
	final void markerSetBack(int markerNumber, Colour back) { // MarkerSetBack
		sendMessageDirect(2042, markerNumber, back.rgb);
	}

	/// Set the background colour used for a particular marker number when its folding block is selected.
	final void markerSetBackSelected(int markerNumber, Colour back) { // MarkerSetBackSelected
		sendMessageDirect(2292, markerNumber, back.rgb);
	}

	/// Enable/disable highlight for current folding bloc (smallest one that contains the caret)
	final void markerEnableHighlight(bool enabled) { // MarkerEnableHighlight
		sendMessageDirect(2293, enabled, 0);
	}

	/// Add a marker to a line, returning an ID which can be used to find or delete the marker.
	final int markerAdd(int line, int markerNumber) { // MarkerAdd
		return sendMessageDirect(2043, line, markerNumber);
	}

	/// Delete a marker from a line.
	final void markerDelete(int line, int markerNumber) { // MarkerDelete
		sendMessageDirect(2044, line, markerNumber);
	}

	/// Delete all markers with a particular number from all lines.
	final void markerDeleteAll(int markerNumber) { // MarkerDeleteAll
		sendMessageDirect(2045, markerNumber, 0);
	}

	/// Get a bit mask of all the markers set on a line.
	final int markerGet(int line) { // MarkerGet
		return sendMessageDirect(2046, line, 0);
	}

	/// Find the next line at or after lineStart that includes a marker in mask.
	/// Return -1 when no more lines.
	final int markerNext(int lineStart, int markerMask) { // MarkerNext
		return sendMessageDirect(2047, lineStart, markerMask);
	}

	/// Find the previous line before lineStart that includes a marker in mask.
	final int markerPrevious(int lineStart, int markerMask) { // MarkerPrevious
		return sendMessageDirect(2048, lineStart, markerMask);
	}

	/// Define a marker from a pixmap.
	final void markerDefinePixmap(int markerNumber, in char[] pixmap) { // MarkerDefinePixmap
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixmap = toCString(__sibuff, pixmap, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2049, markerNumber, cast(size_t) __cstr_pixmap);
	}

	/// Add a set of markers to a line.
	final void markerAddSet(int line, int set) { // MarkerAddSet
		sendMessageDirect(2466, line, set);
	}

	/// Set the alpha used for a marker that is drawn in the text area, not the margin.
	final void markerSetAlpha(int markerNumber, int alpha) { // MarkerSetAlpha
		sendMessageDirect(2476, markerNumber, alpha);
	}

	/// Set a margin to be either numeric or symbolic.
	final void setMarginTypeN(int margin, int marginType) { // SetMarginTypeN
		sendMessageDirect(2240, margin, marginType);
	}

	/// Retrieve the type of a margin.
	final int getMarginTypeN(int margin) { // GetMarginTypeN
		return sendMessageDirect(2241, margin, 0);
	}

	/// Set the width of a margin to a width expressed in pixels.
	final void setMarginWidthN(int margin, int pixelWidth) { // SetMarginWidthN
		sendMessageDirect(2242, margin, pixelWidth);
	}

	/// Retrieve the width of a margin in pixels.
	final int getMarginWidthN(int margin) { // GetMarginWidthN
		return sendMessageDirect(2243, margin, 0);
	}

	/// Set a mask that determines which markers are displayed in a margin.
	final void setMarginMaskN(int margin, int mask) { // SetMarginMaskN
		sendMessageDirect(2244, margin, mask);
	}

	/// Retrieve the marker mask of a margin.
	final int getMarginMaskN(int margin) { // GetMarginMaskN
		return sendMessageDirect(2245, margin, 0);
	}

	/// Make a margin sensitive or insensitive to mouse clicks.
	final void setMarginSensitiveN(int margin, bool sensitive) { // SetMarginSensitiveN
		sendMessageDirect(2246, margin, sensitive);
	}

	/// Retrieve the mouse click sensitivity of a margin.
	final bool getMarginSensitiveN(int margin) { // GetMarginSensitiveN
		return !!sendMessageDirect(2247, margin, 0);
	}

	/// Set the cursor shown when the mouse is inside a margin.
	final void setMarginCursorN(int margin, int cursor) { // SetMarginCursorN
		sendMessageDirect(2248, margin, cursor);
	}

	/// Retrieve the cursor shown in a margin.
	final int getMarginCursorN(int margin) { // GetMarginCursorN
		return sendMessageDirect(2249, margin, 0);
	}

	/// Clear all the styles and make equivalent to the global default style.
	final void styleClearAll() { // StyleClearAll
		sendMessageDirect(2050, 0, 0);
	}

	/// Set the foreground colour of a style.
	final void styleSetFore(int style, Colour fore) { // StyleSetFore
		sendMessageDirect(2051, style, fore.rgb);
	}

	/// Set the background colour of a style.
	final void styleSetBack(int style, Colour back) { // StyleSetBack
		sendMessageDirect(2052, style, back.rgb);
	}

	/// Set a style to be bold or not.
	final void styleSetBold(int style, bool bold) { // StyleSetBold
		sendMessageDirect(2053, style, bold);
	}

	/// Set a style to be italic or not.
	final void styleSetItalic(int style, bool italic) { // StyleSetItalic
		sendMessageDirect(2054, style, italic);
	}

	/// Set the size of characters of a style.
	final void styleSetSize(int style, int sizePoints) { // StyleSetSize
		sendMessageDirect(2055, style, sizePoints);
	}

	/// Set the font of a style.
	final void styleSetFont(int style, in char[] fontName) { // StyleSetFont
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_fontName = toCString(__sibuff, fontName, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2056, style, cast(size_t) __cstr_fontName);
	}

	/// Set a style to have its end of line filled or not.
	final void styleSetEOLFilled(int style, bool filled) { // StyleSetEOLFilled
		sendMessageDirect(2057, style, filled);
	}

	/// Reset the default style to its state at startup
	final void styleResetDefault() { // StyleResetDefault
		sendMessageDirect(2058, 0, 0);
	}

	/// Set a style to be underlined or not.
	final void styleSetUnderline(int style, bool underline) { // StyleSetUnderline
		sendMessageDirect(2059, style, underline);
	}

	/// Get the foreground colour of a style.
	final Colour styleGetFore(int style) { // StyleGetFore
		return cast(Colour)sendMessageDirect(2481, style, 0);
	}

	/// Get the background colour of a style.
	final Colour styleGetBack(int style) { // StyleGetBack
		return cast(Colour)sendMessageDirect(2482, style, 0);
	}

	/// Get is a style bold or not.
	final bool styleGetBold(int style) { // StyleGetBold
		return !!sendMessageDirect(2483, style, 0);
	}

	/// Get is a style italic or not.
	final bool styleGetItalic(int style) { // StyleGetItalic
		return !!sendMessageDirect(2484, style, 0);
	}

	/// Get the size of characters of a style.
	final int styleGetSize(int style) { // StyleGetSize
		return sendMessageDirect(2485, style, 0);
	}

	/// Get the font of a style.
	/// Returns the length of the fontName
	final string styleGetFont(int style) { // StyleGetFont
		immutable __len = sendMessageDirect(2486, style, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2486, style, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int styleGetFont(char[] fontNameBuff, int style, out char[] fontName) {
		alias fontNameBuff buff;
		return toDStringBuff(sendMessageDirect(2486, style, cast(size_t) buff.ptr), fontNameBuff, fontName);
	}

	/// Get is a style to have its end of line filled or not.
	final bool styleGetEOLFilled(int style) { // StyleGetEOLFilled
		return !!sendMessageDirect(2487, style, 0);
	}

	/// Get is a style underlined or not.
	final bool styleGetUnderline(int style) { // StyleGetUnderline
		return !!sendMessageDirect(2488, style, 0);
	}

	/// Get is a style mixed case, or to force upper or lower case.
	final int styleGetCase(int style) { // StyleGetCase
		return sendMessageDirect(2489, style, 0);
	}

	/// Get the character get of the font in a style.
	final int styleGetCharacterSet(int style) { // StyleGetCharacterSet
		return sendMessageDirect(2490, style, 0);
	}

	/// Get is a style visible or not.
	final bool styleGetVisible(int style) { // StyleGetVisible
		return !!sendMessageDirect(2491, style, 0);
	}

	/// Get is a style changeable or not (read only).
	/// Experimental feature, currently buggy.
	final bool styleGetChangeable(int style) { // StyleGetChangeable
		return !!sendMessageDirect(2492, style, 0);
	}

	/// Get is a style a hotspot or not.
	final bool styleGetHotSpot(int style) { // StyleGetHotSpot
		return !!sendMessageDirect(2493, style, 0);
	}

	/// Set a style to be mixed case, or to force upper or lower case.
	final void styleSetCase(int style, int caseForce) { // StyleSetCase
		sendMessageDirect(2060, style, caseForce);
	}

	/// Set the size of characters of a style. Size is in points multiplied by 100.
	final void styleSetSizeFractional(int style, int caseForce) { // StyleSetSizeFractional
		sendMessageDirect(2061, style, caseForce);
	}

	/// Get the size of characters of a style in points multiplied by 100
	final int styleGetSizeFractional(int style) { // StyleGetSizeFractional
		return sendMessageDirect(2062, style, 0);
	}

	/// Set the weight of characters of a style.
	final void styleSetWeight(int style, int weight) { // StyleSetWeight
		sendMessageDirect(2063, style, weight);
	}

	/// Get the weight of characters of a style.
	final int styleGetWeight(int style) { // StyleGetWeight
		return sendMessageDirect(2064, style, 0);
	}

	/// Set the character set of the font in a style.
	final void styleSetCharacterSet(int style, int characterSet) { // StyleSetCharacterSet
		sendMessageDirect(2066, style, characterSet);
	}

	/// Set a style to be a hotspot or not.
	final void styleSetHotSpot(int style, bool hotspot) { // StyleSetHotSpot
		sendMessageDirect(2409, style, hotspot);
	}

	/// Set the foreground colour of the main and additional selections and whether to use this setting.
	final void setSelFore(bool useSetting, Colour fore) { // SetSelFore
		sendMessageDirect(2067, useSetting, fore.rgb);
	}

	/// Set the background colour of the main and additional selections and whether to use this setting.
	final void setSelBack(bool useSetting, Colour back) { // SetSelBack
		sendMessageDirect(2068, useSetting, back.rgb);
	}

	/// Get the alpha of the selection.
	final @property int selAlpha() { // GetSelAlpha
		return sendMessageDirect(2477, 0, 0);
	}

	/// Set the alpha of the selection.
	final @property void selAlpha(int alpha) { // SetSelAlpha
		sendMessageDirect(2478, alpha, 0);
	}

	/// Is the selection end of line filled?
	final @property bool selEOLFilled() { // GetSelEOLFilled
		return !!sendMessageDirect(2479, 0, 0);
	}

	/// Set the selection to have its end of line filled or not.
	final @property void selEOLFilled(bool filled) { // SetSelEOLFilled
		sendMessageDirect(2480, filled, 0);
	}

	/// Set the foreground colour of the caret.
	final @property void caretFore(Colour fore) { // SetCaretFore
		sendMessageDirect(2069, fore.rgb, 0);
	}

	/// When key+modifier combination km is pressed perform msg.
	final void assignCmdKey(in KeyMod km, int msg) { // AssignCmdKey
		sendMessageDirect(2070, km, msg);
	}

	/// When key+modifier combination km is pressed do nothing.
	final void clearCmdKey(in KeyMod km) { // ClearCmdKey
		sendMessageDirect(2071, km, 0);
	}

	/// Drop all key mappings.
	final void clearAllCmdKeys() { // ClearAllCmdKeys
		sendMessageDirect(2072, 0, 0);
	}

	/// Set the styles for a segment of the document.
	final void setStylingEx(in char[] styles) { // SetStylingEx
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_styles = toCString(__sibuff, styles, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2073, __len+1, cast(size_t) __cstr_styles);
	}

	/// Set a style to be visible or not.
	final void styleSetVisible(int style, bool visible) { // StyleSetVisible
		sendMessageDirect(2074, style, visible);
	}

	/// Get the time in milliseconds that the caret is on and off.
	final @property int caretPeriod() { // GetCaretPeriod
		return sendMessageDirect(2075, 0, 0);
	}

	/// Get the time in milliseconds that the caret is on and off. 0 = steady on.
	final @property void caretPeriod(int periodMilliseconds) { // SetCaretPeriod
		sendMessageDirect(2076, periodMilliseconds, 0);
	}

	/// Set the set of characters making up words for when moving or selecting by word.
	/// First sets defaults like SetCharsDefault.
	final @property void wordChars(in char[] characters) { // SetWordChars
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characters = toCString(__sibuff, characters, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2077, 0, cast(size_t) __cstr_characters);
	}

	/// Start a sequence of actions that is undone and redone as a unit.
	/// May be nested.
	final void beginUndoAction() { // BeginUndoAction
		sendMessageDirect(2078, 0, 0);
	}

	/// End a sequence of actions that is undone and redone as a unit.
	final void endUndoAction() { // EndUndoAction
		sendMessageDirect(2079, 0, 0);
	}

	/// Set an indicator to plain, squiggle or TT.
	final void indicSetStyle(int indic, int style) { // IndicSetStyle
		sendMessageDirect(2080, indic, style);
	}

	/// Retrieve the style of an indicator.
	final int indicGetStyle(int indic) { // IndicGetStyle
		return sendMessageDirect(2081, indic, 0);
	}

	/// Set the foreground colour of an indicator.
	final void indicSetFore(int indic, Colour fore) { // IndicSetFore
		sendMessageDirect(2082, indic, fore.rgb);
	}

	/// Retrieve the foreground colour of an indicator.
	final Colour indicGetFore(int indic) { // IndicGetFore
		return cast(Colour)sendMessageDirect(2083, indic, 0);
	}

	/// Set an indicator to draw under text or over(default).
	final void indicSetUnder(int indic, bool under) { // IndicSetUnder
		sendMessageDirect(2510, indic, under);
	}

	/// Retrieve whether indicator drawn under or over text.
	final bool indicGetUnder(int indic) { // IndicGetUnder
		return !!sendMessageDirect(2511, indic, 0);
	}

	/// Set the foreground colour of all whitespace and whether to use this setting.
	final void setWhitespaceFore(bool useSetting, Colour fore) { // SetWhitespaceFore
		sendMessageDirect(2084, useSetting, fore.rgb);
	}

	/// Set the background colour of all whitespace and whether to use this setting.
	final void setWhitespaceBack(bool useSetting, Colour back) { // SetWhitespaceBack
		sendMessageDirect(2085, useSetting, back.rgb);
	}

	/// Set the size of the dots used to mark space characters.
	final @property void whitespaceSize(int size) { // SetWhitespaceSize
		sendMessageDirect(2086, size, 0);
	}

	/// Get the size of the dots used to mark space characters.
	final @property int whitespaceSize() { // GetWhitespaceSize
		return sendMessageDirect(2087, 0, 0);
	}

	/// Divide each styling byte into lexical class bits (default: 5) and indicator
	/// bits (default: 3). If a lexer requires more than 32 lexical states, then this
	/// is used to expand the possible states.
	final @property void styleBits(int bits) { // SetStyleBits
		sendMessageDirect(2090, bits, 0);
	}

	/// Retrieve number of bits in style bytes used to hold the lexical state.
	final @property int styleBits() { // GetStyleBits
		return sendMessageDirect(2091, 0, 0);
	}

	/// Used to hold extra styling information for each line.
	final void setLineState(int line, int state) { // SetLineState
		sendMessageDirect(2092, line, state);
	}

	/// Retrieve the extra styling information for a line.
	final int getLineState(int line) { // GetLineState
		return sendMessageDirect(2093, line, 0);
	}

	/// Retrieve the last line number that has line state.
	final @property int maxLineState() { // GetMaxLineState
		return sendMessageDirect(2094, 0, 0);
	}

	/// Is the background of the line containing the caret in a different colour?
	final @property bool caretLineVisible() { // GetCaretLineVisible
		return !!sendMessageDirect(2095, 0, 0);
	}

	/// Display the background of the line containing the caret in a different colour.
	final @property void caretLineVisible(bool show) { // SetCaretLineVisible
		sendMessageDirect(2096, show, 0);
	}

	/// Get the colour of the background of the line containing the caret.
	final @property Colour caretLineBack() { // GetCaretLineBack
		return cast(Colour)sendMessageDirect(2097, 0, 0);
	}

	/// Set the colour of the background of the line containing the caret.
	final @property void caretLineBack(Colour back) { // SetCaretLineBack
		sendMessageDirect(2098, back.rgb, 0);
	}

	/// Set a style to be changeable or not (read only).
	/// Experimental feature, currently buggy.
	final void styleSetChangeable(int style, bool changeable) { // StyleSetChangeable
		sendMessageDirect(2099, style, changeable);
	}

	/// Display a auto-completion list.
	/// The lenEntered parameter indicates how many characters before
	/// the caret should be used to provide context.
	final void autoCShow(int lenEntered, in char[] itemList) { // AutoCShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_itemList = toCString(__sibuff, itemList, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2100, lenEntered, cast(size_t) __cstr_itemList);
	}

	/// Remove the auto-completion list from the screen.
	final void autoCCancel() { // AutoCCancel
		sendMessageDirect(2101, 0, 0);
	}

	/// Is there an auto-completion list visible?
	final bool autoCActive() { // AutoCActive
		return !!sendMessageDirect(2102, 0, 0);
	}

	/// Retrieve the position of the caret when the auto-completion list was displayed.
	final Position autoCPosStart() { // AutoCPosStart
		return sendMessageDirect(2103, 0, 0);
	}

	/// User has selected an item so remove the list and insert the selection.
	final void autoCComplete() { // AutoCComplete
		sendMessageDirect(2104, 0, 0);
	}

	/// Define a set of character that when typed cancel the auto-completion list.
	final void autoCStops(in char[] characterSet) { // AutoCStops
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characterSet = toCString(__sibuff, characterSet, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2105, 0, cast(size_t) __cstr_characterSet);
	}

	/// Change the separator character in the string setting up an auto-completion list.
	/// Default is space but can be changed if items contain space.
	final @property void autoCSetSeparator(int separatorCharacter) { // AutoCSetSeparator
		sendMessageDirect(2106, separatorCharacter, 0);
	}

	/// Retrieve the auto-completion list separator character.
	final @property int autoCGetSeparator() { // AutoCGetSeparator
		return sendMessageDirect(2107, 0, 0);
	}

	/// Select the item in the auto-completion list that starts with a string.
	final void autoCSelect(in char[] text) { // AutoCSelect
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2108, 0, cast(size_t) __cstr_text);
	}

	/// Should the auto-completion list be cancelled if the user backspaces to a
	/// position before where the box was created.
	final @property void autoCSetCancelAtStart(bool cancel) { // AutoCSetCancelAtStart
		sendMessageDirect(2110, cancel, 0);
	}

	/// Retrieve whether auto-completion cancelled by backspacing before start.
	final @property bool autoCGetCancelAtStart() { // AutoCGetCancelAtStart
		return !!sendMessageDirect(2111, 0, 0);
	}

	/// Define a set of characters that when typed will cause the autocompletion to
	/// choose the selected item.
	final @property void autoCSetFillUps(in char[] characterSet) { // AutoCSetFillUps
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characterSet = toCString(__sibuff, characterSet, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2112, 0, cast(size_t) __cstr_characterSet);
	}

	/// Should a single item auto-completion list automatically choose the item.
	final @property void autoCSetChooseSingle(bool chooseSingle) { // AutoCSetChooseSingle
		sendMessageDirect(2113, chooseSingle, 0);
	}

	/// Retrieve whether a single item auto-completion list automatically choose the item.
	final @property bool autoCGetChooseSingle() { // AutoCGetChooseSingle
		return !!sendMessageDirect(2114, 0, 0);
	}

	/// Set whether case is significant when performing auto-completion searches.
	final @property void autoCSetIgnoreCase(bool ignoreCase) { // AutoCSetIgnoreCase
		sendMessageDirect(2115, ignoreCase, 0);
	}

	/// Retrieve state of ignore case flag.
	final @property bool autoCGetIgnoreCase() { // AutoCGetIgnoreCase
		return !!sendMessageDirect(2116, 0, 0);
	}

	/// Display a list of strings and send notification when user chooses one.
	final void userListShow(int listType, in char[] itemList) { // UserListShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_itemList = toCString(__sibuff, itemList, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2117, listType, cast(size_t) __cstr_itemList);
	}

	/// Set whether or not autocompletion is hidden automatically when nothing matches.
	final @property void autoCSetAutoHide(bool autoHide) { // AutoCSetAutoHide
		sendMessageDirect(2118, autoHide, 0);
	}

	/// Retrieve whether or not autocompletion is hidden automatically when nothing matches.
	final @property bool autoCGetAutoHide() { // AutoCGetAutoHide
		return !!sendMessageDirect(2119, 0, 0);
	}

	/// Set whether or not autocompletion deletes any word characters
	/// after the inserted text upon completion.
	final @property void autoCSetDropRestOfWord(bool dropRestOfWord) { // AutoCSetDropRestOfWord
		sendMessageDirect(2270, dropRestOfWord, 0);
	}

	/// Retrieve whether or not autocompletion deletes any word characters
	/// after the inserted text upon completion.
	final @property bool autoCGetDropRestOfWord() { // AutoCGetDropRestOfWord
		return !!sendMessageDirect(2271, 0, 0);
	}

	/// Register an XPM image for use in autocompletion lists.
	final void registerImage(int type, in char[] xpmData) { // RegisterImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_xpmData = toCString(__sibuff, xpmData, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2405, type, cast(size_t) __cstr_xpmData);
	}

	/// Clear all the registered XPM images.
	final void clearRegisteredImages() { // ClearRegisteredImages
		sendMessageDirect(2408, 0, 0);
	}

	/// Retrieve the auto-completion list type-separator character.
	final @property int autoCGetTypeSeparator() { // AutoCGetTypeSeparator
		return sendMessageDirect(2285, 0, 0);
	}

	/// Change the type-separator character in the string setting up an auto-completion list.
	/// Default is '?' but can be changed if items contain '?'.
	final @property void autoCSetTypeSeparator(int separatorCharacter) { // AutoCSetTypeSeparator
		sendMessageDirect(2286, separatorCharacter, 0);
	}

	/// Set the maximum width, in characters, of auto-completion and user lists.
	/// Set to 0 to autosize to fit longest item, which is the default.
	final @property void autoCSetMaxWidth(int characterCount) { // AutoCSetMaxWidth
		sendMessageDirect(2208, characterCount, 0);
	}

	/// Get the maximum width, in characters, of auto-completion and user lists.
	final @property int autoCGetMaxWidth() { // AutoCGetMaxWidth
		return sendMessageDirect(2209, 0, 0);
	}

	/// Set the maximum height, in rows, of auto-completion and user lists.
	/// The default is 5 rows.
	final @property void autoCSetMaxHeight(int rowCount) { // AutoCSetMaxHeight
		sendMessageDirect(2210, rowCount, 0);
	}

	/// Set the maximum height, in rows, of auto-completion and user lists.
	final @property int autoCGetMaxHeight() { // AutoCGetMaxHeight
		return sendMessageDirect(2211, 0, 0);
	}

	/// Set the number of spaces used for one level of indentation.
	final @property void indent(int indentSize) { // SetIndent
		sendMessageDirect(2122, indentSize, 0);
	}

	/// Retrieve indentation size.
	final @property int indent() { // GetIndent
		return sendMessageDirect(2123, 0, 0);
	}

	/// Indentation will only use space characters if useTabs is false, otherwise
	/// it will use a combination of tabs and spaces.
	final @property void useTabs(bool useTabs) { // SetUseTabs
		sendMessageDirect(2124, useTabs, 0);
	}

	/// Retrieve whether tabs will be used in indentation.
	final @property bool useTabs() { // GetUseTabs
		return !!sendMessageDirect(2125, 0, 0);
	}

	/// Change the indentation of a line to a number of columns.
	final void setLineIndentation(int line, int indentSize) { // SetLineIndentation
		sendMessageDirect(2126, line, indentSize);
	}

	/// Retrieve the number of columns that a line is indented.
	final int getLineIndentation(int line) { // GetLineIndentation
		return sendMessageDirect(2127, line, 0);
	}

	/// Retrieve the position before the first non indentation character on a line.
	final Position getLineIndentPosition(int line) { // GetLineIndentPosition
		return sendMessageDirect(2128, line, 0);
	}

	/// Retrieve the column number of a position, taking tab width into account.
	final int getColumn(Position pos) { // GetColumn
		return sendMessageDirect(2129, pos, 0);
	}

	/// Count characters between two positions.
	final int countCharacters(int startPos, int endPos) { // CountCharacters
		return sendMessageDirect(2633, startPos, endPos);
	}

	/// Show or hide the horizontal scroll bar.
	final @property void hScrollBar(bool show) { // SetHScrollBar
		sendMessageDirect(2130, show, 0);
	}

	/// Is the horizontal scroll bar visible?
	final @property bool hScrollBar() { // GetHScrollBar
		return !!sendMessageDirect(2131, 0, 0);
	}

	/// Show or hide indentation guides.
	final @property void indentationGuides(int indentView) { // SetIndentationGuides
		sendMessageDirect(2132, indentView, 0);
	}

	/// Are the indentation guides visible?
	final @property int indentationGuides() { // GetIndentationGuides
		return sendMessageDirect(2133, 0, 0);
	}

	/// Set the highlighted indentation guide column.
	/// 0 = no highlighted guide.
	final @property void highlightGuide(int column) { // SetHighlightGuide
		sendMessageDirect(2134, column, 0);
	}

	/// Get the highlighted indentation guide column.
	final @property int highlightGuide() { // GetHighlightGuide
		return sendMessageDirect(2135, 0, 0);
	}

	/// Get the position after the last visible characters on a line.
	final int getLineEndPosition(int line) { // GetLineEndPosition
		return sendMessageDirect(2136, line, 0);
	}

	/// Get the code page used to interpret the bytes of the document as characters.
	final @property int codePage() { // GetCodePage
		return sendMessageDirect(2137, 0, 0);
	}

	/// Get the foreground colour of the caret.
	final @property Colour caretFore() { // GetCaretFore
		return cast(Colour)sendMessageDirect(2138, 0, 0);
	}

	/// In read-only mode?
	final @property bool readOnly() { // GetReadOnly
		return !!sendMessageDirect(2140, 0, 0);
	}

	/// Sets the position of the caret.
	final @property void currentPos(Position pos) { // SetCurrentPos
		sendMessageDirect(2141, pos, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	final @property void selectionStart(Position pos) { // SetSelectionStart
		sendMessageDirect(2142, pos, 0);
	}

	/// Returns the position at the start of the selection.
	final @property Position selectionStart() { // GetSelectionStart
		return sendMessageDirect(2143, 0, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	final @property void selectionEnd(Position pos) { // SetSelectionEnd
		sendMessageDirect(2144, pos, 0);
	}

	/// Returns the position at the end of the selection.
	final @property Position selectionEnd() { // GetSelectionEnd
		return sendMessageDirect(2145, 0, 0);
	}

	/// Set caret to a position, while removing any existing selection.
	final void setEmptySelection(Position pos) { // SetEmptySelection
		sendMessageDirect(2556, pos, 0);
	}

	/// Sets the print magnification added to the point size of each style for printing.
	final @property void printMagnification(int magnification) { // SetPrintMagnification
		sendMessageDirect(2146, magnification, 0);
	}

	/// Returns the print magnification.
	final @property int printMagnification() { // GetPrintMagnification
		return sendMessageDirect(2147, 0, 0);
	}

	/// Modify colours when printing for clearer printed text.
	final @property void printColourMode(int mode) { // SetPrintColourMode
		sendMessageDirect(2148, mode, 0);
	}

	/// Returns the print colour mode.
	final @property int printColourMode() { // GetPrintColourMode
		return sendMessageDirect(2149, 0, 0);
	}

	/// Find some text in the document.
	final Position findText(int flags, ref TextToFind ft) { // FindText
		return sendMessageDirect(2150, flags, cast(size_t) &ft);
	}

	/// On Windows, will draw the document into a display context such as a printer.
	final Position formatRange(bool draw, ref RangeToFormat fr) { // FormatRange
		return sendMessageDirect(2151, draw, cast(size_t) &fr);
	}

	/// Retrieve the display line at the top of the display.
	final @property int firstVisibleLine() { // GetFirstVisibleLine
		return sendMessageDirect(2152, 0, 0);
	}

	/// Retrieve the contents of a line.
	/// Returns the length of the line.
	final string getLine(int line) { // GetLine
		immutable __len = sendMessageDirect(2153, line, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2153, line, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getLine(char[] textBuff, int line, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(2153, line, cast(size_t) buff.ptr), textBuff, text);
	}

	/// Returns the number of lines in the document. There is always at least one.
	final @property int lineCount() { // GetLineCount
		return sendMessageDirect(2154, 0, 0);
	}

	/// Sets the size in pixels of the left margin.
	final @property void marginLeft(int pixelWidth) { // SetMarginLeft
		sendMessageDirect(2155, 0, pixelWidth);
	}

	/// Returns the size in pixels of the left margin.
	final @property int marginLeft() { // GetMarginLeft
		return sendMessageDirect(2156, 0, 0);
	}

	/// Sets the size in pixels of the right margin.
	final @property void marginRight(int pixelWidth) { // SetMarginRight
		sendMessageDirect(2157, 0, pixelWidth);
	}

	/// Returns the size in pixels of the right margin.
	final @property int marginRight() { // GetMarginRight
		return sendMessageDirect(2158, 0, 0);
	}

	/// Is the document different from when it was last saved?
	final @property bool modify() { // GetModify
		return !!sendMessageDirect(2159, 0, 0);
	}

	/// Select a range of text.
	final void setSel(Position start, Position end) { // SetSel
		sendMessageDirect(2160, start, end);
	}

	/// Retrieve the selected text.
	/// Return the length of the text.
	final string getSelText() { // GetSelText
		immutable __len = sendMessageDirect(2161, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2161, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getSelText(char[] textBuff, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(2161, 0, cast(size_t) buff.ptr), textBuff, text);
	}

	/// Retrieve a range of text.
	/// Return the length of the text.
	final int getTextRange(ref TextRange tr) { // GetTextRange
		return sendMessageDirect(2162, 0, cast(size_t) &tr);
	}

	/// Draw the selection in normal style or with selection highlighted.
	final void hideSelection(bool normal) { // HideSelection
		sendMessageDirect(2163, normal, 0);
	}

	/// Retrieve the x value of the point in the window where a position is displayed.
	final int pointXFromPosition(Position pos) { // PointXFromPosition
		return sendMessageDirect(2164, 0, pos);
	}

	/// Retrieve the y value of the point in the window where a position is displayed.
	final int pointYFromPosition(Position pos) { // PointYFromPosition
		return sendMessageDirect(2165, 0, pos);
	}

	/// Retrieve the line containing a position.
	final int lineFromPosition(Position pos) { // LineFromPosition
		return sendMessageDirect(2166, pos, 0);
	}

	/// Retrieve the position at the start of a line.
	final Position positionFromLine(int line) { // PositionFromLine
		return sendMessageDirect(2167, line, 0);
	}

	/// Scroll horizontally and vertically.
	final void lineScroll(int columns, int lines) { // LineScroll
		sendMessageDirect(2168, columns, lines);
	}

	/// Ensure the caret is visible.
	final void scrollCaret() { // ScrollCaret
		sendMessageDirect(2169, 0, 0);
	}

	/// Replace the selected text with the argument text.
	final void replaceSel(in char[] text) { // ReplaceSel
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2170, 0, cast(size_t) __cstr_text);
	}

	/// Set to read only or read write.
	final @property void readOnly(bool readOnly) { // SetReadOnly
		sendMessageDirect(2171, readOnly, 0);
	}

	/// Null operation.
	final void null_() { // Null
		sendMessageDirect(2172, 0, 0);
	}

	/// Will a paste succeed?
	final bool canPaste() { // CanPaste
		return !!sendMessageDirect(2173, 0, 0);
	}

	/// Are there any undoable actions in the undo history?
	final bool canUndo() { // CanUndo
		return !!sendMessageDirect(2174, 0, 0);
	}

	/// Delete the undo history.
	final void emptyUndoBuffer() { // EmptyUndoBuffer
		sendMessageDirect(2175, 0, 0);
	}

	/// Undo one action in the undo history.
	final void undo() { // Undo
		sendMessageDirect(2176, 0, 0);
	}

	/// Cut the selection to the clipboard.
	final void cut() { // Cut
		sendMessageDirect(2177, 0, 0);
	}

	/// Copy the selection to the clipboard.
	final void copy() { // Copy
		sendMessageDirect(2178, 0, 0);
	}

	/// Paste the contents of the clipboard into the document replacing the selection.
	final void paste() { // Paste
		sendMessageDirect(2179, 0, 0);
	}

	/// Clear the selection.
	final void clear() { // Clear
		sendMessageDirect(2180, 0, 0);
	}

	/// Replace the contents of the document with the argument text.
	final void setText(in char[] text) { // SetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2181, 0, cast(size_t) __cstr_text);
	}

	/// Retrieve all the text in the document.
	/// Returns number of characters retrieved.
	final string getText() { // GetText
		immutable __len = sendMessageDirect(2182, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2182, __len+1, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getText(char[] textBuff, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(2182, buff.length, cast(size_t) buff.ptr), textBuff, text);
	}

	/// Retrieve the number of characters in the document.
	final @property int textLength() { // GetTextLength
		return sendMessageDirect(2183, 0, 0);
	}

	/// Retrieve a pointer to a function that processes messages for this Scintilla.
	final @property int directFunction() { // GetDirectFunction
		return sendMessageDirect(2184, 0, 0);
	}

	/// Retrieve a pointer value to use as the first argument when calling
	/// the function returned by GetDirectFunction.
	final @property int directPointer() { // GetDirectPointer
		return sendMessageDirect(2185, 0, 0);
	}

	/// Set to overtype (true) or insert mode.
	final @property void overtype(bool overtype) { // SetOvertype
		sendMessageDirect(2186, overtype, 0);
	}

	/// Returns true if overtype mode is active otherwise false is returned.
	final @property bool overtype() { // GetOvertype
		return !!sendMessageDirect(2187, 0, 0);
	}

	/// Set the width of the insert mode caret.
	final @property void caretWidth(int pixelWidth) { // SetCaretWidth
		sendMessageDirect(2188, pixelWidth, 0);
	}

	/// Returns the width of the insert mode caret.
	final @property int caretWidth() { // GetCaretWidth
		return sendMessageDirect(2189, 0, 0);
	}

	/// Sets the position that starts the target which is used for updating the
	/// document without affecting the scroll position.
	final @property void targetStart(Position pos) { // SetTargetStart
		sendMessageDirect(2190, pos, 0);
	}

	/// Get the position that starts the target.
	final @property Position targetStart() { // GetTargetStart
		return sendMessageDirect(2191, 0, 0);
	}

	/// Sets the position that ends the target which is used for updating the
	/// document without affecting the scroll position.
	final @property void targetEnd(Position pos) { // SetTargetEnd
		sendMessageDirect(2192, pos, 0);
	}

	/// Get the position that ends the target.
	final @property Position targetEnd() { // GetTargetEnd
		return sendMessageDirect(2193, 0, 0);
	}

	/// Replace the target text with the argument text.
	/// Text is counted so it can contain NULs.
	/// Returns the length of the replacement text.
	final int replaceTarget(in char[] text) { // ReplaceTarget
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2194, __len+1, cast(size_t) __cstr_text);
	}

	/// Replace the target text with the argument text after \d processing.
	/// Text is counted so it can contain NULs.
	/// Looks for \d where d is between 1 and 9 and replaces these with the strings
	/// matched in the last search operation which were surrounded by \( and \).
	/// Returns the length of the replacement text including any change
	/// caused by processing the \d patterns.
	final int replaceTargetRE(in char[] text) { // ReplaceTargetRE
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2195, __len+1, cast(size_t) __cstr_text);
	}

	/// Search for a counted string in the target and set the target to the found
	/// range. Text is counted so it can contain NULs.
	/// Returns length of range or -1 for failure in which case target is not moved.
	final int searchInTarget(in char[] text) { // SearchInTarget
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2197, __len+1, cast(size_t) __cstr_text);
	}

	/// Set the search flags used by SearchInTarget.
	final @property void searchFlags(int flags) { // SetSearchFlags
		sendMessageDirect(2198, flags, 0);
	}

	/// Get the search flags used by SearchInTarget.
	final @property int searchFlags() { // GetSearchFlags
		return sendMessageDirect(2199, 0, 0);
	}

	/// Show a call tip containing a definition near position pos.
	final void callTipShow(Position pos, in char[] definition) { // CallTipShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_definition = toCString(__sibuff, definition, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2200, pos, cast(size_t) __cstr_definition);
	}

	/// Remove the call tip from the screen.
	final void callTipCancel() { // CallTipCancel
		sendMessageDirect(2201, 0, 0);
	}

	/// Is there an active call tip?
	final bool callTipActive() { // CallTipActive
		return !!sendMessageDirect(2202, 0, 0);
	}

	/// Retrieve the position where the caret was before displaying the call tip.
	final Position callTipPosStart() { // CallTipPosStart
		return sendMessageDirect(2203, 0, 0);
	}

	/// Highlight a segment of the definition.
	final void callTipSetHlt(int start, int end) { // CallTipSetHlt
		sendMessageDirect(2204, start, end);
	}

	/// Set the background colour for the call tip.
	final @property void callTipSetBack(Colour back) { // CallTipSetBack
		sendMessageDirect(2205, back.rgb, 0);
	}

	/// Set the foreground colour for the call tip.
	final @property void callTipSetFore(Colour fore) { // CallTipSetFore
		sendMessageDirect(2206, fore.rgb, 0);
	}

	/// Set the foreground colour for the highlighted part of the call tip.
	final @property void callTipSetForeHlt(Colour fore) { // CallTipSetForeHlt
		sendMessageDirect(2207, fore.rgb, 0);
	}

	/// Enable use of STYLE_CALLTIP and set call tip tab size in pixels.
	final @property void callTipUseStyle(int tabSize) { // CallTipUseStyle
		sendMessageDirect(2212, tabSize, 0);
	}

	/// Set position of calltip, above or below text.
	final @property void callTipSetPosition(bool above) { // CallTipSetPosition
		sendMessageDirect(2213, above, 0);
	}

	/// Find the display line of a document line taking hidden lines into account.
	final int visibleFromDocLine(int line) { // VisibleFromDocLine
		return sendMessageDirect(2220, line, 0);
	}

	/// Find the document line of a display line taking hidden lines into account.
	final int docLineFromVisible(int lineDisplay) { // DocLineFromVisible
		return sendMessageDirect(2221, lineDisplay, 0);
	}

	/// The number of display lines needed to wrap a document line
	final int wrapCount(int line) { // WrapCount
		return sendMessageDirect(2235, line, 0);
	}

	/// Set the fold level of a line.
	/// This encodes an integer level along with flags indicating whether the
	/// line is a header and whether it is effectively white space.
	final void setFoldLevel(int line, int level) { // SetFoldLevel
		sendMessageDirect(2222, line, level);
	}

	/// Retrieve the fold level of a line.
	final int getFoldLevel(int line) { // GetFoldLevel
		return sendMessageDirect(2223, line, 0);
	}

	/// Find the last child line of a header line.
	final int getLastChild(int line, int level) { // GetLastChild
		return sendMessageDirect(2224, line, level);
	}

	/// Find the parent line of a child line.
	final int getFoldParent(int line) { // GetFoldParent
		return sendMessageDirect(2225, line, 0);
	}

	/// Make a range of lines visible.
	final void showLines(int lineStart, int lineEnd) { // ShowLines
		sendMessageDirect(2226, lineStart, lineEnd);
	}

	/// Make a range of lines invisible.
	final void hideLines(int lineStart, int lineEnd) { // HideLines
		sendMessageDirect(2227, lineStart, lineEnd);
	}

	/// Is a line visible?
	final bool getLineVisible(int line) { // GetLineVisible
		return !!sendMessageDirect(2228, line, 0);
	}

	/// Are all lines visible?
	final @property bool allLinesVisible() { // GetAllLinesVisible
		return !!sendMessageDirect(2236, 0, 0);
	}

	/// Show the children of a header line.
	final void setFoldExpanded(int line, bool expanded) { // SetFoldExpanded
		sendMessageDirect(2229, line, expanded);
	}

	/// Is a header line expanded?
	final bool getFoldExpanded(int line) { // GetFoldExpanded
		return !!sendMessageDirect(2230, line, 0);
	}

	/// Switch a header line between expanded and contracted.
	final void toggleFold(int line) { // ToggleFold
		sendMessageDirect(2231, line, 0);
	}

	/// Ensure a particular line is visible by expanding any header line hiding it.
	final void ensureVisible(int line) { // EnsureVisible
		sendMessageDirect(2232, line, 0);
	}

	/// Set some style options for folding.
	final void setFoldFlags(int flags) { // SetFoldFlags
		sendMessageDirect(2233, flags, 0);
	}

	/// Ensure a particular line is visible by expanding any header line hiding it.
	/// Use the currently set visibility policy to determine which range to display.
	final void ensureVisibleEnforcePolicy(int line) { // EnsureVisibleEnforcePolicy
		sendMessageDirect(2234, line, 0);
	}

	/// Sets whether a tab pressed when caret is within indentation indents.
	final @property void tabIndents(bool tabIndents) { // SetTabIndents
		sendMessageDirect(2260, tabIndents, 0);
	}

	/// Does a tab pressed when caret is within indentation indent?
	final @property bool tabIndents() { // GetTabIndents
		return !!sendMessageDirect(2261, 0, 0);
	}

	/// Sets whether a backspace pressed when caret is within indentation unindents.
	final @property void backSpaceUnIndents(bool bsUnIndents) { // SetBackSpaceUnIndents
		sendMessageDirect(2262, bsUnIndents, 0);
	}

	/// Does a backspace pressed when caret is within indentation unindent?
	final @property bool backSpaceUnIndents() { // GetBackSpaceUnIndents
		return !!sendMessageDirect(2263, 0, 0);
	}

	/// Sets the time the mouse must sit still to generate a mouse dwell event.
	final @property void mouseDwellTime(int periodMilliseconds) { // SetMouseDwellTime
		sendMessageDirect(2264, periodMilliseconds, 0);
	}

	/// Retrieve the time the mouse must sit still to generate a mouse dwell event.
	final @property int mouseDwellTime() { // GetMouseDwellTime
		return sendMessageDirect(2265, 0, 0);
	}

	/// Get position of start of word.
	final int wordStartPosition(Position pos, bool onlyWordCharacters) { // WordStartPosition
		return sendMessageDirect(2266, pos, onlyWordCharacters);
	}

	/// Get position of end of word.
	final int wordEndPosition(Position pos, bool onlyWordCharacters) { // WordEndPosition
		return sendMessageDirect(2267, pos, onlyWordCharacters);
	}

	/// Sets whether text is word wrapped.
	final @property void wrapMode(int mode) { // SetWrapMode
		sendMessageDirect(2268, mode, 0);
	}

	/// Retrieve whether text is word wrapped.
	final @property int wrapMode() { // GetWrapMode
		return sendMessageDirect(2269, 0, 0);
	}

	/// Set the display mode of visual flags for wrapped lines.
	final @property void wrapVisualFlags(int wrapVisualFlags) { // SetWrapVisualFlags
		sendMessageDirect(2460, wrapVisualFlags, 0);
	}

	/// Retrive the display mode of visual flags for wrapped lines.
	final @property int wrapVisualFlags() { // GetWrapVisualFlags
		return sendMessageDirect(2461, 0, 0);
	}

	/// Set the location of visual flags for wrapped lines.
	final @property void wrapVisualFlagsLocation(int wrapVisualFlagsLocation) { // SetWrapVisualFlagsLocation
		sendMessageDirect(2462, wrapVisualFlagsLocation, 0);
	}

	/// Retrive the location of visual flags for wrapped lines.
	final @property int wrapVisualFlagsLocation() { // GetWrapVisualFlagsLocation
		return sendMessageDirect(2463, 0, 0);
	}

	/// Set the start indent for wrapped lines.
	final @property void wrapStartIndent(int indent) { // SetWrapStartIndent
		sendMessageDirect(2464, indent, 0);
	}

	/// Retrive the start indent for wrapped lines.
	final @property int wrapStartIndent() { // GetWrapStartIndent
		return sendMessageDirect(2465, 0, 0);
	}

	/// Sets how wrapped sublines are placed. Default is fixed.
	final @property void wrapIndentMode(int mode) { // SetWrapIndentMode
		sendMessageDirect(2472, mode, 0);
	}

	/// Retrieve how wrapped sublines are placed. Default is fixed.
	final @property int wrapIndentMode() { // GetWrapIndentMode
		return sendMessageDirect(2473, 0, 0);
	}

	/// Sets the degree of caching of layout information.
	final @property void layoutCache(int mode) { // SetLayoutCache
		sendMessageDirect(2272, mode, 0);
	}

	/// Retrieve the degree of caching of layout information.
	final @property int layoutCache() { // GetLayoutCache
		return sendMessageDirect(2273, 0, 0);
	}

	/// Sets the document width assumed for scrolling.
	final @property void scrollWidth(int pixelWidth) { // SetScrollWidth
		sendMessageDirect(2274, pixelWidth, 0);
	}

	/// Retrieve the document width assumed for scrolling.
	final @property int scrollWidth() { // GetScrollWidth
		return sendMessageDirect(2275, 0, 0);
	}

	/// Sets whether the maximum width line displayed is used to set scroll width.
	final @property void scrollWidthTracking(bool tracking) { // SetScrollWidthTracking
		sendMessageDirect(2516, tracking, 0);
	}

	/// Retrieve whether the scroll width tracks wide lines.
	final @property bool scrollWidthTracking() { // GetScrollWidthTracking
		return !!sendMessageDirect(2517, 0, 0);
	}

	/// Measure the pixel width of some text in a particular style.
	/// NUL terminated text argument.
	/// Does not handle tab or control characters.
	final int textWidth(int style, in char[] text) { // TextWidth
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2276, style, cast(size_t) __cstr_text);
	}

	/// Sets the scroll range so that maximum scroll position has
	/// the last line at the bottom of the view (default).
	/// Setting this to false allows scrolling one page below the last line.
	final @property void endAtLastLine(bool endAtLastLine) { // SetEndAtLastLine
		sendMessageDirect(2277, endAtLastLine, 0);
	}

	/// Retrieve whether the maximum scroll position has the last
	/// line at the bottom of the view.
	final @property bool endAtLastLine() { // GetEndAtLastLine
		return !!sendMessageDirect(2278, 0, 0);
	}

	/// Retrieve the height of a particular line of text in pixels.
	final int textHeight(int line) { // TextHeight
		return sendMessageDirect(2279, line, 0);
	}

	/// Show or hide the vertical scroll bar.
	final @property void vScrollBar(bool show) { // SetVScrollBar
		sendMessageDirect(2280, show, 0);
	}

	/// Is the vertical scroll bar visible?
	final @property bool vScrollBar() { // GetVScrollBar
		return !!sendMessageDirect(2281, 0, 0);
	}

	/// Append a string to the end of the document without changing the selection.
	final void appendText(in char[] text) { // AppendText
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2282, __len+1, cast(size_t) __cstr_text);
	}

	/// Is drawing done in two phases with backgrounds drawn before faoregrounds?
	final @property bool twoPhaseDraw() { // GetTwoPhaseDraw
		return !!sendMessageDirect(2283, 0, 0);
	}

	/// In twoPhaseDraw mode, drawing is performed in two phases, first the background
	/// and then the foreground. This avoids chopping off characters that overlap the next run.
	final @property void twoPhaseDraw(bool twoPhase) { // SetTwoPhaseDraw
		sendMessageDirect(2284, twoPhase, 0);
	}

	/// Choose the quality level for text from the FontQuality enumeration.
	final @property void fontQuality(int fontQuality) { // SetFontQuality
		sendMessageDirect(2611, fontQuality, 0);
	}

	/// Retrieve the quality level for text.
	final @property int fontQuality() { // GetFontQuality
		return sendMessageDirect(2612, 0, 0);
	}

	/// Scroll so that a display line is at the top of the display.
	final @property void firstVisibleLine(int lineDisplay) { // SetFirstVisibleLine
		sendMessageDirect(2613, lineDisplay, 0);
	}

	/// Change the effect of pasting when there are multiple selections.
	final @property void multiPaste(int multiPaste) { // SetMultiPaste
		sendMessageDirect(2614, multiPaste, 0);
	}

	/// Retrieve the effect of pasting when there are multiple selections..
	final @property int multiPaste() { // GetMultiPaste
		return sendMessageDirect(2615, 0, 0);
	}

	/// Retrieve the value of a tag from a regular expression search.
	final string getTag(int tagNumber) { // GetTag
		immutable __len = sendMessageDirect(2616, tagNumber, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2616, tagNumber, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getTag(char[] tagValueBuff, int tagNumber, out char[] tagValue) {
		alias tagValueBuff buff;
		return toDStringBuff(sendMessageDirect(2616, tagNumber, cast(size_t) buff.ptr), tagValueBuff, tagValue);
	}

	/// Make the target range start and end be the same as the selection range start and end.
	final void targetFromSelection() { // TargetFromSelection
		sendMessageDirect(2287, 0, 0);
	}

	/// Join the lines in the target.
	final void linesJoin() { // LinesJoin
		sendMessageDirect(2288, 0, 0);
	}

	/// Split the lines in the target into lines that are less wide than pixelWidth
	/// where possible.
	final void linesSplit(int pixelWidth) { // LinesSplit
		sendMessageDirect(2289, pixelWidth, 0);
	}

	/// Set the colours used as a chequerboard pattern in the fold margin
	final void setFoldMarginColour(bool useSetting, Colour back) { // SetFoldMarginColour
		sendMessageDirect(2290, useSetting, back.rgb);
	}

	final void setFoldMarginHiColour(bool useSetting, Colour fore) { // SetFoldMarginHiColour
		sendMessageDirect(2291, useSetting, fore.rgb);
	}

	/// Move caret down one line.
	final void lineDown() { // LineDown
		sendMessageDirect(2300, 0, 0);
	}

	/// Move caret down one line extending selection to new caret position.
	final void lineDownExtend() { // LineDownExtend
		sendMessageDirect(2301, 0, 0);
	}

	/// Move caret up one line.
	final void lineUp() { // LineUp
		sendMessageDirect(2302, 0, 0);
	}

	/// Move caret up one line extending selection to new caret position.
	final void lineUpExtend() { // LineUpExtend
		sendMessageDirect(2303, 0, 0);
	}

	/// Move caret left one character.
	final void charLeft() { // CharLeft
		sendMessageDirect(2304, 0, 0);
	}

	/// Move caret left one character extending selection to new caret position.
	final void charLeftExtend() { // CharLeftExtend
		sendMessageDirect(2305, 0, 0);
	}

	/// Move caret right one character.
	final void charRight() { // CharRight
		sendMessageDirect(2306, 0, 0);
	}

	/// Move caret right one character extending selection to new caret position.
	final void charRightExtend() { // CharRightExtend
		sendMessageDirect(2307, 0, 0);
	}

	/// Move caret left one word.
	final void wordLeft() { // WordLeft
		sendMessageDirect(2308, 0, 0);
	}

	/// Move caret left one word extending selection to new caret position.
	final void wordLeftExtend() { // WordLeftExtend
		sendMessageDirect(2309, 0, 0);
	}

	/// Move caret right one word.
	final void wordRight() { // WordRight
		sendMessageDirect(2310, 0, 0);
	}

	/// Move caret right one word extending selection to new caret position.
	final void wordRightExtend() { // WordRightExtend
		sendMessageDirect(2311, 0, 0);
	}

	/// Move caret to first position on line.
	final void home() { // Home
		sendMessageDirect(2312, 0, 0);
	}

	/// Move caret to first position on line extending selection to new caret position.
	final void homeExtend() { // HomeExtend
		sendMessageDirect(2313, 0, 0);
	}

	/// Move caret to last position on line.
	final void lineEnd() { // LineEnd
		sendMessageDirect(2314, 0, 0);
	}

	/// Move caret to last position on line extending selection to new caret position.
	final void lineEndExtend() { // LineEndExtend
		sendMessageDirect(2315, 0, 0);
	}

	/// Move caret to first position in document.
	final void documentStart() { // DocumentStart
		sendMessageDirect(2316, 0, 0);
	}

	/// Move caret to first position in document extending selection to new caret position.
	final void documentStartExtend() { // DocumentStartExtend
		sendMessageDirect(2317, 0, 0);
	}

	/// Move caret to last position in document.
	final void documentEnd() { // DocumentEnd
		sendMessageDirect(2318, 0, 0);
	}

	/// Move caret to last position in document extending selection to new caret position.
	final void documentEndExtend() { // DocumentEndExtend
		sendMessageDirect(2319, 0, 0);
	}

	/// Move caret one page up.
	final void pageUp() { // PageUp
		sendMessageDirect(2320, 0, 0);
	}

	/// Move caret one page up extending selection to new caret position.
	final void pageUpExtend() { // PageUpExtend
		sendMessageDirect(2321, 0, 0);
	}

	/// Move caret one page down.
	final void pageDown() { // PageDown
		sendMessageDirect(2322, 0, 0);
	}

	/// Move caret one page down extending selection to new caret position.
	final void pageDownExtend() { // PageDownExtend
		sendMessageDirect(2323, 0, 0);
	}

	/// Switch from insert to overtype mode or the reverse.
	final void editToggleOvertype() { // EditToggleOvertype
		sendMessageDirect(2324, 0, 0);
	}

	/// Cancel any modes such as call tip or auto-completion list display.
	final void cancel() { // Cancel
		sendMessageDirect(2325, 0, 0);
	}

	/// Delete the selection or if no selection, the character before the caret.
	final void deleteBack() { // DeleteBack
		sendMessageDirect(2326, 0, 0);
	}

	/// If selection is empty or all on one line replace the selection with a tab character.
	/// If more than one line selected, indent the lines.
	final void tab() { // Tab
		sendMessageDirect(2327, 0, 0);
	}

	/// Dedent the selected lines.
	final void backTab() { // BackTab
		sendMessageDirect(2328, 0, 0);
	}

	/// Insert a new line, may use a CRLF, CR or LF depending on EOL mode.
	final void newLine() { // NewLine
		sendMessageDirect(2329, 0, 0);
	}

	/// Insert a Form Feed character.
	final void formFeed() { // FormFeed
		sendMessageDirect(2330, 0, 0);
	}

	/// Move caret to before first visible character on line.
	/// If already there move to first character on line.
	final void vCHome() { // VCHome
		sendMessageDirect(2331, 0, 0);
	}

	/// Like VCHome but extending selection to new caret position.
	final void vCHomeExtend() { // VCHomeExtend
		sendMessageDirect(2332, 0, 0);
	}

	/// Magnify the displayed text by increasing the sizes by 1 point.
	final void zoomIn() { // ZoomIn
		sendMessageDirect(2333, 0, 0);
	}

	/// Make the displayed text smaller by decreasing the sizes by 1 point.
	final void zoomOut() { // ZoomOut
		sendMessageDirect(2334, 0, 0);
	}

	/// Delete the word to the left of the caret.
	final void delWordLeft() { // DelWordLeft
		sendMessageDirect(2335, 0, 0);
	}

	/// Delete the word to the right of the caret.
	final void delWordRight() { // DelWordRight
		sendMessageDirect(2336, 0, 0);
	}

	/// Delete the word to the right of the caret, but not the trailing non-word characters.
	final void delWordRightEnd() { // DelWordRightEnd
		sendMessageDirect(2518, 0, 0);
	}

	/// Cut the line containing the caret.
	final void lineCut() { // LineCut
		sendMessageDirect(2337, 0, 0);
	}

	/// Delete the line containing the caret.
	final void lineDelete() { // LineDelete
		sendMessageDirect(2338, 0, 0);
	}

	/// Switch the current line with the previous.
	final void lineTranspose() { // LineTranspose
		sendMessageDirect(2339, 0, 0);
	}

	/// Duplicate the current line.
	final void lineDuplicate() { // LineDuplicate
		sendMessageDirect(2404, 0, 0);
	}

	/// Transform the selection to lower case.
	final void lowerCase() { // LowerCase
		sendMessageDirect(2340, 0, 0);
	}

	/// Transform the selection to upper case.
	final void upperCase() { // UpperCase
		sendMessageDirect(2341, 0, 0);
	}

	/// Scroll the document down, keeping the caret visible.
	final void lineScrollDown() { // LineScrollDown
		sendMessageDirect(2342, 0, 0);
	}

	/// Scroll the document up, keeping the caret visible.
	final void lineScrollUp() { // LineScrollUp
		sendMessageDirect(2343, 0, 0);
	}

	/// Delete the selection or if no selection, the character before the caret.
	/// Will not delete the character before at the start of a line.
	final void deleteBackNotLine() { // DeleteBackNotLine
		sendMessageDirect(2344, 0, 0);
	}

	/// Move caret to first position on display line.
	final void homeDisplay() { // HomeDisplay
		sendMessageDirect(2345, 0, 0);
	}

	/// Move caret to first position on display line extending selection to
	/// new caret position.
	final void homeDisplayExtend() { // HomeDisplayExtend
		sendMessageDirect(2346, 0, 0);
	}

	/// Move caret to last position on display line.
	final void lineEndDisplay() { // LineEndDisplay
		sendMessageDirect(2347, 0, 0);
	}

	/// Move caret to last position on display line extending selection to new
	/// caret position.
	final void lineEndDisplayExtend() { // LineEndDisplayExtend
		sendMessageDirect(2348, 0, 0);
	}

	/// These are like their namesakes Home(Extend)?, LineEnd(Extend)?, VCHome(Extend)?
	/// except they behave differently when word-wrap is enabled:
	/// They go first to the start / end of the display line, like (Home|LineEnd)Display
	/// The difference is that, the cursor is already at the point, it goes on to the start
	/// or end of the document line, as appropriate for (Home|LineEnd|VCHome)(Extend)?.
	final void homeWrap() { // HomeWrap
		sendMessageDirect(2349, 0, 0);
	}

	final void homeWrapExtend() { // HomeWrapExtend
		sendMessageDirect(2450, 0, 0);
	}

	final void lineEndWrap() { // LineEndWrap
		sendMessageDirect(2451, 0, 0);
	}

	final void lineEndWrapExtend() { // LineEndWrapExtend
		sendMessageDirect(2452, 0, 0);
	}

	final void vCHomeWrap() { // VCHomeWrap
		sendMessageDirect(2453, 0, 0);
	}

	final void vCHomeWrapExtend() { // VCHomeWrapExtend
		sendMessageDirect(2454, 0, 0);
	}

	/// Copy the line containing the caret.
	final void lineCopy() { // LineCopy
		sendMessageDirect(2455, 0, 0);
	}

	/// Move the caret inside current view if it's not there already.
	final void moveCaretInsideView() { // MoveCaretInsideView
		sendMessageDirect(2401, 0, 0);
	}

	/// How many characters are on a line, including end of line characters?
	final int lineLength(int line) { // LineLength
		return sendMessageDirect(2350, line, 0);
	}

	/// Highlight the characters at two positions.
	final void braceHighlight(Position pos1, Position pos2) { // BraceHighlight
		sendMessageDirect(2351, pos1, pos2);
	}

	/// Use specified indicator to highlight matching braces instead of changing their style.
	final void braceHighlightIndicator(bool useBraceHighlightIndicator, int indicator) { // BraceHighlightIndicator
		sendMessageDirect(2498, useBraceHighlightIndicator, indicator);
	}

	/// Highlight the character at a position indicating there is no matching brace.
	final void braceBadLight(Position pos) { // BraceBadLight
		sendMessageDirect(2352, pos, 0);
	}

	/// Use specified indicator to highlight non matching brace instead of changing its style.
	final void braceBadLightIndicator(bool useBraceBadLightIndicator, int indicator) { // BraceBadLightIndicator
		sendMessageDirect(2499, useBraceBadLightIndicator, indicator);
	}

	/// Find the position of a matching brace or INVALID_POSITION if no match.
	final Position braceMatch(Position pos) { // BraceMatch
		return sendMessageDirect(2353, pos, 0);
	}

	/// Are the end of line characters visible?
	final @property bool viewEOL() { // GetViewEOL
		return !!sendMessageDirect(2355, 0, 0);
	}

	/// Make the end of line characters visible or invisible.
	final @property void viewEOL(bool visible) { // SetViewEOL
		sendMessageDirect(2356, visible, 0);
	}

	/// Retrieve a pointer to the document object.
	final @property int docPointer() { // GetDocPointer
		return sendMessageDirect(2357, 0, 0);
	}

	/// Change the document object used.
	final @property void docPointer(int pointer) { // SetDocPointer
		sendMessageDirect(2358, 0, pointer);
	}

	/// Set which document modification events are sent to the container.
	final @property void modEventMask(int mask) { // SetModEventMask
		sendMessageDirect(2359, mask, 0);
	}

	/// Retrieve the column number which text should be kept within.
	final @property int edgeColumn() { // GetEdgeColumn
		return sendMessageDirect(2360, 0, 0);
	}

	/// Set the column number of the edge.
	/// If text goes past the edge then it is highlighted.
	final @property void edgeColumn(int column) { // SetEdgeColumn
		sendMessageDirect(2361, column, 0);
	}

	/// Retrieve the edge highlight mode.
	final @property int edgeMode() { // GetEdgeMode
		return sendMessageDirect(2362, 0, 0);
	}

	/// The edge may be displayed by a line (EDGE_LINE) or by highlighting text that
	/// goes beyond it (EDGE_BACKGROUND) or not displayed at all (EDGE_NONE).
	final @property void edgeMode(int mode) { // SetEdgeMode
		sendMessageDirect(2363, mode, 0);
	}

	/// Retrieve the colour used in edge indication.
	final @property Colour edgeColour() { // GetEdgeColour
		return cast(Colour)sendMessageDirect(2364, 0, 0);
	}

	/// Change the colour used in edge indication.
	final @property void edgeColour(Colour edgeColour) { // SetEdgeColour
		sendMessageDirect(2365, edgeColour.rgb, 0);
	}

	/// Sets the current caret position to be the search anchor.
	final void searchAnchor() { // SearchAnchor
		sendMessageDirect(2366, 0, 0);
	}

	/// Find some text starting at the search anchor.
	/// Does not ensure the selection is visible.
	final int searchNext(int flags, in char[] text) { // SearchNext
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2367, flags, cast(size_t) __cstr_text);
	}

	/// Find some text starting at the search anchor and moving backwards.
	/// Does not ensure the selection is visible.
	final int searchPrev(int flags, in char[] text) { // SearchPrev
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2368, flags, cast(size_t) __cstr_text);
	}

	/// Retrieves the number of lines completely visible.
	final @property int linesOnScreen() { // LinesOnScreen
		return sendMessageDirect(2370, 0, 0);
	}

	/// Set whether a pop up menu is displayed automatically when the user presses
	/// the wrong mouse button.
	final void usePopUp(bool allowPopUp) { // UsePopUp
		sendMessageDirect(2371, allowPopUp, 0);
	}

	/// Is the selection rectangular? The alternative is the more common stream selection.
	final @property bool selectionIsRectangle() { // SelectionIsRectangle
		return !!sendMessageDirect(2372, 0, 0);
	}

	/// Set the zoom level. This number of points is added to the size of all fonts.
	/// It may be positive to magnify or negative to reduce.
	final @property void zoom(int zoom) { // SetZoom
		sendMessageDirect(2373, zoom, 0);
	}

	/// Retrieve the zoom level.
	final @property int zoom() { // GetZoom
		return sendMessageDirect(2374, 0, 0);
	}

	/// Create a new document object.
	/// Starts with reference count of 1 and not selected into editor.
	final int createDocument() { // CreateDocument
		return sendMessageDirect(2375, 0, 0);
	}

	/// Extend life of document.
	final void addRefDocument(int doc) { // AddRefDocument
		sendMessageDirect(2376, 0, doc);
	}

	/// Release a reference to the document, deleting document if it fades to black.
	final void releaseDocument(int doc) { // ReleaseDocument
		sendMessageDirect(2377, 0, doc);
	}

	/// Get which document modification events are sent to the container.
	final @property int modEventMask() { // GetModEventMask
		return sendMessageDirect(2378, 0, 0);
	}

	/// Change internal focus flag.
	final @property void focus(bool focus) { // SetFocus
		sendMessageDirect(2380, focus, 0);
	}

	/// Get internal focus flag.
	final @property bool focus() { // GetFocus
		return !!sendMessageDirect(2381, 0, 0);
	}

	/// Change error status - 0 = OK.
	final @property void status(int statusCode) { // SetStatus
		sendMessageDirect(2382, statusCode, 0);
	}

	/// Get error status.
	final @property int status() { // GetStatus
		return sendMessageDirect(2383, 0, 0);
	}

	/// Set whether the mouse is captured when its button is pressed.
	final @property void mouseDownCaptures(bool captures) { // SetMouseDownCaptures
		sendMessageDirect(2384, captures, 0);
	}

	/// Get whether mouse gets captured.
	final @property bool mouseDownCaptures() { // GetMouseDownCaptures
		return !!sendMessageDirect(2385, 0, 0);
	}

	/// Sets the cursor to one of the SC_CURSOR* values.
	final @property void cursor(int cursorType) { // SetCursor
		sendMessageDirect(2386, cursorType, 0);
	}

	/// Get cursor type.
	final @property int cursor() { // GetCursor
		return sendMessageDirect(2387, 0, 0);
	}

	/// Change the way control characters are displayed:
	/// If symbol is < 32, keep the drawn way, else, use the given character.
	final @property void controlCharSymbol(int symbol) { // SetControlCharSymbol
		sendMessageDirect(2388, symbol, 0);
	}

	/// Get the way control characters are displayed.
	final @property int controlCharSymbol() { // GetControlCharSymbol
		return sendMessageDirect(2389, 0, 0);
	}

	/// Move to the previous change in capitalisation.
	final void wordPartLeft() { // WordPartLeft
		sendMessageDirect(2390, 0, 0);
	}

	/// Move to the previous change in capitalisation extending selection
	/// to new caret position.
	final void wordPartLeftExtend() { // WordPartLeftExtend
		sendMessageDirect(2391, 0, 0);
	}

	/// Move to the change next in capitalisation.
	final void wordPartRight() { // WordPartRight
		sendMessageDirect(2392, 0, 0);
	}

	/// Move to the next change in capitalisation extending selection
	/// to new caret position.
	final void wordPartRightExtend() { // WordPartRightExtend
		sendMessageDirect(2393, 0, 0);
	}

	/// Set the way the display area is determined when a particular line
	/// is to be moved to by Find, FindNext, GotoLine, etc.
	final void setVisiblePolicy(int visiblePolicy, int visibleSlop) { // SetVisiblePolicy
		sendMessageDirect(2394, visiblePolicy, visibleSlop);
	}

	/// Delete back from the current position to the start of the line.
	final void delLineLeft() { // DelLineLeft
		sendMessageDirect(2395, 0, 0);
	}

	/// Delete forwards from the current position to the end of the line.
	final void delLineRight() { // DelLineRight
		sendMessageDirect(2396, 0, 0);
	}

	/// Get and Set the xOffset (ie, horizonal scroll position).
	final @property void xOffset(int newOffset) { // SetXOffset
		sendMessageDirect(2397, newOffset, 0);
	}

	final @property int xOffset() { // GetXOffset
		return sendMessageDirect(2398, 0, 0);
	}

	/// Set the last x chosen value to be the caret x position.
	final void chooseCaretX() { // ChooseCaretX
		sendMessageDirect(2399, 0, 0);
	}

	/// Set the focus to this Scintilla widget.
	final void grabFocus() { // GrabFocus
		sendMessageDirect(2400, 0, 0);
	}

	/// Set the way the caret is kept visible when going sideway.
	/// The exclusion zone is given in pixels.
	final void setXCaretPolicy(int caretPolicy, int caretSlop) { // SetXCaretPolicy
		sendMessageDirect(2402, caretPolicy, caretSlop);
	}

	/// Set the way the line the caret is on is kept visible.
	/// The exclusion zone is given in lines.
	final void setYCaretPolicy(int caretPolicy, int caretSlop) { // SetYCaretPolicy
		sendMessageDirect(2403, caretPolicy, caretSlop);
	}

	/// Set printing to line wrapped (SC_WRAP_WORD) or not line wrapped (SC_WRAP_NONE).
	final @property void printWrapMode(int mode) { // SetPrintWrapMode
		sendMessageDirect(2406, mode, 0);
	}

	/// Is printing line wrapped?
	final @property int printWrapMode() { // GetPrintWrapMode
		return sendMessageDirect(2407, 0, 0);
	}

	/// Set a fore colour for active hotspots.
	final void setHotspotActiveFore(bool useSetting, Colour fore) { // SetHotspotActiveFore
		sendMessageDirect(2410, useSetting, fore.rgb);
	}

	/// Get the fore colour for active hotspots.
	final @property Colour hotspotActiveFore() { // GetHotspotActiveFore
		return cast(Colour)sendMessageDirect(2494, 0, 0);
	}

	/// Set a back colour for active hotspots.
	final void setHotspotActiveBack(bool useSetting, Colour back) { // SetHotspotActiveBack
		sendMessageDirect(2411, useSetting, back.rgb);
	}

	/// Get the back colour for active hotspots.
	final @property Colour hotspotActiveBack() { // GetHotspotActiveBack
		return cast(Colour)sendMessageDirect(2495, 0, 0);
	}

	/// Enable / Disable underlining active hotspots.
	final @property void hotspotActiveUnderline(bool underline) { // SetHotspotActiveUnderline
		sendMessageDirect(2412, underline, 0);
	}

	/// Get whether underlining for active hotspots.
	final @property bool hotspotActiveUnderline() { // GetHotspotActiveUnderline
		return !!sendMessageDirect(2496, 0, 0);
	}

	/// Limit hotspots to single line so hotspots on two lines don't merge.
	final @property void hotspotSingleLine(bool singleLine) { // SetHotspotSingleLine
		sendMessageDirect(2421, singleLine, 0);
	}

	/// Get the HotspotSingleLine property
	final @property bool hotspotSingleLine() { // GetHotspotSingleLine
		return !!sendMessageDirect(2497, 0, 0);
	}

	/// Move caret between paragraphs (delimited by empty lines).
	final void paraDown() { // ParaDown
		sendMessageDirect(2413, 0, 0);
	}

	final void paraDownExtend() { // ParaDownExtend
		sendMessageDirect(2414, 0, 0);
	}

	final void paraUp() { // ParaUp
		sendMessageDirect(2415, 0, 0);
	}

	final void paraUpExtend() { // ParaUpExtend
		sendMessageDirect(2416, 0, 0);
	}

	/// Given a valid document position, return the previous position taking code
	/// page into account. Returns 0 if passed 0.
	final Position positionBefore(Position pos) { // PositionBefore
		return sendMessageDirect(2417, pos, 0);
	}

	/// Given a valid document position, return the next position taking code
	/// page into account. Maximum value returned is the last position in the document.
	final Position positionAfter(Position pos) { // PositionAfter
		return sendMessageDirect(2418, pos, 0);
	}

	/// Copy a range of text to the clipboard. Positions are clipped into the document.
	final void copyRange(Position start, Position end) { // CopyRange
		sendMessageDirect(2419, start, end);
	}

	/// Copy argument text to the clipboard.
	final void copyText(in char[] text) { // CopyText
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2420, __len+1, cast(size_t) __cstr_text);
	}

	/// Set the selection mode to stream (SC_SEL_STREAM) or rectangular (SC_SEL_RECTANGLE/SC_SEL_THIN) or
	/// by lines (SC_SEL_LINES).
	final @property void selectionMode(int mode) { // SetSelectionMode
		sendMessageDirect(2422, mode, 0);
	}

	/// Get the mode of the current selection.
	final @property int selectionMode() { // GetSelectionMode
		return sendMessageDirect(2423, 0, 0);
	}

	/// Retrieve the position of the start of the selection at the given line (INVALID_POSITION if no selection on this line).
	final Position getLineSelStartPosition(int line) { // GetLineSelStartPosition
		return sendMessageDirect(2424, line, 0);
	}

	/// Retrieve the position of the end of the selection at the given line (INVALID_POSITION if no selection on this line).
	final Position getLineSelEndPosition(int line) { // GetLineSelEndPosition
		return sendMessageDirect(2425, line, 0);
	}

	/// Move caret down one line, extending rectangular selection to new caret position.
	final void lineDownRectExtend() { // LineDownRectExtend
		sendMessageDirect(2426, 0, 0);
	}

	/// Move caret up one line, extending rectangular selection to new caret position.
	final void lineUpRectExtend() { // LineUpRectExtend
		sendMessageDirect(2427, 0, 0);
	}

	/// Move caret left one character, extending rectangular selection to new caret position.
	final void charLeftRectExtend() { // CharLeftRectExtend
		sendMessageDirect(2428, 0, 0);
	}

	/// Move caret right one character, extending rectangular selection to new caret position.
	final void charRightRectExtend() { // CharRightRectExtend
		sendMessageDirect(2429, 0, 0);
	}

	/// Move caret to first position on line, extending rectangular selection to new caret position.
	final void homeRectExtend() { // HomeRectExtend
		sendMessageDirect(2430, 0, 0);
	}

	/// Move caret to before first visible character on line.
	/// If already there move to first character on line.
	/// In either case, extend rectangular selection to new caret position.
	final void vCHomeRectExtend() { // VCHomeRectExtend
		sendMessageDirect(2431, 0, 0);
	}

	/// Move caret to last position on line, extending rectangular selection to new caret position.
	final void lineEndRectExtend() { // LineEndRectExtend
		sendMessageDirect(2432, 0, 0);
	}

	/// Move caret one page up, extending rectangular selection to new caret position.
	final void pageUpRectExtend() { // PageUpRectExtend
		sendMessageDirect(2433, 0, 0);
	}

	/// Move caret one page down, extending rectangular selection to new caret position.
	final void pageDownRectExtend() { // PageDownRectExtend
		sendMessageDirect(2434, 0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page.
	final void stutteredPageUp() { // StutteredPageUp
		sendMessageDirect(2435, 0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page, extending selection to new caret position.
	final void stutteredPageUpExtend() { // StutteredPageUpExtend
		sendMessageDirect(2436, 0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page.
	final void stutteredPageDown() { // StutteredPageDown
		sendMessageDirect(2437, 0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page, extending selection to new caret position.
	final void stutteredPageDownExtend() { // StutteredPageDownExtend
		sendMessageDirect(2438, 0, 0);
	}

	/// Move caret left one word, position cursor at end of word.
	final void wordLeftEnd() { // WordLeftEnd
		sendMessageDirect(2439, 0, 0);
	}

	/// Move caret left one word, position cursor at end of word, extending selection to new caret position.
	final void wordLeftEndExtend() { // WordLeftEndExtend
		sendMessageDirect(2440, 0, 0);
	}

	/// Move caret right one word, position cursor at end of word.
	final void wordRightEnd() { // WordRightEnd
		sendMessageDirect(2441, 0, 0);
	}

	/// Move caret right one word, position cursor at end of word, extending selection to new caret position.
	final void wordRightEndExtend() { // WordRightEndExtend
		sendMessageDirect(2442, 0, 0);
	}

	/// Set the set of characters making up whitespace for when moving or selecting by word.
	/// Should be called after SetWordChars.
	final @property void whitespaceChars(in char[] characters) { // SetWhitespaceChars
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characters = toCString(__sibuff, characters, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2443, 0, cast(size_t) __cstr_characters);
	}

	/// Reset the set of characters for whitespace and word characters to the defaults.
	final void setCharsDefault() { // SetCharsDefault
		sendMessageDirect(2444, 0, 0);
	}

	/// Get currently selected item position in the auto-completion list
	final int autoCGetCurrent() { // AutoCGetCurrent
		return sendMessageDirect(2445, 0, 0);
	}

	/// Get currently selected item text in the auto-completion list
	/// Returns the length of the item text
	final string autoCGetCurrentText() { // AutoCGetCurrentText
		immutable __len = sendMessageDirect(2610, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2610, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int autoCGetCurrentText(char[] sBuff, out char[] s) {
		alias sBuff buff;
		return toDStringBuff(sendMessageDirect(2610, 0, cast(size_t) buff.ptr), sBuff, s);
	}

	/// Enlarge the document to a particular size of text bytes.
	final void allocate(int bytes) { // Allocate
		sendMessageDirect(2446, bytes, 0);
	}

	/// Returns the target converted to UTF8.
	/// Return the length in bytes.
	final string targetAsUTF8() { // TargetAsUTF8
		immutable __len = sendMessageDirect(2447, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2447, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int targetAsUTF8(char[] sBuff, out char[] s) {
		alias sBuff buff;
		return toDStringBuff(sendMessageDirect(2447, 0, cast(size_t) buff.ptr), sBuff, s);
	}

	/// Set the length of the utf8 argument for calling EncodedFromUTF8.
	/// Set to -1 and the string will be measured to the first nul.
	final void setLengthForEncode(int bytes) { // SetLengthForEncode
		sendMessageDirect(2448, bytes, 0);
	}

	/// Translates a UTF8 string into the document encoding.
	/// Return the length of the result in bytes.
	/// On error return 0.
	final string encodedFromUTF8(in char[] utf8) { // EncodedFromUTF8
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_utf8 = toCString(__sibuff, utf8, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(2449, cast(size_t) __cstr_utf8, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2449, cast(size_t) __cstr_utf8, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int encodedFromUTF8(char[] encodedBuff, in char[] utf8, out char[] encoded) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_utf8 = toCString(__sibuff, utf8, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		alias encodedBuff buff;
		return toDStringBuff(sendMessageDirect(2449, cast(size_t) __cstr_utf8, cast(size_t) buff.ptr), encodedBuff, encoded);
	}

	/// Find the position of a column on a line taking into account tabs and
	/// multi-byte characters. If beyond end of line, return line end position.
	final int findColumn(int line, int column) { // FindColumn
		return sendMessageDirect(2456, line, column);
	}

	/// Can the caret preferred x position only be changed by explicit movement commands?
	final @property int caretSticky() { // GetCaretSticky
		return sendMessageDirect(2457, 0, 0);
	}

	/// Stop the caret preferred x position changing when the user types.
	final @property void caretSticky(int useCaretStickyBehaviour) { // SetCaretSticky
		sendMessageDirect(2458, useCaretStickyBehaviour, 0);
	}

	/// Switch between sticky and non-sticky: meant to be bound to a key.
	final void toggleCaretSticky() { // ToggleCaretSticky
		sendMessageDirect(2459, 0, 0);
	}

	/// Enable/Disable convert-on-paste for line endings
	final @property void pasteConvertEndings(bool convert) { // SetPasteConvertEndings
		sendMessageDirect(2467, convert, 0);
	}

	/// Get convert-on-paste setting
	final @property bool pasteConvertEndings() { // GetPasteConvertEndings
		return !!sendMessageDirect(2468, 0, 0);
	}

	/// Duplicate the selection. If selection empty duplicate the line containing the caret.
	final void selectionDuplicate() { // SelectionDuplicate
		sendMessageDirect(2469, 0, 0);
	}

	/// Set background alpha of the caret line.
	final @property void caretLineBackAlpha(int alpha) { // SetCaretLineBackAlpha
		sendMessageDirect(2470, alpha, 0);
	}

	/// Get the background alpha of the caret line.
	final @property int caretLineBackAlpha() { // GetCaretLineBackAlpha
		return sendMessageDirect(2471, 0, 0);
	}

	/// Set the style of the caret to be drawn.
	final @property void caretStyle(int caretStyle) { // SetCaretStyle
		sendMessageDirect(2512, caretStyle, 0);
	}

	/// Returns the current style of the caret.
	final @property int caretStyle() { // GetCaretStyle
		return sendMessageDirect(2513, 0, 0);
	}

	/// Set the indicator used for IndicatorFillRange and IndicatorClearRange
	final @property void indicatorCurrent(int indicator) { // SetIndicatorCurrent
		sendMessageDirect(2500, indicator, 0);
	}

	/// Get the current indicator
	final @property int indicatorCurrent() { // GetIndicatorCurrent
		return sendMessageDirect(2501, 0, 0);
	}

	/// Set the value used for IndicatorFillRange
	final @property void indicatorValue(int value) { // SetIndicatorValue
		sendMessageDirect(2502, value, 0);
	}

	/// Get the current indicator vaue
	final @property int indicatorValue() { // GetIndicatorValue
		return sendMessageDirect(2503, 0, 0);
	}

	/// Turn a indicator on over a range.
	final void indicatorFillRange(int position, int fillLength) { // IndicatorFillRange
		sendMessageDirect(2504, position, fillLength);
	}

	/// Turn a indicator off over a range.
	final void indicatorClearRange(int position, int clearLength) { // IndicatorClearRange
		sendMessageDirect(2505, position, clearLength);
	}

	/// Are any indicators present at position?
	final int indicatorAllOnFor(int position) { // IndicatorAllOnFor
		return sendMessageDirect(2506, position, 0);
	}

	/// What value does a particular indicator have at at a position?
	final int indicatorValueAt(int indicator, int position) { // IndicatorValueAt
		return sendMessageDirect(2507, indicator, position);
	}

	/// Where does a particular indicator start?
	final int indicatorStart(int indicator, int position) { // IndicatorStart
		return sendMessageDirect(2508, indicator, position);
	}

	/// Where does a particular indicator end?
	final int indicatorEnd(int indicator, int position) { // IndicatorEnd
		return sendMessageDirect(2509, indicator, position);
	}

	/// Set number of entries in position cache
	final @property void positionCache(int size) { // SetPositionCache
		sendMessageDirect(2514, size, 0);
	}

	/// How many entries are allocated to the position cache?
	final @property int positionCache() { // GetPositionCache
		return sendMessageDirect(2515, 0, 0);
	}

	/// Copy the selection, if selection empty copy the line with the caret
	final void copyAllowLine() { // CopyAllowLine
		sendMessageDirect(2519, 0, 0);
	}

	/// Compact the document buffer and return a read-only pointer to the
	/// characters in the document.
	final @property int characterPointer() { // GetCharacterPointer
		return sendMessageDirect(2520, 0, 0);
	}

	/// Always interpret keyboard input as Unicode
	final @property void keysUnicode(bool keysUnicode) { // SetKeysUnicode
		sendMessageDirect(2521, keysUnicode, 0);
	}

	/// Are keys always interpreted as Unicode?
	final @property bool keysUnicode() { // GetKeysUnicode
		return !!sendMessageDirect(2522, 0, 0);
	}

	/// Set the alpha fill colour of the given indicator.
	final void indicSetAlpha(int indicator, int alpha) { // IndicSetAlpha
		sendMessageDirect(2523, indicator, alpha);
	}

	/// Get the alpha fill colour of the given indicator.
	final int indicGetAlpha(int indicator) { // IndicGetAlpha
		return sendMessageDirect(2524, indicator, 0);
	}

	/// Set the alpha outline colour of the given indicator.
	final void indicSetOutlineAlpha(int indicator, int alpha) { // IndicSetOutlineAlpha
		sendMessageDirect(2558, indicator, alpha);
	}

	/// Get the alpha outline colour of the given indicator.
	final int indicGetOutlineAlpha(int indicator) { // IndicGetOutlineAlpha
		return sendMessageDirect(2559, indicator, 0);
	}

	/// Set extra ascent for each line
	final @property void extraAscent(int extraAscent) { // SetExtraAscent
		sendMessageDirect(2525, extraAscent, 0);
	}

	/// Get extra ascent for each line
	final @property int extraAscent() { // GetExtraAscent
		return sendMessageDirect(2526, 0, 0);
	}

	/// Set extra descent for each line
	final @property void extraDescent(int extraDescent) { // SetExtraDescent
		sendMessageDirect(2527, extraDescent, 0);
	}

	/// Get extra descent for each line
	final @property int extraDescent() { // GetExtraDescent
		return sendMessageDirect(2528, 0, 0);
	}

	/// Which symbol was defined for markerNumber with MarkerDefine
	final int markerSymbolDefined(int markerNumber) { // MarkerSymbolDefined
		return sendMessageDirect(2529, markerNumber, 0);
	}

	/// Set the text in the text margin for a line
	final void marginSetText(int line, in char[] text) { // MarginSetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2530, line, cast(size_t) __cstr_text);
	}

	/// Get the text in the text margin for a line
	final string marginGetText(int line) { // MarginGetText
		immutable __len = sendMessageDirect(2531, line, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2531, line, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int marginGetText(char[] textBuff, int line, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(2531, line, cast(size_t) buff.ptr), textBuff, text);
	}

	/// Set the style number for the text margin for a line
	final void marginSetStyle(int line, int style) { // MarginSetStyle
		sendMessageDirect(2532, line, style);
	}

	/// Get the style number for the text margin for a line
	final int marginGetStyle(int line) { // MarginGetStyle
		return sendMessageDirect(2533, line, 0);
	}

	/// Set the style in the text margin for a line
	final void marginSetStyles(int line, in char[] styles) { // MarginSetStyles
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_styles = toCString(__sibuff, styles, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2534, line, cast(size_t) __cstr_styles);
	}

	/// Get the styles in the text margin for a line
	final string marginGetStyles(int line) { // MarginGetStyles
		immutable __len = sendMessageDirect(2535, line, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2535, line, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int marginGetStyles(char[] stylesBuff, int line, out char[] styles) {
		alias stylesBuff buff;
		return toDStringBuff(sendMessageDirect(2535, line, cast(size_t) buff.ptr), stylesBuff, styles);
	}

	/// Clear the margin text on all lines
	final void marginTextClearAll() { // MarginTextClearAll
		sendMessageDirect(2536, 0, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	final @property void marginSetStyleOffset(int style) { // MarginSetStyleOffset
		sendMessageDirect(2537, style, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	final @property int marginGetStyleOffset() { // MarginGetStyleOffset
		return sendMessageDirect(2538, 0, 0);
	}

	/// Set the margin options.
	final @property void marginOptions(int marginOptions) { // SetMarginOptions
		sendMessageDirect(2539, marginOptions, 0);
	}

	/// Get the margin options.
	final @property int marginOptions() { // GetMarginOptions
		return sendMessageDirect(2557, 0, 0);
	}

	/// Set the annotation text for a line
	final void annotationSetText(int line, in char[] text) { // AnnotationSetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2540, line, cast(size_t) __cstr_text);
	}

	/// Get the annotation text for a line
	final string annotationGetText(int line) { // AnnotationGetText
		immutable __len = sendMessageDirect(2541, line, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2541, line, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int annotationGetText(char[] textBuff, int line, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(2541, line, cast(size_t) buff.ptr), textBuff, text);
	}

	/// Set the style number for the annotations for a line
	final void annotationSetStyle(int line, int style) { // AnnotationSetStyle
		sendMessageDirect(2542, line, style);
	}

	/// Get the style number for the annotations for a line
	final int annotationGetStyle(int line) { // AnnotationGetStyle
		return sendMessageDirect(2543, line, 0);
	}

	/// Set the annotation styles for a line
	final void annotationSetStyles(int line, in char[] styles) { // AnnotationSetStyles
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_styles = toCString(__sibuff, styles, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2544, line, cast(size_t) __cstr_styles);
	}

	/// Get the annotation styles for a line
	final string annotationGetStyles(int line) { // AnnotationGetStyles
		immutable __len = sendMessageDirect(2545, line, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(2545, line, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int annotationGetStyles(char[] stylesBuff, int line, out char[] styles) {
		alias stylesBuff buff;
		return toDStringBuff(sendMessageDirect(2545, line, cast(size_t) buff.ptr), stylesBuff, styles);
	}

	/// Get the number of annotation lines for a line
	final int annotationGetLines(int line) { // AnnotationGetLines
		return sendMessageDirect(2546, line, 0);
	}

	/// Clear the annotations from all lines
	final void annotationClearAll() { // AnnotationClearAll
		sendMessageDirect(2547, 0, 0);
	}

	/// Set the visibility for the annotations for a view
	final @property void annotationSetVisible(int visible) { // AnnotationSetVisible
		sendMessageDirect(2548, visible, 0);
	}

	/// Get the visibility for the annotations for a view
	final @property int annotationGetVisible() { // AnnotationGetVisible
		return sendMessageDirect(2549, 0, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	final @property void annotationSetStyleOffset(int style) { // AnnotationSetStyleOffset
		sendMessageDirect(2550, style, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	final @property int annotationGetStyleOffset() { // AnnotationGetStyleOffset
		return sendMessageDirect(2551, 0, 0);
	}

	/// Add a container action to the undo stack
	final void addUndoAction(int token, int flags) { // AddUndoAction
		sendMessageDirect(2560, token, flags);
	}

	/// Find the position of a character from a point within the window.
	final Position charPositionFromPoint(int x, int y) { // CharPositionFromPoint
		return sendMessageDirect(2561, x, y);
	}

	/// Find the position of a character from a point within the window.
	/// Return INVALID_POSITION if not close to text.
	final Position charPositionFromPointClose(int x, int y) { // CharPositionFromPointClose
		return sendMessageDirect(2562, x, y);
	}

	/// Set whether multiple selections can be made
	final @property void multipleSelection(bool multipleSelection) { // SetMultipleSelection
		sendMessageDirect(2563, multipleSelection, 0);
	}

	/// Whether multiple selections can be made
	final @property bool multipleSelection() { // GetMultipleSelection
		return !!sendMessageDirect(2564, 0, 0);
	}

	/// Set whether typing can be performed into multiple selections
	final @property void additionalSelectionTyping(bool additionalSelectionTyping) { // SetAdditionalSelectionTyping
		sendMessageDirect(2565, additionalSelectionTyping, 0);
	}

	/// Whether typing can be performed into multiple selections
	final @property bool additionalSelectionTyping() { // GetAdditionalSelectionTyping
		return !!sendMessageDirect(2566, 0, 0);
	}

	/// Set whether additional carets will blink
	final @property void additionalCaretsBlink(bool additionalCaretsBlink) { // SetAdditionalCaretsBlink
		sendMessageDirect(2567, additionalCaretsBlink, 0);
	}

	/// Whether additional carets will blink
	final @property bool additionalCaretsBlink() { // GetAdditionalCaretsBlink
		return !!sendMessageDirect(2568, 0, 0);
	}

	/// Set whether additional carets are visible
	final @property void additionalCaretsVisible(bool additionalCaretsBlink) { // SetAdditionalCaretsVisible
		sendMessageDirect(2608, additionalCaretsBlink, 0);
	}

	/// Whether additional carets are visible
	final @property bool additionalCaretsVisible() { // GetAdditionalCaretsVisible
		return !!sendMessageDirect(2609, 0, 0);
	}

	/// How many selections are there?
	final @property int selections() { // GetSelections
		return sendMessageDirect(2570, 0, 0);
	}

	/// Clear selections to a single empty stream selection
	final void clearSelections() { // ClearSelections
		sendMessageDirect(2571, 0, 0);
	}

	/// Set a simple selection
	final int setSelection(int caret, int anchor) { // SetSelection
		return sendMessageDirect(2572, caret, anchor);
	}

	/// Add a selection
	final int addSelection(int caret, int anchor) { // AddSelection
		return sendMessageDirect(2573, caret, anchor);
	}

	/// Set the main selection
	final @property void mainSelection(int selection) { // SetMainSelection
		sendMessageDirect(2574, selection, 0);
	}

	/// Which selection is the main selection
	final @property int mainSelection() { // GetMainSelection
		return sendMessageDirect(2575, 0, 0);
	}

	final void setSelectionNCaret(int selection, Position pos) { // SetSelectionNCaret
		sendMessageDirect(2576, selection, pos);
	}

	final Position getSelectionNCaret(int selection) { // GetSelectionNCaret
		return sendMessageDirect(2577, selection, 0);
	}

	final void setSelectionNAnchor(int selection, Position posAnchor) { // SetSelectionNAnchor
		sendMessageDirect(2578, selection, posAnchor);
	}

	final Position getSelectionNAnchor(int selection) { // GetSelectionNAnchor
		return sendMessageDirect(2579, selection, 0);
	}

	final void setSelectionNCaretVirtualSpace(int selection, int space) { // SetSelectionNCaretVirtualSpace
		sendMessageDirect(2580, selection, space);
	}

	final int getSelectionNCaretVirtualSpace(int selection) { // GetSelectionNCaretVirtualSpace
		return sendMessageDirect(2581, selection, 0);
	}

	final void setSelectionNAnchorVirtualSpace(int selection, int space) { // SetSelectionNAnchorVirtualSpace
		sendMessageDirect(2582, selection, space);
	}

	final int getSelectionNAnchorVirtualSpace(int selection) { // GetSelectionNAnchorVirtualSpace
		return sendMessageDirect(2583, selection, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	final void setSelectionNStart(int selection, Position pos) { // SetSelectionNStart
		sendMessageDirect(2584, selection, pos);
	}

	/// Returns the position at the start of the selection.
	final Position getSelectionNStart(int selection) { // GetSelectionNStart
		return sendMessageDirect(2585, selection, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	final void setSelectionNEnd(int selection, Position pos) { // SetSelectionNEnd
		sendMessageDirect(2586, selection, pos);
	}

	/// Returns the position at the end of the selection.
	final Position getSelectionNEnd(int selection) { // GetSelectionNEnd
		return sendMessageDirect(2587, selection, 0);
	}

	final @property void rectangularSelectionCaret(Position pos) { // SetRectangularSelectionCaret
		sendMessageDirect(2588, pos, 0);
	}

	final @property Position rectangularSelectionCaret() { // GetRectangularSelectionCaret
		return sendMessageDirect(2589, 0, 0);
	}

	final @property void rectangularSelectionAnchor(Position posAnchor) { // SetRectangularSelectionAnchor
		sendMessageDirect(2590, posAnchor, 0);
	}

	final @property Position rectangularSelectionAnchor() { // GetRectangularSelectionAnchor
		return sendMessageDirect(2591, 0, 0);
	}

	final @property void rectangularSelectionCaretVirtualSpace(int space) { // SetRectangularSelectionCaretVirtualSpace
		sendMessageDirect(2592, space, 0);
	}

	final @property int rectangularSelectionCaretVirtualSpace() { // GetRectangularSelectionCaretVirtualSpace
		return sendMessageDirect(2593, 0, 0);
	}

	final @property void rectangularSelectionAnchorVirtualSpace(int space) { // SetRectangularSelectionAnchorVirtualSpace
		sendMessageDirect(2594, space, 0);
	}

	final @property int rectangularSelectionAnchorVirtualSpace() { // GetRectangularSelectionAnchorVirtualSpace
		return sendMessageDirect(2595, 0, 0);
	}

	final @property void virtualSpaceOptions(int virtualSpaceOptions) { // SetVirtualSpaceOptions
		sendMessageDirect(2596, virtualSpaceOptions, 0);
	}

	final @property int virtualSpaceOptions() { // GetVirtualSpaceOptions
		return sendMessageDirect(2597, 0, 0);
	}

	/// On GTK+, allow selecting the modifier key to use for mouse-based
	/// rectangular selection. Often the window manager requires Alt+Mouse Drag
	/// for moving windows.
	/// Valid values are SCMOD_CTRL(default), SCMOD_ALT, or SCMOD_SUPER.
	final @property void rectangularSelectionModifier(int modifier) { // SetRectangularSelectionModifier
		sendMessageDirect(2598, modifier, 0);
	}

	/// Get the modifier key used for rectangular selection.
	final @property int rectangularSelectionModifier() { // GetRectangularSelectionModifier
		return sendMessageDirect(2599, 0, 0);
	}

	/// Set the foreground colour of additional selections.
	/// Must have previously called SetSelFore with non-zero first argument for this to have an effect.
	final @property void additionalSelFore(Colour fore) { // SetAdditionalSelFore
		sendMessageDirect(2600, fore.rgb, 0);
	}

	/// Set the background colour of additional selections.
	/// Must have previously called SetSelBack with non-zero first argument for this to have an effect.
	final @property void additionalSelBack(Colour back) { // SetAdditionalSelBack
		sendMessageDirect(2601, back.rgb, 0);
	}

	/// Set the alpha of the selection.
	final @property void additionalSelAlpha(int alpha) { // SetAdditionalSelAlpha
		sendMessageDirect(2602, alpha, 0);
	}

	/// Get the alpha of the selection.
	final @property int additionalSelAlpha() { // GetAdditionalSelAlpha
		return sendMessageDirect(2603, 0, 0);
	}

	/// Set the foreground colour of additional carets.
	final @property void additionalCaretFore(Colour fore) { // SetAdditionalCaretFore
		sendMessageDirect(2604, fore.rgb, 0);
	}

	/// Get the foreground colour of additional carets.
	final @property Colour additionalCaretFore() { // GetAdditionalCaretFore
		return cast(Colour)sendMessageDirect(2605, 0, 0);
	}

	/// Set the main selection to the next selection.
	final void rotateSelection() { // RotateSelection
		sendMessageDirect(2606, 0, 0);
	}

	/// Swap that caret and anchor of the main selection.
	final void swapMainAnchorCaret() { // SwapMainAnchorCaret
		sendMessageDirect(2607, 0, 0);
	}

	/// Indicate that the internal state of a lexer has changed over a range and therefore
	/// there may be a need to redraw.
	final int changeLexerState(Position start, Position end) { // ChangeLexerState
		return sendMessageDirect(2617, start, end);
	}

	/// Find the next line at or after lineStart that is a contracted fold header line.
	/// Return -1 when no more lines.
	final int contractedFoldNext(int lineStart) { // ContractedFoldNext
		return sendMessageDirect(2618, lineStart, 0);
	}

	/// Centre current line in window.
	final void verticalCentreCaret() { // VerticalCentreCaret
		sendMessageDirect(2619, 0, 0);
	}

	/// Move the selected lines up one line, shifting the line above after the selection
	final void moveSelectedLinesUp() { // MoveSelectedLinesUp
		sendMessageDirect(2620, 0, 0);
	}

	/// Move the selected lines down one line, shifting the line below before the selection
	final void moveSelectedLinesDown() { // MoveSelectedLinesDown
		sendMessageDirect(2621, 0, 0);
	}

	/// Set the identifier reported as idFrom in notification messages.
	final @property void identifier(int identifier) { // SetIdentifier
		sendMessageDirect(2622, identifier, 0);
	}

	/// Get the identifier.
	final @property int identifier() { // GetIdentifier
		return sendMessageDirect(2623, 0, 0);
	}

	/// Set the width for future RGBA image data.
	final @property void rGBAImageSetWidth(int width) { // RGBAImageSetWidth
		sendMessageDirect(2624, width, 0);
	}

	/// Set the height for future RGBA image data.
	final @property void rGBAImageSetHeight(int height) { // RGBAImageSetHeight
		sendMessageDirect(2625, height, 0);
	}

	/// Define a marker from RGBA data.
	/// It has the width and height from RGBAImageSetWidth/Height
	final void markerDefineRGBAImage(int markerNumber, in char[] pixels) { // MarkerDefineRGBAImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixels = toCString(__sibuff, pixels, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2626, markerNumber, cast(size_t) __cstr_pixels);
	}

	/// Register an RGBA image for use in autocompletion lists.
	/// It has the width and height from RGBAImageSetWidth/Height
	final void registerRGBAImage(int type, in char[] pixels) { // RegisterRGBAImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixels = toCString(__sibuff, pixels, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2627, type, cast(size_t) __cstr_pixels);
	}

	/// Scroll to start of document.
	final void scrollToStart() { // ScrollToStart
		sendMessageDirect(2628, 0, 0);
	}

	/// Scroll to end of document.
	final void scrollToEnd() { // ScrollToEnd
		sendMessageDirect(2629, 0, 0);
	}

	/// Set the technolgy used.
	final @property void technology(int technology) { // SetTechnology
		sendMessageDirect(2630, technology, 0);
	}

	/// Get the tech.
	final @property int technology() { // GetTechnology
		return sendMessageDirect(2631, 0, 0);
	}

	/// Create an ILoader*.
	final int createLoader(int bytes) { // CreateLoader
		return sendMessageDirect(2632, bytes, 0);
	}

	/// Start notifying the container of all key presses and commands.
	final void startRecord() { // StartRecord
		sendMessageDirect(3001, 0, 0);
	}

	/// Stop notifying the container of all key presses and commands.
	final void stopRecord() { // StopRecord
		sendMessageDirect(3002, 0, 0);
	}

	/// Set the lexing language of the document.
	final @property void lexer(int lexer) { // SetLexer
		sendMessageDirect(4001, lexer, 0);
	}

	/// Retrieve the lexing language of the document.
	final @property int lexer() { // GetLexer
		return sendMessageDirect(4002, 0, 0);
	}

	/// Colourise a segment of the document using the current lexing language.
	final void colourise(Position start, Position end) { // Colourise
		sendMessageDirect(4003, start, end);
	}

	/// Set up a value that may be used by a lexer for some optional feature.
	final void setProperty(in char[] key, in char[] value) { // SetProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key, __cstr_value;
		toCStrings(__sibuff, key, value, __cstr_key, __cstr_value, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4004, cast(size_t) __cstr_key, cast(size_t) __cstr_value);
	}

	/// Set up the key words used by the lexer.
	final void setKeyWords(int keywordSet, in char[] keyWords) { // SetKeyWords
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_keyWords = toCString(__sibuff, keyWords, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4005, keywordSet, cast(size_t) __cstr_keyWords);
	}

	/// Set the lexing language of the document based on string name.
	final @property void lexerLanguage(in char[] language) { // SetLexerLanguage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_language = toCString(__sibuff, language, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4006, 0, cast(size_t) __cstr_language);
	}

	/// Load a lexer library (dll / so).
	final void loadLexerLibrary(in char[] path) { // LoadLexerLibrary
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_path = toCString(__sibuff, path, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4007, 0, cast(size_t) __cstr_path);
	}

	/// Retrieve a "property" value previously set with SetProperty.
	final string getProperty(in char[] key) { // GetProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4008, cast(size_t) __cstr_key, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4008, cast(size_t) __cstr_key, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getProperty(char[] bufBuff, in char[] key, out char[] buf) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		alias bufBuff buff;
		return toDStringBuff(sendMessageDirect(4008, cast(size_t) __cstr_key, cast(size_t) buff.ptr), bufBuff, buf);
	}

	/// Retrieve a "property" value previously set with SetProperty,
	/// with "$()" variable replacement on returned buffer.
	final string getPropertyExpanded(in char[] key) { // GetPropertyExpanded
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4009, cast(size_t) __cstr_key, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4009, cast(size_t) __cstr_key, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int getPropertyExpanded(char[] bufBuff, in char[] key, out char[] buf) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		alias bufBuff buff;
		return toDStringBuff(sendMessageDirect(4009, cast(size_t) __cstr_key, cast(size_t) buff.ptr), bufBuff, buf);
	}

	/// Retrieve a "property" value previously set with SetProperty,
	/// interpreted as an int AFTER any "$()" variable replacement.
	final int getPropertyInt(in char[] key) { // GetPropertyInt
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(4010, cast(size_t) __cstr_key, 0);
	}

	/// Retrieve the number of bits the current lexer needs for styling.
	final @property int styleBitsNeeded() { // GetStyleBitsNeeded
		return sendMessageDirect(4011, 0, 0);
	}

	/// Retrieve the name of the lexer.
	/// Return the length of the text.
	final @property string lexerLanguage() { // GetLexerLanguage
		immutable __len = sendMessageDirect(4012, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4012, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int lexerLanguage(char[] textBuff, out char[] text) {
		alias textBuff buff;
		return toDStringBuff(sendMessageDirect(4012, 0, cast(size_t) buff.ptr), textBuff, text);
	}

	/// For private communication between an application and a known lexer.
	final int privateLexerCall(int operation, int pointer) { // PrivateLexerCall
		return sendMessageDirect(4013, operation, pointer);
	}

	/// Retrieve a '\n' separated list of properties understood by the current lexer.
	final string propertyNames() { // PropertyNames
		immutable __len = sendMessageDirect(4014, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4014, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int propertyNames(char[] namesBuff, out char[] names) {
		alias namesBuff buff;
		return toDStringBuff(sendMessageDirect(4014, 0, cast(size_t) buff.ptr), namesBuff, names);
	}

	/// Retrieve the type of a property.
	final int propertyType(in char[] name) { // PropertyType
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(4015, cast(size_t) __cstr_name, 0);
	}

	/// Describe a property.
	final string describeProperty(in char[] name) { // DescribeProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4016, cast(size_t) __cstr_name, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4016, cast(size_t) __cstr_name, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int describeProperty(char[] descriptionBuff, in char[] name, out char[] description) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		alias descriptionBuff buff;
		return toDStringBuff(sendMessageDirect(4016, cast(size_t) __cstr_name, cast(size_t) buff.ptr), descriptionBuff, description);
	}

	/// Retrieve a '\n' separated list of descriptions of the keyword sets understood by the current lexer.
	final string describeKeyWordSets() { // DescribeKeyWordSets
		immutable __len = sendMessageDirect(4017, 0, 0);
		char[2] __sobuff;
		char* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;
		immutable __res = sendMessageDirect(4017, 0, cast(size_t) __optr);
		return toDString(__len, __optr);
	}

	/// ditto
	final int describeKeyWordSets(char[] descriptionsBuff, out char[] descriptions) {
		alias descriptionsBuff buff;
		return toDStringBuff(sendMessageDirect(4017, 0, cast(size_t) buff.ptr), descriptionsBuff, descriptions);
	}
}
