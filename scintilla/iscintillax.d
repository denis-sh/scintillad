﻿// Generated from ../Scintilla.iface-improvements/Scintilla.iface, Basics category
module scintilla.iscintilla;

import scintilla.types, scintilla.internal.functions;

interface IScintilla {
	sptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam);

	/// Add text to the document at current position.
	/// Generated from: 'fun void AddText=2001(len length, chars text)'
	final void addText(in char[] text) { // AddText
		sendMessageDirect(2001, text.length, cast(size_t) text.ptr);
	}

	/// Add array of cells to document.
	/// Generated from: 'fun void AddStyledText=2002(int length, cells c)'
	final void addStyledText(int length, in Cell[] c) { // AddStyledText
		sendMessageDirect(2002, length, cast(size_t) c.ptr);
	}

	/// Insert string at a position.
	/// Generated from: 'fun void InsertText=2003(position pos, cstring text)'
	final void insertText(Position pos, in char[] text) { // InsertText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2003, pos, cast(size_t) __cstr_text);
	}

	/// Delete all text in the document.
	/// Generated from: 'fun void ClearAll=2004(,)'
	final void clearAll() { // ClearAll
		sendMessageDirect(2004, 0, 0);
	}

	/// Set all style bytes to 0, remove all folding information.
	/// Generated from: 'fun void ClearDocumentStyle=2005(,)'
	final void clearDocumentStyle() { // ClearDocumentStyle
		sendMessageDirect(2005, 0, 0);
	}

	/// Returns the number of bytes in the document.
	/// Generated from: 'get int GetLength=2006(,)'
	final @property int length() { // GetLength
		return sendMessageDirect(2006, 0, 0);
	}

	/// Returns the character byte at the position.
	/// Generated from: 'get int GetCharAt=2007(position pos,)'
	final int getCharAt(Position pos) { // GetCharAt
		return sendMessageDirect(2007, pos, 0);
	}

	/// Returns the position of the caret.
	/// Generated from: 'get position GetCurrentPos=2008(,)'
	final @property Position currentPos() { // GetCurrentPos
		return sendMessageDirect(2008, 0, 0);
	}

	/// Returns the position of the opposite end of the selection to the caret.
	/// Generated from: 'get position GetAnchor=2009(,)'
	final @property Position anchor() { // GetAnchor
		return sendMessageDirect(2009, 0, 0);
	}

	/// Returns the style byte at the position.
	/// Generated from: 'get int GetStyleAt=2010(position pos,)'
	final int getStyleAt(Position pos) { // GetStyleAt
		return sendMessageDirect(2010, pos, 0);
	}

	/// Redoes the next action on the undo history.
	/// Generated from: 'fun void Redo=2011(,)'
	final void redo() { // Redo
		sendMessageDirect(2011, 0, 0);
	}

	/// Choose between collecting actions into the undo
	/// history and discarding them.
	/// Generated from: 'set void SetUndoCollection=2012(bool collectUndo,)'
	final @property void undoCollection(bool collectUndo) { // SetUndoCollection
		sendMessageDirect(2012, collectUndo, 0);
	}

	/// Select all the text in the document.
	/// Generated from: 'fun void SelectAll=2013(,)'
	final void selectAll() { // SelectAll
		sendMessageDirect(2013, 0, 0);
	}

	/// Remember the current position in the undo history as the position
	/// at which the document was saved.
	/// Generated from: 'fun void SetSavePoint=2014(,)'
	final void setSavePoint() { // SetSavePoint
		sendMessageDirect(2014, 0, 0);
	}

	/// Retrieve a buffer of cells.
	/// Returns the number of bytes in the buffer not including terminating NULs.
	/// Generated from: 'get int GetStyledText=2015(, textrange tr)'
	final int getStyledText(ref TextRange tr) { // GetStyledText
		return sendMessageDirect(2015, 0, cast(size_t) &tr);
	}

	/// Are there any redoable actions in the undo history?
	/// Generated from: 'fun bool CanRedo=2016(,)'
	final bool canRedo() { // CanRedo
		return !!sendMessageDirect(2016, 0, 0);
	}

	/// Retrieve the line number at which a particular marker is located.
	/// Generated from: 'fun int MarkerLineFromHandle=2017(int handle,)'
	final int markerLineFromHandle(int handle) { // MarkerLineFromHandle
		return sendMessageDirect(2017, handle, 0);
	}

	/// Delete a marker.
	/// Generated from: 'fun void MarkerDeleteHandle=2018(int handle,)'
	final void markerDeleteHandle(int handle) { // MarkerDeleteHandle
		sendMessageDirect(2018, handle, 0);
	}

	/// Is undo history being collected?
	/// Generated from: 'get bool GetUndoCollection=2019(,)'
	final @property bool undoCollection() { // GetUndoCollection
		return !!sendMessageDirect(2019, 0, 0);
	}

	/// Are white space characters currently visible?
	/// Returns one of SCWS_* constants.
	/// Generated from: 'get int GetViewWS=2020(,)'
	final @property int viewWs() { // GetViewWS
		return sendMessageDirect(2020, 0, 0);
	}

	/// Make white space characters invisible, always visible or visible outside indentation.
	/// Generated from: 'set void SetViewWS=2021(int viewWS,)'
	final @property void viewWs(int viewWS) { // SetViewWS
		sendMessageDirect(2021, viewWS, 0);
	}

	/// Find the position from a point within the window.
	/// Generated from: 'fun position PositionFromPoint=2022(int x, int y)'
	final Position positionFromPoint(int x, int y) { // PositionFromPoint
		return sendMessageDirect(2022, x, y);
	}

	/// Find the position from a point within the window but return
	/// INVALID_POSITION if not close to text.
	/// Generated from: 'fun position PositionFromPointClose=2023(int x, int y)'
	final Position positionFromPointClose(int x, int y) { // PositionFromPointClose
		return sendMessageDirect(2023, x, y);
	}

	/// Set caret to start of a line and ensure it is visible.
	/// Generated from: 'fun void GotoLine=2024(int line,)'
	final void gotoLine(int line) { // GotoLine
		sendMessageDirect(2024, line, 0);
	}

	/// Set caret to a position and ensure it is visible.
	/// Generated from: 'fun void GotoPos=2025(position pos,)'
	final void gotoPos(Position pos) { // GotoPos
		sendMessageDirect(2025, pos, 0);
	}

	/// Set the selection anchor to a position. The anchor is the opposite
	/// end of the selection from the caret.
	/// Generated from: 'set void SetAnchor=2026(position posAnchor,)'
	final @property void anchor(Position posAnchor) { // SetAnchor
		sendMessageDirect(2026, posAnchor, 0);
	}

	/// Retrieve the text of the line containing the caret.
	/// Returns the index of the caret on the line.
	/// Generated from: 'get int GetCurLine=2027(bytelen length, cstringresult text)'
	final int getCurLine(out char[] text) { // GetCurLine
		immutable __len = sendMessageDirect(2027, 0, 0);
		assert(__len > 0);
		char[1] __sobuff;
		text = __len == 1 ? __sobuff : (new char[__len])[0 .. $-1];
		immutable __res = sendMessageDirect(2027, __len, cast(size_t) text.ptr);
		assert(text.ptr[__len-1] == '\0');
		if(__len == 1) text = null;
		return __res;
	}

	/// ditto
	final int getCurLine(char[] textBuff, out char[] text) {
		text = textBuff;
		immutable __len = sendMessageDirect(2027, 0, 0);
		text = text[0 .. __len];
		return sendMessageDirect(2027, __len, cast(size_t) text.ptr);
	}

	/// Retrieve the position of the last correctly styled character.
	/// Generated from: 'get position GetEndStyled=2028(,)'
	final @property Position endStyled() { // GetEndStyled
		return sendMessageDirect(2028, 0, 0);
	}

	/// Convert all line endings in the document to one mode.
	/// Generated from: 'fun void ConvertEOLs=2029(int eolMode,)'
	final void convertEols(int eolMode) { // ConvertEOLs
		sendMessageDirect(2029, eolMode, 0);
	}

	/// Retrieve the current end of line mode - one of CRLF, CR, or LF.
	/// Generated from: 'get int GetEOLMode=2030(,)'
	final @property int eolMode() { // GetEOLMode
		return sendMessageDirect(2030, 0, 0);
	}

	/// Set the current end of line mode.
	/// Generated from: 'set void SetEOLMode=2031(int eolMode,)'
	final @property void eolMode(int eolMode) { // SetEOLMode
		sendMessageDirect(2031, eolMode, 0);
	}

	/// Set the current styling position to pos and the styling mask to mask.
	/// The styling mask can be used to protect some bits in each styling byte from modification.
	/// Generated from: 'fun void StartStyling=2032(position pos, int mask)'
	final void startStyling(Position pos, int mask) { // StartStyling
		sendMessageDirect(2032, pos, mask);
	}

	/// Change style from current styling position for length characters to a style
	/// and move the current styling position to after this newly styled segment.
	/// Generated from: 'fun void SetStyling=2033(int length, int style)'
	final void setStyling(int length, int style) { // SetStyling
		sendMessageDirect(2033, length, style);
	}

	/// Is drawing done first into a buffer or direct to the screen?
	/// Generated from: 'get bool GetBufferedDraw=2034(,)'
	final @property bool bufferedDraw() { // GetBufferedDraw
		return !!sendMessageDirect(2034, 0, 0);
	}

	/// If drawing is buffered then each line of text is drawn into a bitmap buffer
	/// before drawing it to the screen to avoid flicker.
	/// Generated from: 'set void SetBufferedDraw=2035(bool buffered,)'
	final @property void bufferedDraw(bool buffered) { // SetBufferedDraw
		sendMessageDirect(2035, buffered, 0);
	}

	/// Change the visible size of a tab to be a multiple of the width of a space character.
	/// Generated from: 'set void SetTabWidth=2036(int tabWidth,)'
	final @property void tabWidth(int tabWidth) { // SetTabWidth
		sendMessageDirect(2036, tabWidth, 0);
	}

	/// Retrieve the visible size of a tab.
	/// Generated from: 'get int GetTabWidth=2121(,)'
	final @property int tabWidth() { // GetTabWidth
		return sendMessageDirect(2121, 0, 0);
	}

	/// Set the code page used to interpret the bytes of the document as characters.
	/// The SC_CP_UTF8 value can be used to enter Unicode mode.
	/// Generated from: 'set void SetCodePage=2037(int codePage,)'
	final @property void codePage(int codePage) { // SetCodePage
		sendMessageDirect(2037, codePage, 0);
	}

	/// Set the symbol used for a particular marker number.
	/// Generated from: 'fun void MarkerDefine=2040(int markerNumber, int markerSymbol)'
	final void markerDefine(int markerNumber, int markerSymbol) { // MarkerDefine
		sendMessageDirect(2040, markerNumber, markerSymbol);
	}

	/// Set the foreground colour used for a particular marker number.
	/// Generated from: 'set void MarkerSetFore=2041(int markerNumber, colour fore)'
	final void markerSetFore(int markerNumber, Colour fore) { // MarkerSetFore
		sendMessageDirect(2041, markerNumber, fore.rgb);
	}

	/// Set the background colour used for a particular marker number.
	/// Generated from: 'set void MarkerSetBack=2042(int markerNumber, colour back)'
	final void markerSetBack(int markerNumber, Colour back) { // MarkerSetBack
		sendMessageDirect(2042, markerNumber, back.rgb);
	}

	/// Set the background colour used for a particular marker number when its folding block is selected.
	/// Generated from: 'set void MarkerSetBackSelected=2292(int markerNumber, colour back)'
	final void markerSetBackSelected(int markerNumber, Colour back) { // MarkerSetBackSelected
		sendMessageDirect(2292, markerNumber, back.rgb);
	}

	/// Enable/disable highlight for current folding bloc (smallest one that contains the caret)
	/// Generated from: 'fun void MarkerEnableHighlight=2293(bool enabled,)'
	final void markerEnableHighlight(bool enabled) { // MarkerEnableHighlight
		sendMessageDirect(2293, enabled, 0);
	}

	/// Add a marker to a line, returning an ID which can be used to find or delete the marker.
	/// Generated from: 'fun int MarkerAdd=2043(int line, int markerNumber)'
	final int markerAdd(int line, int markerNumber) { // MarkerAdd
		return sendMessageDirect(2043, line, markerNumber);
	}

	/// Delete a marker from a line.
	/// Generated from: 'fun void MarkerDelete=2044(int line, int markerNumber)'
	final void markerDelete(int line, int markerNumber) { // MarkerDelete
		sendMessageDirect(2044, line, markerNumber);
	}

	/// Delete all markers with a particular number from all lines.
	/// Generated from: 'fun void MarkerDeleteAll=2045(int markerNumber,)'
	final void markerDeleteAll(int markerNumber) { // MarkerDeleteAll
		sendMessageDirect(2045, markerNumber, 0);
	}

	/// Get a bit mask of all the markers set on a line.
	/// Generated from: 'get int MarkerGet=2046(int line,)'
	final int markerGet(int line) { // MarkerGet
		return sendMessageDirect(2046, line, 0);
	}

	/// Find the next line at or after lineStart that includes a marker in mask.
	/// Return -1 when no more lines.
	/// Generated from: 'fun int MarkerNext=2047(int lineStart, int markerMask)'
	final int markerNext(int lineStart, int markerMask) { // MarkerNext
		return sendMessageDirect(2047, lineStart, markerMask);
	}

	/// Find the previous line before lineStart that includes a marker in mask.
	/// Generated from: 'fun int MarkerPrevious=2048(int lineStart, int markerMask)'
	final int markerPrevious(int lineStart, int markerMask) { // MarkerPrevious
		return sendMessageDirect(2048, lineStart, markerMask);
	}

	/// Define a marker from a pixmap.
	/// Generated from: 'fun void MarkerDefinePixmap=2049(int markerNumber, cstring pixmap)'
	final void markerDefinePixmap(int markerNumber, in char[] pixmap) { // MarkerDefinePixmap
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixmap = toCString(__sibuff, pixmap, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2049, markerNumber, cast(size_t) __cstr_pixmap);
	}

	/// Add a set of markers to a line.
	/// Generated from: 'fun void MarkerAddSet=2466(int line, int set)'
	final void markerAddSet(int line, int set) { // MarkerAddSet
		sendMessageDirect(2466, line, set);
	}

	/// Set the alpha used for a marker that is drawn in the text area, not the margin.
	/// Generated from: 'set void MarkerSetAlpha=2476(int markerNumber, int alpha)'
	final void markerSetAlpha(int markerNumber, int alpha) { // MarkerSetAlpha
		sendMessageDirect(2476, markerNumber, alpha);
	}

	/// Set a margin to be either numeric or symbolic.
	/// Generated from: 'set void SetMarginTypeN=2240(int margin, int marginType)'
	final void setMarginTypeN(int margin, int marginType) { // SetMarginTypeN
		sendMessageDirect(2240, margin, marginType);
	}

	/// Retrieve the type of a margin.
	/// Generated from: 'get int GetMarginTypeN=2241(int margin,)'
	final int getMarginTypeN(int margin) { // GetMarginTypeN
		return sendMessageDirect(2241, margin, 0);
	}

	/// Set the width of a margin to a width expressed in pixels.
	/// Generated from: 'set void SetMarginWidthN=2242(int margin, int pixelWidth)'
	final void setMarginWidthN(int margin, int pixelWidth) { // SetMarginWidthN
		sendMessageDirect(2242, margin, pixelWidth);
	}

	/// Retrieve the width of a margin in pixels.
	/// Generated from: 'get int GetMarginWidthN=2243(int margin,)'
	final int getMarginWidthN(int margin) { // GetMarginWidthN
		return sendMessageDirect(2243, margin, 0);
	}

	/// Set a mask that determines which markers are displayed in a margin.
	/// Generated from: 'set void SetMarginMaskN=2244(int margin, int mask)'
	final void setMarginMaskN(int margin, int mask) { // SetMarginMaskN
		sendMessageDirect(2244, margin, mask);
	}

	/// Retrieve the marker mask of a margin.
	/// Generated from: 'get int GetMarginMaskN=2245(int margin,)'
	final int getMarginMaskN(int margin) { // GetMarginMaskN
		return sendMessageDirect(2245, margin, 0);
	}

	/// Make a margin sensitive or insensitive to mouse clicks.
	/// Generated from: 'set void SetMarginSensitiveN=2246(int margin, bool sensitive)'
	final void setMarginSensitiveN(int margin, bool sensitive) { // SetMarginSensitiveN
		sendMessageDirect(2246, margin, sensitive);
	}

	/// Retrieve the mouse click sensitivity of a margin.
	/// Generated from: 'get bool GetMarginSensitiveN=2247(int margin,)'
	final bool getMarginSensitiveN(int margin) { // GetMarginSensitiveN
		return !!sendMessageDirect(2247, margin, 0);
	}

	/// Set the cursor shown when the mouse is inside a margin.
	/// Generated from: 'set void SetMarginCursorN=2248(int margin, int cursor)'
	final void setMarginCursorN(int margin, int cursor) { // SetMarginCursorN
		sendMessageDirect(2248, margin, cursor);
	}

	/// Retrieve the cursor shown in a margin.
	/// Generated from: 'get int GetMarginCursorN=2249(int margin,)'
	final int getMarginCursorN(int margin) { // GetMarginCursorN
		return sendMessageDirect(2249, margin, 0);
	}

	/// Clear all the styles and make equivalent to the global default style.
	/// Generated from: 'fun void StyleClearAll=2050(,)'
	final void styleClearAll() { // StyleClearAll
		sendMessageDirect(2050, 0, 0);
	}

	/// Set the foreground colour of a style.
	/// Generated from: 'set void StyleSetFore=2051(int style, colour fore)'
	final void styleSetFore(int style, Colour fore) { // StyleSetFore
		sendMessageDirect(2051, style, fore.rgb);
	}

	/// Set the background colour of a style.
	/// Generated from: 'set void StyleSetBack=2052(int style, colour back)'
	final void styleSetBack(int style, Colour back) { // StyleSetBack
		sendMessageDirect(2052, style, back.rgb);
	}

	/// Set a style to be bold or not.
	/// Generated from: 'set void StyleSetBold=2053(int style, bool bold)'
	final void styleSetBold(int style, bool bold) { // StyleSetBold
		sendMessageDirect(2053, style, bold);
	}

	/// Set a style to be italic or not.
	/// Generated from: 'set void StyleSetItalic=2054(int style, bool italic)'
	final void styleSetItalic(int style, bool italic) { // StyleSetItalic
		sendMessageDirect(2054, style, italic);
	}

	/// Set the size of characters of a style.
	/// Generated from: 'set void StyleSetSize=2055(int style, int sizePoints)'
	final void styleSetSize(int style, int sizePoints) { // StyleSetSize
		sendMessageDirect(2055, style, sizePoints);
	}

	/// Set the font of a style.
	/// Generated from: 'set void StyleSetFont=2056(int style, cstring fontName)'
	final void styleSetFont(int style, in char[] fontName) { // StyleSetFont
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_fontName = toCString(__sibuff, fontName, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2056, style, cast(size_t) __cstr_fontName);
	}

	/// Set a style to have its end of line filled or not.
	/// Generated from: 'set void StyleSetEOLFilled=2057(int style, bool filled)'
	final void styleSetEolFilled(int style, bool filled) { // StyleSetEOLFilled
		sendMessageDirect(2057, style, filled);
	}

	/// Reset the default style to its state at startup
	/// Generated from: 'fun void StyleResetDefault=2058(,)'
	final void styleResetDefault() { // StyleResetDefault
		sendMessageDirect(2058, 0, 0);
	}

	/// Set a style to be underlined or not.
	/// Generated from: 'set void StyleSetUnderline=2059(int style, bool underline)'
	final void styleSetUnderline(int style, bool underline) { // StyleSetUnderline
		sendMessageDirect(2059, style, underline);
	}

	/// Get the foreground colour of a style.
	/// Generated from: 'get colour StyleGetFore=2481(int style,)'
	final Colour styleGetFore(int style) { // StyleGetFore
		return cast(Colour)sendMessageDirect(2481, style, 0);
	}

	/// Get the background colour of a style.
	/// Generated from: 'get colour StyleGetBack=2482(int style,)'
	final Colour styleGetBack(int style) { // StyleGetBack
		return cast(Colour)sendMessageDirect(2482, style, 0);
	}

	/// Get is a style bold or not.
	/// Generated from: 'get bool StyleGetBold=2483(int style,)'
	final bool styleGetBold(int style) { // StyleGetBold
		return !!sendMessageDirect(2483, style, 0);
	}

	/// Get is a style italic or not.
	/// Generated from: 'get bool StyleGetItalic=2484(int style,)'
	final bool styleGetItalic(int style) { // StyleGetItalic
		return !!sendMessageDirect(2484, style, 0);
	}

	/// Get the size of characters of a style.
	/// Generated from: 'get int StyleGetSize=2485(int style,)'
	final int styleGetSize(int style) { // StyleGetSize
		return sendMessageDirect(2485, style, 0);
	}

	/// Get the font of a style.
	/// Returns the length of the fontName
	/// Generated from: 'get strlen StyleGetFont=2486(int style, cstringresult fontName)'
	final char[] styleGetFont(int style) { // StyleGetFont
		immutable __len = sendMessageDirect(2486, style, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] fontName = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(2486, style, cast(size_t) fontName.ptr) + 1;
		assert(fontName.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return fontName;
	}

	/// ditto
	final char[] styleGetFont(char[] fontNameBuff, int style) {
		assert(!fontNameBuff || fontNameBuff.length >= sendMessageDirect(2486, style, 0) + 1);
		char[] fontName = fontNameBuff;
		auto __len = fontNameBuff.length;
		__len = sendMessageDirect(2486, style, cast(size_t) fontName.ptr) + 1;
		return fontName[0 .. __len];
	}

	/// Get is a style to have its end of line filled or not.
	/// Generated from: 'get bool StyleGetEOLFilled=2487(int style,)'
	final bool styleGetEolFilled(int style) { // StyleGetEOLFilled
		return !!sendMessageDirect(2487, style, 0);
	}

	/// Get is a style underlined or not.
	/// Generated from: 'get bool StyleGetUnderline=2488(int style,)'
	final bool styleGetUnderline(int style) { // StyleGetUnderline
		return !!sendMessageDirect(2488, style, 0);
	}

	/// Get is a style mixed case, or to force upper or lower case.
	/// Generated from: 'get int StyleGetCase=2489(int style,)'
	final int styleGetCase(int style) { // StyleGetCase
		return sendMessageDirect(2489, style, 0);
	}

	/// Get the character get of the font in a style.
	/// Generated from: 'get int StyleGetCharacterSet=2490(int style,)'
	final int styleGetCharacterSet(int style) { // StyleGetCharacterSet
		return sendMessageDirect(2490, style, 0);
	}

	/// Get is a style visible or not.
	/// Generated from: 'get bool StyleGetVisible=2491(int style,)'
	final bool styleGetVisible(int style) { // StyleGetVisible
		return !!sendMessageDirect(2491, style, 0);
	}

	/// Get is a style changeable or not (read only).
	/// Experimental feature, currently buggy.
	/// Generated from: 'get bool StyleGetChangeable=2492(int style,)'
	final bool styleGetChangeable(int style) { // StyleGetChangeable
		return !!sendMessageDirect(2492, style, 0);
	}

	/// Get is a style a hotspot or not.
	/// Generated from: 'get bool StyleGetHotSpot=2493(int style,)'
	final bool styleGetHotSpot(int style) { // StyleGetHotSpot
		return !!sendMessageDirect(2493, style, 0);
	}

	/// Set a style to be mixed case, or to force upper or lower case.
	/// Generated from: 'set void StyleSetCase=2060(int style, int caseForce)'
	final void styleSetCase(int style, int caseForce) { // StyleSetCase
		sendMessageDirect(2060, style, caseForce);
	}

	/// Set the size of characters of a style. Size is in points multiplied by 100.
	/// Generated from: 'set void StyleSetSizeFractional=2061(int style, int caseForce)'
	final void styleSetSizeFractional(int style, int caseForce) { // StyleSetSizeFractional
		sendMessageDirect(2061, style, caseForce);
	}

	/// Get the size of characters of a style in points multiplied by 100
	/// Generated from: 'get int StyleGetSizeFractional=2062(int style,)'
	final int styleGetSizeFractional(int style) { // StyleGetSizeFractional
		return sendMessageDirect(2062, style, 0);
	}

	/// Set the weight of characters of a style.
	/// Generated from: 'set void StyleSetWeight=2063(int style, int weight)'
	final void styleSetWeight(int style, int weight) { // StyleSetWeight
		sendMessageDirect(2063, style, weight);
	}

	/// Get the weight of characters of a style.
	/// Generated from: 'get int StyleGetWeight=2064(int style,)'
	final int styleGetWeight(int style) { // StyleGetWeight
		return sendMessageDirect(2064, style, 0);
	}

	/// Set the character set of the font in a style.
	/// Generated from: 'set void StyleSetCharacterSet=2066(int style, int characterSet)'
	final void styleSetCharacterSet(int style, int characterSet) { // StyleSetCharacterSet
		sendMessageDirect(2066, style, characterSet);
	}

	/// Set a style to be a hotspot or not.
	/// Generated from: 'set void StyleSetHotSpot=2409(int style, bool hotspot)'
	final void styleSetHotSpot(int style, bool hotspot) { // StyleSetHotSpot
		sendMessageDirect(2409, style, hotspot);
	}

	/// Set the foreground colour of the main and additional selections and whether to use this setting.
	/// Generated from: 'set void SetSelFore=2067(bool useSetting, colour fore)'
	final void setSelFore(bool useSetting, Colour fore) { // SetSelFore
		sendMessageDirect(2067, useSetting, fore.rgb);
	}

	/// Set the background colour of the main and additional selections and whether to use this setting.
	/// Generated from: 'set void SetSelBack=2068(bool useSetting, colour back)'
	final void setSelBack(bool useSetting, Colour back) { // SetSelBack
		sendMessageDirect(2068, useSetting, back.rgb);
	}

	/// Get the alpha of the selection.
	/// Generated from: 'get int GetSelAlpha=2477(,)'
	final @property int selAlpha() { // GetSelAlpha
		return sendMessageDirect(2477, 0, 0);
	}

	/// Set the alpha of the selection.
	/// Generated from: 'set void SetSelAlpha=2478(int alpha,)'
	final @property void selAlpha(int alpha) { // SetSelAlpha
		sendMessageDirect(2478, alpha, 0);
	}

	/// Is the selection end of line filled?
	/// Generated from: 'get bool GetSelEOLFilled=2479(,)'
	final @property bool selEolFilled() { // GetSelEOLFilled
		return !!sendMessageDirect(2479, 0, 0);
	}

	/// Set the selection to have its end of line filled or not.
	/// Generated from: 'set void SetSelEOLFilled=2480(bool filled,)'
	final @property void selEolFilled(bool filled) { // SetSelEOLFilled
		sendMessageDirect(2480, filled, 0);
	}

	/// Set the foreground colour of the caret.
	/// Generated from: 'set void SetCaretFore=2069(colour fore,)'
	final @property void caretFore(Colour fore) { // SetCaretFore
		sendMessageDirect(2069, fore.rgb, 0);
	}

	/// When key+modifier combination km is pressed perform msg.
	/// Generated from: 'fun void AssignCmdKey=2070(keymod km, int msg)'
	final void assignCmdKey(in KeyMod km, int msg) { // AssignCmdKey
		sendMessageDirect(2070, km, msg);
	}

	/// When key+modifier combination km is pressed do nothing.
	/// Generated from: 'fun void ClearCmdKey=2071(keymod km,)'
	final void clearCmdKey(in KeyMod km) { // ClearCmdKey
		sendMessageDirect(2071, km, 0);
	}

	/// Drop all key mappings.
	/// Generated from: 'fun void ClearAllCmdKeys=2072(,)'
	final void clearAllCmdKeys() { // ClearAllCmdKeys
		sendMessageDirect(2072, 0, 0);
	}

	/// Set the styles for a segment of the document.
	/// Generated from: 'fun void SetStylingEx=2073(len length, chars styles)'
	final void setStylingEx(in char[] styles) { // SetStylingEx
		sendMessageDirect(2073, styles.length, cast(size_t) styles.ptr);
	}

	/// Set a style to be visible or not.
	/// Generated from: 'set void StyleSetVisible=2074(int style, bool visible)'
	final void styleSetVisible(int style, bool visible) { // StyleSetVisible
		sendMessageDirect(2074, style, visible);
	}

	/// Get the time in milliseconds that the caret is on and off.
	/// Generated from: 'get int GetCaretPeriod=2075(,)'
	final @property int caretPeriod() { // GetCaretPeriod
		return sendMessageDirect(2075, 0, 0);
	}

	/// Get the time in milliseconds that the caret is on and off. 0 = steady on.
	/// Generated from: 'set void SetCaretPeriod=2076(int periodMilliseconds,)'
	final @property void caretPeriod(int periodMilliseconds) { // SetCaretPeriod
		sendMessageDirect(2076, periodMilliseconds, 0);
	}

	/// Set the set of characters making up words for when moving or selecting by word.
	/// First sets defaults like SetCharsDefault.
	/// Generated from: 'set void SetWordChars=2077(, cstring characters)'
	final @property void wordChars(in char[] characters) { // SetWordChars
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characters = toCString(__sibuff, characters, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2077, 0, cast(size_t) __cstr_characters);
	}

	/// Start a sequence of actions that is undone and redone as a unit.
	/// May be nested.
	/// Generated from: 'fun void BeginUndoAction=2078(,)'
	final void beginUndoAction() { // BeginUndoAction
		sendMessageDirect(2078, 0, 0);
	}

	/// End a sequence of actions that is undone and redone as a unit.
	/// Generated from: 'fun void EndUndoAction=2079(,)'
	final void endUndoAction() { // EndUndoAction
		sendMessageDirect(2079, 0, 0);
	}

	/// Set an indicator to plain, squiggle or TT.
	/// Generated from: 'set void IndicSetStyle=2080(int indic, int style)'
	final void indicSetStyle(int indic, int style) { // IndicSetStyle
		sendMessageDirect(2080, indic, style);
	}

	/// Retrieve the style of an indicator.
	/// Generated from: 'get int IndicGetStyle=2081(int indic,)'
	final int indicGetStyle(int indic) { // IndicGetStyle
		return sendMessageDirect(2081, indic, 0);
	}

	/// Set the foreground colour of an indicator.
	/// Generated from: 'set void IndicSetFore=2082(int indic, colour fore)'
	final void indicSetFore(int indic, Colour fore) { // IndicSetFore
		sendMessageDirect(2082, indic, fore.rgb);
	}

	/// Retrieve the foreground colour of an indicator.
	/// Generated from: 'get colour IndicGetFore=2083(int indic,)'
	final Colour indicGetFore(int indic) { // IndicGetFore
		return cast(Colour)sendMessageDirect(2083, indic, 0);
	}

	/// Set an indicator to draw under text or over(default).
	/// Generated from: 'set void IndicSetUnder=2510(int indic, bool under)'
	final void indicSetUnder(int indic, bool under) { // IndicSetUnder
		sendMessageDirect(2510, indic, under);
	}

	/// Retrieve whether indicator drawn under or over text.
	/// Generated from: 'get bool IndicGetUnder=2511(int indic,)'
	final bool indicGetUnder(int indic) { // IndicGetUnder
		return !!sendMessageDirect(2511, indic, 0);
	}

	/// Set the foreground colour of all whitespace and whether to use this setting.
	/// Generated from: 'set void SetWhitespaceFore=2084(bool useSetting, colour fore)'
	final void setWhitespaceFore(bool useSetting, Colour fore) { // SetWhitespaceFore
		sendMessageDirect(2084, useSetting, fore.rgb);
	}

	/// Set the background colour of all whitespace and whether to use this setting.
	/// Generated from: 'set void SetWhitespaceBack=2085(bool useSetting, colour back)'
	final void setWhitespaceBack(bool useSetting, Colour back) { // SetWhitespaceBack
		sendMessageDirect(2085, useSetting, back.rgb);
	}

	/// Set the size of the dots used to mark space characters.
	/// Generated from: 'set void SetWhitespaceSize=2086(int size,)'
	final @property void whitespaceSize(int size) { // SetWhitespaceSize
		sendMessageDirect(2086, size, 0);
	}

	/// Get the size of the dots used to mark space characters.
	/// Generated from: 'get int GetWhitespaceSize=2087(,)'
	final @property int whitespaceSize() { // GetWhitespaceSize
		return sendMessageDirect(2087, 0, 0);
	}

	/// Divide each styling byte into lexical class bits (default: 5) and indicator
	/// bits (default: 3). If a lexer requires more than 32 lexical states, then this
	/// is used to expand the possible states.
	/// Generated from: 'set void SetStyleBits=2090(int bits,)'
	final @property void styleBits(int bits) { // SetStyleBits
		sendMessageDirect(2090, bits, 0);
	}

	/// Retrieve number of bits in style bytes used to hold the lexical state.
	/// Generated from: 'get int GetStyleBits=2091(,)'
	final @property int styleBits() { // GetStyleBits
		return sendMessageDirect(2091, 0, 0);
	}

	/// Used to hold extra styling information for each line.
	/// Generated from: 'set void SetLineState=2092(int line, int state)'
	final void setLineState(int line, int state) { // SetLineState
		sendMessageDirect(2092, line, state);
	}

	/// Retrieve the extra styling information for a line.
	/// Generated from: 'get int GetLineState=2093(int line,)'
	final int getLineState(int line) { // GetLineState
		return sendMessageDirect(2093, line, 0);
	}

	/// Retrieve the last line number that has line state.
	/// Generated from: 'get int GetMaxLineState=2094(,)'
	final @property int maxLineState() { // GetMaxLineState
		return sendMessageDirect(2094, 0, 0);
	}

	/// Is the background of the line containing the caret in a different colour?
	/// Generated from: 'get bool GetCaretLineVisible=2095(,)'
	final @property bool caretLineVisible() { // GetCaretLineVisible
		return !!sendMessageDirect(2095, 0, 0);
	}

	/// Display the background of the line containing the caret in a different colour.
	/// Generated from: 'set void SetCaretLineVisible=2096(bool show,)'
	final @property void caretLineVisible(bool show) { // SetCaretLineVisible
		sendMessageDirect(2096, show, 0);
	}

	/// Get the colour of the background of the line containing the caret.
	/// Generated from: 'get colour GetCaretLineBack=2097(,)'
	final @property Colour caretLineBack() { // GetCaretLineBack
		return cast(Colour)sendMessageDirect(2097, 0, 0);
	}

	/// Set the colour of the background of the line containing the caret.
	/// Generated from: 'set void SetCaretLineBack=2098(colour back,)'
	final @property void caretLineBack(Colour back) { // SetCaretLineBack
		sendMessageDirect(2098, back.rgb, 0);
	}

	/// Set a style to be changeable or not (read only).
	/// Experimental feature, currently buggy.
	/// Generated from: 'set void StyleSetChangeable=2099(int style, bool changeable)'
	final void styleSetChangeable(int style, bool changeable) { // StyleSetChangeable
		sendMessageDirect(2099, style, changeable);
	}

	/// Display a auto-completion list.
	/// The lenEntered parameter indicates how many characters before
	/// the caret should be used to provide context.
	/// Generated from: 'fun void AutoCShow=2100(int lenEntered, cstring itemList)'
	final void autoCShow(int lenEntered, in char[] itemList) { // AutoCShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_itemList = toCString(__sibuff, itemList, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2100, lenEntered, cast(size_t) __cstr_itemList);
	}

	/// Remove the auto-completion list from the screen.
	/// Generated from: 'fun void AutoCCancel=2101(,)'
	final void autoCCancel() { // AutoCCancel
		sendMessageDirect(2101, 0, 0);
	}

	/// Is there an auto-completion list visible?
	/// Generated from: 'fun bool AutoCActive=2102(,)'
	final bool autoCActive() { // AutoCActive
		return !!sendMessageDirect(2102, 0, 0);
	}

	/// Retrieve the position of the caret when the auto-completion list was displayed.
	/// Generated from: 'fun position AutoCPosStart=2103(,)'
	final Position autoCPosStart() { // AutoCPosStart
		return sendMessageDirect(2103, 0, 0);
	}

	/// User has selected an item so remove the list and insert the selection.
	/// Generated from: 'fun void AutoCComplete=2104(,)'
	final void autoCComplete() { // AutoCComplete
		sendMessageDirect(2104, 0, 0);
	}

	/// Define a set of character that when typed cancel the auto-completion list.
	/// Generated from: 'fun void AutoCStops=2105(, cstring characterSet)'
	final void autoCStops(in char[] characterSet) { // AutoCStops
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characterSet = toCString(__sibuff, characterSet, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2105, 0, cast(size_t) __cstr_characterSet);
	}

	/// Change the separator character in the string setting up an auto-completion list.
	/// Default is space but can be changed if items contain space.
	/// Generated from: 'set void AutoCSetSeparator=2106(int separatorCharacter,)'
	final @property void autoCSetSeparator(int separatorCharacter) { // AutoCSetSeparator
		sendMessageDirect(2106, separatorCharacter, 0);
	}

	/// Retrieve the auto-completion list separator character.
	/// Generated from: 'get int AutoCGetSeparator=2107(,)'
	final @property int autoCGetSeparator() { // AutoCGetSeparator
		return sendMessageDirect(2107, 0, 0);
	}

	/// Select the item in the auto-completion list that starts with a string.
	/// Generated from: 'fun void AutoCSelect=2108(, cstring text)'
	final void autoCSelect(in char[] text) { // AutoCSelect
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2108, 0, cast(size_t) __cstr_text);
	}

	/// Should the auto-completion list be cancelled if the user backspaces to a
	/// position before where the box was created.
	/// Generated from: 'set void AutoCSetCancelAtStart=2110(bool cancel,)'
	final @property void autoCSetCancelAtStart(bool cancel) { // AutoCSetCancelAtStart
		sendMessageDirect(2110, cancel, 0);
	}

	/// Retrieve whether auto-completion cancelled by backspacing before start.
	/// Generated from: 'get bool AutoCGetCancelAtStart=2111(,)'
	final @property bool autoCGetCancelAtStart() { // AutoCGetCancelAtStart
		return !!sendMessageDirect(2111, 0, 0);
	}

	/// Define a set of characters that when typed will cause the autocompletion to
	/// choose the selected item.
	/// Generated from: 'set void AutoCSetFillUps=2112(, cstring characterSet)'
	final @property void autoCSetFillUps(in char[] characterSet) { // AutoCSetFillUps
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characterSet = toCString(__sibuff, characterSet, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2112, 0, cast(size_t) __cstr_characterSet);
	}

	/// Should a single item auto-completion list automatically choose the item.
	/// Generated from: 'set void AutoCSetChooseSingle=2113(bool chooseSingle,)'
	final @property void autoCSetChooseSingle(bool chooseSingle) { // AutoCSetChooseSingle
		sendMessageDirect(2113, chooseSingle, 0);
	}

	/// Retrieve whether a single item auto-completion list automatically choose the item.
	/// Generated from: 'get bool AutoCGetChooseSingle=2114(,)'
	final @property bool autoCGetChooseSingle() { // AutoCGetChooseSingle
		return !!sendMessageDirect(2114, 0, 0);
	}

	/// Set whether case is significant when performing auto-completion searches.
	/// Generated from: 'set void AutoCSetIgnoreCase=2115(bool ignoreCase,)'
	final @property void autoCSetIgnoreCase(bool ignoreCase) { // AutoCSetIgnoreCase
		sendMessageDirect(2115, ignoreCase, 0);
	}

	/// Retrieve state of ignore case flag.
	/// Generated from: 'get bool AutoCGetIgnoreCase=2116(,)'
	final @property bool autoCGetIgnoreCase() { // AutoCGetIgnoreCase
		return !!sendMessageDirect(2116, 0, 0);
	}

	/// Display a list of strings and send notification when user chooses one.
	/// Generated from: 'fun void UserListShow=2117(int listType, cstring itemList)'
	final void userListShow(int listType, in char[] itemList) { // UserListShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_itemList = toCString(__sibuff, itemList, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2117, listType, cast(size_t) __cstr_itemList);
	}

	/// Set whether or not autocompletion is hidden automatically when nothing matches.
	/// Generated from: 'set void AutoCSetAutoHide=2118(bool autoHide,)'
	final @property void autoCSetAutoHide(bool autoHide) { // AutoCSetAutoHide
		sendMessageDirect(2118, autoHide, 0);
	}

	/// Retrieve whether or not autocompletion is hidden automatically when nothing matches.
	/// Generated from: 'get bool AutoCGetAutoHide=2119(,)'
	final @property bool autoCGetAutoHide() { // AutoCGetAutoHide
		return !!sendMessageDirect(2119, 0, 0);
	}

	/// Set whether or not autocompletion deletes any word characters
	/// after the inserted text upon completion.
	/// Generated from: 'set void AutoCSetDropRestOfWord=2270(bool dropRestOfWord,)'
	final @property void autoCSetDropRestOfWord(bool dropRestOfWord) { // AutoCSetDropRestOfWord
		sendMessageDirect(2270, dropRestOfWord, 0);
	}

	/// Retrieve whether or not autocompletion deletes any word characters
	/// after the inserted text upon completion.
	/// Generated from: 'get bool AutoCGetDropRestOfWord=2271(,)'
	final @property bool autoCGetDropRestOfWord() { // AutoCGetDropRestOfWord
		return !!sendMessageDirect(2271, 0, 0);
	}

	/// Register an XPM image for use in autocompletion lists.
	/// Generated from: 'fun void RegisterImage=2405(int type, cstring xpmData)'
	final void registerImage(int type, in char[] xpmData) { // RegisterImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_xpmData = toCString(__sibuff, xpmData, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2405, type, cast(size_t) __cstr_xpmData);
	}

	/// Clear all the registered XPM images.
	/// Generated from: 'fun void ClearRegisteredImages=2408(,)'
	final void clearRegisteredImages() { // ClearRegisteredImages
		sendMessageDirect(2408, 0, 0);
	}

	/// Retrieve the auto-completion list type-separator character.
	/// Generated from: 'get int AutoCGetTypeSeparator=2285(,)'
	final @property int autoCGetTypeSeparator() { // AutoCGetTypeSeparator
		return sendMessageDirect(2285, 0, 0);
	}

	/// Change the type-separator character in the string setting up an auto-completion list.
	/// Default is '?' but can be changed if items contain '?'.
	/// Generated from: 'set void AutoCSetTypeSeparator=2286(int separatorCharacter,)'
	final @property void autoCSetTypeSeparator(int separatorCharacter) { // AutoCSetTypeSeparator
		sendMessageDirect(2286, separatorCharacter, 0);
	}

	/// Set the maximum width, in characters, of auto-completion and user lists.
	/// Set to 0 to autosize to fit longest item, which is the default.
	/// Generated from: 'set void AutoCSetMaxWidth=2208(int characterCount,)'
	final @property void autoCSetMaxWidth(int characterCount) { // AutoCSetMaxWidth
		sendMessageDirect(2208, characterCount, 0);
	}

	/// Get the maximum width, in characters, of auto-completion and user lists.
	/// Generated from: 'get int AutoCGetMaxWidth=2209(,)'
	final @property int autoCGetMaxWidth() { // AutoCGetMaxWidth
		return sendMessageDirect(2209, 0, 0);
	}

	/// Set the maximum height, in rows, of auto-completion and user lists.
	/// The default is 5 rows.
	/// Generated from: 'set void AutoCSetMaxHeight=2210(int rowCount,)'
	final @property void autoCSetMaxHeight(int rowCount) { // AutoCSetMaxHeight
		sendMessageDirect(2210, rowCount, 0);
	}

	/// Set the maximum height, in rows, of auto-completion and user lists.
	/// Generated from: 'get int AutoCGetMaxHeight=2211(,)'
	final @property int autoCGetMaxHeight() { // AutoCGetMaxHeight
		return sendMessageDirect(2211, 0, 0);
	}

	/// Set the number of spaces used for one level of indentation.
	/// Generated from: 'set void SetIndent=2122(int indentSize,)'
	final @property void indent(int indentSize) { // SetIndent
		sendMessageDirect(2122, indentSize, 0);
	}

	/// Retrieve indentation size.
	/// Generated from: 'get int GetIndent=2123(,)'
	final @property int indent() { // GetIndent
		return sendMessageDirect(2123, 0, 0);
	}

	/// Indentation will only use space characters if useTabs is false, otherwise
	/// it will use a combination of tabs and spaces.
	/// Generated from: 'set void SetUseTabs=2124(bool useTabs,)'
	final @property void useTabs(bool useTabs) { // SetUseTabs
		sendMessageDirect(2124, useTabs, 0);
	}

	/// Retrieve whether tabs will be used in indentation.
	/// Generated from: 'get bool GetUseTabs=2125(,)'
	final @property bool useTabs() { // GetUseTabs
		return !!sendMessageDirect(2125, 0, 0);
	}

	/// Change the indentation of a line to a number of columns.
	/// Generated from: 'set void SetLineIndentation=2126(int line, int indentSize)'
	final void setLineIndentation(int line, int indentSize) { // SetLineIndentation
		sendMessageDirect(2126, line, indentSize);
	}

	/// Retrieve the number of columns that a line is indented.
	/// Generated from: 'get int GetLineIndentation=2127(int line,)'
	final int getLineIndentation(int line) { // GetLineIndentation
		return sendMessageDirect(2127, line, 0);
	}

	/// Retrieve the position before the first non indentation character on a line.
	/// Generated from: 'get position GetLineIndentPosition=2128(int line,)'
	final Position getLineIndentPosition(int line) { // GetLineIndentPosition
		return sendMessageDirect(2128, line, 0);
	}

	/// Retrieve the column number of a position, taking tab width into account.
	/// Generated from: 'get int GetColumn=2129(position pos,)'
	final int getColumn(Position pos) { // GetColumn
		return sendMessageDirect(2129, pos, 0);
	}

	/// Count characters between two positions.
	/// Generated from: 'fun int CountCharacters=2633(int startPos, int endPos)'
	final int countCharacters(int startPos, int endPos) { // CountCharacters
		return sendMessageDirect(2633, startPos, endPos);
	}

	/// Show or hide the horizontal scroll bar.
	/// Generated from: 'set void SetHScrollBar=2130(bool show,)'
	final @property void hScrollBar(bool show) { // SetHScrollBar
		sendMessageDirect(2130, show, 0);
	}

	/// Is the horizontal scroll bar visible?
	/// Generated from: 'get bool GetHScrollBar=2131(,)'
	final @property bool hScrollBar() { // GetHScrollBar
		return !!sendMessageDirect(2131, 0, 0);
	}

	/// Show or hide indentation guides.
	/// Generated from: 'set void SetIndentationGuides=2132(int indentView,)'
	final @property void indentationGuides(int indentView) { // SetIndentationGuides
		sendMessageDirect(2132, indentView, 0);
	}

	/// Are the indentation guides visible?
	/// Generated from: 'get int GetIndentationGuides=2133(,)'
	final @property int indentationGuides() { // GetIndentationGuides
		return sendMessageDirect(2133, 0, 0);
	}

	/// Set the highlighted indentation guide column.
	/// 0 = no highlighted guide.
	/// Generated from: 'set void SetHighlightGuide=2134(int column,)'
	final @property void highlightGuide(int column) { // SetHighlightGuide
		sendMessageDirect(2134, column, 0);
	}

	/// Get the highlighted indentation guide column.
	/// Generated from: 'get int GetHighlightGuide=2135(,)'
	final @property int highlightGuide() { // GetHighlightGuide
		return sendMessageDirect(2135, 0, 0);
	}

	/// Get the position after the last visible characters on a line.
	/// Generated from: 'get position GetLineEndPosition=2136(int line,)'
	final Position getLineEndPosition(int line) { // GetLineEndPosition
		return sendMessageDirect(2136, line, 0);
	}

	/// Get the code page used to interpret the bytes of the document as characters.
	/// Generated from: 'get int GetCodePage=2137(,)'
	final @property int codePage() { // GetCodePage
		return sendMessageDirect(2137, 0, 0);
	}

	/// Get the foreground colour of the caret.
	/// Generated from: 'get colour GetCaretFore=2138(,)'
	final @property Colour caretFore() { // GetCaretFore
		return cast(Colour)sendMessageDirect(2138, 0, 0);
	}

	/// In read-only mode?
	/// Generated from: 'get bool GetReadOnly=2140(,)'
	final @property bool readOnly() { // GetReadOnly
		return !!sendMessageDirect(2140, 0, 0);
	}

	/// Sets the position of the caret.
	/// Generated from: 'set void SetCurrentPos=2141(position pos,)'
	final @property void currentPos(Position pos) { // SetCurrentPos
		sendMessageDirect(2141, pos, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	/// Generated from: 'set void SetSelectionStart=2142(position pos,)'
	final @property void selectionStart(Position pos) { // SetSelectionStart
		sendMessageDirect(2142, pos, 0);
	}

	/// Returns the position at the start of the selection.
	/// Generated from: 'get position GetSelectionStart=2143(,)'
	final @property Position selectionStart() { // GetSelectionStart
		return sendMessageDirect(2143, 0, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	/// Generated from: 'set void SetSelectionEnd=2144(position pos,)'
	final @property void selectionEnd(Position pos) { // SetSelectionEnd
		sendMessageDirect(2144, pos, 0);
	}

	/// Returns the position at the end of the selection.
	/// Generated from: 'get position GetSelectionEnd=2145(,)'
	final @property Position selectionEnd() { // GetSelectionEnd
		return sendMessageDirect(2145, 0, 0);
	}

	/// Set caret to a position, while removing any existing selection.
	/// Generated from: 'fun void SetEmptySelection=2556(position pos,)'
	final void setEmptySelection(Position pos) { // SetEmptySelection
		sendMessageDirect(2556, pos, 0);
	}

	/// Sets the print magnification added to the point size of each style for printing.
	/// Generated from: 'set void SetPrintMagnification=2146(int magnification,)'
	final @property void printMagnification(int magnification) { // SetPrintMagnification
		sendMessageDirect(2146, magnification, 0);
	}

	/// Returns the print magnification.
	/// Generated from: 'get int GetPrintMagnification=2147(,)'
	final @property int printMagnification() { // GetPrintMagnification
		return sendMessageDirect(2147, 0, 0);
	}

	/// Modify colours when printing for clearer printed text.
	/// Generated from: 'set void SetPrintColourMode=2148(int mode,)'
	final @property void printColourMode(int mode) { // SetPrintColourMode
		sendMessageDirect(2148, mode, 0);
	}

	/// Returns the print colour mode.
	/// Generated from: 'get int GetPrintColourMode=2149(,)'
	final @property int printColourMode() { // GetPrintColourMode
		return sendMessageDirect(2149, 0, 0);
	}

	/// Find some text in the document.
	/// Generated from: 'fun position FindText=2150(int flags, findtext ft)'
	final Position findText(int flags, ref TextToFind ft) { // FindText
		return sendMessageDirect(2150, flags, cast(size_t) &ft);
	}

	/// On Windows, will draw the document into a display context such as a printer.
	/// Generated from: 'fun position FormatRange=2151(bool draw, formatrange fr)'
	final Position formatRange(bool draw, ref RangeToFormat fr) { // FormatRange
		return sendMessageDirect(2151, draw, cast(size_t) &fr);
	}

	/// Retrieve the display line at the top of the display.
	/// Generated from: 'get int GetFirstVisibleLine=2152(,)'
	final @property int firstVisibleLine() { // GetFirstVisibleLine
		return sendMessageDirect(2152, 0, 0);
	}

	/// Retrieve the contents of a line.
	/// Returns the length of the line.
	/// Generated from: 'get len GetLine=2153(int line, charsresult text)'
	final char[] getLine(int line) { // GetLine
		immutable __len = sendMessageDirect(2153, line, 0);
		if(!__len) return null;
		char[] text = new char[__len];
		immutable __len2 = sendMessageDirect(2153, line, cast(size_t) text.ptr);
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] getLine(char[] textBuff, int line) {
		assert(!textBuff || textBuff.length >= sendMessageDirect(2153, line, 0));
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(2153, line, cast(size_t) text.ptr);
		return text[0 .. __len];
	}

	/// Returns the number of lines in the document. There is always at least one.
	/// Generated from: 'get int GetLineCount=2154(,)'
	final @property int lineCount() { // GetLineCount
		return sendMessageDirect(2154, 0, 0);
	}

	/// Sets the size in pixels of the left margin.
	/// Generated from: 'set void SetMarginLeft=2155(, int pixelWidth)'
	final @property void marginLeft(int pixelWidth) { // SetMarginLeft
		sendMessageDirect(2155, 0, pixelWidth);
	}

	/// Returns the size in pixels of the left margin.
	/// Generated from: 'get int GetMarginLeft=2156(,)'
	final @property int marginLeft() { // GetMarginLeft
		return sendMessageDirect(2156, 0, 0);
	}

	/// Sets the size in pixels of the right margin.
	/// Generated from: 'set void SetMarginRight=2157(, int pixelWidth)'
	final @property void marginRight(int pixelWidth) { // SetMarginRight
		sendMessageDirect(2157, 0, pixelWidth);
	}

	/// Returns the size in pixels of the right margin.
	/// Generated from: 'get int GetMarginRight=2158(,)'
	final @property int marginRight() { // GetMarginRight
		return sendMessageDirect(2158, 0, 0);
	}

	/// Is the document different from when it was last saved?
	/// Generated from: 'get bool GetModify=2159(,)'
	final @property bool modify() { // GetModify
		return !!sendMessageDirect(2159, 0, 0);
	}

	/// Select a range of text.
	/// Generated from: 'fun void SetSel=2160(position start, position end)'
	final void setSel(Position start, Position end) { // SetSel
		sendMessageDirect(2160, start, end);
	}

	/// Retrieve the selected text.
	/// Return the length of the text.
	/// Generated from: 'get bytelen GetSelText=2161(, cstringresult text)'
	final @property char[] selText() { // GetSelText
		immutable __len = sendMessageDirect(2161, 0, 0);
		assert(__len > 0);
		if(__len == 1) return null;
		char[] text = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(2161, 0, cast(size_t) text.ptr);
		assert(text.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] getSelText(char[] textBuff) {
		assert(!textBuff || textBuff.length >= sendMessageDirect(2161, 0, 0));
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(2161, 0, cast(size_t) text.ptr);
		return text[0 .. __len];
	}

	/// Retrieve a range of text.
	/// Return the length of the text.
	/// Generated from: 'get int GetTextRange=2162(, textrange tr)'
	final int getTextRange(ref TextRange tr) { // GetTextRange
		return sendMessageDirect(2162, 0, cast(size_t) &tr);
	}

	/// Draw the selection in normal style or with selection highlighted.
	/// Generated from: 'fun void HideSelection=2163(bool normal,)'
	final void hideSelection(bool normal) { // HideSelection
		sendMessageDirect(2163, normal, 0);
	}

	/// Retrieve the x value of the point in the window where a position is displayed.
	/// Generated from: 'fun int PointXFromPosition=2164(, position pos)'
	final int pointXFromPosition(Position pos) { // PointXFromPosition
		return sendMessageDirect(2164, 0, pos);
	}

	/// Retrieve the y value of the point in the window where a position is displayed.
	/// Generated from: 'fun int PointYFromPosition=2165(, position pos)'
	final int pointYFromPosition(Position pos) { // PointYFromPosition
		return sendMessageDirect(2165, 0, pos);
	}

	/// Retrieve the line containing a position.
	/// Generated from: 'fun int LineFromPosition=2166(position pos,)'
	final int lineFromPosition(Position pos) { // LineFromPosition
		return sendMessageDirect(2166, pos, 0);
	}

	/// Retrieve the position at the start of a line.
	/// Generated from: 'fun position PositionFromLine=2167(int line,)'
	final Position positionFromLine(int line) { // PositionFromLine
		return sendMessageDirect(2167, line, 0);
	}

	/// Scroll horizontally and vertically.
	/// Generated from: 'fun void LineScroll=2168(int columns, int lines)'
	final void lineScroll(int columns, int lines) { // LineScroll
		sendMessageDirect(2168, columns, lines);
	}

	/// Ensure the caret is visible.
	/// Generated from: 'fun void ScrollCaret=2169(,)'
	final void scrollCaret() { // ScrollCaret
		sendMessageDirect(2169, 0, 0);
	}

	/// Replace the selected text with the argument text.
	/// Generated from: 'fun void ReplaceSel=2170(, cstring text)'
	final void replaceSel(in char[] text) { // ReplaceSel
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2170, 0, cast(size_t) __cstr_text);
	}

	/// Set to read only or read write.
	/// Generated from: 'set void SetReadOnly=2171(bool readOnly,)'
	final @property void readOnly(bool readOnly) { // SetReadOnly
		sendMessageDirect(2171, readOnly, 0);
	}

	/// Null operation.
	/// Generated from: 'fun void Null=2172(,)'
	final void null_() { // Null
		sendMessageDirect(2172, 0, 0);
	}

	/// Will a paste succeed?
	/// Generated from: 'fun bool CanPaste=2173(,)'
	final bool canPaste() { // CanPaste
		return !!sendMessageDirect(2173, 0, 0);
	}

	/// Are there any undoable actions in the undo history?
	/// Generated from: 'fun bool CanUndo=2174(,)'
	final bool canUndo() { // CanUndo
		return !!sendMessageDirect(2174, 0, 0);
	}

	/// Delete the undo history.
	/// Generated from: 'fun void EmptyUndoBuffer=2175(,)'
	final void emptyUndoBuffer() { // EmptyUndoBuffer
		sendMessageDirect(2175, 0, 0);
	}

	/// Undo one action in the undo history.
	/// Generated from: 'fun void Undo=2176(,)'
	final void undo() { // Undo
		sendMessageDirect(2176, 0, 0);
	}

	/// Cut the selection to the clipboard.
	/// Generated from: 'fun void Cut=2177(,)'
	final void cut() { // Cut
		sendMessageDirect(2177, 0, 0);
	}

	/// Copy the selection to the clipboard.
	/// Generated from: 'fun void Copy=2178(,)'
	final void copy() { // Copy
		sendMessageDirect(2178, 0, 0);
	}

	/// Paste the contents of the clipboard into the document replacing the selection.
	/// Generated from: 'fun void Paste=2179(,)'
	final void paste() { // Paste
		sendMessageDirect(2179, 0, 0);
	}

	/// Clear the selection.
	/// Generated from: 'fun void Clear=2180(,)'
	final void clear() { // Clear
		sendMessageDirect(2180, 0, 0);
	}

	/// Replace the contents of the document with the argument text.
	/// Generated from: 'set void SetText=2181(, cstring text)'
	final @property void text(in char[] text) { // SetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2181, 0, cast(size_t) __cstr_text);
	}

	/// Retrieve all the text in the document.
	/// Returns number of characters retrieved.
	/// Generated from: 'get bytelen GetText=2182(bytelen length, cstringresult text)'
	final @property char[] text() { // GetText
		immutable __len = sendMessageDirect(2182, 0, 0);
		assert(__len > 0);
		if(__len == 1) return null;
		char[] text = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(2182, __len, cast(size_t) text.ptr) + 1;
		assert(text.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] getText(char[] textBuff) {
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(2182, __len, cast(size_t) text.ptr) + 1;
		return text[0 .. __len];
	}

	/// Retrieve the number of characters in the document.
	/// Generated from: 'get int GetTextLength=2183(,)'
	final @property int textLength() { // GetTextLength
		return sendMessageDirect(2183, 0, 0);
	}

	/// Retrieve a pointer to a function that processes messages for this Scintilla.
	/// Generated from: 'get int GetDirectFunction=2184(,)'
	final @property int directFunction() { // GetDirectFunction
		return sendMessageDirect(2184, 0, 0);
	}

	/// Retrieve a pointer value to use as the first argument when calling
	/// the function returned by GetDirectFunction.
	/// Generated from: 'get int GetDirectPointer=2185(,)'
	final @property int directPointer() { // GetDirectPointer
		return sendMessageDirect(2185, 0, 0);
	}

	/// Set to overtype (true) or insert mode.
	/// Generated from: 'set void SetOvertype=2186(bool overtype,)'
	final @property void overtype(bool overtype) { // SetOvertype
		sendMessageDirect(2186, overtype, 0);
	}

	/// Returns true if overtype mode is active otherwise false is returned.
	/// Generated from: 'get bool GetOvertype=2187(,)'
	final @property bool overtype() { // GetOvertype
		return !!sendMessageDirect(2187, 0, 0);
	}

	/// Set the width of the insert mode caret.
	/// Generated from: 'set void SetCaretWidth=2188(int pixelWidth,)'
	final @property void caretWidth(int pixelWidth) { // SetCaretWidth
		sendMessageDirect(2188, pixelWidth, 0);
	}

	/// Returns the width of the insert mode caret.
	/// Generated from: 'get int GetCaretWidth=2189(,)'
	final @property int caretWidth() { // GetCaretWidth
		return sendMessageDirect(2189, 0, 0);
	}

	/// Sets the position that starts the target which is used for updating the
	/// document without affecting the scroll position.
	/// Generated from: 'set void SetTargetStart=2190(position pos,)'
	final @property void targetStart(Position pos) { // SetTargetStart
		sendMessageDirect(2190, pos, 0);
	}

	/// Get the position that starts the target.
	/// Generated from: 'get position GetTargetStart=2191(,)'
	final @property Position targetStart() { // GetTargetStart
		return sendMessageDirect(2191, 0, 0);
	}

	/// Sets the position that ends the target which is used for updating the
	/// document without affecting the scroll position.
	/// Generated from: 'set void SetTargetEnd=2192(position pos,)'
	final @property void targetEnd(Position pos) { // SetTargetEnd
		sendMessageDirect(2192, pos, 0);
	}

	/// Get the position that ends the target.
	/// Generated from: 'get position GetTargetEnd=2193(,)'
	final @property Position targetEnd() { // GetTargetEnd
		return sendMessageDirect(2193, 0, 0);
	}

	/// Replace the target text with the argument text.
	/// Text is counted so it can contain NULs.
	/// Returns the length of the replacement text.
	/// Generated from: 'fun len ReplaceTarget=2194(len length, chars text)'
	final void replaceTarget(in char[] text) { // ReplaceTarget
		sendMessageDirect(2194, text.length, cast(size_t) text.ptr);
	}

	/// Replace the target text with the argument text after \d processing.
	/// Text is counted so it can contain NULs.
	/// Looks for \d where d is between 1 and 9 and replaces these with the strings
	/// matched in the last search operation which were surrounded by \( and \).
	/// Returns the length of the replacement text including any change
	/// caused by processing the \d patterns.
	/// Generated from: 'fun int ReplaceTargetRE=2195(len length, chars text)'
	final int replaceTargetRe(in char[] text) { // ReplaceTargetRE
		return sendMessageDirect(2195, text.length, cast(size_t) text.ptr);
	}

	/// Search for a counted string in the target and set the target to the found
	/// range. Text is counted so it can contain NULs.
	/// Returns length of range or -1 for failure in which case target is not moved.
	/// Generated from: 'fun int SearchInTarget=2197(len length, chars text)'
	final int searchInTarget(in char[] text) { // SearchInTarget
		return sendMessageDirect(2197, text.length, cast(size_t) text.ptr);
	}

	/// Set the search flags used by SearchInTarget.
	/// Generated from: 'set void SetSearchFlags=2198(int flags,)'
	final @property void searchFlags(int flags) { // SetSearchFlags
		sendMessageDirect(2198, flags, 0);
	}

	/// Get the search flags used by SearchInTarget.
	/// Generated from: 'get int GetSearchFlags=2199(,)'
	final @property int searchFlags() { // GetSearchFlags
		return sendMessageDirect(2199, 0, 0);
	}

	/// Show a call tip containing a definition near position pos.
	/// Generated from: 'fun void CallTipShow=2200(position pos, cstring definition)'
	final void callTipShow(Position pos, in char[] definition) { // CallTipShow
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_definition = toCString(__sibuff, definition, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2200, pos, cast(size_t) __cstr_definition);
	}

	/// Remove the call tip from the screen.
	/// Generated from: 'fun void CallTipCancel=2201(,)'
	final void callTipCancel() { // CallTipCancel
		sendMessageDirect(2201, 0, 0);
	}

	/// Is there an active call tip?
	/// Generated from: 'fun bool CallTipActive=2202(,)'
	final bool callTipActive() { // CallTipActive
		return !!sendMessageDirect(2202, 0, 0);
	}

	/// Retrieve the position where the caret was before displaying the call tip.
	/// Generated from: 'fun position CallTipPosStart=2203(,)'
	final Position callTipPosStart() { // CallTipPosStart
		return sendMessageDirect(2203, 0, 0);
	}

	/// Highlight a segment of the definition.
	/// Generated from: 'fun void CallTipSetHlt=2204(int start, int end)'
	final void callTipSetHlt(int start, int end) { // CallTipSetHlt
		sendMessageDirect(2204, start, end);
	}

	/// Set the background colour for the call tip.
	/// Generated from: 'set void CallTipSetBack=2205(colour back,)'
	final @property void callTipSetBack(Colour back) { // CallTipSetBack
		sendMessageDirect(2205, back.rgb, 0);
	}

	/// Set the foreground colour for the call tip.
	/// Generated from: 'set void CallTipSetFore=2206(colour fore,)'
	final @property void callTipSetFore(Colour fore) { // CallTipSetFore
		sendMessageDirect(2206, fore.rgb, 0);
	}

	/// Set the foreground colour for the highlighted part of the call tip.
	/// Generated from: 'set void CallTipSetForeHlt=2207(colour fore,)'
	final @property void callTipSetForeHlt(Colour fore) { // CallTipSetForeHlt
		sendMessageDirect(2207, fore.rgb, 0);
	}

	/// Enable use of STYLE_CALLTIP and set call tip tab size in pixels.
	/// Generated from: 'set void CallTipUseStyle=2212(int tabSize,)'
	final @property void callTipUseStyle(int tabSize) { // CallTipUseStyle
		sendMessageDirect(2212, tabSize, 0);
	}

	/// Set position of calltip, above or below text.
	/// Generated from: 'set void CallTipSetPosition=2213(bool above,)'
	final @property void callTipSetPosition(bool above) { // CallTipSetPosition
		sendMessageDirect(2213, above, 0);
	}

	/// Find the display line of a document line taking hidden lines into account.
	/// Generated from: 'fun int VisibleFromDocLine=2220(int line,)'
	final int visibleFromDocLine(int line) { // VisibleFromDocLine
		return sendMessageDirect(2220, line, 0);
	}

	/// Find the document line of a display line taking hidden lines into account.
	/// Generated from: 'fun int DocLineFromVisible=2221(int lineDisplay,)'
	final int docLineFromVisible(int lineDisplay) { // DocLineFromVisible
		return sendMessageDirect(2221, lineDisplay, 0);
	}

	/// The number of display lines needed to wrap a document line
	/// Generated from: 'fun int WrapCount=2235(int line,)'
	final int wrapCount(int line) { // WrapCount
		return sendMessageDirect(2235, line, 0);
	}

	/// Set the fold level of a line.
	/// This encodes an integer level along with flags indicating whether the
	/// line is a header and whether it is effectively white space.
	/// Generated from: 'set void SetFoldLevel=2222(int line, int level)'
	final void setFoldLevel(int line, int level) { // SetFoldLevel
		sendMessageDirect(2222, line, level);
	}

	/// Retrieve the fold level of a line.
	/// Generated from: 'get int GetFoldLevel=2223(int line,)'
	final int getFoldLevel(int line) { // GetFoldLevel
		return sendMessageDirect(2223, line, 0);
	}

	/// Find the last child line of a header line.
	/// Generated from: 'get int GetLastChild=2224(int line, int level)'
	final int getLastChild(int line, int level) { // GetLastChild
		return sendMessageDirect(2224, line, level);
	}

	/// Find the parent line of a child line.
	/// Generated from: 'get int GetFoldParent=2225(int line,)'
	final int getFoldParent(int line) { // GetFoldParent
		return sendMessageDirect(2225, line, 0);
	}

	/// Make a range of lines visible.
	/// Generated from: 'fun void ShowLines=2226(int lineStart, int lineEnd)'
	final void showLines(int lineStart, int lineEnd) { // ShowLines
		sendMessageDirect(2226, lineStart, lineEnd);
	}

	/// Make a range of lines invisible.
	/// Generated from: 'fun void HideLines=2227(int lineStart, int lineEnd)'
	final void hideLines(int lineStart, int lineEnd) { // HideLines
		sendMessageDirect(2227, lineStart, lineEnd);
	}

	/// Is a line visible?
	/// Generated from: 'get bool GetLineVisible=2228(int line,)'
	final bool getLineVisible(int line) { // GetLineVisible
		return !!sendMessageDirect(2228, line, 0);
	}

	/// Are all lines visible?
	/// Generated from: 'get bool GetAllLinesVisible=2236(,)'
	final @property bool allLinesVisible() { // GetAllLinesVisible
		return !!sendMessageDirect(2236, 0, 0);
	}

	/// Show the children of a header line.
	/// Generated from: 'set void SetFoldExpanded=2229(int line, bool expanded)'
	final void setFoldExpanded(int line, bool expanded) { // SetFoldExpanded
		sendMessageDirect(2229, line, expanded);
	}

	/// Is a header line expanded?
	/// Generated from: 'get bool GetFoldExpanded=2230(int line,)'
	final bool getFoldExpanded(int line) { // GetFoldExpanded
		return !!sendMessageDirect(2230, line, 0);
	}

	/// Switch a header line between expanded and contracted.
	/// Generated from: 'fun void ToggleFold=2231(int line,)'
	final void toggleFold(int line) { // ToggleFold
		sendMessageDirect(2231, line, 0);
	}

	/// Ensure a particular line is visible by expanding any header line hiding it.
	/// Generated from: 'fun void EnsureVisible=2232(int line,)'
	final void ensureVisible(int line) { // EnsureVisible
		sendMessageDirect(2232, line, 0);
	}

	/// Set some style options for folding.
	/// Generated from: 'set void SetFoldFlags=2233(int flags,)'
	final @property void foldFlags(int flags) { // SetFoldFlags
		sendMessageDirect(2233, flags, 0);
	}

	/// Ensure a particular line is visible by expanding any header line hiding it.
	/// Use the currently set visibility policy to determine which range to display.
	/// Generated from: 'fun void EnsureVisibleEnforcePolicy=2234(int line,)'
	final void ensureVisibleEnforcePolicy(int line) { // EnsureVisibleEnforcePolicy
		sendMessageDirect(2234, line, 0);
	}

	/// Sets whether a tab pressed when caret is within indentation indents.
	/// Generated from: 'set void SetTabIndents=2260(bool tabIndents,)'
	final @property void tabIndents(bool tabIndents) { // SetTabIndents
		sendMessageDirect(2260, tabIndents, 0);
	}

	/// Does a tab pressed when caret is within indentation indent?
	/// Generated from: 'get bool GetTabIndents=2261(,)'
	final @property bool tabIndents() { // GetTabIndents
		return !!sendMessageDirect(2261, 0, 0);
	}

	/// Sets whether a backspace pressed when caret is within indentation unindents.
	/// Generated from: 'set void SetBackSpaceUnIndents=2262(bool bsUnIndents,)'
	final @property void backSpaceUnIndents(bool bsUnIndents) { // SetBackSpaceUnIndents
		sendMessageDirect(2262, bsUnIndents, 0);
	}

	/// Does a backspace pressed when caret is within indentation unindent?
	/// Generated from: 'get bool GetBackSpaceUnIndents=2263(,)'
	final @property bool backSpaceUnIndents() { // GetBackSpaceUnIndents
		return !!sendMessageDirect(2263, 0, 0);
	}

	/// Sets the time the mouse must sit still to generate a mouse dwell event.
	/// Generated from: 'set void SetMouseDwellTime=2264(int periodMilliseconds,)'
	final @property void mouseDwellTime(int periodMilliseconds) { // SetMouseDwellTime
		sendMessageDirect(2264, periodMilliseconds, 0);
	}

	/// Retrieve the time the mouse must sit still to generate a mouse dwell event.
	/// Generated from: 'get int GetMouseDwellTime=2265(,)'
	final @property int mouseDwellTime() { // GetMouseDwellTime
		return sendMessageDirect(2265, 0, 0);
	}

	/// Get position of start of word.
	/// Generated from: 'fun int WordStartPosition=2266(position pos, bool onlyWordCharacters)'
	final int wordStartPosition(Position pos, bool onlyWordCharacters) { // WordStartPosition
		return sendMessageDirect(2266, pos, onlyWordCharacters);
	}

	/// Get position of end of word.
	/// Generated from: 'fun int WordEndPosition=2267(position pos, bool onlyWordCharacters)'
	final int wordEndPosition(Position pos, bool onlyWordCharacters) { // WordEndPosition
		return sendMessageDirect(2267, pos, onlyWordCharacters);
	}

	/// Sets whether text is word wrapped.
	/// Generated from: 'set void SetWrapMode=2268(int mode,)'
	final @property void wrapMode(int mode) { // SetWrapMode
		sendMessageDirect(2268, mode, 0);
	}

	/// Retrieve whether text is word wrapped.
	/// Generated from: 'get int GetWrapMode=2269(,)'
	final @property int wrapMode() { // GetWrapMode
		return sendMessageDirect(2269, 0, 0);
	}

	/// Set the display mode of visual flags for wrapped lines.
	/// Generated from: 'set void SetWrapVisualFlags=2460(int wrapVisualFlags,)'
	final @property void wrapVisualFlags(int wrapVisualFlags) { // SetWrapVisualFlags
		sendMessageDirect(2460, wrapVisualFlags, 0);
	}

	/// Retrive the display mode of visual flags for wrapped lines.
	/// Generated from: 'get int GetWrapVisualFlags=2461(,)'
	final @property int wrapVisualFlags() { // GetWrapVisualFlags
		return sendMessageDirect(2461, 0, 0);
	}

	/// Set the location of visual flags for wrapped lines.
	/// Generated from: 'set void SetWrapVisualFlagsLocation=2462(int wrapVisualFlagsLocation,)'
	final @property void wrapVisualFlagsLocation(int wrapVisualFlagsLocation) { // SetWrapVisualFlagsLocation
		sendMessageDirect(2462, wrapVisualFlagsLocation, 0);
	}

	/// Retrive the location of visual flags for wrapped lines.
	/// Generated from: 'get int GetWrapVisualFlagsLocation=2463(,)'
	final @property int wrapVisualFlagsLocation() { // GetWrapVisualFlagsLocation
		return sendMessageDirect(2463, 0, 0);
	}

	/// Set the start indent for wrapped lines.
	/// Generated from: 'set void SetWrapStartIndent=2464(int indent,)'
	final @property void wrapStartIndent(int indent) { // SetWrapStartIndent
		sendMessageDirect(2464, indent, 0);
	}

	/// Retrive the start indent for wrapped lines.
	/// Generated from: 'get int GetWrapStartIndent=2465(,)'
	final @property int wrapStartIndent() { // GetWrapStartIndent
		return sendMessageDirect(2465, 0, 0);
	}

	/// Sets how wrapped sublines are placed. Default is fixed.
	/// Generated from: 'set void SetWrapIndentMode=2472(int mode,)'
	final @property void wrapIndentMode(int mode) { // SetWrapIndentMode
		sendMessageDirect(2472, mode, 0);
	}

	/// Retrieve how wrapped sublines are placed. Default is fixed.
	/// Generated from: 'get int GetWrapIndentMode=2473(,)'
	final @property int wrapIndentMode() { // GetWrapIndentMode
		return sendMessageDirect(2473, 0, 0);
	}

	/// Sets the degree of caching of layout information.
	/// Generated from: 'set void SetLayoutCache=2272(int mode,)'
	final @property void layoutCache(int mode) { // SetLayoutCache
		sendMessageDirect(2272, mode, 0);
	}

	/// Retrieve the degree of caching of layout information.
	/// Generated from: 'get int GetLayoutCache=2273(,)'
	final @property int layoutCache() { // GetLayoutCache
		return sendMessageDirect(2273, 0, 0);
	}

	/// Sets the document width assumed for scrolling.
	/// Generated from: 'set void SetScrollWidth=2274(int pixelWidth,)'
	final @property void scrollWidth(int pixelWidth) { // SetScrollWidth
		sendMessageDirect(2274, pixelWidth, 0);
	}

	/// Retrieve the document width assumed for scrolling.
	/// Generated from: 'get int GetScrollWidth=2275(,)'
	final @property int scrollWidth() { // GetScrollWidth
		return sendMessageDirect(2275, 0, 0);
	}

	/// Sets whether the maximum width line displayed is used to set scroll width.
	/// Generated from: 'set void SetScrollWidthTracking=2516(bool tracking,)'
	final @property void scrollWidthTracking(bool tracking) { // SetScrollWidthTracking
		sendMessageDirect(2516, tracking, 0);
	}

	/// Retrieve whether the scroll width tracks wide lines.
	/// Generated from: 'get bool GetScrollWidthTracking=2517(,)'
	final @property bool scrollWidthTracking() { // GetScrollWidthTracking
		return !!sendMessageDirect(2517, 0, 0);
	}

	/// Measure the pixel width of some text in a particular style.
	/// NUL terminated text argument.
	/// Does not handle tab or control characters.
	/// Generated from: 'fun int TextWidth=2276(int style, cstring text)'
	final int textWidth(int style, in char[] text) { // TextWidth
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2276, style, cast(size_t) __cstr_text);
	}

	/// Sets the scroll range so that maximum scroll position has
	/// the last line at the bottom of the view (default).
	/// Setting this to false allows scrolling one page below the last line.
	/// Generated from: 'set void SetEndAtLastLine=2277(bool endAtLastLine,)'
	final @property void endAtLastLine(bool endAtLastLine) { // SetEndAtLastLine
		sendMessageDirect(2277, endAtLastLine, 0);
	}

	/// Retrieve whether the maximum scroll position has the last
	/// line at the bottom of the view.
	/// Generated from: 'get bool GetEndAtLastLine=2278(,)'
	final @property bool endAtLastLine() { // GetEndAtLastLine
		return !!sendMessageDirect(2278, 0, 0);
	}

	/// Retrieve the height of a particular line of text in pixels.
	/// Generated from: 'fun int TextHeight=2279(int line,)'
	final int textHeight(int line) { // TextHeight
		return sendMessageDirect(2279, line, 0);
	}

	/// Show or hide the vertical scroll bar.
	/// Generated from: 'set void SetVScrollBar=2280(bool show,)'
	final @property void vScrollBar(bool show) { // SetVScrollBar
		sendMessageDirect(2280, show, 0);
	}

	/// Is the vertical scroll bar visible?
	/// Generated from: 'get bool GetVScrollBar=2281(,)'
	final @property bool vScrollBar() { // GetVScrollBar
		return !!sendMessageDirect(2281, 0, 0);
	}

	/// Append a string to the end of the document without changing the selection.
	/// Generated from: 'fun void AppendText=2282(len length, chars text)'
	final void appendText(in char[] text) { // AppendText
		sendMessageDirect(2282, text.length, cast(size_t) text.ptr);
	}

	/// Is drawing done in two phases with backgrounds drawn before faoregrounds?
	/// Generated from: 'get bool GetTwoPhaseDraw=2283(,)'
	final @property bool twoPhaseDraw() { // GetTwoPhaseDraw
		return !!sendMessageDirect(2283, 0, 0);
	}

	/// In twoPhaseDraw mode, drawing is performed in two phases, first the background
	/// and then the foreground. This avoids chopping off characters that overlap the next run.
	/// Generated from: 'set void SetTwoPhaseDraw=2284(bool twoPhase,)'
	final @property void twoPhaseDraw(bool twoPhase) { // SetTwoPhaseDraw
		sendMessageDirect(2284, twoPhase, 0);
	}

	/// Choose the quality level for text from the FontQuality enumeration.
	/// Generated from: 'set void SetFontQuality=2611(int fontQuality,)'
	final @property void fontQuality(int fontQuality) { // SetFontQuality
		sendMessageDirect(2611, fontQuality, 0);
	}

	/// Retrieve the quality level for text.
	/// Generated from: 'get int GetFontQuality=2612(,)'
	final @property int fontQuality() { // GetFontQuality
		return sendMessageDirect(2612, 0, 0);
	}

	/// Scroll so that a display line is at the top of the display.
	/// Generated from: 'set void SetFirstVisibleLine=2613(int lineDisplay,)'
	final @property void firstVisibleLine(int lineDisplay) { // SetFirstVisibleLine
		sendMessageDirect(2613, lineDisplay, 0);
	}

	/// Change the effect of pasting when there are multiple selections.
	/// Generated from: 'set void SetMultiPaste=2614(int multiPaste,)'
	final @property void multiPaste(int multiPaste) { // SetMultiPaste
		sendMessageDirect(2614, multiPaste, 0);
	}

	/// Retrieve the effect of pasting when there are multiple selections..
	/// Generated from: 'get int GetMultiPaste=2615(,)'
	final @property int multiPaste() { // GetMultiPaste
		return sendMessageDirect(2615, 0, 0);
	}

	/// Retrieve the value of a tag from a regular expression search.
	/// Generated from: 'get strlen GetTag=2616(int tagNumber, cstringresult tagValue)'
	final char[] getTag(int tagNumber) { // GetTag
		immutable __len = sendMessageDirect(2616, tagNumber, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] tagValue = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(2616, tagNumber, cast(size_t) tagValue.ptr) + 1;
		assert(tagValue.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return tagValue;
	}

	/// ditto
	final char[] getTag(char[] tagValueBuff, int tagNumber) {
		assert(!tagValueBuff || tagValueBuff.length >= sendMessageDirect(2616, tagNumber, 0) + 1);
		char[] tagValue = tagValueBuff;
		auto __len = tagValueBuff.length;
		__len = sendMessageDirect(2616, tagNumber, cast(size_t) tagValue.ptr) + 1;
		return tagValue[0 .. __len];
	}

	/// Make the target range start and end be the same as the selection range start and end.
	/// Generated from: 'fun void TargetFromSelection=2287(,)'
	final void targetFromSelection() { // TargetFromSelection
		sendMessageDirect(2287, 0, 0);
	}

	/// Join the lines in the target.
	/// Generated from: 'fun void LinesJoin=2288(,)'
	final void linesJoin() { // LinesJoin
		sendMessageDirect(2288, 0, 0);
	}

	/// Split the lines in the target into lines that are less wide than pixelWidth
	/// where possible.
	/// Generated from: 'fun void LinesSplit=2289(int pixelWidth,)'
	final void linesSplit(int pixelWidth) { // LinesSplit
		sendMessageDirect(2289, pixelWidth, 0);
	}

	/// Set the colours used as a chequerboard pattern in the fold margin
	/// Generated from: 'set void SetFoldMarginColour=2290(bool useSetting, colour back)'
	final void setFoldMarginColour(bool useSetting, Colour back) { // SetFoldMarginColour
		sendMessageDirect(2290, useSetting, back.rgb);
	}

	/// Generated from: 'set void SetFoldMarginHiColour=2291(bool useSetting, colour fore)'
	final void setFoldMarginHiColour(bool useSetting, Colour fore) { // SetFoldMarginHiColour
		sendMessageDirect(2291, useSetting, fore.rgb);
	}

	/// Move caret down one line.
	/// Generated from: 'fun void LineDown=2300(,)'
	final void lineDown() { // LineDown
		sendMessageDirect(2300, 0, 0);
	}

	/// Move caret down one line extending selection to new caret position.
	/// Generated from: 'fun void LineDownExtend=2301(,)'
	final void lineDownExtend() { // LineDownExtend
		sendMessageDirect(2301, 0, 0);
	}

	/// Move caret up one line.
	/// Generated from: 'fun void LineUp=2302(,)'
	final void lineUp() { // LineUp
		sendMessageDirect(2302, 0, 0);
	}

	/// Move caret up one line extending selection to new caret position.
	/// Generated from: 'fun void LineUpExtend=2303(,)'
	final void lineUpExtend() { // LineUpExtend
		sendMessageDirect(2303, 0, 0);
	}

	/// Move caret left one character.
	/// Generated from: 'fun void CharLeft=2304(,)'
	final void charLeft() { // CharLeft
		sendMessageDirect(2304, 0, 0);
	}

	/// Move caret left one character extending selection to new caret position.
	/// Generated from: 'fun void CharLeftExtend=2305(,)'
	final void charLeftExtend() { // CharLeftExtend
		sendMessageDirect(2305, 0, 0);
	}

	/// Move caret right one character.
	/// Generated from: 'fun void CharRight=2306(,)'
	final void charRight() { // CharRight
		sendMessageDirect(2306, 0, 0);
	}

	/// Move caret right one character extending selection to new caret position.
	/// Generated from: 'fun void CharRightExtend=2307(,)'
	final void charRightExtend() { // CharRightExtend
		sendMessageDirect(2307, 0, 0);
	}

	/// Move caret left one word.
	/// Generated from: 'fun void WordLeft=2308(,)'
	final void wordLeft() { // WordLeft
		sendMessageDirect(2308, 0, 0);
	}

	/// Move caret left one word extending selection to new caret position.
	/// Generated from: 'fun void WordLeftExtend=2309(,)'
	final void wordLeftExtend() { // WordLeftExtend
		sendMessageDirect(2309, 0, 0);
	}

	/// Move caret right one word.
	/// Generated from: 'fun void WordRight=2310(,)'
	final void wordRight() { // WordRight
		sendMessageDirect(2310, 0, 0);
	}

	/// Move caret right one word extending selection to new caret position.
	/// Generated from: 'fun void WordRightExtend=2311(,)'
	final void wordRightExtend() { // WordRightExtend
		sendMessageDirect(2311, 0, 0);
	}

	/// Move caret to first position on line.
	/// Generated from: 'fun void Home=2312(,)'
	final void home() { // Home
		sendMessageDirect(2312, 0, 0);
	}

	/// Move caret to first position on line extending selection to new caret position.
	/// Generated from: 'fun void HomeExtend=2313(,)'
	final void homeExtend() { // HomeExtend
		sendMessageDirect(2313, 0, 0);
	}

	/// Move caret to last position on line.
	/// Generated from: 'fun void LineEnd=2314(,)'
	final void lineEnd() { // LineEnd
		sendMessageDirect(2314, 0, 0);
	}

	/// Move caret to last position on line extending selection to new caret position.
	/// Generated from: 'fun void LineEndExtend=2315(,)'
	final void lineEndExtend() { // LineEndExtend
		sendMessageDirect(2315, 0, 0);
	}

	/// Move caret to first position in document.
	/// Generated from: 'fun void DocumentStart=2316(,)'
	final void documentStart() { // DocumentStart
		sendMessageDirect(2316, 0, 0);
	}

	/// Move caret to first position in document extending selection to new caret position.
	/// Generated from: 'fun void DocumentStartExtend=2317(,)'
	final void documentStartExtend() { // DocumentStartExtend
		sendMessageDirect(2317, 0, 0);
	}

	/// Move caret to last position in document.
	/// Generated from: 'fun void DocumentEnd=2318(,)'
	final void documentEnd() { // DocumentEnd
		sendMessageDirect(2318, 0, 0);
	}

	/// Move caret to last position in document extending selection to new caret position.
	/// Generated from: 'fun void DocumentEndExtend=2319(,)'
	final void documentEndExtend() { // DocumentEndExtend
		sendMessageDirect(2319, 0, 0);
	}

	/// Move caret one page up.
	/// Generated from: 'fun void PageUp=2320(,)'
	final void pageUp() { // PageUp
		sendMessageDirect(2320, 0, 0);
	}

	/// Move caret one page up extending selection to new caret position.
	/// Generated from: 'fun void PageUpExtend=2321(,)'
	final void pageUpExtend() { // PageUpExtend
		sendMessageDirect(2321, 0, 0);
	}

	/// Move caret one page down.
	/// Generated from: 'fun void PageDown=2322(,)'
	final void pageDown() { // PageDown
		sendMessageDirect(2322, 0, 0);
	}

	/// Move caret one page down extending selection to new caret position.
	/// Generated from: 'fun void PageDownExtend=2323(,)'
	final void pageDownExtend() { // PageDownExtend
		sendMessageDirect(2323, 0, 0);
	}

	/// Switch from insert to overtype mode or the reverse.
	/// Generated from: 'fun void EditToggleOvertype=2324(,)'
	final void editToggleOvertype() { // EditToggleOvertype
		sendMessageDirect(2324, 0, 0);
	}

	/// Cancel any modes such as call tip or auto-completion list display.
	/// Generated from: 'fun void Cancel=2325(,)'
	final void cancel() { // Cancel
		sendMessageDirect(2325, 0, 0);
	}

	/// Delete the selection or if no selection, the character before the caret.
	/// Generated from: 'fun void DeleteBack=2326(,)'
	final void deleteBack() { // DeleteBack
		sendMessageDirect(2326, 0, 0);
	}

	/// If selection is empty or all on one line replace the selection with a tab character.
	/// If more than one line selected, indent the lines.
	/// Generated from: 'fun void Tab=2327(,)'
	final void tab() { // Tab
		sendMessageDirect(2327, 0, 0);
	}

	/// Dedent the selected lines.
	/// Generated from: 'fun void BackTab=2328(,)'
	final void backTab() { // BackTab
		sendMessageDirect(2328, 0, 0);
	}

	/// Insert a new line, may use a CRLF, CR or LF depending on EOL mode.
	/// Generated from: 'fun void NewLine=2329(,)'
	final void newLine() { // NewLine
		sendMessageDirect(2329, 0, 0);
	}

	/// Insert a Form Feed character.
	/// Generated from: 'fun void FormFeed=2330(,)'
	final void formFeed() { // FormFeed
		sendMessageDirect(2330, 0, 0);
	}

	/// Move caret to before first visible character on line.
	/// If already there move to first character on line.
	/// Generated from: 'fun void VCHome=2331(,)'
	final void vcHome() { // VCHome
		sendMessageDirect(2331, 0, 0);
	}

	/// Like VCHome but extending selection to new caret position.
	/// Generated from: 'fun void VCHomeExtend=2332(,)'
	final void vcHomeExtend() { // VCHomeExtend
		sendMessageDirect(2332, 0, 0);
	}

	/// Magnify the displayed text by increasing the sizes by 1 point.
	/// Generated from: 'fun void ZoomIn=2333(,)'
	final void zoomIn() { // ZoomIn
		sendMessageDirect(2333, 0, 0);
	}

	/// Make the displayed text smaller by decreasing the sizes by 1 point.
	/// Generated from: 'fun void ZoomOut=2334(,)'
	final void zoomOut() { // ZoomOut
		sendMessageDirect(2334, 0, 0);
	}

	/// Delete the word to the left of the caret.
	/// Generated from: 'fun void DelWordLeft=2335(,)'
	final void delWordLeft() { // DelWordLeft
		sendMessageDirect(2335, 0, 0);
	}

	/// Delete the word to the right of the caret.
	/// Generated from: 'fun void DelWordRight=2336(,)'
	final void delWordRight() { // DelWordRight
		sendMessageDirect(2336, 0, 0);
	}

	/// Delete the word to the right of the caret, but not the trailing non-word characters.
	/// Generated from: 'fun void DelWordRightEnd=2518(,)'
	final void delWordRightEnd() { // DelWordRightEnd
		sendMessageDirect(2518, 0, 0);
	}

	/// Cut the line containing the caret.
	/// Generated from: 'fun void LineCut=2337(,)'
	final void lineCut() { // LineCut
		sendMessageDirect(2337, 0, 0);
	}

	/// Delete the line containing the caret.
	/// Generated from: 'fun void LineDelete=2338(,)'
	final void lineDelete() { // LineDelete
		sendMessageDirect(2338, 0, 0);
	}

	/// Switch the current line with the previous.
	/// Generated from: 'fun void LineTranspose=2339(,)'
	final void lineTranspose() { // LineTranspose
		sendMessageDirect(2339, 0, 0);
	}

	/// Duplicate the current line.
	/// Generated from: 'fun void LineDuplicate=2404(,)'
	final void lineDuplicate() { // LineDuplicate
		sendMessageDirect(2404, 0, 0);
	}

	/// Transform the selection to lower case.
	/// Generated from: 'fun void LowerCase=2340(,)'
	final void lowerCase() { // LowerCase
		sendMessageDirect(2340, 0, 0);
	}

	/// Transform the selection to upper case.
	/// Generated from: 'fun void UpperCase=2341(,)'
	final void upperCase() { // UpperCase
		sendMessageDirect(2341, 0, 0);
	}

	/// Scroll the document down, keeping the caret visible.
	/// Generated from: 'fun void LineScrollDown=2342(,)'
	final void lineScrollDown() { // LineScrollDown
		sendMessageDirect(2342, 0, 0);
	}

	/// Scroll the document up, keeping the caret visible.
	/// Generated from: 'fun void LineScrollUp=2343(,)'
	final void lineScrollUp() { // LineScrollUp
		sendMessageDirect(2343, 0, 0);
	}

	/// Delete the selection or if no selection, the character before the caret.
	/// Will not delete the character before at the start of a line.
	/// Generated from: 'fun void DeleteBackNotLine=2344(,)'
	final void deleteBackNotLine() { // DeleteBackNotLine
		sendMessageDirect(2344, 0, 0);
	}

	/// Move caret to first position on display line.
	/// Generated from: 'fun void HomeDisplay=2345(,)'
	final void homeDisplay() { // HomeDisplay
		sendMessageDirect(2345, 0, 0);
	}

	/// Move caret to first position on display line extending selection to
	/// new caret position.
	/// Generated from: 'fun void HomeDisplayExtend=2346(,)'
	final void homeDisplayExtend() { // HomeDisplayExtend
		sendMessageDirect(2346, 0, 0);
	}

	/// Move caret to last position on display line.
	/// Generated from: 'fun void LineEndDisplay=2347(,)'
	final void lineEndDisplay() { // LineEndDisplay
		sendMessageDirect(2347, 0, 0);
	}

	/// Move caret to last position on display line extending selection to new
	/// caret position.
	/// Generated from: 'fun void LineEndDisplayExtend=2348(,)'
	final void lineEndDisplayExtend() { // LineEndDisplayExtend
		sendMessageDirect(2348, 0, 0);
	}

	/// These are like their namesakes Home(Extend)?, LineEnd(Extend)?, VCHome(Extend)?
	/// except they behave differently when word-wrap is enabled:
	/// They go first to the start / end of the display line, like (Home|LineEnd)Display
	/// The difference is that, the cursor is already at the point, it goes on to the start
	/// or end of the document line, as appropriate for (Home|LineEnd|VCHome)(Extend)?.
	/// Generated from: 'fun void HomeWrap=2349(,)'
	final void homeWrap() { // HomeWrap
		sendMessageDirect(2349, 0, 0);
	}

	/// Generated from: 'fun void HomeWrapExtend=2450(,)'
	final void homeWrapExtend() { // HomeWrapExtend
		sendMessageDirect(2450, 0, 0);
	}

	/// Generated from: 'fun void LineEndWrap=2451(,)'
	final void lineEndWrap() { // LineEndWrap
		sendMessageDirect(2451, 0, 0);
	}

	/// Generated from: 'fun void LineEndWrapExtend=2452(,)'
	final void lineEndWrapExtend() { // LineEndWrapExtend
		sendMessageDirect(2452, 0, 0);
	}

	/// Generated from: 'fun void VCHomeWrap=2453(,)'
	final void vcHomeWrap() { // VCHomeWrap
		sendMessageDirect(2453, 0, 0);
	}

	/// Generated from: 'fun void VCHomeWrapExtend=2454(,)'
	final void vcHomeWrapExtend() { // VCHomeWrapExtend
		sendMessageDirect(2454, 0, 0);
	}

	/// Copy the line containing the caret.
	/// Generated from: 'fun void LineCopy=2455(,)'
	final void lineCopy() { // LineCopy
		sendMessageDirect(2455, 0, 0);
	}

	/// Move the caret inside current view if it's not there already.
	/// Generated from: 'fun void MoveCaretInsideView=2401(,)'
	final void moveCaretInsideView() { // MoveCaretInsideView
		sendMessageDirect(2401, 0, 0);
	}

	/// How many characters are on a line, including end of line characters?
	/// Generated from: 'fun int LineLength=2350(int line,)'
	final int lineLength(int line) { // LineLength
		return sendMessageDirect(2350, line, 0);
	}

	/// Highlight the characters at two positions.
	/// Generated from: 'fun void BraceHighlight=2351(position pos1, position pos2)'
	final void braceHighlight(Position pos1, Position pos2) { // BraceHighlight
		sendMessageDirect(2351, pos1, pos2);
	}

	/// Use specified indicator to highlight matching braces instead of changing their style.
	/// Generated from: 'fun void BraceHighlightIndicator=2498(bool useBraceHighlightIndicator, int indicator)'
	final void braceHighlightIndicator(bool useBraceHighlightIndicator, int indicator) { // BraceHighlightIndicator
		sendMessageDirect(2498, useBraceHighlightIndicator, indicator);
	}

	/// Highlight the character at a position indicating there is no matching brace.
	/// Generated from: 'fun void BraceBadLight=2352(position pos,)'
	final void braceBadLight(Position pos) { // BraceBadLight
		sendMessageDirect(2352, pos, 0);
	}

	/// Use specified indicator to highlight non matching brace instead of changing its style.
	/// Generated from: 'fun void BraceBadLightIndicator=2499(bool useBraceBadLightIndicator, int indicator)'
	final void braceBadLightIndicator(bool useBraceBadLightIndicator, int indicator) { // BraceBadLightIndicator
		sendMessageDirect(2499, useBraceBadLightIndicator, indicator);
	}

	/// Find the position of a matching brace or INVALID_POSITION if no match.
	/// Generated from: 'fun position BraceMatch=2353(position pos,)'
	final Position braceMatch(Position pos) { // BraceMatch
		return sendMessageDirect(2353, pos, 0);
	}

	/// Are the end of line characters visible?
	/// Generated from: 'get bool GetViewEOL=2355(,)'
	final @property bool viewEol() { // GetViewEOL
		return !!sendMessageDirect(2355, 0, 0);
	}

	/// Make the end of line characters visible or invisible.
	/// Generated from: 'set void SetViewEOL=2356(bool visible,)'
	final @property void viewEol(bool visible) { // SetViewEOL
		sendMessageDirect(2356, visible, 0);
	}

	/// Retrieve a pointer to the document object.
	/// Generated from: 'get int GetDocPointer=2357(,)'
	final @property int docPointer() { // GetDocPointer
		return sendMessageDirect(2357, 0, 0);
	}

	/// Change the document object used.
	/// Generated from: 'set void SetDocPointer=2358(, int pointer)'
	final @property void docPointer(int pointer) { // SetDocPointer
		sendMessageDirect(2358, 0, pointer);
	}

	/// Set which document modification events are sent to the container.
	/// Generated from: 'set void SetModEventMask=2359(int mask,)'
	final @property void modEventMask(int mask) { // SetModEventMask
		sendMessageDirect(2359, mask, 0);
	}

	/// Retrieve the column number which text should be kept within.
	/// Generated from: 'get int GetEdgeColumn=2360(,)'
	final @property int edgeColumn() { // GetEdgeColumn
		return sendMessageDirect(2360, 0, 0);
	}

	/// Set the column number of the edge.
	/// If text goes past the edge then it is highlighted.
	/// Generated from: 'set void SetEdgeColumn=2361(int column,)'
	final @property void edgeColumn(int column) { // SetEdgeColumn
		sendMessageDirect(2361, column, 0);
	}

	/// Retrieve the edge highlight mode.
	/// Generated from: 'get int GetEdgeMode=2362(,)'
	final @property int edgeMode() { // GetEdgeMode
		return sendMessageDirect(2362, 0, 0);
	}

	/// The edge may be displayed by a line (EDGE_LINE) or by highlighting text that
	/// goes beyond it (EDGE_BACKGROUND) or not displayed at all (EDGE_NONE).
	/// Generated from: 'set void SetEdgeMode=2363(int mode,)'
	final @property void edgeMode(int mode) { // SetEdgeMode
		sendMessageDirect(2363, mode, 0);
	}

	/// Retrieve the colour used in edge indication.
	/// Generated from: 'get colour GetEdgeColour=2364(,)'
	final @property Colour edgeColour() { // GetEdgeColour
		return cast(Colour)sendMessageDirect(2364, 0, 0);
	}

	/// Change the colour used in edge indication.
	/// Generated from: 'set void SetEdgeColour=2365(colour edgeColour,)'
	final @property void edgeColour(Colour edgeColour) { // SetEdgeColour
		sendMessageDirect(2365, edgeColour.rgb, 0);
	}

	/// Sets the current caret position to be the search anchor.
	/// Generated from: 'fun void SearchAnchor=2366(,)'
	final void searchAnchor() { // SearchAnchor
		sendMessageDirect(2366, 0, 0);
	}

	/// Find some text starting at the search anchor.
	/// Does not ensure the selection is visible.
	/// Generated from: 'fun int SearchNext=2367(int flags, cstring text)'
	final int searchNext(int flags, in char[] text) { // SearchNext
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2367, flags, cast(size_t) __cstr_text);
	}

	/// Find some text starting at the search anchor and moving backwards.
	/// Does not ensure the selection is visible.
	/// Generated from: 'fun int SearchPrev=2368(int flags, cstring text)'
	final int searchPrev(int flags, in char[] text) { // SearchPrev
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(2368, flags, cast(size_t) __cstr_text);
	}

	/// Retrieves the number of lines completely visible.
	/// Generated from: 'get int LinesOnScreen=2370(,)'
	final @property int linesOnScreen() { // LinesOnScreen
		return sendMessageDirect(2370, 0, 0);
	}

	/// Set whether a pop up menu is displayed automatically when the user presses
	/// the wrong mouse button.
	/// Generated from: 'fun void UsePopUp=2371(bool allowPopUp,)'
	final void usePopUp(bool allowPopUp) { // UsePopUp
		sendMessageDirect(2371, allowPopUp, 0);
	}

	/// Is the selection rectangular? The alternative is the more common stream selection.
	/// Generated from: 'get bool SelectionIsRectangle=2372(,)'
	final @property bool selectionIsRectangle() { // SelectionIsRectangle
		return !!sendMessageDirect(2372, 0, 0);
	}

	/// Set the zoom level. This number of points is added to the size of all fonts.
	/// It may be positive to magnify or negative to reduce.
	/// Generated from: 'set void SetZoom=2373(int zoom,)'
	final @property void zoom(int zoom) { // SetZoom
		sendMessageDirect(2373, zoom, 0);
	}

	/// Retrieve the zoom level.
	/// Generated from: 'get int GetZoom=2374(,)'
	final @property int zoom() { // GetZoom
		return sendMessageDirect(2374, 0, 0);
	}

	/// Create a new document object.
	/// Starts with reference count of 1 and not selected into editor.
	/// Generated from: 'fun int CreateDocument=2375(,)'
	final int createDocument() { // CreateDocument
		return sendMessageDirect(2375, 0, 0);
	}

	/// Extend life of document.
	/// Generated from: 'fun void AddRefDocument=2376(, int doc)'
	final void addRefDocument(int doc) { // AddRefDocument
		sendMessageDirect(2376, 0, doc);
	}

	/// Release a reference to the document, deleting document if it fades to black.
	/// Generated from: 'fun void ReleaseDocument=2377(, int doc)'
	final void releaseDocument(int doc) { // ReleaseDocument
		sendMessageDirect(2377, 0, doc);
	}

	/// Get which document modification events are sent to the container.
	/// Generated from: 'get int GetModEventMask=2378(,)'
	final @property int modEventMask() { // GetModEventMask
		return sendMessageDirect(2378, 0, 0);
	}

	/// Change internal focus flag.
	/// Generated from: 'set void SetFocus=2380(bool focus,)'
	final @property void focus(bool focus) { // SetFocus
		sendMessageDirect(2380, focus, 0);
	}

	/// Get internal focus flag.
	/// Generated from: 'get bool GetFocus=2381(,)'
	final @property bool focus() { // GetFocus
		return !!sendMessageDirect(2381, 0, 0);
	}

	/// Change error status - 0 = OK.
	/// Generated from: 'set void SetStatus=2382(int statusCode,)'
	final @property void status(int statusCode) { // SetStatus
		sendMessageDirect(2382, statusCode, 0);
	}

	/// Get error status.
	/// Generated from: 'get int GetStatus=2383(,)'
	final @property int status() { // GetStatus
		return sendMessageDirect(2383, 0, 0);
	}

	/// Set whether the mouse is captured when its button is pressed.
	/// Generated from: 'set void SetMouseDownCaptures=2384(bool captures,)'
	final @property void mouseDownCaptures(bool captures) { // SetMouseDownCaptures
		sendMessageDirect(2384, captures, 0);
	}

	/// Get whether mouse gets captured.
	/// Generated from: 'get bool GetMouseDownCaptures=2385(,)'
	final @property bool mouseDownCaptures() { // GetMouseDownCaptures
		return !!sendMessageDirect(2385, 0, 0);
	}

	/// Sets the cursor to one of the SC_CURSOR* values.
	/// Generated from: 'set void SetCursor=2386(int cursorType,)'
	final @property void cursor(int cursorType) { // SetCursor
		sendMessageDirect(2386, cursorType, 0);
	}

	/// Get cursor type.
	/// Generated from: 'get int GetCursor=2387(,)'
	final @property int cursor() { // GetCursor
		return sendMessageDirect(2387, 0, 0);
	}

	/// Change the way control characters are displayed:
	/// If symbol is < 32, keep the drawn way, else, use the given character.
	/// Generated from: 'set void SetControlCharSymbol=2388(int symbol,)'
	final @property void controlCharSymbol(int symbol) { // SetControlCharSymbol
		sendMessageDirect(2388, symbol, 0);
	}

	/// Get the way control characters are displayed.
	/// Generated from: 'get int GetControlCharSymbol=2389(,)'
	final @property int controlCharSymbol() { // GetControlCharSymbol
		return sendMessageDirect(2389, 0, 0);
	}

	/// Move to the previous change in capitalisation.
	/// Generated from: 'fun void WordPartLeft=2390(,)'
	final void wordPartLeft() { // WordPartLeft
		sendMessageDirect(2390, 0, 0);
	}

	/// Move to the previous change in capitalisation extending selection
	/// to new caret position.
	/// Generated from: 'fun void WordPartLeftExtend=2391(,)'
	final void wordPartLeftExtend() { // WordPartLeftExtend
		sendMessageDirect(2391, 0, 0);
	}

	/// Move to the change next in capitalisation.
	/// Generated from: 'fun void WordPartRight=2392(,)'
	final void wordPartRight() { // WordPartRight
		sendMessageDirect(2392, 0, 0);
	}

	/// Move to the next change in capitalisation extending selection
	/// to new caret position.
	/// Generated from: 'fun void WordPartRightExtend=2393(,)'
	final void wordPartRightExtend() { // WordPartRightExtend
		sendMessageDirect(2393, 0, 0);
	}

	/// Set the way the display area is determined when a particular line
	/// is to be moved to by Find, FindNext, GotoLine, etc.
	/// Generated from: 'fun void SetVisiblePolicy=2394(int visiblePolicy, int visibleSlop)'
	final void setVisiblePolicy(int visiblePolicy, int visibleSlop) { // SetVisiblePolicy
		sendMessageDirect(2394, visiblePolicy, visibleSlop);
	}

	/// Delete back from the current position to the start of the line.
	/// Generated from: 'fun void DelLineLeft=2395(,)'
	final void delLineLeft() { // DelLineLeft
		sendMessageDirect(2395, 0, 0);
	}

	/// Delete forwards from the current position to the end of the line.
	/// Generated from: 'fun void DelLineRight=2396(,)'
	final void delLineRight() { // DelLineRight
		sendMessageDirect(2396, 0, 0);
	}

	/// Get and Set the xOffset (ie, horizonal scroll position).
	/// Generated from: 'set void SetXOffset=2397(int newOffset,)'
	final @property void xOffset(int newOffset) { // SetXOffset
		sendMessageDirect(2397, newOffset, 0);
	}

	/// Generated from: 'get int GetXOffset=2398(,)'
	final @property int xOffset() { // GetXOffset
		return sendMessageDirect(2398, 0, 0);
	}

	/// Set the last x chosen value to be the caret x position.
	/// Generated from: 'fun void ChooseCaretX=2399(,)'
	final void chooseCaretX() { // ChooseCaretX
		sendMessageDirect(2399, 0, 0);
	}

	/// Set the focus to this Scintilla widget.
	/// Generated from: 'fun void GrabFocus=2400(,)'
	final void grabFocus() { // GrabFocus
		sendMessageDirect(2400, 0, 0);
	}

	/// Set the way the caret is kept visible when going sideway.
	/// The exclusion zone is given in pixels.
	/// Generated from: 'fun void SetXCaretPolicy=2402(int caretPolicy, int caretSlop)'
	final void setXCaretPolicy(int caretPolicy, int caretSlop) { // SetXCaretPolicy
		sendMessageDirect(2402, caretPolicy, caretSlop);
	}

	/// Set the way the line the caret is on is kept visible.
	/// The exclusion zone is given in lines.
	/// Generated from: 'fun void SetYCaretPolicy=2403(int caretPolicy, int caretSlop)'
	final void setYCaretPolicy(int caretPolicy, int caretSlop) { // SetYCaretPolicy
		sendMessageDirect(2403, caretPolicy, caretSlop);
	}

	/// Set printing to line wrapped (SC_WRAP_WORD) or not line wrapped (SC_WRAP_NONE).
	/// Generated from: 'set void SetPrintWrapMode=2406(int mode,)'
	final @property void printWrapMode(int mode) { // SetPrintWrapMode
		sendMessageDirect(2406, mode, 0);
	}

	/// Is printing line wrapped?
	/// Generated from: 'get int GetPrintWrapMode=2407(,)'
	final @property int printWrapMode() { // GetPrintWrapMode
		return sendMessageDirect(2407, 0, 0);
	}

	/// Set a fore colour for active hotspots.
	/// Generated from: 'set void SetHotspotActiveFore=2410(bool useSetting, colour fore)'
	final void setHotspotActiveFore(bool useSetting, Colour fore) { // SetHotspotActiveFore
		sendMessageDirect(2410, useSetting, fore.rgb);
	}

	/// Get the fore colour for active hotspots.
	/// Generated from: 'get colour GetHotspotActiveFore=2494(,)'
	final @property Colour hotspotActiveFore() { // GetHotspotActiveFore
		return cast(Colour)sendMessageDirect(2494, 0, 0);
	}

	/// Set a back colour for active hotspots.
	/// Generated from: 'set void SetHotspotActiveBack=2411(bool useSetting, colour back)'
	final void setHotspotActiveBack(bool useSetting, Colour back) { // SetHotspotActiveBack
		sendMessageDirect(2411, useSetting, back.rgb);
	}

	/// Get the back colour for active hotspots.
	/// Generated from: 'get colour GetHotspotActiveBack=2495(,)'
	final @property Colour hotspotActiveBack() { // GetHotspotActiveBack
		return cast(Colour)sendMessageDirect(2495, 0, 0);
	}

	/// Enable / Disable underlining active hotspots.
	/// Generated from: 'set void SetHotspotActiveUnderline=2412(bool underline,)'
	final @property void hotspotActiveUnderline(bool underline) { // SetHotspotActiveUnderline
		sendMessageDirect(2412, underline, 0);
	}

	/// Get whether underlining for active hotspots.
	/// Generated from: 'get bool GetHotspotActiveUnderline=2496(,)'
	final @property bool hotspotActiveUnderline() { // GetHotspotActiveUnderline
		return !!sendMessageDirect(2496, 0, 0);
	}

	/// Limit hotspots to single line so hotspots on two lines don't merge.
	/// Generated from: 'set void SetHotspotSingleLine=2421(bool singleLine,)'
	final @property void hotspotSingleLine(bool singleLine) { // SetHotspotSingleLine
		sendMessageDirect(2421, singleLine, 0);
	}

	/// Get the HotspotSingleLine property
	/// Generated from: 'get bool GetHotspotSingleLine=2497(,)'
	final @property bool hotspotSingleLine() { // GetHotspotSingleLine
		return !!sendMessageDirect(2497, 0, 0);
	}

	/// Move caret between paragraphs (delimited by empty lines).
	/// Generated from: 'fun void ParaDown=2413(,)'
	final void paraDown() { // ParaDown
		sendMessageDirect(2413, 0, 0);
	}

	/// Generated from: 'fun void ParaDownExtend=2414(,)'
	final void paraDownExtend() { // ParaDownExtend
		sendMessageDirect(2414, 0, 0);
	}

	/// Generated from: 'fun void ParaUp=2415(,)'
	final void paraUp() { // ParaUp
		sendMessageDirect(2415, 0, 0);
	}

	/// Generated from: 'fun void ParaUpExtend=2416(,)'
	final void paraUpExtend() { // ParaUpExtend
		sendMessageDirect(2416, 0, 0);
	}

	/// Given a valid document position, return the previous position taking code
	/// page into account. Returns 0 if passed 0.
	/// Generated from: 'fun position PositionBefore=2417(position pos,)'
	final Position positionBefore(Position pos) { // PositionBefore
		return sendMessageDirect(2417, pos, 0);
	}

	/// Given a valid document position, return the next position taking code
	/// page into account. Maximum value returned is the last position in the document.
	/// Generated from: 'fun position PositionAfter=2418(position pos,)'
	final Position positionAfter(Position pos) { // PositionAfter
		return sendMessageDirect(2418, pos, 0);
	}

	/// Copy a range of text to the clipboard. Positions are clipped into the document.
	/// Generated from: 'fun void CopyRange=2419(position start, position end)'
	final void copyRange(Position start, Position end) { // CopyRange
		sendMessageDirect(2419, start, end);
	}

	/// Copy argument text to the clipboard.
	/// Generated from: 'fun void CopyText=2420(strlen length, cstring text)'
	final void copyText(in char[] text) { // CopyText
		char[4096] __sibuff = void; char* tmpBuff;
		size_t __len;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff, __len);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2420, __len-1, cast(size_t) __cstr_text);
	}

	/// Set the selection mode to stream (SC_SEL_STREAM) or rectangular (SC_SEL_RECTANGLE/SC_SEL_THIN) or
	/// by lines (SC_SEL_LINES).
	/// Generated from: 'set void SetSelectionMode=2422(int mode,)'
	final @property void selectionMode(int mode) { // SetSelectionMode
		sendMessageDirect(2422, mode, 0);
	}

	/// Get the mode of the current selection.
	/// Generated from: 'get int GetSelectionMode=2423(,)'
	final @property int selectionMode() { // GetSelectionMode
		return sendMessageDirect(2423, 0, 0);
	}

	/// Retrieve the position of the start of the selection at the given line (INVALID_POSITION if no selection on this line).
	/// Generated from: 'get position GetLineSelStartPosition=2424(int line,)'
	final Position getLineSelStartPosition(int line) { // GetLineSelStartPosition
		return sendMessageDirect(2424, line, 0);
	}

	/// Retrieve the position of the end of the selection at the given line (INVALID_POSITION if no selection on this line).
	/// Generated from: 'get position GetLineSelEndPosition=2425(int line,)'
	final Position getLineSelEndPosition(int line) { // GetLineSelEndPosition
		return sendMessageDirect(2425, line, 0);
	}

	/// Move caret down one line, extending rectangular selection to new caret position.
	/// Generated from: 'fun void LineDownRectExtend=2426(,)'
	final void lineDownRectExtend() { // LineDownRectExtend
		sendMessageDirect(2426, 0, 0);
	}

	/// Move caret up one line, extending rectangular selection to new caret position.
	/// Generated from: 'fun void LineUpRectExtend=2427(,)'
	final void lineUpRectExtend() { // LineUpRectExtend
		sendMessageDirect(2427, 0, 0);
	}

	/// Move caret left one character, extending rectangular selection to new caret position.
	/// Generated from: 'fun void CharLeftRectExtend=2428(,)'
	final void charLeftRectExtend() { // CharLeftRectExtend
		sendMessageDirect(2428, 0, 0);
	}

	/// Move caret right one character, extending rectangular selection to new caret position.
	/// Generated from: 'fun void CharRightRectExtend=2429(,)'
	final void charRightRectExtend() { // CharRightRectExtend
		sendMessageDirect(2429, 0, 0);
	}

	/// Move caret to first position on line, extending rectangular selection to new caret position.
	/// Generated from: 'fun void HomeRectExtend=2430(,)'
	final void homeRectExtend() { // HomeRectExtend
		sendMessageDirect(2430, 0, 0);
	}

	/// Move caret to before first visible character on line.
	/// If already there move to first character on line.
	/// In either case, extend rectangular selection to new caret position.
	/// Generated from: 'fun void VCHomeRectExtend=2431(,)'
	final void vcHomeRectExtend() { // VCHomeRectExtend
		sendMessageDirect(2431, 0, 0);
	}

	/// Move caret to last position on line, extending rectangular selection to new caret position.
	/// Generated from: 'fun void LineEndRectExtend=2432(,)'
	final void lineEndRectExtend() { // LineEndRectExtend
		sendMessageDirect(2432, 0, 0);
	}

	/// Move caret one page up, extending rectangular selection to new caret position.
	/// Generated from: 'fun void PageUpRectExtend=2433(,)'
	final void pageUpRectExtend() { // PageUpRectExtend
		sendMessageDirect(2433, 0, 0);
	}

	/// Move caret one page down, extending rectangular selection to new caret position.
	/// Generated from: 'fun void PageDownRectExtend=2434(,)'
	final void pageDownRectExtend() { // PageDownRectExtend
		sendMessageDirect(2434, 0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page.
	/// Generated from: 'fun void StutteredPageUp=2435(,)'
	final void stutteredPageUp() { // StutteredPageUp
		sendMessageDirect(2435, 0, 0);
	}

	/// Move caret to top of page, or one page up if already at top of page, extending selection to new caret position.
	/// Generated from: 'fun void StutteredPageUpExtend=2436(,)'
	final void stutteredPageUpExtend() { // StutteredPageUpExtend
		sendMessageDirect(2436, 0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page.
	/// Generated from: 'fun void StutteredPageDown=2437(,)'
	final void stutteredPageDown() { // StutteredPageDown
		sendMessageDirect(2437, 0, 0);
	}

	/// Move caret to bottom of page, or one page down if already at bottom of page, extending selection to new caret position.
	/// Generated from: 'fun void StutteredPageDownExtend=2438(,)'
	final void stutteredPageDownExtend() { // StutteredPageDownExtend
		sendMessageDirect(2438, 0, 0);
	}

	/// Move caret left one word, position cursor at end of word.
	/// Generated from: 'fun void WordLeftEnd=2439(,)'
	final void wordLeftEnd() { // WordLeftEnd
		sendMessageDirect(2439, 0, 0);
	}

	/// Move caret left one word, position cursor at end of word, extending selection to new caret position.
	/// Generated from: 'fun void WordLeftEndExtend=2440(,)'
	final void wordLeftEndExtend() { // WordLeftEndExtend
		sendMessageDirect(2440, 0, 0);
	}

	/// Move caret right one word, position cursor at end of word.
	/// Generated from: 'fun void WordRightEnd=2441(,)'
	final void wordRightEnd() { // WordRightEnd
		sendMessageDirect(2441, 0, 0);
	}

	/// Move caret right one word, position cursor at end of word, extending selection to new caret position.
	/// Generated from: 'fun void WordRightEndExtend=2442(,)'
	final void wordRightEndExtend() { // WordRightEndExtend
		sendMessageDirect(2442, 0, 0);
	}

	/// Set the set of characters making up whitespace for when moving or selecting by word.
	/// Should be called after SetWordChars.
	/// Generated from: 'set void SetWhitespaceChars=2443(, cstring characters)'
	final @property void whitespaceChars(in char[] characters) { // SetWhitespaceChars
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_characters = toCString(__sibuff, characters, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2443, 0, cast(size_t) __cstr_characters);
	}

	/// Reset the set of characters for whitespace and word characters to the defaults.
	/// Generated from: 'fun void SetCharsDefault=2444(,)'
	final void setCharsDefault() { // SetCharsDefault
		sendMessageDirect(2444, 0, 0);
	}

	/// Get currently selected item position in the auto-completion list
	/// Generated from: 'get int AutoCGetCurrent=2445(,)'
	final @property int autoCGetCurrent() { // AutoCGetCurrent
		return sendMessageDirect(2445, 0, 0);
	}

	/// Get currently selected item text in the auto-completion list
	/// Returns the length of the item text
	/// Generated from: 'get strlen AutoCGetCurrentText=2610(, cstringresult s)'
	final @property char[] autoCGetCurrentText() { // AutoCGetCurrentText
		immutable __len = sendMessageDirect(2610, 0, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] s = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(2610, 0, cast(size_t) s.ptr) + 1;
		assert(s.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return s;
	}

	/// ditto
	final char[] autoCGetCurrentText(char[] sBuff) {
		assert(!sBuff || sBuff.length >= sendMessageDirect(2610, 0, 0) + 1);
		char[] s = sBuff;
		auto __len = sBuff.length;
		__len = sendMessageDirect(2610, 0, cast(size_t) s.ptr) + 1;
		return s[0 .. __len];
	}

	/// Set auto-completion case insensitive behaviour to either prefer case-sensitive matches or have no preference.
	/// Generated from: 'set void AutoCSetCaseInsensitiveBehaviour=2634(int behaviour,)'
	final @property void autoCSetCaseInsensitiveBehaviour(int behaviour) { // AutoCSetCaseInsensitiveBehaviour
		sendMessageDirect(2634, behaviour, 0);
	}

	/// Get auto-completion case insensitive behaviour.
	/// Generated from: 'get int AutoCGetCaseInsensitiveBehaviour=2635(,)'
	final @property int autoCGetCaseInsensitiveBehaviour() { // AutoCGetCaseInsensitiveBehaviour
		return sendMessageDirect(2635, 0, 0);
	}

	/// Enlarge the document to a particular size of text bytes.
	/// Generated from: 'fun void Allocate=2446(int bytes,)'
	final void allocate(int bytes) { // Allocate
		sendMessageDirect(2446, bytes, 0);
	}

	/// Returns the target converted to UTF8.
	/// Return the length in bytes.
	/// Generated from: 'fun len TargetAsUTF8=2447(, charsresult s)'
	final char[] targetAsUtf8() { // TargetAsUTF8
		immutable __len = sendMessageDirect(2447, 0, 0);
		if(!__len) return null;
		char[] s = new char[__len];
		immutable __len2 = sendMessageDirect(2447, 0, cast(size_t) s.ptr);
		assert(__len2 == __len);
		return s;
	}

	/// ditto
	final char[] targetAsUtf8(char[] sBuff) {
		assert(!sBuff || sBuff.length >= sendMessageDirect(2447, 0, 0));
		char[] s = sBuff;
		auto __len = sBuff.length;
		__len = sendMessageDirect(2447, 0, cast(size_t) s.ptr);
		return s[0 .. __len];
	}

	/// Set the length of the utf8 argument for calling EncodedFromUTF8.
	/// Set to -1 and the string will be measured to the first nul.
	/// Generated from: 'set void SetLengthForEncode=2448(int bytes,)'
	final @property void lengthForEncode(int bytes) { // SetLengthForEncode
		sendMessageDirect(2448, bytes, 0);
	}

	/// Translates a UTF8 string into the document encoding.
	/// Return the length of the result in bytes.
	/// On error return 0.
	/// Generated from: 'fun len EncodedFromUTF8=2449(cstring utf8, charsresult encoded)'
	final char[] encodedFromUtf8(in char[] utf8) { // EncodedFromUTF8
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_utf8 = toCString(__sibuff, utf8, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(2449, cast(size_t) __cstr_utf8, 0);
		if(!__len) return null;
		char[] encoded = new char[__len];
		immutable __len2 = sendMessageDirect(2449, cast(size_t) __cstr_utf8, cast(size_t) encoded.ptr);
		assert(__len2 == __len);
		return encoded;
	}

	/// ditto
	final char[] encodedFromUtf8(char[] encodedBuff, in char[] utf8) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_utf8 = toCString(__sibuff, utf8, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		assert(!encodedBuff || encodedBuff.length >= sendMessageDirect(2449, cast(size_t) __cstr_utf8, 0));
		char[] encoded = encodedBuff;
		auto __len = encodedBuff.length;
		__len = sendMessageDirect(2449, cast(size_t) __cstr_utf8, cast(size_t) encoded.ptr);
		return encoded[0 .. __len];
	}

	/// Find the position of a column on a line taking into account tabs and
	/// multi-byte characters. If beyond end of line, return line end position.
	/// Generated from: 'fun int FindColumn=2456(int line, int column)'
	final int findColumn(int line, int column) { // FindColumn
		return sendMessageDirect(2456, line, column);
	}

	/// Can the caret preferred x position only be changed by explicit movement commands?
	/// Generated from: 'get int GetCaretSticky=2457(,)'
	final @property int caretSticky() { // GetCaretSticky
		return sendMessageDirect(2457, 0, 0);
	}

	/// Stop the caret preferred x position changing when the user types.
	/// Generated from: 'set void SetCaretSticky=2458(int useCaretStickyBehaviour,)'
	final @property void caretSticky(int useCaretStickyBehaviour) { // SetCaretSticky
		sendMessageDirect(2458, useCaretStickyBehaviour, 0);
	}

	/// Switch between sticky and non-sticky: meant to be bound to a key.
	/// Generated from: 'fun void ToggleCaretSticky=2459(,)'
	final void toggleCaretSticky() { // ToggleCaretSticky
		sendMessageDirect(2459, 0, 0);
	}

	/// Enable/Disable convert-on-paste for line endings
	/// Generated from: 'set void SetPasteConvertEndings=2467(bool convert,)'
	final @property void pasteConvertEndings(bool convert) { // SetPasteConvertEndings
		sendMessageDirect(2467, convert, 0);
	}

	/// Get convert-on-paste setting
	/// Generated from: 'get bool GetPasteConvertEndings=2468(,)'
	final @property bool pasteConvertEndings() { // GetPasteConvertEndings
		return !!sendMessageDirect(2468, 0, 0);
	}

	/// Duplicate the selection. If selection empty duplicate the line containing the caret.
	/// Generated from: 'fun void SelectionDuplicate=2469(,)'
	final void selectionDuplicate() { // SelectionDuplicate
		sendMessageDirect(2469, 0, 0);
	}

	/// Set background alpha of the caret line.
	/// Generated from: 'set void SetCaretLineBackAlpha=2470(int alpha,)'
	final @property void caretLineBackAlpha(int alpha) { // SetCaretLineBackAlpha
		sendMessageDirect(2470, alpha, 0);
	}

	/// Get the background alpha of the caret line.
	/// Generated from: 'get int GetCaretLineBackAlpha=2471(,)'
	final @property int caretLineBackAlpha() { // GetCaretLineBackAlpha
		return sendMessageDirect(2471, 0, 0);
	}

	/// Set the style of the caret to be drawn.
	/// Generated from: 'set void SetCaretStyle=2512(int caretStyle,)'
	final @property void caretStyle(int caretStyle) { // SetCaretStyle
		sendMessageDirect(2512, caretStyle, 0);
	}

	/// Returns the current style of the caret.
	/// Generated from: 'get int GetCaretStyle=2513(,)'
	final @property int caretStyle() { // GetCaretStyle
		return sendMessageDirect(2513, 0, 0);
	}

	/// Set the indicator used for IndicatorFillRange and IndicatorClearRange
	/// Generated from: 'set void SetIndicatorCurrent=2500(int indicator,)'
	final @property void indicatorCurrent(int indicator) { // SetIndicatorCurrent
		sendMessageDirect(2500, indicator, 0);
	}

	/// Get the current indicator
	/// Generated from: 'get int GetIndicatorCurrent=2501(,)'
	final @property int indicatorCurrent() { // GetIndicatorCurrent
		return sendMessageDirect(2501, 0, 0);
	}

	/// Set the value used for IndicatorFillRange
	/// Generated from: 'set void SetIndicatorValue=2502(int value,)'
	final @property void indicatorValue(int value) { // SetIndicatorValue
		sendMessageDirect(2502, value, 0);
	}

	/// Get the current indicator vaue
	/// Generated from: 'get int GetIndicatorValue=2503(,)'
	final @property int indicatorValue() { // GetIndicatorValue
		return sendMessageDirect(2503, 0, 0);
	}

	/// Turn a indicator on over a range.
	/// Generated from: 'fun void IndicatorFillRange=2504(int position, int fillLength)'
	final void indicatorFillRange(int position, int fillLength) { // IndicatorFillRange
		sendMessageDirect(2504, position, fillLength);
	}

	/// Turn a indicator off over a range.
	/// Generated from: 'fun void IndicatorClearRange=2505(int position, int clearLength)'
	final void indicatorClearRange(int position, int clearLength) { // IndicatorClearRange
		sendMessageDirect(2505, position, clearLength);
	}

	/// Are any indicators present at position?
	/// Generated from: 'fun int IndicatorAllOnFor=2506(int position,)'
	final int indicatorAllOnFor(int position) { // IndicatorAllOnFor
		return sendMessageDirect(2506, position, 0);
	}

	/// What value does a particular indicator have at at a position?
	/// Generated from: 'fun int IndicatorValueAt=2507(int indicator, int position)'
	final int indicatorValueAt(int indicator, int position) { // IndicatorValueAt
		return sendMessageDirect(2507, indicator, position);
	}

	/// Where does a particular indicator start?
	/// Generated from: 'fun int IndicatorStart=2508(int indicator, int position)'
	final int indicatorStart(int indicator, int position) { // IndicatorStart
		return sendMessageDirect(2508, indicator, position);
	}

	/// Where does a particular indicator end?
	/// Generated from: 'fun int IndicatorEnd=2509(int indicator, int position)'
	final int indicatorEnd(int indicator, int position) { // IndicatorEnd
		return sendMessageDirect(2509, indicator, position);
	}

	/// Set number of entries in position cache
	/// Generated from: 'set void SetPositionCache=2514(int size,)'
	final @property void positionCache(int size) { // SetPositionCache
		sendMessageDirect(2514, size, 0);
	}

	/// How many entries are allocated to the position cache?
	/// Generated from: 'get int GetPositionCache=2515(,)'
	final @property int positionCache() { // GetPositionCache
		return sendMessageDirect(2515, 0, 0);
	}

	/// Copy the selection, if selection empty copy the line with the caret
	/// Generated from: 'fun void CopyAllowLine=2519(,)'
	final void copyAllowLine() { // CopyAllowLine
		sendMessageDirect(2519, 0, 0);
	}

	/// Compact the document buffer and return a read-only pointer to the
	/// characters in the document.
	/// Generated from: 'get int GetCharacterPointer=2520(,)'
	final @property int characterPointer() { // GetCharacterPointer
		return sendMessageDirect(2520, 0, 0);
	}

	/// Always interpret keyboard input as Unicode
	/// Generated from: 'set void SetKeysUnicode=2521(bool keysUnicode,)'
	final @property void keysUnicode(bool keysUnicode) { // SetKeysUnicode
		sendMessageDirect(2521, keysUnicode, 0);
	}

	/// Are keys always interpreted as Unicode?
	/// Generated from: 'get bool GetKeysUnicode=2522(,)'
	final @property bool keysUnicode() { // GetKeysUnicode
		return !!sendMessageDirect(2522, 0, 0);
	}

	/// Set the alpha fill colour of the given indicator.
	/// Generated from: 'set void IndicSetAlpha=2523(int indicator, int alpha)'
	final void indicSetAlpha(int indicator, int alpha) { // IndicSetAlpha
		sendMessageDirect(2523, indicator, alpha);
	}

	/// Get the alpha fill colour of the given indicator.
	/// Generated from: 'get int IndicGetAlpha=2524(int indicator,)'
	final int indicGetAlpha(int indicator) { // IndicGetAlpha
		return sendMessageDirect(2524, indicator, 0);
	}

	/// Set the alpha outline colour of the given indicator.
	/// Generated from: 'set void IndicSetOutlineAlpha=2558(int indicator, int alpha)'
	final void indicSetOutlineAlpha(int indicator, int alpha) { // IndicSetOutlineAlpha
		sendMessageDirect(2558, indicator, alpha);
	}

	/// Get the alpha outline colour of the given indicator.
	/// Generated from: 'get int IndicGetOutlineAlpha=2559(int indicator,)'
	final int indicGetOutlineAlpha(int indicator) { // IndicGetOutlineAlpha
		return sendMessageDirect(2559, indicator, 0);
	}

	/// Set extra ascent for each line
	/// Generated from: 'set void SetExtraAscent=2525(int extraAscent,)'
	final @property void extraAscent(int extraAscent) { // SetExtraAscent
		sendMessageDirect(2525, extraAscent, 0);
	}

	/// Get extra ascent for each line
	/// Generated from: 'get int GetExtraAscent=2526(,)'
	final @property int extraAscent() { // GetExtraAscent
		return sendMessageDirect(2526, 0, 0);
	}

	/// Set extra descent for each line
	/// Generated from: 'set void SetExtraDescent=2527(int extraDescent,)'
	final @property void extraDescent(int extraDescent) { // SetExtraDescent
		sendMessageDirect(2527, extraDescent, 0);
	}

	/// Get extra descent for each line
	/// Generated from: 'get int GetExtraDescent=2528(,)'
	final @property int extraDescent() { // GetExtraDescent
		return sendMessageDirect(2528, 0, 0);
	}

	/// Which symbol was defined for markerNumber with MarkerDefine
	/// Generated from: 'fun int MarkerSymbolDefined=2529(int markerNumber,)'
	final int markerSymbolDefined(int markerNumber) { // MarkerSymbolDefined
		return sendMessageDirect(2529, markerNumber, 0);
	}

	/// Set the text in the text margin for a line
	/// Generated from: 'set void MarginSetText=2530(int line, cstring text)'
	final void marginSetText(int line, in char[] text) { // MarginSetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2530, line, cast(size_t) __cstr_text);
	}

	/// Get the text in the text margin for a line
	/// Generated from: 'get len MarginGetText=2531(int line, charsresult text)'
	final char[] marginGetText(int line) { // MarginGetText
		immutable __len = sendMessageDirect(2531, line, 0);
		if(!__len) return null;
		char[] text = new char[__len];
		immutable __len2 = sendMessageDirect(2531, line, cast(size_t) text.ptr);
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] marginGetText(char[] textBuff, int line) {
		assert(!textBuff || textBuff.length >= sendMessageDirect(2531, line, 0));
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(2531, line, cast(size_t) text.ptr);
		return text[0 .. __len];
	}

	/// Set the style number for the text margin for a line
	/// Generated from: 'set void MarginSetStyle=2532(int line, int style)'
	final void marginSetStyle(int line, int style) { // MarginSetStyle
		sendMessageDirect(2532, line, style);
	}

	/// Get the style number for the text margin for a line
	/// Generated from: 'get int MarginGetStyle=2533(int line,)'
	final int marginGetStyle(int line) { // MarginGetStyle
		return sendMessageDirect(2533, line, 0);
	}

	/// Set the style in the text margin for a line
	/// Generated from: 'set void MarginSetStyles=2534(int line, cstring styles)'
	final void marginSetStyles(int line, in char[] styles) { // MarginSetStyles
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_styles = toCString(__sibuff, styles, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2534, line, cast(size_t) __cstr_styles);
	}

	/// Get the styles in the text margin for a line
	/// Generated from: 'get len MarginGetStyles=2535(int line, charsresult styles)'
	final char[] marginGetStyles(int line) { // MarginGetStyles
		immutable __len = sendMessageDirect(2535, line, 0);
		if(!__len) return null;
		char[] styles = new char[__len];
		immutable __len2 = sendMessageDirect(2535, line, cast(size_t) styles.ptr);
		assert(__len2 == __len);
		return styles;
	}

	/// ditto
	final char[] marginGetStyles(char[] stylesBuff, int line) {
		assert(!stylesBuff || stylesBuff.length >= sendMessageDirect(2535, line, 0));
		char[] styles = stylesBuff;
		auto __len = stylesBuff.length;
		__len = sendMessageDirect(2535, line, cast(size_t) styles.ptr);
		return styles[0 .. __len];
	}

	/// Clear the margin text on all lines
	/// Generated from: 'fun void MarginTextClearAll=2536(,)'
	final void marginTextClearAll() { // MarginTextClearAll
		sendMessageDirect(2536, 0, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	/// Generated from: 'set void MarginSetStyleOffset=2537(int style,)'
	final @property void marginSetStyleOffset(int style) { // MarginSetStyleOffset
		sendMessageDirect(2537, style, 0);
	}

	/// Get the start of the range of style numbers used for margin text
	/// Generated from: 'get int MarginGetStyleOffset=2538(,)'
	final @property int marginGetStyleOffset() { // MarginGetStyleOffset
		return sendMessageDirect(2538, 0, 0);
	}

	/// Set the margin options.
	/// Generated from: 'set void SetMarginOptions=2539(int marginOptions,)'
	final @property void marginOptions(int marginOptions) { // SetMarginOptions
		sendMessageDirect(2539, marginOptions, 0);
	}

	/// Get the margin options.
	/// Generated from: 'get int GetMarginOptions=2557(,)'
	final @property int marginOptions() { // GetMarginOptions
		return sendMessageDirect(2557, 0, 0);
	}

	/// Set the annotation text for a line
	/// Generated from: 'set void AnnotationSetText=2540(int line, cstring text)'
	final void annotationSetText(int line, in char[] text) { // AnnotationSetText
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_text = toCString(__sibuff, text, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2540, line, cast(size_t) __cstr_text);
	}

	/// Get the annotation text for a line
	/// Generated from: 'get len AnnotationGetText=2541(int line, charsresult text)'
	final char[] annotationGetText(int line) { // AnnotationGetText
		immutable __len = sendMessageDirect(2541, line, 0);
		if(!__len) return null;
		char[] text = new char[__len];
		immutable __len2 = sendMessageDirect(2541, line, cast(size_t) text.ptr);
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] annotationGetText(char[] textBuff, int line) {
		assert(!textBuff || textBuff.length >= sendMessageDirect(2541, line, 0));
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(2541, line, cast(size_t) text.ptr);
		return text[0 .. __len];
	}

	/// Set the style number for the annotations for a line
	/// Generated from: 'set void AnnotationSetStyle=2542(int line, int style)'
	final void annotationSetStyle(int line, int style) { // AnnotationSetStyle
		sendMessageDirect(2542, line, style);
	}

	/// Get the style number for the annotations for a line
	/// Generated from: 'get int AnnotationGetStyle=2543(int line,)'
	final int annotationGetStyle(int line) { // AnnotationGetStyle
		return sendMessageDirect(2543, line, 0);
	}

	/// Set the annotation styles for a line
	/// Generated from: 'set void AnnotationSetStyles=2544(int line, cstring styles)'
	final void annotationSetStyles(int line, in char[] styles) { // AnnotationSetStyles
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_styles = toCString(__sibuff, styles, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2544, line, cast(size_t) __cstr_styles);
	}

	/// Get the annotation styles for a line
	/// Generated from: 'get len AnnotationGetStyles=2545(int line, charsresult styles)'
	final char[] annotationGetStyles(int line) { // AnnotationGetStyles
		immutable __len = sendMessageDirect(2545, line, 0);
		if(!__len) return null;
		char[] styles = new char[__len];
		immutable __len2 = sendMessageDirect(2545, line, cast(size_t) styles.ptr);
		assert(__len2 == __len);
		return styles;
	}

	/// ditto
	final char[] annotationGetStyles(char[] stylesBuff, int line) {
		assert(!stylesBuff || stylesBuff.length >= sendMessageDirect(2545, line, 0));
		char[] styles = stylesBuff;
		auto __len = stylesBuff.length;
		__len = sendMessageDirect(2545, line, cast(size_t) styles.ptr);
		return styles[0 .. __len];
	}

	/// Get the number of annotation lines for a line
	/// Generated from: 'get int AnnotationGetLines=2546(int line,)'
	final int annotationGetLines(int line) { // AnnotationGetLines
		return sendMessageDirect(2546, line, 0);
	}

	/// Clear the annotations from all lines
	/// Generated from: 'fun void AnnotationClearAll=2547(,)'
	final void annotationClearAll() { // AnnotationClearAll
		sendMessageDirect(2547, 0, 0);
	}

	/// Set the visibility for the annotations for a view
	/// Generated from: 'set void AnnotationSetVisible=2548(int visible,)'
	final @property void annotationSetVisible(int visible) { // AnnotationSetVisible
		sendMessageDirect(2548, visible, 0);
	}

	/// Get the visibility for the annotations for a view
	/// Generated from: 'get int AnnotationGetVisible=2549(,)'
	final @property int annotationGetVisible() { // AnnotationGetVisible
		return sendMessageDirect(2549, 0, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	/// Generated from: 'set void AnnotationSetStyleOffset=2550(int style,)'
	final @property void annotationSetStyleOffset(int style) { // AnnotationSetStyleOffset
		sendMessageDirect(2550, style, 0);
	}

	/// Get the start of the range of style numbers used for annotations
	/// Generated from: 'get int AnnotationGetStyleOffset=2551(,)'
	final @property int annotationGetStyleOffset() { // AnnotationGetStyleOffset
		return sendMessageDirect(2551, 0, 0);
	}

	/// Add a container action to the undo stack
	/// Generated from: 'fun void AddUndoAction=2560(int token, int flags)'
	final void addUndoAction(int token, int flags) { // AddUndoAction
		sendMessageDirect(2560, token, flags);
	}

	/// Find the position of a character from a point within the window.
	/// Generated from: 'fun position CharPositionFromPoint=2561(int x, int y)'
	final Position charPositionFromPoint(int x, int y) { // CharPositionFromPoint
		return sendMessageDirect(2561, x, y);
	}

	/// Find the position of a character from a point within the window.
	/// Return INVALID_POSITION if not close to text.
	/// Generated from: 'fun position CharPositionFromPointClose=2562(int x, int y)'
	final Position charPositionFromPointClose(int x, int y) { // CharPositionFromPointClose
		return sendMessageDirect(2562, x, y);
	}

	/// Set whether multiple selections can be made
	/// Generated from: 'set void SetMultipleSelection=2563(bool multipleSelection,)'
	final @property void multipleSelection(bool multipleSelection) { // SetMultipleSelection
		sendMessageDirect(2563, multipleSelection, 0);
	}

	/// Whether multiple selections can be made
	/// Generated from: 'get bool GetMultipleSelection=2564(,)'
	final @property bool multipleSelection() { // GetMultipleSelection
		return !!sendMessageDirect(2564, 0, 0);
	}

	/// Set whether typing can be performed into multiple selections
	/// Generated from: 'set void SetAdditionalSelectionTyping=2565(bool additionalSelectionTyping,)'
	final @property void additionalSelectionTyping(bool additionalSelectionTyping) { // SetAdditionalSelectionTyping
		sendMessageDirect(2565, additionalSelectionTyping, 0);
	}

	/// Whether typing can be performed into multiple selections
	/// Generated from: 'get bool GetAdditionalSelectionTyping=2566(,)'
	final @property bool additionalSelectionTyping() { // GetAdditionalSelectionTyping
		return !!sendMessageDirect(2566, 0, 0);
	}

	/// Set whether additional carets will blink
	/// Generated from: 'set void SetAdditionalCaretsBlink=2567(bool additionalCaretsBlink,)'
	final @property void additionalCaretsBlink(bool additionalCaretsBlink) { // SetAdditionalCaretsBlink
		sendMessageDirect(2567, additionalCaretsBlink, 0);
	}

	/// Whether additional carets will blink
	/// Generated from: 'get bool GetAdditionalCaretsBlink=2568(,)'
	final @property bool additionalCaretsBlink() { // GetAdditionalCaretsBlink
		return !!sendMessageDirect(2568, 0, 0);
	}

	/// Set whether additional carets are visible
	/// Generated from: 'set void SetAdditionalCaretsVisible=2608(bool additionalCaretsBlink,)'
	final @property void additionalCaretsVisible(bool additionalCaretsBlink) { // SetAdditionalCaretsVisible
		sendMessageDirect(2608, additionalCaretsBlink, 0);
	}

	/// Whether additional carets are visible
	/// Generated from: 'get bool GetAdditionalCaretsVisible=2609(,)'
	final @property bool additionalCaretsVisible() { // GetAdditionalCaretsVisible
		return !!sendMessageDirect(2609, 0, 0);
	}

	/// How many selections are there?
	/// Generated from: 'get int GetSelections=2570(,)'
	final @property int selections() { // GetSelections
		return sendMessageDirect(2570, 0, 0);
	}

	/// Clear selections to a single empty stream selection
	/// Generated from: 'fun void ClearSelections=2571(,)'
	final void clearSelections() { // ClearSelections
		sendMessageDirect(2571, 0, 0);
	}

	/// Set a simple selection
	/// Generated from: 'fun int SetSelection=2572(int caret, int anchor)'
	final int setSelection(int caret, int anchor) { // SetSelection
		return sendMessageDirect(2572, caret, anchor);
	}

	/// Add a selection
	/// Generated from: 'fun int AddSelection=2573(int caret, int anchor)'
	final int addSelection(int caret, int anchor) { // AddSelection
		return sendMessageDirect(2573, caret, anchor);
	}

	/// Set the main selection
	/// Generated from: 'set void SetMainSelection=2574(int selection,)'
	final @property void mainSelection(int selection) { // SetMainSelection
		sendMessageDirect(2574, selection, 0);
	}

	/// Which selection is the main selection
	/// Generated from: 'get int GetMainSelection=2575(,)'
	final @property int mainSelection() { // GetMainSelection
		return sendMessageDirect(2575, 0, 0);
	}

	/// Generated from: 'set void SetSelectionNCaret=2576(int selection, position pos)'
	final void setSelectionNCaret(int selection, Position pos) { // SetSelectionNCaret
		sendMessageDirect(2576, selection, pos);
	}

	/// Generated from: 'get position GetSelectionNCaret=2577(int selection,)'
	final Position getSelectionNCaret(int selection) { // GetSelectionNCaret
		return sendMessageDirect(2577, selection, 0);
	}

	/// Generated from: 'set void SetSelectionNAnchor=2578(int selection, position posAnchor)'
	final void setSelectionNAnchor(int selection, Position posAnchor) { // SetSelectionNAnchor
		sendMessageDirect(2578, selection, posAnchor);
	}

	/// Generated from: 'get position GetSelectionNAnchor=2579(int selection,)'
	final Position getSelectionNAnchor(int selection) { // GetSelectionNAnchor
		return sendMessageDirect(2579, selection, 0);
	}

	/// Generated from: 'set void SetSelectionNCaretVirtualSpace=2580(int selection, int space)'
	final void setSelectionNCaretVirtualSpace(int selection, int space) { // SetSelectionNCaretVirtualSpace
		sendMessageDirect(2580, selection, space);
	}

	/// Generated from: 'get int GetSelectionNCaretVirtualSpace=2581(int selection,)'
	final int getSelectionNCaretVirtualSpace(int selection) { // GetSelectionNCaretVirtualSpace
		return sendMessageDirect(2581, selection, 0);
	}

	/// Generated from: 'set void SetSelectionNAnchorVirtualSpace=2582(int selection, int space)'
	final void setSelectionNAnchorVirtualSpace(int selection, int space) { // SetSelectionNAnchorVirtualSpace
		sendMessageDirect(2582, selection, space);
	}

	/// Generated from: 'get int GetSelectionNAnchorVirtualSpace=2583(int selection,)'
	final int getSelectionNAnchorVirtualSpace(int selection) { // GetSelectionNAnchorVirtualSpace
		return sendMessageDirect(2583, selection, 0);
	}

	/// Sets the position that starts the selection - this becomes the anchor.
	/// Generated from: 'set void SetSelectionNStart=2584(int selection, position pos)'
	final void setSelectionNStart(int selection, Position pos) { // SetSelectionNStart
		sendMessageDirect(2584, selection, pos);
	}

	/// Returns the position at the start of the selection.
	/// Generated from: 'get position GetSelectionNStart=2585(int selection,)'
	final Position getSelectionNStart(int selection) { // GetSelectionNStart
		return sendMessageDirect(2585, selection, 0);
	}

	/// Sets the position that ends the selection - this becomes the currentPosition.
	/// Generated from: 'set void SetSelectionNEnd=2586(int selection, position pos)'
	final void setSelectionNEnd(int selection, Position pos) { // SetSelectionNEnd
		sendMessageDirect(2586, selection, pos);
	}

	/// Returns the position at the end of the selection.
	/// Generated from: 'get position GetSelectionNEnd=2587(int selection,)'
	final Position getSelectionNEnd(int selection) { // GetSelectionNEnd
		return sendMessageDirect(2587, selection, 0);
	}

	/// Generated from: 'set void SetRectangularSelectionCaret=2588(position pos,)'
	final @property void rectangularSelectionCaret(Position pos) { // SetRectangularSelectionCaret
		sendMessageDirect(2588, pos, 0);
	}

	/// Generated from: 'get position GetRectangularSelectionCaret=2589(,)'
	final @property Position rectangularSelectionCaret() { // GetRectangularSelectionCaret
		return sendMessageDirect(2589, 0, 0);
	}

	/// Generated from: 'set void SetRectangularSelectionAnchor=2590(position posAnchor,)'
	final @property void rectangularSelectionAnchor(Position posAnchor) { // SetRectangularSelectionAnchor
		sendMessageDirect(2590, posAnchor, 0);
	}

	/// Generated from: 'get position GetRectangularSelectionAnchor=2591(,)'
	final @property Position rectangularSelectionAnchor() { // GetRectangularSelectionAnchor
		return sendMessageDirect(2591, 0, 0);
	}

	/// Generated from: 'set void SetRectangularSelectionCaretVirtualSpace=2592(int space,)'
	final @property void rectangularSelectionCaretVirtualSpace(int space) { // SetRectangularSelectionCaretVirtualSpace
		sendMessageDirect(2592, space, 0);
	}

	/// Generated from: 'get int GetRectangularSelectionCaretVirtualSpace=2593(,)'
	final @property int rectangularSelectionCaretVirtualSpace() { // GetRectangularSelectionCaretVirtualSpace
		return sendMessageDirect(2593, 0, 0);
	}

	/// Generated from: 'set void SetRectangularSelectionAnchorVirtualSpace=2594(int space,)'
	final @property void rectangularSelectionAnchorVirtualSpace(int space) { // SetRectangularSelectionAnchorVirtualSpace
		sendMessageDirect(2594, space, 0);
	}

	/// Generated from: 'get int GetRectangularSelectionAnchorVirtualSpace=2595(,)'
	final @property int rectangularSelectionAnchorVirtualSpace() { // GetRectangularSelectionAnchorVirtualSpace
		return sendMessageDirect(2595, 0, 0);
	}

	/// Generated from: 'set void SetVirtualSpaceOptions=2596(int virtualSpaceOptions,)'
	final @property void virtualSpaceOptions(int virtualSpaceOptions) { // SetVirtualSpaceOptions
		sendMessageDirect(2596, virtualSpaceOptions, 0);
	}

	/// Generated from: 'get int GetVirtualSpaceOptions=2597(,)'
	final @property int virtualSpaceOptions() { // GetVirtualSpaceOptions
		return sendMessageDirect(2597, 0, 0);
	}

	/// On GTK+, allow selecting the modifier key to use for mouse-based
	/// rectangular selection. Often the window manager requires Alt+Mouse Drag
	/// for moving windows.
	/// Valid values are SCMOD_CTRL(default), SCMOD_ALT, or SCMOD_SUPER.
	/// Generated from: 'set void SetRectangularSelectionModifier=2598(int modifier,)'
	final @property void rectangularSelectionModifier(int modifier) { // SetRectangularSelectionModifier
		sendMessageDirect(2598, modifier, 0);
	}

	/// Get the modifier key used for rectangular selection.
	/// Generated from: 'get int GetRectangularSelectionModifier=2599(,)'
	final @property int rectangularSelectionModifier() { // GetRectangularSelectionModifier
		return sendMessageDirect(2599, 0, 0);
	}

	/// Set the foreground colour of additional selections.
	/// Must have previously called SetSelFore with non-zero first argument for this to have an effect.
	/// Generated from: 'set void SetAdditionalSelFore=2600(colour fore,)'
	final @property void additionalSelFore(Colour fore) { // SetAdditionalSelFore
		sendMessageDirect(2600, fore.rgb, 0);
	}

	/// Set the background colour of additional selections.
	/// Must have previously called SetSelBack with non-zero first argument for this to have an effect.
	/// Generated from: 'set void SetAdditionalSelBack=2601(colour back,)'
	final @property void additionalSelBack(Colour back) { // SetAdditionalSelBack
		sendMessageDirect(2601, back.rgb, 0);
	}

	/// Set the alpha of the selection.
	/// Generated from: 'set void SetAdditionalSelAlpha=2602(int alpha,)'
	final @property void additionalSelAlpha(int alpha) { // SetAdditionalSelAlpha
		sendMessageDirect(2602, alpha, 0);
	}

	/// Get the alpha of the selection.
	/// Generated from: 'get int GetAdditionalSelAlpha=2603(,)'
	final @property int additionalSelAlpha() { // GetAdditionalSelAlpha
		return sendMessageDirect(2603, 0, 0);
	}

	/// Set the foreground colour of additional carets.
	/// Generated from: 'set void SetAdditionalCaretFore=2604(colour fore,)'
	final @property void additionalCaretFore(Colour fore) { // SetAdditionalCaretFore
		sendMessageDirect(2604, fore.rgb, 0);
	}

	/// Get the foreground colour of additional carets.
	/// Generated from: 'get colour GetAdditionalCaretFore=2605(,)'
	final @property Colour additionalCaretFore() { // GetAdditionalCaretFore
		return cast(Colour)sendMessageDirect(2605, 0, 0);
	}

	/// Set the main selection to the next selection.
	/// Generated from: 'fun void RotateSelection=2606(,)'
	final void rotateSelection() { // RotateSelection
		sendMessageDirect(2606, 0, 0);
	}

	/// Swap that caret and anchor of the main selection.
	/// Generated from: 'fun void SwapMainAnchorCaret=2607(,)'
	final void swapMainAnchorCaret() { // SwapMainAnchorCaret
		sendMessageDirect(2607, 0, 0);
	}

	/// Indicate that the internal state of a lexer has changed over a range and therefore
	/// there may be a need to redraw.
	/// Generated from: 'fun int ChangeLexerState=2617(position start, position end)'
	final int changeLexerState(Position start, Position end) { // ChangeLexerState
		return sendMessageDirect(2617, start, end);
	}

	/// Find the next line at or after lineStart that is a contracted fold header line.
	/// Return -1 when no more lines.
	/// Generated from: 'fun int ContractedFoldNext=2618(int lineStart,)'
	final int contractedFoldNext(int lineStart) { // ContractedFoldNext
		return sendMessageDirect(2618, lineStart, 0);
	}

	/// Centre current line in window.
	/// Generated from: 'fun void VerticalCentreCaret=2619(,)'
	final void verticalCentreCaret() { // VerticalCentreCaret
		sendMessageDirect(2619, 0, 0);
	}

	/// Move the selected lines up one line, shifting the line above after the selection
	/// Generated from: 'fun void MoveSelectedLinesUp=2620(,)'
	final void moveSelectedLinesUp() { // MoveSelectedLinesUp
		sendMessageDirect(2620, 0, 0);
	}

	/// Move the selected lines down one line, shifting the line below before the selection
	/// Generated from: 'fun void MoveSelectedLinesDown=2621(,)'
	final void moveSelectedLinesDown() { // MoveSelectedLinesDown
		sendMessageDirect(2621, 0, 0);
	}

	/// Set the identifier reported as idFrom in notification messages.
	/// Generated from: 'set void SetIdentifier=2622(int identifier,)'
	final @property void identifier(int identifier) { // SetIdentifier
		sendMessageDirect(2622, identifier, 0);
	}

	/// Get the identifier.
	/// Generated from: 'get int GetIdentifier=2623(,)'
	final @property int identifier() { // GetIdentifier
		return sendMessageDirect(2623, 0, 0);
	}

	/// Set the width for future RGBA image data.
	/// Generated from: 'set void RGBAImageSetWidth=2624(int width,)'
	final @property void rgbaImageSetWidth(int width) { // RGBAImageSetWidth
		sendMessageDirect(2624, width, 0);
	}

	/// Set the height for future RGBA image data.
	/// Generated from: 'set void RGBAImageSetHeight=2625(int height,)'
	final @property void rgbaImageSetHeight(int height) { // RGBAImageSetHeight
		sendMessageDirect(2625, height, 0);
	}

	/// Define a marker from RGBA data.
	/// It has the width and height from RGBAImageSetWidth/Height
	/// Generated from: 'fun void MarkerDefineRGBAImage=2626(int markerNumber, cstring pixels)'
	final void markerDefineRgbaImage(int markerNumber, in char[] pixels) { // MarkerDefineRGBAImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixels = toCString(__sibuff, pixels, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2626, markerNumber, cast(size_t) __cstr_pixels);
	}

	/// Register an RGBA image for use in autocompletion lists.
	/// It has the width and height from RGBAImageSetWidth/Height
	/// Generated from: 'fun void RegisterRGBAImage=2627(int type, cstring pixels)'
	final void registerRgbaImage(int type, in char[] pixels) { // RegisterRGBAImage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_pixels = toCString(__sibuff, pixels, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(2627, type, cast(size_t) __cstr_pixels);
	}

	/// Scroll to start of document.
	/// Generated from: 'fun void ScrollToStart=2628(,)'
	final void scrollToStart() { // ScrollToStart
		sendMessageDirect(2628, 0, 0);
	}

	/// Scroll to end of document.
	/// Generated from: 'fun void ScrollToEnd=2629(,)'
	final void scrollToEnd() { // ScrollToEnd
		sendMessageDirect(2629, 0, 0);
	}

	/// Set the technolgy used.
	/// Generated from: 'set void SetTechnology=2630(int technology,)'
	final @property void technology(int technology) { // SetTechnology
		sendMessageDirect(2630, technology, 0);
	}

	/// Get the tech.
	/// Generated from: 'get int GetTechnology=2631(,)'
	final @property int technology() { // GetTechnology
		return sendMessageDirect(2631, 0, 0);
	}

	/// Create an ILoader*.
	/// Generated from: 'fun int CreateLoader=2632(int bytes,)'
	final int createLoader(int bytes) { // CreateLoader
		return sendMessageDirect(2632, bytes, 0);
	}

	/// On OS X, show a find indicator.
	/// Generated from: 'fun void FindIndicatorShow=2640(position start, position end)'
	final void findIndicatorShow(Position start, Position end) { // FindIndicatorShow
		sendMessageDirect(2640, start, end);
	}

	/// On OS X, flash a find indicator, then fade out.
	/// Generated from: 'fun void FindIndicatorFlash=2641(position start, position end)'
	final void findIndicatorFlash(Position start, Position end) { // FindIndicatorFlash
		sendMessageDirect(2641, start, end);
	}

	/// On OS X, hide the find indicator.
	/// Generated from: 'fun void FindIndicatorHide=2642(,)'
	final void findIndicatorHide() { // FindIndicatorHide
		sendMessageDirect(2642, 0, 0);
	}

	/// Start notifying the container of all key presses and commands.
	/// Generated from: 'fun void StartRecord=3001(,)'
	final void startRecord() { // StartRecord
		sendMessageDirect(3001, 0, 0);
	}

	/// Stop notifying the container of all key presses and commands.
	/// Generated from: 'fun void StopRecord=3002(,)'
	final void stopRecord() { // StopRecord
		sendMessageDirect(3002, 0, 0);
	}

	/// Set the lexing language of the document.
	/// Generated from: 'set void SetLexer=4001(int lexer,)'
	final @property void lexer(int lexer) { // SetLexer
		sendMessageDirect(4001, lexer, 0);
	}

	/// Retrieve the lexing language of the document.
	/// Generated from: 'get int GetLexer=4002(,)'
	final @property int lexer() { // GetLexer
		return sendMessageDirect(4002, 0, 0);
	}

	/// Colourise a segment of the document using the current lexing language.
	/// Generated from: 'fun void Colourise=4003(position start, position end)'
	final void colourise(Position start, Position end) { // Colourise
		sendMessageDirect(4003, start, end);
	}

	/// Set up a value that may be used by a lexer for some optional feature.
	/// Generated from: 'set void SetProperty=4004(cstring key, cstring value)'
	final void setProperty(in char[] key, in char[] value) { // SetProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key, __cstr_value;
		toCStrings(__sibuff, key, value, __cstr_key, __cstr_value, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4004, cast(size_t) __cstr_key, cast(size_t) __cstr_value);
	}

	/// Set up the key words used by the lexer.
	/// Generated from: 'set void SetKeyWords=4005(int keywordSet, cstring keyWords)'
	final void setKeyWords(int keywordSet, in char[] keyWords) { // SetKeyWords
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_keyWords = toCString(__sibuff, keyWords, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4005, keywordSet, cast(size_t) __cstr_keyWords);
	}

	/// Set the lexing language of the document based on string name.
	/// Generated from: 'set void SetLexerLanguage=4006(, cstring language)'
	final @property void lexerLanguage(in char[] language) { // SetLexerLanguage
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_language = toCString(__sibuff, language, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4006, 0, cast(size_t) __cstr_language);
	}

	/// Load a lexer library (dll / so).
	/// Generated from: 'fun void LoadLexerLibrary=4007(, cstring path)'
	final void loadLexerLibrary(in char[] path) { // LoadLexerLibrary
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_path = toCString(__sibuff, path, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		sendMessageDirect(4007, 0, cast(size_t) __cstr_path);
	}

	/// Retrieve a "property" value previously set with SetProperty.
	/// Generated from: 'get strlen GetProperty=4008(cstring key, cstringresult buf)'
	final char[] getProperty(in char[] key) { // GetProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4008, cast(size_t) __cstr_key, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] buf = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4008, cast(size_t) __cstr_key, cast(size_t) buf.ptr) + 1;
		assert(buf.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return buf;
	}

	/// ditto
	final char[] getProperty(char[] bufBuff, in char[] key) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		assert(!bufBuff || bufBuff.length >= sendMessageDirect(4008, cast(size_t) __cstr_key, 0) + 1);
		char[] buf = bufBuff;
		auto __len = bufBuff.length;
		__len = sendMessageDirect(4008, cast(size_t) __cstr_key, cast(size_t) buf.ptr) + 1;
		return buf[0 .. __len];
	}

	/// Retrieve a "property" value previously set with SetProperty,
	/// with "$()" variable replacement on returned buffer.
	/// Generated from: 'get strlen GetPropertyExpanded=4009(cstring key, cstringresult buf)'
	final char[] getPropertyExpanded(in char[] key) { // GetPropertyExpanded
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4009, cast(size_t) __cstr_key, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] buf = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4009, cast(size_t) __cstr_key, cast(size_t) buf.ptr) + 1;
		assert(buf.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return buf;
	}

	/// ditto
	final char[] getPropertyExpanded(char[] bufBuff, in char[] key) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		assert(!bufBuff || bufBuff.length >= sendMessageDirect(4009, cast(size_t) __cstr_key, 0) + 1);
		char[] buf = bufBuff;
		auto __len = bufBuff.length;
		__len = sendMessageDirect(4009, cast(size_t) __cstr_key, cast(size_t) buf.ptr) + 1;
		return buf[0 .. __len];
	}

	/// Retrieve a "property" value previously set with SetProperty,
	/// interpreted as an int AFTER any "$()" variable replacement.
	/// Generated from: 'get int GetPropertyInt=4010(cstring key,)'
	final int getPropertyInt(in char[] key) { // GetPropertyInt
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_key = toCString(__sibuff, key, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(4010, cast(size_t) __cstr_key, 0);
	}

	/// Retrieve the number of bits the current lexer needs for styling.
	/// Generated from: 'get int GetStyleBitsNeeded=4011(,)'
	final @property int styleBitsNeeded() { // GetStyleBitsNeeded
		return sendMessageDirect(4011, 0, 0);
	}

	/// Retrieve the name of the lexer.
	/// Return the length of the text.
	/// Generated from: 'get strlen GetLexerLanguage=4012(, cstringresult text)'
	final @property char[] lexerLanguage() { // GetLexerLanguage
		immutable __len = sendMessageDirect(4012, 0, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] text = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4012, 0, cast(size_t) text.ptr) + 1;
		assert(text.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return text;
	}

	/// ditto
	final char[] getLexerLanguage(char[] textBuff) {
		assert(!textBuff || textBuff.length >= sendMessageDirect(4012, 0, 0) + 1);
		char[] text = textBuff;
		auto __len = textBuff.length;
		__len = sendMessageDirect(4012, 0, cast(size_t) text.ptr) + 1;
		return text[0 .. __len];
	}

	/// For private communication between an application and a known lexer.
	/// Generated from: 'fun int PrivateLexerCall=4013(int operation, int pointer)'
	final int privateLexerCall(int operation, int pointer) { // PrivateLexerCall
		return sendMessageDirect(4013, operation, pointer);
	}

	/// Retrieve a '\n' separated list of properties understood by the current lexer.
	/// Generated from: 'fun strlen PropertyNames=4014(, cstringresult names)'
	final char[] propertyNames() { // PropertyNames
		immutable __len = sendMessageDirect(4014, 0, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] names = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4014, 0, cast(size_t) names.ptr) + 1;
		assert(names.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return names;
	}

	/// ditto
	final char[] propertyNames(char[] namesBuff) {
		assert(!namesBuff || namesBuff.length >= sendMessageDirect(4014, 0, 0) + 1);
		char[] names = namesBuff;
		auto __len = namesBuff.length;
		__len = sendMessageDirect(4014, 0, cast(size_t) names.ptr) + 1;
		return names[0 .. __len];
	}

	/// Retrieve the type of a property.
	/// Generated from: 'fun int PropertyType=4015(cstring name,)'
	final int propertyType(in char[] name) { // PropertyType
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		return sendMessageDirect(4015, cast(size_t) __cstr_name, 0);
	}

	/// Describe a property.
	/// Generated from: 'fun strlen DescribeProperty=4016(cstring name, cstringresult description)'
	final char[] describeProperty(in char[] name) { // DescribeProperty
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		immutable __len = sendMessageDirect(4016, cast(size_t) __cstr_name, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] description = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4016, cast(size_t) __cstr_name, cast(size_t) description.ptr) + 1;
		assert(description.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return description;
	}

	/// ditto
	final char[] describeProperty(char[] descriptionBuff, in char[] name) {
		char[4096] __sibuff = void; char* tmpBuff;
		const(char)* __cstr_name = toCString(__sibuff, name, tmpBuff);
		scope(exit) freeTmpBuff(tmpBuff);
		assert(!descriptionBuff || descriptionBuff.length >= sendMessageDirect(4016, cast(size_t) __cstr_name, 0) + 1);
		char[] description = descriptionBuff;
		auto __len = descriptionBuff.length;
		__len = sendMessageDirect(4016, cast(size_t) __cstr_name, cast(size_t) description.ptr) + 1;
		return description[0 .. __len];
	}

	/// Retrieve a '\n' separated list of descriptions of the keyword sets understood by the current lexer.
	/// Generated from: 'fun strlen DescribeKeyWordSets=4017(, cstringresult descriptions)'
	final char[] describeKeyWordSets() { // DescribeKeyWordSets
		immutable __len = sendMessageDirect(4017, 0, 0) + 1;
		assert(__len > 0);
		if(__len == 1) return null;
		char[] descriptions = (new char[__len])[0 .. $-1];
		immutable __len2 = sendMessageDirect(4017, 0, cast(size_t) descriptions.ptr) + 1;
		assert(descriptions.ptr[__len-1] == '\0');
		assert(__len2 == __len);
		return descriptions;
	}

	/// ditto
	final char[] describeKeyWordSets(char[] descriptionsBuff) {
		assert(!descriptionsBuff || descriptionsBuff.length >= sendMessageDirect(4017, 0, 0) + 1);
		char[] descriptions = descriptionsBuff;
		auto __len = descriptionsBuff.length;
		__len = sendMessageDirect(4017, 0, cast(size_t) descriptions.ptr) + 1;
		return descriptions[0 .. __len];
	}
}
