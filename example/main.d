﻿module hello;

import dgui.all;

import scintilla.enums;
import scintilla.iscintilla;
import scintilla.dgui.control;
import std.stdio;

class MainForm: Form
{
	private ScintillaControl sci;

	public this()
	{
		this.text = "DGui Form";
		this.size = Size(500, 400);
		this.startPosition = FormStartPosition.CENTER_SCREEN; // Set Form Position

		auto ldef = new LanguageDefinition();
		ldef.language = ScintillaLanguage.d;
		dchar dc = '\U000E0000';

		with(sci = new ScintillaControl("SciLexer.dll")) {
			font = new Font("Courier New", 10);
			dock = DockStyle.FILL;
		//	languageDefinition = ldef;
			parent = this;
			sci.handleCreated.attach(function (s, e) {
				auto isci = cast(IScintilla)s;

				writeln(isci.indent);
				writeln(isci.tabWidth);
				isci.tabWidth = 4;
				writeln(isci.tabWidth);

				writeln("codePage: ", isci.codePage);
				isci.insertText(0, "я!");
				isci.codePage = scCpUtf8;
				writeln("codePage: ", isci.codePage);

			});
			sci.sciCharAdded.attach(function (s, e) {
				char[5] buff;
				auto isci = cast(IScintilla)s;
				writeln(isci.getText(buff));
				writeln(isci.text());
				writeln(cast(int)e.ch, "-", e.ch);
				if(e.ch == '.')
					isci.userListShow(0, "одын два тры! <я>");
			});
		}
	}
}

int main(string[] args)
{
	return Application.run(new MainForm()); // Start the application
}
