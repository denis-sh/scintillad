﻿module wrapper;

import std.algorithm;
import std.array;
import std.ascii: isDigit;
import std.conv;
import std.exception: enforce;
import std.file;
import std.functional: not;
import std.range;
import std.regex;
import std.string: xformat;
import std.stdio;
import std.uni;

enum Types {
	void_         ,
	int_          ,
	bool_         ,
	position      ,
	colour        ,
	chars         ,
	charsresult   ,
	cstring       ,
	cstringresult ,
	cells         ,
	textrange     ,
	findtext      ,
	keymod        ,
	formatrange   ,
}

auto keywords =
"abstract alias align asm assert auto body bool break byte case cast catch cdouble cent cfloat char class const continue creal dchar debug default delegate delete deprecated do double else enum export extern false final finally float for foreach foreach_reverse function goto idouble if ifloat immutable import in inout int interface invariant ireal is lazy long macro mixin module new nothrow null out override package pragma private protected public pure real ref return scope shared short static struct super switch synchronized template this throw true try typedef typeid typeof ubyte ucent uint ulong union unittest ushort version void volatile wchar while with __FILE__ __LINE__ __gshared __thread __traits".split();

struct Type {
	string origin;
	// Derived
	string derived, derivedUBuff;
	Param* lengthOf, thisLength;
	bool
		isDerivedVoid,
		isLength, isArrayLength, isCStringLength, isStrlen,
		isInBuff, isInArray, isInCString,
		isOutBuff, isOutArray, isOutCString,
		inParams;

	this(in string t, in bool isReturn = false) {
		origin = t;
		inParams = true;
		switch(origin) {
			case "void" :
				enforce(isReturn);
				isDerivedVoid = true;
			case "int"  :
			case "bool" : derived = origin; break;
			case "position"      : derived = "Position"         ; break;
			case "colour"        : derived = "Colour"           ; break;

			case "len"           :
				isArrayLength = true;
			case "strlen"        : isStrlen = true;
			case "bytelen"       : derived = isReturn ? "void" : null;
				if(isReturn) isDerivedVoid = true;
				isLength = true;
				isCStringLength = !isArrayLength;
				inParams = false;
				break;

			default:
				enforce(!isReturn);
				switch(origin) {
					case "chars"         : derived = "in char[]"        ; isInArray = isInBuff = true; break;
					case "charsresult"   : derived = "out char[]"       ; isOutArray = isOutBuff = true; break;
					case "cstring"       : derived = "in char[]"        ; isInCString = isInBuff = true; break;
					case "cstringresult" : derived = "out char[]"       ; isOutCString = isOutBuff = true; break;

					case "cells"         : derived = "in Cell[]"        ; break;
					case "textrange"     : derived = "ref TextRange"    ; break;
					case "findtext"      : derived = "ref TextToFind"   ; break;
					case "keymod"        : derived = "in KeyMod"        ; break;
					case "formatrange"   : derived = "ref RangeToFormat"; break;

					default:
						enforce(0, xformat("Unexpected type: '%s'", origin));
						assert(0);
				}
		}
		if(inParams && !derivedUBuff)
			derivedUBuff = derived;
	}
}

struct Param {
	Type type;
	string name; // TODO dName
	// Derived
	//bool opCast(T: bool)() { return !!name; }
}

struct Function {
	string[] comment;
	string originLine;
	enum Kind { regular, getter, setter }
	Kind kind;
	Type returnType;
	string name, number;
	Param[2] originParams;
	// Derived
	bool isProperty;
	string dReturnType, dBuffReturnType, dName, dBuffName;
	Param*[] dParams;
	Param*[] inArrays, inCStrings;
	Param* outArray, outCString, outBuffer;
	bool returnOutParam;
	bool returnsStrlenOnNullOutBuff, returnsStrlenOnNotNullOutBuff;
}

struct Event {
	string originLine;
	string returnType, name, number;
	Param[] originParams;
	// Derived
	string dReturnType, dClassName, dFuncName;
	Param*[] dParams;
	Param*[] inArrays, inCStrings;
	Param* outArray, outCString, outBuffer;
}

struct Value {
	string name, number;
}

// Derived
struct OwnedValue {
	string name, value, prefix;
	Value* entity;
}

struct Enum {
	string name;
	string[] prefixes;
	// Derived
	string commonPrefix;
	OwnedValue[] values;
}

struct Lexer {
	string name, lexerVal;
	string[] prefixes;
	// Derived
	string commonPrefix;
	string dLexerVal;
	OwnedValue[] values;
}

struct Category {
	string name;
	Function[] functions;
	Event[] events;
	Enum[] enums;
	Value[] values;
	Lexer[] lexers;
	// Derived
	string dName;
	OwnedValue[] unownedValues;
}

Category[] categories;

enum ifaceFile = "../Scintilla.iface-improvements/Scintilla.iface";

void read() {
	Regex!char[string] regexes;
	Category* category;
	string[] comment;
	size_t errors = 0, lineNumber = 0;
	foreach(line; File(ifaceFile).byLine()) {
		try {
			++lineNumber;
			if(line.startsWith("#!") || // shebang
			   line.startsWith("##") || // pure comment
			   line.empty)
				continue;

			const(char)[] originLine = line, feature;
			{
				auto r = line.findSplit(" ");
				enforce(!r[1].empty, xformat("No spaces in line '%s'", line));
				line = r[2];
				feature = r[0];
			}
			auto oneFullMatch(in char[] str, string type, string regexStr) {
				if(regexStr !in regexes)
					regexes[regexStr] = regex(regexStr);
				auto match = match(str, regexes[regexStr]);
				enforce(!match.empty && match.pre.empty && match.post.empty, xformat("Malformed %s '%s'", type, str));
				return match.captures;
			}
			auto getParam(in char[] str, string type) {
				if(str.empty)
					return Param.init;
				auto p1 = oneFullMatch(str, type, `^(\w+)\s(\w+)$`);
				return Param(Type(p1[1].idup), p1[2].idup);
			}

			// NOTE: We will aussume that every function and event has a number
			// and params doesn't have values.
			enum typeNameVal = `(\w+)\s(\w+)=(\d+)`, prefixes = `(\w+(?:\s\w+)*)`;
			switch(feature) {
				case "#":
					comment ~= line.idup;
					break;
				case "cat":
					categories ~= Category(line.idup);
					category = &categories[$-1];
					break;
				case "fun":
				case "get":
				case "set":
					// <returnType><ws><name>[=<number](<param>,<param>)
					auto captures = oneFullMatch(line, "function", typeNameVal~`\((.*),(?:|\s(.*))\)`);
					assert(captures.length == 6);

					Function f;
					f.originLine = originLine.idup;
					with(Function.Kind)
						f.kind = feature == "get" ? getter : feature == "set" ? setter : regular;
					f.comment = comment;
					f.returnType = Type(captures[1].idup, true);
					f.name = captures[2].idup;
					f.number = captures[3].idup;
					f.originParams[0] = getParam(captures[4], "function param1");
					f.originParams[1] = getParam(captures[5], "function param2");
					category.functions ~= f;
					break;
				case "evt":
					// <returnType><ws><name>[=<number]([<param>[,<param>]*])
					auto captures = oneFullMatch(line, "event", typeNameVal~`\((.*)\)`);
					assert(captures.length == 5);

					Event e;
					e.originLine = originLine.idup;
					e.returnType = captures[1].idup;
					e.name = captures[2].idup;
					e.number = captures[3].idup;
					if(captures[4] != "void") {
						// dmd @@@BUG7965@@@ workaround
					//	e.originParams = array(map!(a => getParam(a, "event param"))(captures[4].splitter(", ")));
						auto pred(in char[] a) { return getParam(a, "event param"); }
						static typeof(&pred) spred;
						spred = &pred;
						e.originParams = array(map!spred(captures[4].splitter(", ")));
					}
					category.events ~= e;
					break;
				case "val":
					// <name>=<number>
					auto captures = oneFullMatch(line, "value", `(\w+)=(.+)`);
					assert(captures.length == 3);

					Value v;
					v.name = captures[1].idup;
					v.number = captures[2].idup;
					category.values ~= v;
					break;
				case "enu":
					// <enumeration>=<prefix>[<ws><prefix>]*
					auto captures = oneFullMatch(line, "enum", `(\w+)=`~prefixes);
					assert(captures.length == 3);

					Enum e;
					e.name = captures[1].idup;
					e.prefixes = captures[2].idup.split();
				//	if(e.prefixes.length != 1 || e.prefixes[0][$-1]!='_')
				//		writeln(originLine);
					category.enums ~= e;
					break;
				case "lex":
					// <name>=<lexerVal><ws><prefix>[<ws><prefix>]*
					auto captures = oneFullMatch(line, "lexer", `(\w+)=(\w+)\s`~prefixes);
					assert(captures.length == 4);

					Lexer l;
					l.name = captures[1].idup;
					l.lexerVal = captures[2].idup;
					l.prefixes = captures[3].idup.split();
					category.lexers ~= l;
					break;
				default:
					enforce(0, xformat("Unexpected first word '%s'", feature));
			}
			if(feature != "#")
				comment = null;
		} catch(Exception e) {
			++errors;
			writefln("%s(%s): %s", ifaceFile, lineNumber, e.msg);
		}
	}
	enforce(!errors, xformat("%s error[s] while reading", errors));
}

void process() {
	size_t warns = 0;
	void warn(T...)(string fmt, T args) {
		writefln("WARNING #%s: " ~ fmt, ++warns, args);
	}
	// ABCdeF -> AbCdeF
	static string camelcase(string s) {
		// TODO: patterns from external file?
		if(s.endsWith("EOLs"))
			s = s[0 .. $ - 4] ~ "Eols";

		string res;
		bool wasUpper = false;
		foreach(i, dchar c; s) {
			if(!isUpper(c)) {
				res ~= c;
				wasUpper = false;
			} else {
				if(!wasUpper) {
					res ~= c;
					wasUpper = true;
				} else {
					auto t = s[i .. $].drop(1);
					if(t.empty || isDigit(t.front) || isUpper(t.front))
						res ~= toLower(c);
					else
						res ~= c;
				}
			}
		}
		return res;
	}
	// AbCdeF -> abCdeF
	static string firstLetterToLower(string s) {
		enforce(isUpper(s.front) && !s.canFind!`a=='_'`());
		auto res = to!string(toLower(s.front)) ~ camelcase(s).drop(1);
		return keywords.canFind(res) ? res ~ '_' : res;
	}

	// AB_CDE_F -> abCdeF
	static string uppercasedToCamelcased(string s) {
		enforce(!s.canFind!isLower());
		string res;
		bool uppercase = false;
		foreach(i, dchar dc; s)
			if(dc == '_')
				uppercase = !!i;
			else if(uppercase)
				res ~= dc, uppercase = false;
			else
				res ~= toLower(dc);

		if(isDigit(res[0]))
			res = '_' ~ res;
		if(s[$-1] == '_')
			res ~= keywords.canFind(res) ? "__" : "_";
		else if(keywords.canFind(res))
			res ~= '_';
		return res;
	}

	foreach(ref category; categories) {
		category.dName = firstLetterToLower(category.name);

		void processCallable(T)(ref T t) {
			Param*[] originParamsRefs = t.originParams[].map!((ref Param a) => &a)().array();
			t.inArrays   = originParamsRefs.filter!`a.type.isInArray`  ().array();
			t.inCStrings = originParamsRefs.filter!`a.type.isInCString`().array();
			enforce(t.inArrays  .length <= 2, "More that two in chars params aren't supported yet");
			enforce(t.inCStrings.length <= 2, "More that two in cstring params aren't supported yet");

			auto outBuffs = originParamsRefs.filter!`a.type.isOutBuff`();
			if(!outBuffs.empty) {
				enforce(outBuffs.walkLength == 1);
				t.outBuffer = outBuffs.front;
				if(t.outBuffer.type.isOutArray)
					t.outArray = t.outBuffer;
				else
					t.outCString = t.outBuffer;
			}

			auto lengths = originParamsRefs.filter!`a.type.isLength`();
			if(!lengths.empty) {
				enforce(lengths.walkLength == 1);
				auto len = lengths.front;
				if(t.outBuffer) {
					len.type.lengthOf = t.outBuffer;
					t.outBuffer.type.thisLength = len;
					enforce(len.type.isArrayLength == len.type.lengthOf.type.isOutArray);
				} else {
					enforce(t.inArrays.empty || t.inCStrings.empty);
					auto inBuffs = !t.inArrays.empty ? t.inArrays : t.inCStrings;
					enforce(inBuffs.length == 1);
					len.type.lengthOf = inBuffs[0];
					inBuffs[0].type.thisLength = len;
					enforce(len.type.isArrayLength == len.type.lengthOf.type.isInArray);
				}
			}

			t.dParams = originParamsRefs.filter!`a.name && a.type.inParams`().array();
		}

		Param p; bool b = /*!!p && */[p].empty; // dmd @@@BUG7966@@@ workaround

	//	bool[string] subStructs;
		foreach(ref f; category.functions) {
			processCallable(f);

		//	if(f.outCString) writeln(f.originLine);
		/*	if(f.dParams.length == 2) {
				if(f.dParams[0].type == "int")
					subStructs[f.dParams[0].name] = true;
				else if(f.dParams[0].name != "useSetting" && f.dParams[0].type.origin != "position")
					{}//writeln(f.originLine);
			}*/

			if(f.outBuffer) {
				f.dBuffName = firstLetterToLower(f.name);

				if(f.returnType.isDerivedVoid) {
					f.returnOutParam = true;
					f.dParams = f.dParams.filter!`!a.type.isOutBuff`().array();
				} else
					enforce(f.returnType.origin == "int"); // TODO: just for now

				if(f.outArray)
					enforce(!f.originParams[].canFind!`a.type.isCStringLength`() && !f.returnType.isCStringLength);
				else { // f.outCString
					enforce(!f.originParams[].canFind!`a.type.isArrayLength`() && !f.returnType.isArrayLength);
/*
	In:
		cstring param [with strlen]
		two cstring params
		chars param with len [returning len]
	Out:
		charsresult param returning len
		cstringresult param returning strlen or bytelen or int (buggy)
*/

					if(f.returnType.isStrlen)
						f.returnsStrlenOnNullOutBuff = f.returnsStrlenOnNotNullOutBuff = true;
					else {
						if(f.name == "GetCurLine") {
							// returns: outBuff is null ? bytelen : int
						} else if(f.name == "GetSelText") {
							// returns: bytelen with bugs
						} else if(f.name == "GetText") {
							// returns: outBuff is null ? bytelen : strlen
							f.returnsStrlenOnNotNullOutBuff = true;
						} else
							enforce(0, "This 'strange' function isn't supported yet: " ~ f.name);
					}
				}
			} else if(!f.inArrays.empty || !f.inCStrings.empty) {
			} else
				enforce(!f.returnType.isLength);

			f.dName = f.name;
			with(Function.Kind) {
				f.isProperty = false;
				if(f.kind == getter && f.dParams.empty)
					f.dName.skipOver("Get"), f.isProperty = true;
				else if(f.kind == setter && f.dParams.length == 1)
					f.dName.skipOver("Set"), f.isProperty = true;

				if(f.kind == regular) {
					if(f.dName.startsWith("Get") || f.dName.startsWith("Set"))
						{}//warn("Looks like a property: %s", f.originLine);
				} else if(!f.isProperty)
					{}//warn("Not a property: %s", f.originLine);
			}
			f.dName = firstLetterToLower(f.dName);
		}
	//	writeln("subStructs: ", subStructs.keys);
		foreach(ref e; category.events) {
			processCallable(e);

			enforce(e.returnType == "void");
			e.dClassName = e.name;
			e.dFuncName = firstLetterToLower(e.name);
		}

		foreach(ref e; category.enums) {
			e.commonPrefix = e.prefixes.reduce!commonPrefix();
			enforce(!e.commonPrefix.empty);
		}
		foreach(ref l; category.lexers) {
			l.commonPrefix = l.prefixes.reduce!commonPrefix();
			enforce(!l.commonPrefix.empty);
		}

		foreach(ref v; category.values) {
			bool owns(T)(ref T t) {
				foreach(prefix; t.prefixes) if(v.name.startsWith(prefix)) {
					assert(v.name.startsWith(t.commonPrefix));
					auto s = v.name[t.commonPrefix.length .. $];
					t.values ~= OwnedValue(uppercasedToCamelcased(s), v.number, prefix, &v);
					return true;
				}
				return false;
			}

			bool inEnum = false;
			foreach(ref e; category.enums) if(owns!Enum(e)) {
				enforce(!inEnum);
				inEnum = true;

				foreach(ref l; category.lexers) if(l.lexerVal == v.name)
					l.dLexerVal = xformat("%s.%s", e.name, e.values[$-1].name);
			}

			Lexer* firstLexerOwner = null;
			foreach(ref l; category.lexers) if(owns!Lexer(l)) {
				if(firstLexerOwner) {
					auto ownedVal = &l.values[$-1];
					ownedVal.value = xformat("%s.%s", firstLexerOwner.name, ownedVal.name);
				} else
					firstLexerOwner = &l;
			}

			enforce(!(inEnum && firstLexerOwner));

			if(!inEnum && !firstLexerOwner)
				category.unownedValues ~= OwnedValue(uppercasedToCamelcased(v.name), v.number, null, &v);
		}
	}
}


void write() {
	enforce(categories.length == 2 && categories[1].name == "Deprecated");
	auto category = categories[0];

	File openFile(string name, bool addModule = true) {
		auto fw = File(xformat("../scintilla/%s.d", name.replace(".", "/").replace("/inc", ".inc")), "wb");
		fw.writefln("// Generated from %s, %s category", ifaceFile, category.name);
		if(addModule)
			fw.writefln("module scintilla.%s;\n", name);
		return fw;
	}

	auto fw = openFile("enums");

	if(!category.unownedValues.empty) {
		fw.writeln("// Global manifest constants");
		fw.writeln("enum {");
		foreach(v; category.unownedValues) {
			fw.writefln("  %s = %s, // %s", v.name, v.entity.number, v.entity.name);
		}
		fw.writeln("}");
	}

	void writeEnum(T)(T t, string firstElement = null) {
		enforce(!t.values.empty, xformat("Empty enum '%s'", t.name));

		if(t.prefixes.length == 1) {
			assert(t.prefixes[0] == t.commonPrefix);
			fw.writefln("\n// Prefix: %s", t.commonPrefix);
		} else {
			fw.writefln("\n// Prefixes: %s", t.prefixes.joiner(" "));
			fw.writefln("// Common prefix: %s", t.commonPrefix);
		}
		fw.writefln("enum %s {", t.name);
		if(firstElement) fw.writefln("  %s", firstElement);
		foreach(v; t.values) {
			assert(v.entity.name.startsWith(v.prefix));
			fw.writefln("  %s = %s, //%s:%s", v.name, v.entity.number, v.prefix, v.entity.name[v.prefix.length .. $]);
		}
		fw.writeln("}");
	}

	enforce(category.enums[$-1].name == "Lexer");

	foreach(ref e; category.enums[0 .. $-1])
		writeEnum(e);


	fw = openFile("lexer");

	writeEnum(category.enums[$-1]);
	foreach(ref l; category.lexers) {
		writeEnum(l, l.dLexerVal ? xformat("lexerVal = %s, // %s", l.dLexerVal, l.lexerVal) :
			xformat("// lexerVal = ???, // %s", l.lexerVal));
	}


	fw = openFile("iscintilla");
	fw.writeln("import scintilla.types;");
	fw.writeln("import scintilla.enums;");
	fw.writeln("import scintilla.internal.functions;");

	fw.writeln("\ninterface IScintilla {");
	fw.writeln("\tsptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam);");

	foreach(f; category.functions) {
		fw.writefln("%s", f.comment.map!`"\n\t/// "~a`.joiner());
		fw.writefln("\t/// Generated from: '%s'", f.originLine);

		immutable derivedReturnType = f.returnOutParam ? f.outBuffer.type.derived.drop(4) : f.returnType.derived;
		fw.writefln("\tfinal %s%s %s(%s) {", f.isProperty ? "@property " : "",
			derivedReturnType, f.dName,
			f.dParams.map!`a.type.derived~' '~a.name`().joiner(", "));

		// Lib size tests
	//	fw.writeln("\t\tassert(0);");
	//	fw.writefln("\t\treturn emptyFuncWrapper!(typeof(return), sendMessageDirect)(%s, %s);", f.number, f.dParams[].map!`a.name`.joiner(", "));
	//	fw.writefln("\t\treturn emptyFuncWrapperEvery!(typeof(return), sendMessageDirect, %s)(%s);", f.number, f.dParams[].map!`a.name`.joiner(", "));
	//	fw.writeln("\t}");
	//	continue;

		static string paramToString(Param p, bool userBuffer = false) {
			if(!p.name) return "0";
			final switch(p.type.origin) {
				case "int"           :
				case "bool"          :
				case "position"      :
				case "keymod"        : return p.name;
				case "colour"        : return p.name ~ ".rgb";

				case "chars"          : return "cast(size_t) " ~ p.name ~ ".ptr";
				case "cstring"        : return "cast(size_t) __cstr_" ~ p.name;
				case "charsresult"    :
				case "cstringresult"  : return "cast(size_t) " ~ (userBuffer ? "__ubuff" : p.name) ~ ".ptr";
				case "strlen"   : return userBuffer ? "__ubuff.length-1" : "__len-1";
				case "bytelen"  : return userBuffer ? "__ubuff.length" : "__len";
				case "len"      : return (userBuffer ? "__ubuff" : p.type.lengthOf.name) ~ ".length";

				case "cells"         : return xformat("cast(size_t) %s.ptr", p.name);
				case "textrange"     :
				case "findtext"      :
				case "formatrange"   : return "cast(size_t) &" ~ p.name;
			}
		}

		immutable
			gettingLengthDCall = f.outBuffer ? xformat("sendMessageDirect(%s, %s)%s", f.number,
				f.originParams[].map!(a => a.type.isOutBuff || a.type.isLength ? "0" : paramToString(a))().joiner(", "),
				f.returnsStrlenOnNullOutBuff ? " + 1" : "") : null,
			normalDCall = xformat("sendMessageDirect(%s, %s)%s", f.number,
				f.originParams[].map!paramToString().joiner(", "),
				f.returnsStrlenOnNotNullOutBuff ? " + 1" : "");

		void writeInStringPreprocessing() {
			if(f.inCStrings.empty)
				return;
			fw.writeln("\t\tchar[4096] __sibuff = void; char* tmpBuff;");
			if(f.inCStrings.length == 1) {
				auto p = f.inCStrings[0];
				if(p.type.thisLength) fw.writeln("\t\tsize_t __len;");
				fw.writefln("\t\tconst(char)* __cstr_%s = toCString(__sibuff, %1$s, tmpBuff%2$s);",
					p.name, p.type.thisLength ? ", __len" : "");
			} else {
				assert(f.inCStrings.length == 2);
				auto cstrNames = f.inCStrings.map!`"__cstr_" ~ a.name`().joiner(", ");
				fw.writefln("\t\tconst(char)* %s;", cstrNames);
				fw.writefln("\t\ttoCStrings(__sibuff, %s, %s, tmpBuff);",
					f.inCStrings.map!`a.name`().joiner(", "), cstrNames);
			}
			fw.writeln("\t\tscope(exit) freeTmpBuff(tmpBuff);");
		}
		writeInStringPreprocessing();

		void writeReturn(in char[] res) {
			fw.write("\t\t");
			if(!f.returnType.isDerivedVoid) {
				fw.write("return ");
				switch(f.returnType.origin) {
					case "bool": fw.write("!!"); break;
					case "int":
					case "position": break;
					case "colour": fw.write("cast(Colour)"); break;
					default: enforce(0, xformat("Unexpected return type: '%s'", f.returnType));
				}
			}
			fw.writeln(res ~ ";");
		}

		if(f.outBuffer) {
			// __len is a buffer length including \0 if any
			if(f.outArray) {
				fw.writefln("\t\timmutable __len = %s;", gettingLengthDCall);
				enforce(f.returnType.isDerivedVoid);
				fw.writefln("\t\tif(!__len) return%s;", f.returnOutParam ? " null" : "");
				fw.writefln("\t\t%s%s = new char[__len];", f.returnOutParam ? "char[] " : "", f.outArray.name);
				fw.writefln("\t\timmutable __len2 = %s;", normalDCall);
			} else {
				// Some functions returns strlen() instead of required buffer length
				//f.returnType.isLength ? f.returnType.isStrlen :;
				immutable returnStrlenNull = true;
				immutable returnStrlenNotNull = true;
				fw.writefln("\t\timmutable __len = %s;", gettingLengthDCall);
				fw.writeln("\t\tassert(__len > 0);");
				if(f.returnType.isDerivedVoid) {
					fw.writefln("\t\tif(__len == 1) return%s;", f.returnOutParam ? " null" : "");
					fw.writefln("\t\t%s%s = (new char[__len])[0 .. $-1];", f.returnOutParam ? "char[] " : "", f.outCString.name);
					fw.writefln("\t\timmutable __len2 = %s;", normalDCall);
				} else { // GetCurLine
					fw.writefln("\t\tchar[1] __sobuff;");
					fw.writefln("\t\t%s%s = __len == 1 ? __sobuff : (new char[__len])[0 .. $-1];", f.returnOutParam ? "char[] " : "", f.outCString.name);
					fw.writefln("\t\timmutable __res = %s;", normalDCall);
				}

				fw.writefln("\t\tassert(%s.ptr[__len-1] == '\\0');", f.outCString.name);
				if(f.returnType.isDerivedVoid) {
				} else { // GetCurLine
					fw.writefln("\t\tif(__len == 1) %s = null;", f.outCString.name);
				}/*
				if(f.dReturnType == "string")
					fw.writeln("\t\treturn toDString(__len, __optr);");
				else if(f.haveOutStringParams) {
					fw.writefln("\t\t%s = toDString(__len, __optr);", outStringName);
					if(f.dReturnType != "void") {
						writeReturn();
						fw.writeln("__res;");
					}
				}*/
			}
			if(f.returnType.isLength)
				fw.writeln("\t\tassert(__len2 == __len);");

			if(f.returnOutParam)
				fw.writefln("\t\treturn %s;", f.outBuffer.name);
			else if(!f.returnType.isDerivedVoid)
				fw.writeln("\t\treturn __res;");
		} else
			writeReturn(normalDCall);

		fw.writeln("\t}");

		if(f.outBuffer) {
			fw.writeln("\n\t/// ditto");

			fw.writefln("\tfinal %s %s(char[] %sBuff%s) {", derivedReturnType, f.dBuffName, f.outBuffer.name,
				f.dParams.map!`", "~a.type.derived~' '~a.name`().joiner());

			writeInStringPreprocessing();

			if(!f.outBuffer.type.thisLength) {
				// No length argument, should check buffer length
				fw.writefln("\t\tassert(!%sBuff || %1$sBuff.length >= %2$s);", f.outBuffer.name, gettingLengthDCall);
			}

			fw.writefln("\t\t%s%s = %2$sBuff;", f.returnOutParam ? "char[] " : "", f.outBuffer.name);
			if(f.returnType.isLength) {
				fw.writefln("\t\tauto __len = %sBuff.length;", f.outBuffer.name);
				fw.writefln("\t\t__len = %s;", normalDCall);
				fw.writefln("\t\t%s %s[0 .. __len];",
					f.returnOutParam ? "return" : f.outBuffer.name ~ " =", f.outBuffer.name);
			} else if(!f.outBuffer.type.thisLength) {
				fw.writefln("\t\timmutable __len = %s;", gettingLengthDCall);
				fw.writefln("\t\t%s = %1$s[0 .. __len];", f.outBuffer.name);
				fw.writefln("\t\treturn %s;", normalDCall);
			} else { // For GetCurLine
				fw.writefln("\t\timmutable __len = min(%sBuff.length, %s);", f.outBuffer.name, gettingLengthDCall);
				fw.writefln("\t\t%s = %1$s[0 .. __len];", f.outBuffer.name);
				fw.writefln("\t\treturn %s;", normalDCall);
			}

			if(f.outArray) {
			} else {
				/*
				// Some functions returns strlen() instead of required buffer length
				//f.returnType.isLength ? f.returnType.isStrlen :;
				immutable returnStrlenNull = true;
				immutable returnStrlenNotNull = true;
				fw.writefln("\t\timmutable __len = %ssendMessageDirect(%s, %s);",
					returnStrlenNull ? "1 + " : "", f.number,
					f.originParams[].map!(a => paramToString(a, true))().joiner(", "));
				fw.writeln("\t\tassert(__len > 0);");
				if(f.returnType.isDerivedVoid) {
					fw.writefln("\t\tif(__len == 1) return%s;", f.returnOutParam ? " null" : "");
					fw.writefln("\t\t%s%s = (new char[__len])[0 .. $-1];", f.returnOutParam ? "char[] " : "", f.outCString.name);
					fw.writef("\t\timmutable __len2 = %s", returnStrlenNotNull ? "1 + " : "");
				} else { // GetCurLine
					fw.writefln("\t\tchar[1] __sobuff;");
					fw.writefln("\t\t%s%s = __len == 1 ? __sobuff : (new char[__len])[0 .. $-1];", f.returnOutParam ? "char[] " : "", f.outCString.name);
					fw.write("\t\timmutable __res = ");
				}*/
			}


			/*if(f.returnType == "len") {
				fw.writefln("\t\treturn toDStringBuff(sendMessageDirect(%s, %s), %sBuff, %3$s);", f.number,
					f.originParams[].map!(a => paramToString(a, false, true)).joiner(", "), f.outBuffer.name);
			} else {
				fw.writeln("\t\tif(__ubuff) {");
				fw.writefln("\t\t\timmutable __len = sendMessageDirect(%s, %s);", f.number,
					f.originParams[].map!(a => paramToString(a, true))().joiner(", "));
				fw.writefln("\t\t\timmutable __res = sendMessageDirect(%s, %s);", f.number,
					f.originParams[].map!(a => paramToString(a, false, true)).joiner(", "));
				fw.writefln("\t\t\t%s = toDStringBuff(__len, __ubuff);", outStringName);
				fw.writeln("\t\t\treturn __res;");
				fw.writeln("\t\t} else");
					fw.writefln("\t\t\treturn sendMessageDirect(%s, %s);", f.number,
						f.originParams[].map!(a => paramToString(a, true))().joiner(", "));
			}*/
			fw.writeln("\t}");
		}
	}
	fw.writeln("}");


	fw = openFile("dgui.eventargs");
	fw.writeln("import dgui.core.events.eventargs;");
	fw.writeln("import scintilla.enums;");

	static string costumType(string name) {
		switch(name) {
			case "ch"              :  return "dchar";
			case "modifiers"       :  return "KeyMod";
			case "modificationType":  return "ModificationFlags";
			case "updated"         :  return "Update";
			default:
				return null;
		}
	}

	foreach(e; category.events) {
		fw.writefln("\nclass Sci%sEventArgs: EventArgs {", e.dClassName);
		foreach(p; e.dParams) if(!p.type.isLength) {
			auto type = p.type.isInBuff ? "const(char)[]" : p.type.derived;
			if(auto cType = costumType(p.name))
				type = cType;
			fw.writefln("\n\tprivate %s %s_;", type, p.name);
			fw.writefln("\t@property %s %s() const { return %2$s_; }", type, p.name);
			fw.writefln("\t@property %s %s(%1$s value) { return %2$s_ = value; }", type, p.name);
		}
		fw.writeln("}");
	}

	fw = openFile("dgui.eventcases.inc", false);
	foreach(e; category.events) {
		fw.writefln("\ncase %s: // %s", e.number, e.name);
		fw.writefln("\tscope e = new Sci%sEventArgs();", e.dClassName);
		foreach(p; e.dParams) if(!p.type.isLength) {
			if(p.type.isInArray)
				fw.writefln("\te.%s = notification.%1$s[0 .. notification.length];", p.name);
			else if(p.type.isInCString)
				fw.writefln("\te.%s = fromCString(notification.%1$s);", p.name);
			else if(auto type = costumType(p.name))
				fw.writefln("\te.%s = cast(%s)notification.%1$s;", p.name, type);
			else
				fw.writefln("\te.%s = notification.%1$s;", p.name);
		}
		fw.writefln("\tthis.onSci%s(e);", e.dClassName);
		fw.writeln("\tbreak;");
	}

	fw = openFile("dgui.eventmembers.inc", false);
	foreach(e; category.events)
		fw.writefln("public Event!(ScintillaControl, Sci%sEventArgs) sci%1$s;", e.dClassName);
	fw.writeln();
	foreach(e; category.events)
		fw.writefln("protected void onSci%s(Sci%1$sEventArgs e) { this.sci%1$s(this, e); }", e.dClassName);
}

void main() {
	read();
	process();
	write();
}
