﻿module main;

import std.algorithm;
import std.array;
import std.conv;
import std.exception: enforce;
import std.file;
import std.functional: not;
import std.range: walkLength;
import std.regex;
import std.string: format;
import std.stdio;
import std.uni;

struct Param {
	string type, name;
}

struct Function {
	enum Kind { regular, getter, setter }
	Kind kind;
	string returnType, name;
	uint number;
	Param param1, param2;
}

struct Event {
	string returnType, name;
	uint number;
	Param[] params;
}

struct Value {
	string name;
}

struct Category {
	string name;
	Function[] functions;
	Value[] values;
	Event[] events;
}

Category[] categories;

string firstLetterToLower(const(char)[] str) {
	enforce(isUpper(str.front));
	string res = to!string(toLower(str.front));
	str.popFront();
	res ~= str;
	return res;
}

int main() {
	enum fileName = "Scintilla.iface";
	// NOTE: We will aussume that every function and event has a number
	// and params doesn't have values.
	enum typeName = `(\w+)\s(\w+)`,
		typeNameVal = typeName~`=(\d+)`,
		prefixes = `(\w+)(?:\s(\w+))*`;

	auto paramRegex = regex(`^(\w+)\s(\w+)$`);

	auto functionRegex = regex(typeNameVal~`\((.*),(?:|\s(.*))\)`);

	// <returnType><ws><name>[=<number]([<param>[,<param>]*])
	auto eventRegex = regex(typeNameVal~`\((?:void|`~typeName~`(?:,\s`~typeName~`)*)\)`);

	// <enumeration>=<prefix>[<ws><prefix>]*
	auto enumRegex = regex(`(\w+)=`~prefixes);

	// <name>=<lexerVal><ws><prefix>[<ws><prefix>]*
	auto lexerRegex = regex(`(\w+)=(\w+)`~prefixes);

	// <name>=<number>
	auto valueRegex = regex(`(\w+)=(-{0,1}\w+)`);

	Category* category;
	string comment; // TODO
	size_t errors = 0, lineNumber = 0;
	foreach(line; File(fileName).byLine()) {
		try {
			++lineNumber;
			if(line.startsWith("#!") || // shebang
			   line.startsWith("##") || // pure comment
			   line.empty)
				continue;

			char[] nextToken() {
				line = line.find!(not!isWhite);
				static bool isIdOrNumChar(dchar c) {
					return c == '_' || isAlpha(c) || isNumber(c);
				}
				auto tmp = line;
				immutable isIdOrNum = line[0] == '-' || isIdOrNumChar(line.front);
				line.popFront();
				if(isIdOrNum)
					line = line.find!(not!isIdOrNumChar);
				return tmp[0 .. $ - line.length];
			}

			bool skipToken(string token) {
			}

			const(char)[] feature;
			{
				auto r = line.findSplit(" ");
				enforce(!r[1].empty, format("No spaces in line '%s'", line));
				line = r[2];
				feature = r[0];
			}
			auto oneFullMatch(in char[] str, string type, Regex!char r) {
				auto match = match(str, r);
				enforce(!match.empty && match.pre.empty && match.post.empty, format("Malformed %s '%s'", type, str));
				return match.captures;
			}
			Param getParam(in char[] str, string type) {
				auto p1 = oneFullMatch(str, type, paramRegex);
				return Param(p1[1].idup, p1[2].idup);
			}
			switch(feature) {
				case "#":
					comment = line.idup;
					break;
				case "cat":
					categories ~= Category(line.idup);
					category = &categories[$-1];
					break;
				case "fun":
				case "get":
				case "set":
					// <returnType><ws><name>[=<number](<param>,<param>)
					Function f;
					f.kind = r[0] == "get" ? Function.Kind.getter :
						r[0] == "set" ? Function.Kind.setter :
						Function.Kind.regular;
					f.returnType = nextToken();
					{
						auto tmp = nextToken();
						if(f.kind != Function.Kind.regular)
							tmp.skipOver(f.kind == Function.Kind.getter ? "Get" : "Set");
						else if(tmp.startsWith("Get") || tmp.startsWith("Set"))
							writeln(tmp);
						f.name ~= tmp.firstLetterToLower();
					}
					if(skipOver("="))
						f.number = to!uint(nextToken());
					enforce(skipOver("("));
					if(!captures[4].empty)
						f.param1 = getParam(captures[4], "function param1");
					if(!captures[5].empty)
						f.param2 = getParam(captures[5], "function param2");
					break;
				case "val":
					auto captures = oneFullMatch(line, "value", valueRegex);
					break;
				case "evt":
					auto captures = oneFullMatch(line, "event", eventRegex);
					assert(captures.length == 8);

					Event e;
					e.returnType = captures[1].idup;
					e.name ~= captures[2].firstLetterToLower();
					e.number = to!uint(captures[3]);/*
					if(!captures[4].empty)
						f.param1 = getParam(captures[4], "function param1");
					if(!captures[5].empty)
						f.param2 = getParam(captures[5], "function param2");*/
					break;
				case "enu":
					auto captures = oneFullMatch(line, "enum", enumRegex);
					break;
				case "lex":
					auto captures = oneFullMatch(line, "lexer", lexerRegex);
					break;
				default:
					enforce(0, format("Unexpected first word '%s'", feature));
			}
		} catch(Exception e) {
			++errors;
			writefln("%s(%s): %s", fileName, lineNumber, e.msg);
		}
	}
	return !!errors;
}
