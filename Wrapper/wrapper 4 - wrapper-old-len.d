﻿module wrapper;

import std.algorithm;
import std.array;
import std.ascii: isDigit;
import std.conv;
import std.exception: enforce;
import std.file;
import std.functional: not;
import std.range;
import std.regex;
import std.string: format;
import std.stdio;
import std.uni;

enum Types {
	void_         ,
	int_          ,
	bool_         ,
	position      ,
	colour        ,
	string        ,
	stringresult  ,
	cells         ,
	textrange     ,
	findtext      ,
	keymod        ,
	formatrange   ,
}

auto keywords =
"abstract alias align asm assert auto body bool break byte case cast catch cdouble cent cfloat char class const continue creal dchar debug default delegate delete deprecated do double else enum export extern false final finally float for foreach foreach_reverse function goto idouble if ifloat immutable import in inout int interface invariant ireal is lazy long macro mixin module new nothrow null out override package pragma private protected public pure real ref return scope shared short static struct super switch synchronized template this throw true try typedef typeid typeof ubyte ucent uint ulong union unittest ushort version void volatile wchar while with __FILE__ __LINE__ __gshared __thread __traits".split();

struct Param {
	string type, name;
	// Derived
	string dType;
	bool isInString, isOutString;

	bool opCast(T: bool)() { return !!type; }
}

struct Function {
	string[] comment;
	string originLine;
	enum Kind { regular, getter, setter }
	Kind kind;
	string returnType, name, number;
	Param[2] params;
	// Derived
	bool isProperty;
	string dReturnType, dName, dNameBuff;
	Param[] dParams, dParamsBuff;
	size_t inStringParams;
	bool haveOutStringParams;
}

struct Event {
	string originLine;
	string returnType, name, number;
	Param[] params;
	// Derived
	string dReturnType, dClassName, dFuncName;
	Param[] dParams;
	size_t inStringParams;
	bool haveOutStringParams;
}

struct Value {
	string name, number;
}

// Derived
struct OwnedValue {
	string name, value, prefix;
	Value* entity;
}

struct Enum {
	string name;
	string[] prefixes;
	// Derived
	string commonPrefix;
	OwnedValue[] values;
}

struct Lexer {
	string name, lexerVal;
	string[] prefixes;
	// Derived
	string commonPrefix;
	string dLexerVal;
	OwnedValue[] values;
}

struct Category {
	string name;
	Function[] functions;
	Event[] events;
	Enum[] enums;
	Value[] values;
	Lexer[] lexers;
	// Derived
	string dName;
	OwnedValue[] unownedValues;
}

Category[] categories;

enum ifaceFile = "../Scintilla.iface improvements/Scintilla.iface";

void read() {
	Regex!char[string] regexes;
	Category* category;
	string[] comment;
	size_t errors = 0, lineNumber = 0;
	foreach(line; File(ifaceFile).byLine()) {
		try {
			++lineNumber;
			if(line.startsWith("#!") || // shebang
			   line.startsWith("##") || // pure comment
			   line.empty)
				continue;

			const(char)[] originLine = line, feature;
			{
				auto r = line.findSplit(" ");
				enforce(!r[1].empty, format("No spaces in line '%s'", line));
				line = r[2];
				feature = r[0];
			}
			auto oneFullMatch(in char[] str, string type, string regexStr) {
				if(regexStr !in regexes)
					regexes[regexStr] = regex(regexStr);
				auto match = match(str, regexes[regexStr]);
				enforce(!match.empty && match.pre.empty && match.post.empty, format("Malformed %s '%s'", type, str));
				return match.captures;
			}
			auto getParam(in char[] str, string type) {
				if(str.empty)
					return Param.init;
				auto p1 = oneFullMatch(str, type, `^(\w+)\s(\w+)$`);
				return Param(p1[1].idup, p1[2].idup);
			}

			// NOTE: We will aussume that every function and event has a number
			// and params doesn't have values.
			enum typeNameVal = `(\w+)\s(\w+)=(\d+)`, prefixes = `(\w+(?:\s\w+)*)`;
			switch(feature) {
				case "#":
					comment ~= line.idup;
					break;
				case "cat":
					categories ~= Category(line.idup);
					category = &categories[$-1];
					break;
				case "fun":
				case "get":
				case "set":
					// <returnType><ws><name>[=<number](<param>,<param>)
					auto captures = oneFullMatch(line, "function", typeNameVal~`\((.*),(?:|\s(.*))\)`);
					assert(captures.length == 6);

					Function f;
					f.originLine = originLine.idup;
					with(Function.Kind)
						f.kind = feature == "get" ? getter : feature == "set" ? setter : regular;
					f.comment = comment;
					f.returnType = captures[1].idup;
					f.name = captures[2].idup;
					f.number = captures[3].idup;
					f.params[0] = getParam(captures[4], "function param1");
					f.params[1] = getParam(captures[5], "function param2");
					category.functions ~= f;
					break;
				case "evt":
					// <returnType><ws><name>[=<number]([<param>[,<param>]*])
					auto captures = oneFullMatch(line, "event", typeNameVal~`\((.*)\)`);
					assert(captures.length == 5);

					Event e;
					e.originLine = originLine.idup;
					e.returnType = captures[1].idup;
					e.name = captures[2].idup;
					e.number = captures[3].idup;
					if(captures[4] != "void") {
						// dmd @@@BUG7965@@@ workaround
					//	e.params = array(map!(a => getParam(a, "event param"))(captures[4].splitter(", ")));
						auto pred(in char[] a) { return getParam(a, "event param"); }
						static typeof(&pred) spred;
						spred = &pred;
						e.params = array(map!spred(captures[4].splitter(", ")));
					}
					category.events ~= e;
					break;
				case "val":
					// <name>=<number>
					auto captures = oneFullMatch(line, "value", `(\w+)=(.+)`);
					assert(captures.length == 3);

					Value v;
					v.name = captures[1].idup;
					v.number = captures[2].idup;
					category.values ~= v;
					break;
				case "enu":
					// <enumeration>=<prefix>[<ws><prefix>]*
					auto captures = oneFullMatch(line, "enum", `(\w+)=`~prefixes);
					assert(captures.length == 3);

					Enum e;
					e.name = captures[1].idup;
					e.prefixes = captures[2].idup.split();
					if(e.prefixes.length != 1 || e.prefixes[0][$-1]!='_')
						writeln(originLine);
					category.enums ~= e;
					break;
				case "lex":
					// <name>=<lexerVal><ws><prefix>[<ws><prefix>]*
					auto captures = oneFullMatch(line, "lexer", `(\w+)=(\w+)\s`~prefixes);
					assert(captures.length == 4);

					Lexer l;
					l.name = captures[1].idup;
					l.lexerVal = captures[2].idup;
					l.prefixes = captures[3].idup.split();
					category.lexers ~= l;
					break;
				default:
					enforce(0, format("Unexpected first word '%s'", feature));
			}
			if(feature != "#")
				comment = null;
		} catch(Exception e) {
			++errors;
			writefln("%s(%s): %s", ifaceFile, lineNumber, e.msg);
		}
	}
	enforce(!errors, format("%s error[s] while reading", errors));
}

void process() {
	size_t warns = 0;
	void warn(T...)(string fmt, T args) {
		writefln("WARNING #%s: " ~ fmt, ++warns, args);
	}
	// ABCdeF -> AbCdeF
	static string camelcase(string s) {
		// TODO: patterns from external file?
		if(s.endsWith("EOLs"))
			s = s[0 .. $ - 4] ~ "Eols";

		string res;
		bool wasUpper = false;
		foreach(i, dchar c; s) {
			if(!isUpper(c)) {
				res ~= c;
				wasUpper = false;
			} else {
				if(!wasUpper) {
					res ~= c;
					wasUpper = true;
				} else {
					auto t = s[i .. $].drop(1);
					if(t.empty || isDigit(t.front) || isUpper(t.front))
						res ~= toLower(c);
					else
						res ~= c;
				}
			}
		}
		return res;
	}
	// AbCdeF -> abCdeF
	static string firstLetterToLower(string s) {
		enforce(isUpper(s.front) && !s.canFind!`a=='_'`());
		auto res = to!string(toLower(s.front)) ~ camelcase(s).drop(1);
		return keywords.canFind(res) ? res ~ '_' : res;
	}

	// AB_CDE_F -> abCdeF
	static string uppercasedToCamelcased(string s) {
		enforce(!s.canFind!isLower());
		string res;
		bool uppercase = false;
		foreach(i, dchar dc; s)
			if(dc == '_')
				uppercase = !!i;
			else if(uppercase)
				res ~= dc, uppercase = false;
			else
				res ~= toLower(dc);

		if(isDigit(res[0]))
			res = '_' ~ res;
		if(s[$-1] == '_')
			res ~= keywords.canFind(res) ? "__" : "_";
		else if(keywords.canFind(res))
			res ~= '_';
		return res;
	}

	foreach(ref category; categories) {
		category.dName = firstLetterToLower(category.name);

		string typeString(string type) {
			switch(type) {
				case "void":
				case "int":
				case "bool":    return type;
				case "len":     return null;
				case "position"      : return "Position"         ;
				case "colour"        : return "Colour"           ;
				case "string"        : return "in char[]"        ;
				case "stringresult"  : return "out string"       ;
				case "cells"         : return "in Cell[]"        ;
				case "textrange"     : return "ref TextRange"    ;
				case "findtext"      : return "ref TextToFind"   ;
				case "keymod"        : return "in KeyMod"        ;
				case "formatrange"   : return "ref RangeToFormat";
				default:
					enforce(0, format("Unexpected type: '%s'", type));
					assert(0);
			}
		}

		void processCallable(T)(ref T t) {
			t.dReturnType = t.returnType == "len" ? "void" : typeString(t.returnType);
			t.inStringParams = 0;
			t.haveOutStringParams = false;
			foreach(ref p; t.params) if(p) {
				enforce(p.type != "void");
				p.dType = typeString(p.type);
				p.isInString = p.type == "string";
				p.isOutString = p.type == "stringresult";

				t.inStringParams += p.isInString;
				if(p.isOutString) {
					enforce(!t.haveOutStringParams);
					t.haveOutStringParams = true;
				}
			}
			t.dParams = t.params[].filter!`!!a && a.type != "len"`().array();
			enforce(t.inStringParams <= 2, "More that two in string params aren't supported yet");
		}

		Param p; bool b = !!p && [p].empty; // dmd @@@BUG7966@@@ workaround

		bool[string] subStructs;
		foreach(ref f; category.functions) {
			processCallable(f);

			if(f.dParams.length == 2) {
				if(f.dParams[0].type == "int")
					subStructs[f.dParams[0].name] = true;
				else if(f.dParams[0].name != "useSetting" && f.dParams[0].type != "position")
					{}//writeln(f.originLine);
			}

			if(f.haveOutStringParams) {
				f.dNameBuff = firstLetterToLower(f.name);
				f.dParamsBuff = f.dParams.map!`a.isOutString ? a.dType = "out char[]", a : a`().array();
				if(f.dReturnType == "void") {
					f.dReturnType = "string";
					f.dParams = f.dParams.filter!`!a.isOutString`().array();
				} else
					enforce(f.returnType == "int");
			} else
				enforce(f.returnType != "len");

			f.dName = f.name;
			with(Function.Kind) {
				f.isProperty = false;
				if(f.kind == getter && f.dParams.empty)
					f.dName.skipOver("Get"), f.isProperty = true;
				else if(f.kind == setter && f.dParams.length == 1)
					f.dName.skipOver("Set"), f.isProperty = true;

				if(f.kind == regular) {
					if(f.dName.startsWith("Get") || f.dName.startsWith("Set"))
						warn("Looks like a property: %s", f.originLine);
				} else if(!f.isProperty)
					{}//warn("Not a property: %s", f.originLine);
			}
			f.dName = firstLetterToLower(f.dName);
		}
		writeln("subStructs: ", subStructs.keys);
		foreach(ref e; category.events) {
			processCallable(e);

			enforce(e.returnType == "void");
			e.dClassName = e.name;
			e.dFuncName = firstLetterToLower(e.name);
		}

		foreach(ref e; category.enums) {
			e.commonPrefix = e.prefixes.reduce!commonPrefix();
			enforce(!e.commonPrefix.empty);
		}
		foreach(ref l; category.lexers) {
			l.commonPrefix = l.prefixes.reduce!commonPrefix();
			enforce(!l.commonPrefix.empty);
		}

		foreach(ref v; category.values) {
			bool owns(T)(ref T t) {
				foreach(prefix; t.prefixes) if(v.name.startsWith(prefix)) {
					assert(v.name.startsWith(t.commonPrefix));
					auto s = v.name[t.commonPrefix.length .. $];
					t.values ~= OwnedValue(uppercasedToCamelcased(s), v.number, prefix, &v);
					return true;
				}
				return false;
			}

			bool inEnum = false;
			foreach(ref e; category.enums) if(owns!Enum(e)) {
				enforce(!inEnum);
				inEnum = true;

				foreach(ref l; category.lexers) if(l.lexerVal == v.name)
					l.dLexerVal = format("%s.%s", e.name, e.values[$-1].name);
			}

			Lexer* firstLexerOwner = null;
			foreach(ref l; category.lexers) if(owns!Lexer(l)) {
				if(firstLexerOwner) {
					auto ownedVal = &l.values[$-1];
					ownedVal.value = format("%s.%s", firstLexerOwner.name, ownedVal.name);
				} else
					firstLexerOwner = &l;
			}

			enforce(!(inEnum && firstLexerOwner));

			if(!inEnum && !firstLexerOwner)
				category.unownedValues ~= OwnedValue(uppercasedToCamelcased(v.name), v.number, null, &v);
		}
	}
}


void write() {
	enforce(categories.length == 2 && categories[1].name == "Deprecated");
	auto category = categories[0];

	File openFile(string name, bool addModule = true) {
		auto fw = File(format("../scintilla/%s.d", name), "wb");
		fw.writefln("// Generated from %s, %s category", ifaceFile, category.name);
		if(addModule)
			fw.writefln("module scintilla.%s;\n", name);
		return fw;
	}

	auto fw = openFile("enums");

	if(!category.unownedValues.empty) {
		fw.writeln("// Global manifest constants");
		fw.writeln("enum {");
		foreach(v; category.unownedValues) {
			fw.writefln("  %s = %s, // %s", v.name, v.entity.number, v.entity.name);
		}
		fw.writeln("}");
	}

	void writeEnum(T)(T t, string firstElement = null) {
		enforce(!t.values.empty, format("Empty enum '%s'", t.name));

		if(t.prefixes.length == 1) {
			assert(t.prefixes[0] == t.commonPrefix);
			fw.writefln("\n// Prefix: %s", t.commonPrefix);
		} else {
			fw.writefln("\n// Prefixes: %s", t.prefixes.joiner(" "));
			fw.writefln("// Common prefix: %s", t.commonPrefix);
		}
		fw.writefln("enum %s {", t.name);
		if(firstElement) fw.writefln("  %s", firstElement);
		foreach(v; t.values) {
			assert(v.entity.name.startsWith(v.prefix));
			fw.writefln("  %s = %s, //%s:%s", v.name, v.entity.number, v.prefix, v.entity.name[v.prefix.length .. $]);
		}
		fw.writeln("}");
	}

	enforce(category.enums[$-1].name == "Lexer");

	foreach(ref e; category.enums[0 .. $-1])
		writeEnum(e);


	fw = openFile("lexer");

	writeEnum(category.enums[$-1]);
	foreach(ref l; category.lexers) {
		writeEnum(l, l.dLexerVal ? format("lexerVal = %s, // %s", l.dLexerVal, l.lexerVal) :
			format("// lexerVal = ???, // %s", l.lexerVal));
	}


	fw = openFile("iscintilla");
	fw.writeln("import scintilla.types, scintilla.internal.functions;");

	fw.writeln("\ninterface IScintilla {");
	fw.writeln("\tsptr_t sendMessageDirect(uint msg, uptr_t wParam, sptr_t lParam);");

	foreach(f; category.functions) {
		fw.writefln("%s", f.comment.map!`"\n\t/// "~a`.joiner());

		fw.writefln("\tfinal %s%s %s(%s) { // %s", f.isProperty ? "@property " : "",
			f.dReturnType, f.dName,
			f.dParams.map!`a.dType~' '~a.name`().joiner(", "),
			f.name);

		// Lib size tests
	//	fw.writeln("\t\tassert(0);");
	//	fw.writefln("\t\treturn emptyFuncWrapper!(typeof(return), sendMessageDirect)(%s, %s);", f.number, f.dParams[].map!`a.name`.joiner(", "));
	//	fw.writefln("\t\treturn emptyFuncWrapperEvery!(typeof(return), sendMessageDirect, %s)(%s);", f.number, f.dParams[].map!`a.name`.joiner(", "));
	//	fw.writeln("\t}");
	//	continue;

		static string paramToString(Param p, bool gettingLength = false, bool userBuffer = false) {
			if(!p) return "0";
			final switch(p.type) {
				case "int"           :
				case "bool"          :
				case "position"      :
				case "keymod"        : return p.name;
				case "string"        : return "cast(size_t) __cstr_" ~ p.name;
				case "colour"        : return p.name ~ ".rgb";
				case "stringresult"  : return gettingLength ? "0" : "cast(size_t) " ~ (userBuffer ? "__ubuff.ptr" : "__optr");
				case "cells"         : return format("cast(size_t) %s.ptr", p.name);
				case "textrange"     :
				case "findtext"      :
				case "formatrange"   : return "cast(size_t) &" ~ p.name;
				case "len"           : return gettingLength ? "0" : userBuffer ? "__ubuff.length" :"__len+1";
			}
		}
		/*
		return's null:
		StyleGetFont TODO
		*/

		void writeInStringPreprocessing() {
			if(!f.inStringParams)
				return;
			fw.writeln("\t\tchar[4096] __sibuff = void; char* tmpBuff;");
			auto inStrings = f.params[].filter!`a.isInString`();
			if(f.inStringParams == 1) {
				immutable needLength = f.params[].canFind!`a.type == "len"`();
				if(needLength) fw.writeln("\t\tsize_t __len;");
				fw.writefln("\t\tconst(char)* __cstr_%s = toCString(__sibuff, %1$s, tmpBuff%2$s);",
					inStrings.front.name, needLength ? ", __len" : "");
			} else {
				assert(f.inStringParams == 2);
				auto cstrNames = inStrings.map!`"__cstr_" ~ a.name`().joiner(", ");
				fw.writefln("\t\tconst(char)* %s;", cstrNames);
				fw.writefln("\t\ttoCStrings(__sibuff, %s, %s, tmpBuff);", inStrings.map!`a.name`().joiner(", "), cstrNames);
			}
			fw.writeln("\t\tscope(exit) freeTmpBuff(tmpBuff);");
		}
		writeInStringPreprocessing();

		void writeReturn() {
			fw.write("\t\t");
			if(f.dReturnType != "void") {
				fw.write("return ");
				switch(f.returnType) {
					case "bool": fw.write("!!"); break;
					case "int":
					case "position": break;
					case "colour": fw.write("cast(Colour)"); break;
					default: enforce(0, format("Unexpected return type: '%s'", f.returnType));
				}
			}
		}

		if(f.haveOutStringParams) {
			fw.writefln("\t\timmutable __len = sendMessageDirect(%s, %s);", f.number,
				f.params[].map!(a => paramToString(a, true))().joiner(", "));
			// Some functions returns strlen() instead of required buffer length
			fw.writefln("\t\tchar[2] __sobuff;");
			fw.writefln("\t\tchar* __optr = __len <= 1 ? __sobuff.ptr : (new char[__len + 1]).ptr;");

			fw.write("\t\timmutable __res = ");
		} else
			writeReturn();

		fw.writefln("sendMessageDirect(%s, %s);", f.number, f.params[].map!paramToString().joiner(", "));

		immutable outStringName = f.haveOutStringParams ? f.params[].find!`a.isOutString`().front.name : null;

		if(f.dReturnType == "string")
			fw.writeln("\t\treturn toDString(__len, __optr);");
		else if(f.haveOutStringParams) {
			fw.writefln("\t\t%s = toDString(__len, __optr);", outStringName);
			if(f.dReturnType != "void") {
				writeReturn();
				fw.writeln("__res;");
			}
		}

		fw.writeln("\t}");

		if(f.haveOutStringParams) {
			fw.writeln("\n\t/// ditto");

			fw.writefln("\tfinal int %s(char[] %sBuff%s) {", f.dNameBuff, outStringName,
				f.dParamsBuff.map!`", "~a.dType~' '~a.name`().joiner());

			writeInStringPreprocessing();

			fw.writefln("\t\talias %sBuff __ubuff;", outStringName);
			if(f.returnType == "len") {
				fw.writefln("\t\treturn toDStringBuff(sendMessageDirect(%s, %s), __ubuff, %s);", f.number,
					f.params[].map!(a => paramToString(a, false, true)).joiner(", "), outStringName);
			} else {
				fw.writeln("\t\tif(__ubuff) {");
				fw.writefln("\t\t\timmutable __len = sendMessageDirect(%s, %s);", f.number,
					f.params[].map!(a => paramToString(a, true))().joiner(", "));
				fw.writefln("\t\t\timmutable __res = sendMessageDirect(%s, %s);", f.number,
					f.params[].map!(a => paramToString(a, false, true)).joiner(", "));
				fw.writefln("\t\t\t%s = toDStringBuff(__len, __ubuff);", outStringName);
				fw.writeln("\t\t\treturn __res;");
				fw.writeln("\t\t} else");
					fw.writefln("\t\t\treturn sendMessageDirect(%s, %s);", f.number,
						f.params[].map!(a => paramToString(a, true))().joiner(", "));
			}
			fw.writeln("\t}");
		}
	}
	fw.writeln("}");


	fw = openFile("dgui.eventargs", false);
	fw.writeln("\nimport dgui.core.events.eventargs;");
	foreach(e; category.events) {
		fw.writefln("\nclass %sEventArgs: EventArgs {", e.dClassName);
		foreach(p; e.dParams)
			fw.writefln("\tprivate %s %s_;", p.dType, p.name);
		foreach(p; e.dParams) {
			fw.writefln("\n\t@property %s %s() const { return %2$s_; }", p.dType, p.name);
			fw.writefln("\t@property %s %s(%1$s value) { return %2$s_ = value; }", p.dType, p.name);
		}
		fw.writeln("}");
	}

	fw = openFile("dgui.events.inc", false);
	foreach(e; category.events) {
		fw.writefln("\ncase %s: // %s", e.number, e.name);
		fw.writeln("\tauto e = new %sEventArgs();", e.dClassName, e.name);
		foreach(p; e.dParams)
			fw.writefln("\tprivate %s %s_;", p.dType, p.name);
		foreach(p; e.dParams) {
			fw.writefln("\n\t@property %s %s() { return %2$s_; }", p.dType, p.name);
			fw.writefln("\t@property %s %s(%1$s value) { return %2$s_ = value; }", p.dType, p.name);
		}
		fw.writeln("}");
	}
//	fw.writefln("Event!(ScintillaControl, %sEventArgs) tagPageChanged;");
}

void main() {
	read();
	process();
	write();
}
