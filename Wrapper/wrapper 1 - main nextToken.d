﻿module main;

import std.algorithm;
import std.array;
//import std.ascii;
import std.exception: enforce;
import std.file;
import std.functional: not;
import std.range: walkLength;
import std.regex;
import std.string: format;
import std.stdio;
import std.uni;

struct Param {
	string type, name, value;
}

struct Function {
	enum Type { regular, getter, setter }
	string name;
	uint number;
	Type type;
}

struct Value {
	string name;
}

struct Event {
	string name;
}

struct Category {
	string name;
	Function[] functions;
	Value[] values;
	Event[] events;
}

Category[] categories;

int main() {
	Category* category;
	string comment; // TODO
	size_t errors = 0, lineNumber = 0;
	foreach(line; File(fileName).byLine()) {
		try {
			++lineNumber;
			if(line.startsWith("#!") || // shebang
			   line.startsWith("##") || // pure comment
			   line.empty)
				continue;

			char[] nextToken() {
				line = line.find!(not!isWhite);
				static bool isIdChar(dchar c) {
					return c == '_' || isAlpha(c) || isNumber(c);
				}
				auto tmp = line;
				if(isIdChar(line.front))
					line = line.find!(not!isIdChar);
				else
					line.popFront();
				return tmp[0 .. $ - line.length];
			}

			auto r = line.findSplit(" ");
			enforce(!r[1].empty, format("No spaces in line '%s'", line));
			line = r[2];
			auto matchLine(string type, Regex!char r) {
				auto match = match(line, r);
				enforce(match.pre.empty && match.post.empty, format("Malformed %s '%s'", type, line));
				return match;
			}
			switch(r[0]) {
				case "#":
					comment = line.idup;
					break;
				case "cat":
					categories ~= Category(line.idup);
					category = &categories[$-1];
					break;
				case "fun":
				case "get":
				case "set":
					// <returnType><ws><name>[=<number](<param>,<param>)
					auto match = matchLine("function", functionRegex);
					break;
				case "val":
					// <name>=<number>
					auto match = matchLine("value", valueRegex);
					break;
				case "evt":
					// <returnType><ws><name>[=<number]([<param>[,<param>]*])
					auto match = matchLine("event", eventRegex);
					break;
				case "enu":
					// <enumeration>=<prefix>[<ws><prefix>]*
					auto match = matchLine("enum", enumRegex);
					break;
				case "lex":
					// <name>=<lexerVal><ws><prefix>[<ws><prefix>]*
					auto match = matchLine("lexer", lexerRegex);
					break;
				default:
					enforce(0, format("Unexpected first word '%s'", r[0]));
			}
		} catch(Exception e) {
			++errors;
			writefln("%s(%s): %s", fileName, lineNumber, e.msg);
		}
	}
	return !!errors;
}
