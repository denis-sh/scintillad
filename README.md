﻿ScintillaD
===================================
[Scintilla editing component](http://www.scintilla.org/) wrapper for D language.

Status
-----------------------------------
The project is **not ready for use** yet. Contact the author if you want the project to become usable soon as due to lack of time the author isn't sure he will work on it in near future.
Also this is `dev` branch which revision history is temporary and may be changed.

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
